-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Хост: id324970.mysql.ukraine.com.ua
-- Время создания: Окт 11 2018 г., 13:34
-- Версия сервера: 5.7.16-10-log
-- Версия PHP: 7.0.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `id324970_db`
--

-- --------------------------------------------------------

--
-- Структура таблицы `modx_active_users`
--

CREATE TABLE `modx_active_users` (
  `sid` varchar(32) NOT NULL DEFAULT '',
  `internalKey` int(9) NOT NULL DEFAULT '0',
  `username` varchar(50) NOT NULL DEFAULT '',
  `lasthit` int(20) NOT NULL DEFAULT '0',
  `action` varchar(10) NOT NULL DEFAULT '',
  `id` int(10) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Contains data about last user action.';

--
-- Дамп данных таблицы `modx_active_users`
--

INSERT INTO `modx_active_users` (`sid`, `internalKey`, `username`, `lasthit`, `action`, `id`) VALUES
('59gknlf2q11g5n2j4cgefm4062', 1, 'admin', 1539028259, '8', NULL),
('ntejisk0oti3enkkbq0e775o65', 2, 'stasgeller', 1539253737, '3', 24),
('sa0lvb6sjqgt06v9918bouk435', 2, 'stasgeller', 1539028729, '8', NULL),
('2gk94h1kpgvck2j62k7rdu57g2', 3, 'fregat222', 1539028749, '112', 3);

-- --------------------------------------------------------

--
-- Структура таблицы `modx_active_user_locks`
--

CREATE TABLE `modx_active_user_locks` (
  `id` int(10) NOT NULL,
  `sid` varchar(32) NOT NULL DEFAULT '',
  `internalKey` int(9) NOT NULL DEFAULT '0',
  `elementType` int(1) NOT NULL DEFAULT '0',
  `elementId` int(10) NOT NULL DEFAULT '0',
  `lasthit` int(20) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Contains data about locked elements.';

-- --------------------------------------------------------

--
-- Структура таблицы `modx_active_user_sessions`
--

CREATE TABLE `modx_active_user_sessions` (
  `sid` varchar(32) NOT NULL DEFAULT '',
  `internalKey` int(9) NOT NULL DEFAULT '0',
  `lasthit` int(20) NOT NULL DEFAULT '0',
  `ip` varchar(50) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Contains data about valid user sessions.';

--
-- Дамп данных таблицы `modx_active_user_sessions`
--

INSERT INTO `modx_active_user_sessions` (`sid`, `internalKey`, `lasthit`, `ip`) VALUES
('ntejisk0oti3enkkbq0e775o65', 2, 1539254026, '178.151.40.243');

-- --------------------------------------------------------

--
-- Структура таблицы `modx_a_news`
--

CREATE TABLE `modx_a_news` (
  `NewsId` int(11) NOT NULL,
  `Title` varchar(60) NOT NULL,
  `Description` text NOT NULL,
  `DateAdded` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `modx_a_news`
--

INSERT INTO `modx_a_news` (`NewsId`, `Title`, `Description`, `DateAdded`) VALUES
(8, 'Новость 1', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas consequat erat auctor, pharetra mi a, vulputate lectus. Quisque eget sagittis neque. Maecenas sodales magna lacus, at rhoncus erat condimentum et. Nulla nec venenatis nisl, in scelerisque diam. Sed fermentum urna ut augue vehicula, id egestas ex laoreet. Nam vulputate molestie lacinia. Etiam eu sem ut nisl scelerisque accumsan non in turpis. Cras quis sapien pulvinar, posuere turpis ut, euismod libero. Vivamus turpis risus, luctus quis interdum vel, rhoncus vel purus. Vivamus vitae turpis ultricies tellus congue laoreet. Cras quis elit eu mauris vulputate rhoncus. Mauris dolor mauris, posuere sit amet dapibus nec, porta vel augue.</p>\r\n<p>Nam tempor nec urna at porta. Quisque sed dictum orci. Ut efficitur enim non elit cursus, a accumsan ligula facilisis. Proin euismod ligula purus, vel sollicitudin arcu cursus vitae. Donec dictum, tellus in mollis vestibulum, ligula leo dapibus mauris, ac posuere turpis eros nec odio. Duis hendrerit justo risus, ut imperdiet ex pretium in. Aliquam vel neque eget arcu mollis volutpat. Nunc ut ipsum eget nibh ultricies finibus quis at tellus. Pellentesque elementum dui erat, ut ultricies ligula cursus quis. Integer nec consectetur dui. Donec at vestibulum purus. Proin pharetra turpis dui, et fermentum metus sagittis at. Mauris aliquam mi quis iaculis auctor. Donec nec mattis enim, ut vestibulum nisl.</p>\r\n<p>Praesent tincidunt tincidunt sem, nec rhoncus magna sodales eget. Sed nec elit ac sapien vestibulum rutrum a sit amet leo. Integer volutpat purus a augue porttitor, eu egestas sapien venenatis. Proin tempus in ligula id molestie. Integer pretium enim rutrum, porttitor felis vitae, pellentesque justo. Duis id quam orci. Nam erat quam, commodo et tristique sit amet, mollis sodales elit. Quisque maximus felis vitae ante eleifend ultricies. Vivamus ac iaculis justo. Vivamus ut consectetur nibh, eu finibus lacus. Donec sollicitudin felis laoreet, lacinia quam a, tincidunt nisl. Vestibulum purus augue, luctus in tristique et, dignissim id odio. Maecenas non posuere tellus, dictum finibus sem. Nullam eleifend justo eu dui tincidunt, ac euismod dolor aliquam. Proin rhoncus ligula eu pretium lacinia. In aliquet nisl diam, at condimentum libero hendrerit id.</p>\r\n<p>Sed euismod libero sed ligula sodales, laoreet mollis nunc blandit. Sed interdum lacus sed mi venenatis sodales. Phasellus malesuada in erat et molestie. Suspendisse finibus iaculis justo, vitae luctus mauris consectetur nec. Donec lobortis libero eu est vulputate sollicitudin. Nulla facilisi. Sed bibendum dapibus turpis sed bibendum.</p>\r\n<p>Proin vestibulum metus et tortor consectetur blandit. Praesent tristique tellus sed blandit porta. Pellentesque ullamcorper est tellus. Sed hendrerit urna et mauris imperdiet, sit amet facilisis sapien volutpat. Aenean ut tristique magna. Aliquam accumsan massa id nunc viverra aliquam. Etiam aliquam placerat euismod. Nullam in dui justo. Fusce sodales, augue vitae tincidunt aliquet, ipsum erat accumsan eros, lobortis hendrerit diam elit sed ligula. Aenean in malesuada velit. Aliquam id feugiat erat. Sed vel interdum massa, non laoreet arcu. Vivamus vitae varius arcu, eget convallis orci. Vestibulum nec mauris neque.</p>', '2018-09-28 11:37:51');

-- --------------------------------------------------------

--
-- Структура таблицы `modx_a_redirect`
--

CREATE TABLE `modx_a_redirect` (
  `redirect_id` int(11) NOT NULL,
  `redirect_code` int(11) NOT NULL DEFAULT '301',
  `redirect_source` varchar(1024) NOT NULL,
  `redirect_target` varchar(1024) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `modx_a_redirect`
--

INSERT INTO `modx_a_redirect` (`redirect_id`, `redirect_code`, `redirect_source`, `redirect_target`) VALUES
(3, 301, '/uslugi/kouching/', 'consulting/prodazhi1/kouching-prodazhi-i-marketing/'),
(4, 301, '/o-kompanii/nashi-knigi/', 'shop/knigi/'),
(6, 301, '/poleznoe/video/', 'shop/video/'),
(7, 301, '/treningi/trening-prodazhi-po-telefonu/', 'blog/prodazhi/trening-prodazhi-po-telefonu/'),
(8, 301, '/treningi/', 'trainings/'),
(9, 301, '/raspisanie/', 'trainings/'),
(10, 301, '/treningi/oratorskoe-iskusstvo-2.0/', 'blog/orator/oratorskoe-iskusstvo-20/'),
(11, 301, '/treningi/oratorskoe-iskusstvo-2.0-2-modul/', 'trainings/korporativnye/oratorskoe-masterstvo2/'),
(12, 301, '/treningi/trening-po-prodazham/', 'trainings/prodazhi4/trening-po-prodazham/'),
(13, 301, '/treningi/effektivnyij-rukovoditel/', 'blog/rukovoditel/trening-dlya-rukovoditelej1/'),
(14, 301, '/treningi/prodazhi-b2b/', 'blog/prodazhi/prodazhi-b2b/'),
(15, 301, '/treningi/aktivnyie-prodazhi/', 'blog/prodazhi/aktivnye-prodazhi2/'),
(16, 301, '/treningi/konfliktologiya/', 'blog/peregovory/konfliktologiya/'),
(17, 301, '/treningi/chernaya-ritorika/', 'blog/orator/chernaya-ritorika1/'),
(18, 301, '/treningi/zhestkie-peregovoryi/', 'blog/peregovory/zhestkie-peregovory5/'),
(19, 301, '/treningi/mini-mba1/', 'trainings/upravlenie1/mini-mba1/'),
(20, 301, '/treningi/mini-mba/', 'trainings/upravlenie1/mini-mba1/'),
(21, 301, '/treningi/mini-mba2/', 'trainings/upravlenie1/mini-mba1/'),
(22, 301, '/treningi/trening-dlya-trenerov/', 'blog/orator/trening-dlya-trenerov1/'),
(23, 301, '/treningi/tajm-menedzhment/', 'trainings/lichnostnyj-rost1/tajm-menedzhment/'),
(24, 301, '/treningi/tajm-menedzhment-dlya-rukovoditelej/', 'trainings/korporativnye/tajm-menedzhment-dlya-rukovoditelej/'),
(25, 301, '/treningi/sekretyi-peregovorov/', 'blog/peregovory/sekrety-peregovorov1/'),
(26, 301, '/treningi/dostizhenie-czelej/', 'blog/poleznoe/trening-postanovka-i-dostizhenie-celej/'),
(27, 301, '/treningi/magiya-lidera/', 'trainings/upravlenie1/'),
(28, 301, '/treningi/motivacziya-personala/', 'trainings/upravlenie1/motivaciya-personala/'),
(29, 301, '/treningi/iron-man-trening-dlya-muzhchin/', 'blog/poleznoe/iron-man-trening-dlya-muzhchin/'),
(30, 301, '/treningi/delegirovanie-polnomochij-podchinennyim/', 'blog/rukovoditel/delegirovanie-polnomochij-podchinennym1/'),
(31, 301, '/treningi/delovoj-etiket/', 'trainings/korporativnye/delovoj-etiket/'),
(32, 301, '/treningi/upravlenie-konfliktami/', 'trainings/lichnostnyj-rost1/upravlenie-konfliktami/'),
(33, 301, '/treningi/delovyie-peregovoryi/', 'blog/prodazhi/prodazhi-i-delovye-peregovory14/'),
(34, 301, '/treningi/lichnaya-effektivnost/', 'trainings/lichnostnyj-rost1/lichnaya-effektivnost/'),
(35, 301, '/blog/biznes-treningi-v-dnepropetrovske/', 'blog/poleznoe/biznes-treningi-v-dnepropetrovske/'),
(36, 301, '/blog/biznes-treningi-v-dnepropetrovske/', 'blog/poleznoe/biznes-treningi-v-dnepropetrovske/'),
(37, 301, '/blog/2015/7-uslovij-pravilnoj-motivaczii-personala/', 'blog/rukovoditel/7-uslovij-pravilnoj-motivacii-personala/'),
(38, 301, '/blog/biznes-treningi-v-doneczke/', 'blog/poleznoe/biznes-treningi-v-donecke/'),
(39, 301, '/blog/5-luchshix-filmov-po-oratorskomu-iskusstvu-i-publichnyim-vyistupleniyam/', 'blog/top/5-luchshih-filmov-po-oratorskomu-iskusstvu-i-publichnym-vystupleniyam/'),
(40, 301, '/blog/oratorskoe-iskusstvo-kakaya-osnovnaya-czel-publichnogo-vyistupleniya/', 'blog/orator/oratorskoe-iskusstvo-kakaya-osnovnaya-cel-publichnogo-vystupleniya/'),
(41, 301, '/blog/sekretyi-manipulyaczij-kak-izbezhat-manipulirovaniya-soboj-drugimi-lyudmi/', 'blog/poleznoe/sekrety-manipulyacij-kak-izbezhat-manipulirovaniya-soboj-drugimi-lyudmi/'),
(42, 301, '/blog/2015/top-10-luchshix-filmov-o-liderstve/', 'blog/top/top-10-luchshih-filmov-o-liderstve/'),
(43, 301, '/blog/2015/luchshij-biznes-trener-xarkova-2015/', 'blog/poleznoe/luchshij-biznes-trener-harkova-2015/'),
(44, 301, '/blog/2015/top-5-filmov-po-lichnoj-effektivnosti/', 'blog/top/top-5-filmov-po-lichnoj-effektivnosti/'),
(45, 301, '/blog/chto-takoe-chernaya-ritorika/', 'blog/orator/chto-takoe-chernaya-ritorika/'),
(46, 301, '/blog/sekretyi-upravleniya-personalom/', 'blog/rukovoditel/sekrety-upravleniya-personalom/'),
(47, 301, '/o-kompanii/o-nas/', 'about/'),
(48, 301, '/o-kompanii/dlya-smi/', 'about/'),
(49, 301, '/blog/2016/tur-dnepr-zaporozhe-letnyaya-versiya/', 'blog/peregovory/tur-dnepr-zaporozhe-letnyaya-versiya/'),
(50, 301, '/raspisanie/biznes-konferencziya-menedzhment-marketing-prodazhi/', 'trainings/'),
(51, 301, '/raspisanie/effektivnyij-prodazhnik/', 'trainings/'),
(52, 301, '/1/', ''),
(53, 301, '/raspisanie/videokurs-prodazhi-i-delovyie-peregovoryi/', 'trainings/'),
(54, 301, '/o-kompanii/nashi-partneryi/', 'about/'),
(55, 301, '/treningi/rabota-s-vozrazheniyami1/', 'trainings/'),
(56, 301, '/raspisanie/videokurs-99-sekretov-oi/', 'trainings/'),
(57, 301, '/uslugi/', ''),
(58, 301, '/raspisanie/lichnostnyij-rost/', 'trainings/'),
(59, 301, '/uslugi/marketing-plan/', ''),
(60, 301, '/uslugi/platinovaya-gruppa/', ''),
(61, 301, '/uslugi/konferencziya-pod-klyuch/', ''),
(63, 301, '/raspisanie/effektivnyij-rukovoditel1/', 'shop/video/'),
(64, 301, '/blog/2016/mmp-2016-kak-eto-byilo/', 'blog/mmp/mmp-2016-kak-eto-bylo/'),
(65, 301, '/uslugi/programma-po-razvitiyu-kompetenczij-rukovoditelya-akademiya-rukovoditelya/', 'trainings/'),
(66, 301, '/uslugi/buxgalterskie-uslugi/', 'consulting/drugie-uslugi/buhgalterskie-uslugi/'),
(67, 301, '/raspisanie/testovyij-trening/', 'shop/'),
(68, 301, '/uslugi/kopirajting1/', ''),
(69, 301, '/uslugi/konsalting/', ''),
(70, 301, '/raspisanie/biznes-knigi/', 'shop/'),
(71, 301, '/raspisanie/kompleksnoe-razvitie-biznesa/', 'shop/'),
(72, 301, '/uslugi/podarochnyij-sertifikat/', ''),
(73, 301, '/o-kompanii/nasha-komanda/', 'about/'),
(74, 301, '/uslugi/korporativnaya-kniga-prodazh/', ''),
(75, 301, '/kontaktyi/', 'contacts/'),
(76, 301, '/treningi/prodayushhaya-prezentacziya/', 'trainings/'),
(77, 301, '/uslugi/kompleksnaya-godovaya-programma-po-razvitiyu-i-sistematizaczii-biznesa/', 'trainings/'),
(78, 301, '/poleznoe/pritchi/', ''),
(79, 301, '/raspisanie/antikrizis/', 'trainings/'),
(80, 301, '/poleznoe/pritchi/pravila-biznesa-dzheka-dorsi/', ''),
(81, 301, '/poleznoe/pritchi/partnyor-ili-drug/', ''),
(83, 301, '/poleznoe/pritchi/samyij-luchshij-biznes/', ''),
(84, 301, '/poleznoe/pritchi/skolko-vam-let/', ''),
(85, 301, '/blog/treningi-po-prodazham-kiev-xarkov-ukraina/', 'blog/prodazhi/treningi-po-prodazham/'),
(86, 301, '/blog/2015/obuchenie-deputatov-oratorskomu-iskusstvu/', 'blog/poleznoe/obuchenie-deputatov-oratorskomu-iskusstvu/'),
(87, 301, '/blog/2015/kak-trening-czentr-lider-obuchal-budushhix-deputatov/', 'blog/poleznoe/kak-lider-obuchal-budushhih-deputatov/'),
(88, 301, '/poleznoe/pritchi/pritcha-pro-karandash/', ''),
(89, 301, '/blog/effektivnyie-resheniya-chto-eto-takoe-i-kak-ix-prinimat/', 'blog/poleznoe/effektivnye-resheniya-chto-eto-takoe-i-kak-ih-prinimat/'),
(90, 301, '/blog/biznes-konferencziya-menedzhment-marketing-prodazhi-2014-dva-uspeshnyix-dnya/', 'blog/mmp/vip-den-biznes-konferencii-menedzhment-marketing-prodazhi/'),
(91, 301, '/blog/top-5-zapreshhennyie-filmyi-k-pokazu/', 'blog/top/top-5-zapreshhennye-filmy-k-pokazu/'),
(92, 301, '/poleznoe/pritchi/zhadnost/', ''),
(93, 301, '/blog/chto-daet-kurs-po-oratorskomu-iskusstvu/', 'blog/orator/chto-daet-kurs-po-oratorskomu-iskusstvu/'),
(94, 301, '/blog/vip-den-biznes-konferenczii-mmp2014/', 'blog/mmp/vip-den-biznes-konferencii-menedzhment-marketing-prodazhi/'),
(95, 301, '/blog/trening-zhestkie-peregovoryi-kak-eto-byilo/', 'blog/peregovory/trening-zhestkie-peregovory-kak-eto-bylo/'),
(96, 301, '/poleznoe/pritchi/znat-gde-udarit/', ''),
(97, 301, '/poleznoe/pritchi/padaesh-vstavaj/', ''),
(98, 301, '/poleznoe/pritchi/beri-ot-zhizni-bolshe/', ''),
(99, 301, '/blog/2015/zhenshhina-ili-rukovoditel/', 'blog/rukovoditel/zhenshhina-ili-rukovoditel/'),
(100, 301, '/blog/2015/top-5-filmov-prodazhi-po-telefonu/', 'blog/top/top-5-filmov-prodazhi-po-telefonu/'),
(101, 301, '/poleznoe/pritchi/izvineniya/', ''),
(102, 301, '/poleznoe/pritchi/pritcha-o-vozmozhnosti/', ''),
(103, 301, '/blog/2015/biznes-na-maksimum-3.0-kak-eto-byilo/', 'blog/poleznoe/biznes-na-maksimum-30-kak-eto-bylo/'),
(104, 301, '/poleznoe/pritchi/ne-bojsya-ne-zhdi-a-dejstvuj/', ''),
(105, 301, '/blog/postroenie-lichnogo-brenda/', 'blog/marketing/postroenie-lichnogo-brenda/'),
(106, 301, '/blog/treningi-po-peregovoram/', 'blog/peregovory/treningi-po-peregovoram/'),
(107, 301, '/poleznoe/pritchi/stakan-vodyi/', ''),
(108, 301, '/poleznoe/pritchi/zakon-trex-utrom/', ''),
(109, 301, '/poleznoe/pritchi/rastopi-led/', ''),
(110, 301, '/blog/psixologicheskie-treningi-v-xarkove/', 'blog/poleznoe/psihologicheskie-treningi-v-harkove/'),
(111, 301, '/blog/2015/pervyij-model-kursa-mini-mba-kak-eto-byilo/', 'blog/poleznoe/pervyj-modul-kursa-mini-mba-kak-eto-bylo/'),
(112, 301, '/poleznoe/pritchi/bolshe-sladostej/', ''),
(113, 301, '/blog/2015/5-sekretov-uspeshnyix-prodazh-po-telefonu/', 'blog/prodazhi/5-sekretov-uspeshnyh-prodazh-po-telefonu/'),
(114, 301, '/blog/2015/top-5-filmov-po-marketingu-i-reklame/', 'blog/top/top-5-filmov-po-marketingu-i-reklame/'),
(115, 301, '/blog/2015/top-5-filmov-zhestkie-peregovoryi/', 'blog/top/top-5-filmov-zhestkie-peregovory/'),
(116, 301, '/blog/2015/top-5-istochnikov-dlya-napisaniya-interesnyix-tekstov/', 'blog/kopirajting/top-5-istochnikov-dlya-napisaniya-interesnyh-tekstov/'),
(117, 301, '/blog/2015/kak-raspoznat-manipulyatora-i-protivodejstvovat-emu/', 'blog/poleznoe/kak-raspoznat-manipulyatora-i-protivodejstvovat-emu/'),
(118, 301, '/blog/2015/5-naibolee-dejstvennyix-sposobov-soxranit-obemyi-prodazh-v-krizis/', 'blog/prodazhi/5-naibolee-dejstvennyh-sposobov-sohranit-obemy-prodazh-v-krizis/'),
(119, 301, '/blog/2015/5-klyuchevyix-sposobov-podbora-personala/', 'blog/prodazhi/5-naibolee-dejstvennyh-sposobov-sohranit-obemy-prodazh-v-krizis/'),
(120, 301, '/blog/2015/povyishaem-stressoustojchivost-7-dejstvennyix-sovetov/', 'blog/rukovoditel/5-klyuchevyh-sposobov-podbora-personala/'),
(121, 301, '/blog/top-7-luchshix-filmov-2014-goda/', 'blog/top/top-7-luchshih-filmov-2014-goda/'),
(122, 301, '/blog/2016/sekretyi-prodazh-g.xarkov-kak-eto-byilo/', 'blog/prodazhi/sekrety-prodazh-gharkov-kak-eto-bylo/'),
(123, 301, '/treningi/texnologii-podbora-i-najma-personala/', 'trainings/'),
(124, 301, '/blog/2015/vip-den-mmp-2015/', 'blog/mmp/vip-den-mmp-2015/'),
(125, 301, '/blog/article2/', 'blog/marketing/proverennye-sposoby-privlecheniya-klientov/'),
(126, 301, '/blog/vip-den-biznes-konferenczii-mmp-2014-kiev/', 'blog/mmp/vip-den-biznes-konferencii-mmp-2014-kiev/'),
(127, 301, '/blog/article1/', 'blog/marketing/kak-proverit-rabotaet-li-vasha-reklama/'),
(128, 301, '/blog/2015/v-chem-sekret-effektivnyix-xolodnyix-zvonkov/', 'blog/prodazhi/v-chem-sekret-effektivnyh-holodnyh-zvonkov/'),
(129, 301, '/blog/2015/5-sekretov-uspeshnyix-prodazh/', 'blog/prodazhi/5-sekretov-uspeshnyh-prodazh/'),
(130, 301, '/blog/2015/30-zolotyix-pravil-rukovoditelya/', 'blog/rukovoditel/30-zolotyh-pravil-rukovoditelya/'),
(131, 301, '/blog/2015/top-5-filmov-pretendentov-na-premiyu-oskar-2015/', 'blog/top/top-5-filmov-pretendentov-na-premiyu-oskar-2015/'),
(132, 301, '/blog/5-luchshix-psixologicheskix-trillerov/', 'blog/top/5-luchshih-psihologicheskih-trillerov/'),
(133, 301, '/poleznoe/pritchi/bednyie-bogatyie/', ''),
(134, 301, '/treningi/peregovoryi-v-zakupkax/', 'trainings/'),
(135, 301, '/blog/5-klyuchevyix-sovetov-kak-soxranit-i-priumnozhit-dengi/', 'blog/poleznoe/5-klyuchevyh-sovetov-kak-sohranit-i-priumnozhit-dengi/'),
(136, 301, '/blog/2015/top-5-knig-po-delovyim-peregovoram/', 'blog/top/top-5-knig-po-delovym-peregovoram/'),
(137, 301, '/blog/2015/5-klyuchevyix-sekretov-uspeshnyix-delovyix-peregovorov/', 'blog/peregovory/5-klyuchevyh-sekretov-uspeshnyh-delovyh-peregovorov/'),
(138, 301, '/blog/2015/sovetyi-nachinayushhemu-biznesmenu/', 'blog/poleznoe/sovety-nachinayushhemu-biznesmenu/'),
(139, 301, '/blog/2015/7-sovetov-po-effektivnoj-motivaczii-personala/', 'blog/rukovoditel/7-sovetov-po-effektivnoj-motivacii-personala/'),
(140, 301, '/blog/2015/8-klyuchevyix-sekretov-oratorskogo-masterstva/', 'blog/orator/8-klyuchevyh-sekretov-oratorskogo-masterstva/'),
(141, 301, '/blog/2015/5-metodik-rabotyi-s-vozrazheniyami-v-telefonnyix-prodazhax/', 'blog/prodazhi/5-metodik-raboty-s-vozrazheniyami-v-telefonnyh-prodazhah/'),
(142, 301, '/blog/2015/10-sovetov-kotoryie-v-razyi-povyisyat-vashu-lichnuyu-effektivnost/', 'blog/rukovoditel/10-sovetov-kotorye-v-razy-povysyat-vashu-lichnuyu-effektivnost/'),
(143, 301, '/poleznoe/literatura/', ''),
(144, 301, '/blog/2015/3-glavnyix-sekreta-psixologii-deneg/', 'blog/poleznoe/3-glavnyh-sekreta-psihologii-deneg/'),
(145, 301, '/poleznoe/pritchi/sledyi-na-peske/', ''),
(146, 301, '/treningi/99-sposobov-privlecheniya-novyix-klientov/', 'trainings/'),
(147, 301, '/blog/2015/chto-obyazatelno-dolzhen-znat-o-motivaczii-kazhdyij-rukovoditel/', 'blog/rukovoditel/chto-obyazatelno-dolzhen-znat-o-motivacii-kazhdyj-rukovoditel/'),
(148, 301, '/treningi/bolshe-deneg-ot-vashego-biznesa/', 'trainings/'),
(149, 301, '/blog/2015/top-5-luchshix-filmov-o-biznes-ledi/', 'blog/top/top-5-luchshih-filmov-o-biznes-ledi/'),
(150, 301, '/treningi/udvoenie-prodazh/', 'trainings/'),
(151, 301, '/blog/3-klyuchevyix-navyika-po-upravleniyu-dengami/', 'blog/poleznoe/3-klyuchevyh-navyka-po-upravleniyu-dengami/'),
(152, 301, '/blog/5-luchshix-filmov-po-prodazham-i-peregovoram/', 'blog/top/5-luchshih-filmov-po-prodazham-i-peregovoram/'),
(153, 301, '/blog/9-glavnyix-princzipov-oratorskogo-iskusstva/', 'blog/orator/kakie-byvayut-idei-dlya-biznesa14/'),
(154, 301, '/blog/17-samyix-interesnyix-pravil-etiketa/', 'blog/top/17-samyh-interesnyh-pravil-etiketa/'),
(155, 301, '/o-kompanii/nashi-klientyi/', 'about/'),
(156, 301, '/blog/2015/mmp2015-kak-eto-byilo-fotootchet/', 'blog/mmp/mmp2015-kak-eto-bylo-fotootchet/'),
(157, 301, '/blog/2015/oratorskoe-iskusstvo-kak-eto-byilo/', 'blog/orator/oratorskoe-iskusstvo-kak-eto-bylo/'),
(158, 301, '/blog/2015/5-naibolee-rasprostranyonnyix-sposobov-manipulirovaniya-lyudmi/', 'blog/poleznoe/5-rasprostranyonnyh-sposobov-manipulirovaniya-lyudmi/'),
(159, 301, '/blog/2015/kak-myi-poluchili-lichnuyu-blagodarnost-ot-gubernatora-igorya-rajnina/', 'blog/poleznoe/kak-my-poluchili-lichnuyu-blagodarnost-ot-gubernatora-igorya-rajnina/'),
(160, 301, '/poleznoe/pritchi/gospodin-kojner-i-potop/', ''),
(161, 301, '/o-kompanii/smi-o-nas/', ''),
(162, 301, '/raspisanie/novogodnyaya-akcziya/', 'trainings/'),
(163, 301, '/poleznoe/pritchi/pritcha-pro-oslika-ili-pyat-prostyix-pravil-chtobyi-byit-schastlivyim/', ''),
(164, 301, '/blog/9-glavnyix-prichin-iz-za-kotoryix-voznikayut-konfliktyi-v-organizaczii/', 'blog/rukovoditel/9-glavnyh-prichin-iz-za-kotoryh-voznikayut-konflikty-v-organizacii/'),
(165, 301, '/blog/2015/top-5-filmov-o-manipulyacziyax/', 'blog/top/top-5-filmov-o-manipulyaciyah/'),
(166, 301, '/blog/2015/kak-podgotovitsya-k-publichnomu-vyistupleniyu/', 'blog/orator/kak-podgotovitsya-k-publichnomu-vystupleniyu/'),
(167, 301, '/blog/13-sekretov-effektivnogo-rukovoditelya/', 'blog/rukovoditel/13-sekretov-effektivnogo-rukovoditelya/'),
(168, 301, '/uslugi/nalogovyij-konsalting/', 'consulting/drugie-uslugi/nalogovyj-konsalting/'),
(169, 301, '/blog/2015/chtu-nuzhno-znat-o-manipulyacziyax-v-delovyix-peregovorax/', 'blog/peregovory/chto-nuzhno-znat-o-manipulyaciyah-v-delovyh-peregovorah/'),
(170, 301, '/blog/2015/biznes-na-maksimum-3.0-v-poltave-kak-eto-byilo/', 'blog/poleznoe/biznes-na-maksimum-30-v-poltave-kak-eto-bylo/'),
(171, 301, '/poleznoe/pritchi/loshad-sdoxla-slez/', ''),
(172, 301, '/blog/2015/biznes-na-maksimum-3.0-g.-dnepropetrovsk-kak-eto-byilo/', 'blog/poleznoe/biznes-na-maksimum-30-g-dnepropetrovsk-kak-eto-bylo/'),
(173, 301, '/blog/2015/oratorskoe-iskusstvo-2.0-kak-eto-byilo/', 'blog/orator/oratorskoe-iskusstvo-20-kak-eto-bylo/'),
(174, 301, '/blog/2015/kak-provesti-effektivnoe-soveshhanie/', 'blog/rukovoditel/kak-provesti-effektivnoe-soveshhanie/'),
(175, 301, '/blog/oratorskoe-iskusstvo-kak-podgotovitsya-k-publichnomu-vyistupleniyu/', 'blog/orator/oratorskoe-iskusstvo-kak-podgotovitsya-k-publichnomu-vystupleniyu/'),
(176, 301, '/blog/pravila-kotoryie-perevernut-vashu-zhizn/', 'blog/poleznoe/pravila-kotorye-perevernut-vashu-zhizn/'),
(177, 301, '/blog/vip-den-biznes-konferenczii-menedzhment-marketing-prodazhi/5-luchshix-filmov-dlya-top-menedzherov-i-rukovoditelej/', 'blog/top/5-luchshih-filmov-dlya-top-menedzherov-i-rukovoditelej/'),
(178, 301, '/blog/zolotyie-pravila-kopirajtinga/', 'blog/kopirajting/zolotye-pravila-kopirajtinga/'),
(179, 301, '/blog/vidyi-konfliktov/', 'blog/poleznoe/vidy-konfliktov/'),
(180, 301, '/poleznoe/pritchi/pri-neobxodimosti-povtorit/', ''),
(181, 301, '/blog/2015/zolotoj-spisok-treningov-dlya-predprinimatelej/', 'blog/poleznoe/zolotoj-spisok-treningov-dlya-predprinimatelej/'),
(182, 301, '/treningi/effektivnaya-reklama.-unikalnyie-sekretyi/', 'trainings/'),
(183, 301, '/blog/2015/kak-eto-byilo-mmp-2015-fotootchet/', 'blog/mmp/kak-eto-bylo-mmp-2015-fotootchet/'),
(184, 301, '/blog/5-luchshix-filmov-o-biznese/', 'blog/top/5-luchshih-filmov-o-biznese/'),
(185, 301, '/poleznoe/pritchi/v-chem-smyisl-zhizni/', ''),
(186, 301, '/blog/trening-postroenie-personalnogo-brenda-kak-eto-byilo/', 'blog/marketing/trening-postroenie-personalnogo-brenda-kak-eto-bylo/'),
(187, 301, '/fotogalereya/prodazhi-i-delovyie-peregovoryi/', ''),
(188, 301, '/treningi/stressoustojchivost/', 'trainings/'),
(189, 301, '/blog/2015/kak-udvoit-prodazhi/', 'blog/prodazhi/kak-udvoit-prodazhi/'),
(190, 301, '/treningi/oratorskoe-masterstvo/', 'trainings/'),
(191, 301, '/poleznoe/pritchi/izvestnaya-istoriya/', ''),
(192, 301, '/blog/sistemnoe-myishlenie-chto-eto-takoe-komu-i-zachem-ono-nuzhno/', 'blog/poleznoe/sistemnoe-myshlenie-chto-eto-takoe-komu-i-zachem-ono-nuzhno1/'),
(193, 301, '/blog/2015/czel-kak-osnova-uspexa/', 'blog/poleznoe/cel-kak-osnova-uspeha/'),
(194, 301, '/blog/2015/10-naibolee-rasprostranennyix-oshibok-rukovoditelya/', 'blog/rukovoditel/10-naibolee-rasprostranennyh-oshibok-rukovoditelya/'),
(195, 301, '/blog/2015/kak-eto-byilo-biznes-na-maksimum-2.0/', 'blog/poleznoe/kak-eto-bylo-biznes-na-maksimum-20/'),
(196, 301, '/treningi/biznes-intensiv-prodazhi-na-maksimum/', 'trainings/'),
(197, 301, '/blog/5-sekretov-vedeniya-zhestkix-peregovorov/', 'blog/peregovory/5-sekretov-vedeniya-zhestkih-peregovorov/'),
(198, 301, '/treningi/lichnyie-finansyi/', 'trainings/'),
(199, 301, '/blog/2015/top-5-filmov-dlya-generalnyix-direktorov/', 'blog/top/top-5-filmov-dlya-generalnyh-direktorov/'),
(200, 301, '/blog/2015/5-klyuchevyix-rekomendaczij-postanovki-czeli-v-novom-godu/', ''),
(201, 301, '/blog/5-sposobov-privlecheniya-klientov-s-pomoshhyu-interneta/', 'blog/rukovoditel/5-sposobov-privlecheniya-klientov-s-pomoshhyu-interneta/'),
(202, 301, '/treningi/kak-uvelichit-pribyil-kompanii.-malobyudzhetnyie-texniki/', 'trainings/'),
(203, 301, '/blog/2015/top-5-filmov-po-chernoj-ritorike/', 'blog/top/top-5-filmov-po-chernoj-ritorike/'),
(204, 301, '/blog/sekretyi-zhestkix-peregovorov/', 'blog/peregovory/sekrety-zhestkih-peregovorov/'),
(205, 301, '/treningi/finansyi-dlya-rukovoditelej/', 'trainings/'),
(206, 301, '/blog/v-biznes-konferencziya-mmp-2014-v-kieve/', 'blog/mmp/v-biznes-konferenciya-mmp-2014-v-kieve/'),
(207, 301, '/treningi/biznes-na-maksimum-2.0-poltava/', 'trainings/'),
(208, 301, '/treningi/coczialnyie-seti-dlya-razvitiya-biznesa/', 'trainings/'),
(209, 301, '/blog/sekretyi-uspexa/', 'blog/poleznoe/sekrety-uspeha/'),
(210, 301, '/fotogalereya/brandwall-mmp-2013/', ''),
(211, 301, '/treningi/besplatno-biznes-na-maksimum-3.0/', 'trainings/'),
(212, 301, '/treningi/besplatno-biznes-na-maksimum-3.0/', 'trainings/'),
(213, 301, '/fotogalereya/tendernyie-peregovoryi/', ''),
(214, 301, '/treningi/facebook-ubojnyie-sekretyi/', ''),
(215, 301, '/fotogalereya/oczenshhiki/', ''),
(216, 301, '/fotogalereya/trening-meditacziya/', ''),
(217, 301, '/fotogalereya/superfinansist/', ''),
(218, 301, '/fotogalereya/trening-delovyie-peregovoryi1/', ''),
(219, 301, '/fotogalereya/psixologiya-lzhi1/', ''),
(220, 301, '/fotogalereya/trening-psixologiya-lzhi/', ''),
(221, 301, '/fotogalereya/delovaya-kommunikacziya/', ''),
(222, 301, '/fotogalereya/lichnaya-effektivnost1/', ''),
(223, 301, '/fotogalereya/udvoenie-prodazh1/', ''),
(224, 301, '/fotogalereya/delovaya-kommunikacziya1/', ''),
(225, 301, '/fotogalereya/biznes-konferencziya-mmp2014-xarkov/', ''),
(226, 301, '/blog/9-glavnyix-pravil-provedeniya-soveshhanij/', 'blog/rukovoditel/9-glavnyh-pravil-provedeniya-soveshhanij/'),
(227, 301, '/treningi/besplatno-biznes-na-maksimum-3.0-poltava/', 'trainings/'),
(228, 301, '/treningi/lichnaya-effektivnost-kiev/', 'trainings/'),
(229, 301, '/treningi/vlast-rukovoditelya/', 'trainings/'),
(230, 301, '/blog/top-5-luchshie-novogodnie-i-rozhdestvenskie-filmyi/', 'blog/top/top-5-luchshie-novogodnie-i-rozhdestvenskie-filmy/'),
(231, 301, '/blog/2015/chto-znachit-byit-effektivnyim-rukovoditelem-7-klyuchevyix-kachestv/', 'blog/rukovoditel/chto-znachit-byt-effektivnym-rukovoditelem-7-klyuchevyh-kachestv/'),
(232, 301, '/blog/biznes-treningi-novyij-uchebnyij-sezon-xarkov-kiev-dnepropetrovsk/', 'blog/poleznoe/biznes-treningi-novyj-uchebnyj-sezon/'),
(233, 301, '/blog/2015/7-bazovyix-sovetov-po-kopirajtingu/', 'blog/kopirajting/7-bazovyh-sovetov-po-kopirajtingu/'),
(234, 301, '/treningi/peregovoryi-po-telefonu/', 'trainings/'),
(235, 301, '/blog/2015/7-sekretov-uspeshnyix-delovyix-peregovorov/', 'blog/peregovory/7-sekretov-uspeshnyh-delovyh-peregovorov/'),
(236, 301, '/poleznoe/pritchi/istoriya-o-drovoseke/', ''),
(237, 301, '/blog/2015/top-5-sekretov-ubeditelnogo-vyistupleniya/', 'blog/orator/top-5-sekretov-ubeditelnogo-vystupleniya/'),
(238, 301, '/blog/20-shagov-na-puti-postroeniya-uspeshnoj-kareryi1/', 'blog/poleznoe/20-shagov-na-puti-postroeniya-uspeshnoj-karery/'),
(239, 301, '/poleznoe/pritchi/zhena-muzh-i-olen/', ''),
(240, 301, '/blog/2015/4-pravila-oratorskogo-iskusstva/', 'blog/orator/4-pravila-oratorskogo-iskusstva/'),
(241, 301, '/poleznoe/pritchi/strannaya-blagodarnost/', ''),
(242, 301, '/treningi/biznes-na-maksimum-4.0-xarkov/', 'trainings/'),
(243, 301, '/treningi/kak-vospitat-uspeshnogo-rebenka/', 'trainings/'),
(244, 301, '/treningi/trening-po-prodazham-i-delovyim-peregovoram/', 'trainings/'),
(245, 301, '/blog/biznes-konferencziya-marketing-i-prodazhi-kak-eto-byilo/', 'blog/'),
(246, 301, '/treningi/biznes-konferencziya-marketing-i-prodazhi/', 'trainings/'),
(247, 301, '/blog/pravila-vedeniya-besedyi/', 'blog/peregovory/pravila-vedeniya-besedy/'),
(248, 301, '/blog/sekretyi-oratorskogo-iskusstva/', 'blog/orator/sekrety-oratorskogo-iskusstva/'),
(249, 301, '/treningi/upravlenie-dengami/', 'trainings/'),
(250, 301, '/blog/treningi-lichnostnogo-rosta/', 'blog/poleznoe/treningi-lichnostnogo-rosta/'),
(251, 301, '/blog/22-pravila-telefonnogo-etiketa/', 'blog/prodazhi/22-pravila-telefonnogo-etiketa/'),
(252, 301, '/blog/trening-dlya-rukovoditelej-kak-eto-byilo/', 'blog/rukovoditel/trening-dlya-rukovoditelej-kak-eto-bylo/'),
(253, 301, '/blog/2015/kak-myi-obuchali-novuyu-patrulnuyu-policziyu-ukrainyi-v-xarkove/', 'blog/poleznoe/kak-my-obuchali-novuyu-patrulnuyu-policiyu-ukrainy/'),
(254, 301, 'blog/2015/konflikt-v-kollektive-dejstviya-rukovoditelya', 'blog/rukovoditel/konflikt-v-kollektive-dejstviya-rukovoditelya/'),
(255, 301, '/blog/9-sekretov-effektivnogo-rukovoditelya/', 'blog/rukovoditel/9-sekretov-effektivnogo-rukovoditelya/'),
(256, 301, '/treningi/effektivnaya-kommunikacziya/', 'trainings/'),
(257, 301, '/blog/biznes-konferencziya-mmp2014-kak-eto-byilo/', 'blog/mmp/biznes-konferenciya-mmp2014-kak-eto-bylo/'),
(258, 301, '/fotogalereya/', ''),
(259, 301, '/fotogalereya/biznes-konferencziya/', ''),
(260, 301, '/fotogalereya/biznes-konferencziya-menedzhment-marketing-prodazhi-2013/', ''),
(261, 301, '/fotogalereya/biznes-konferencziya-marketing-i-prodazhi1/', ''),
(262, 301, '/fotogalereya/biznes-na-maksimum-2.0/', ''),
(263, 301, '/fotogalereya/oratorskoe-iskusstvo-2.01/', ''),
(264, 301, '/treningi/biznes-konferencziya-mmp-2106-osen/', 'trainings/'),
(265, 301, '/fotogalereya/biznes-konferencziya-mmp-2014-kiev/', ''),
(266, 301, '/treningi/biznes-konferencziya-mmp-2017/', 'trainings/'),
(267, 301, '/treningi/korporativnyie-treningi/', 'trainings/'),
(268, 301, '/treningi/biznes-konferencziya-mmp-2015/', 'trainings/'),
(269, 301, '/treningi/biznes-konferencziya-mmp-2016-vesna/', 'trainings/'),
(270, 301, '/treningi/biznes-konferencziya-mmp-2015-vesna/', 'trainings/'),
(271, 301, '/treningi/motivacziya-personala-v-roznichnyix-prodazhax/', 'trainings/'),
(272, 301, '/treningi/aktivnyie-prodazhi-zaporozhe/', 'trainings/'),
(273, 301, '/treningi/10-sekretov-upravleniya-personalom/', 'trainings/'),
(274, 301, '/treningi/biznes-na-maksimum-4.0-dnepropetrovsk/', 'trainings/'),
(275, 301, '/treningi/aktivnyie-prodazhi-v-dnepropetrovske/', 'trainings/'),
(276, 301, '/treningi/kak-poluchat-podarki-ot-muzhchin/', 'trainings/'),
(277, 301, '/treningi/100-voprosov-biznes-treneru/', 'trainings/'),
(278, 301, '/treningi/koordinacziya-v-sisteme-upravleniya-kompaniej/', 'trainings/'),
(279, 301, '/treningi/reglamentyi.-put-k-sistematizaczii-biznesa/', 'trainings/'),
(280, 301, '/treningi/sekretyi-prodazh-xarkov/', 'trainings/'),
(281, 301, '/treningi/prodazhi-po-telefonu-poltava/', 'trainings/'),
(282, 301, '/treningi/lichnaya-vlast/', 'trainings/'),
(283, 301, '/treningi/aktivnyie-prodazhi-v-xarkove/', 'trainings/'),
(284, 301, '/treningi/provedenie-effektivnyix-soveshhanij/', 'trainings/'),
(285, 301, '/treningi/motivacziya-bez-knuta-i-pryanika/', 'trainings/'),
(286, 301, '/treningi/sekretyi-manipulyaczij-master-klass/', 'trainings/'),
(287, 301, '/treningi/antikrizis.-kak-ispolzovat-krizis-v-svoyu-polzu/', 'trainings/'),
(288, 301, '/blog/delovoj-etiket-pravila-znakomstva-i-predstavleniya/', 'blog/peregovory/delovoj-etiket-pravila-znakomstva-i-predstavleniya/'),
(289, 301, '/treningi/biznes-na-maksimum-3.0-dnepropetrovsk/', 'trainings/'),
(290, 301, '/blog/marketing-kit-kak-instrument-povyisheniya-prodazh/', 'blog/marketing/marketing-kit-kak-instrument-povysheniya-prodazh/'),
(291, 301, '/blog/12-glavnyix-sovetov-po-xolodnyim-prodazham/', 'blog/prodazhi/13-glavnyh-sovetov-po-holodnym-prodazham/'),
(292, 301, '/blog/7-klyuchevyix-sekretov-liderstva/', 'blog/rukovoditel/7-klyuchevyh-sekretov-liderstva/'),
(293, 301, '/blog/2015/top-5-kanalov-na-youtube-rasshiryayushhix-mirovozzrenie/', 'blog/top/top-5-kanalov-na-youtube-rasshiryayushhih-mirovozzrenie/'),
(294, 301, '/blog/10-prakticheskix-sovetov-rukovoditelyu/', 'blog/rukovoditel/10-prakticheskih-sovetov-rukovoditelyu/'),
(295, 301, '/blog/prodazhi-dlya-chego-nuzhna-poiskovaya-optimizacziya-ili-seo/', '/blog/marketing/prodazhi-dlya-chego-nuzhna-poiskovaya-optimizaciya-ili-seo/'),
(296, 301, '/treningi/sekretyi-prodazh-poltava/', 'trainings/'),
(297, 301, '/blog/2016/turne-zaporozhe-dnepr-xarkov/', 'blog/prodazhi/turne-zaporozhe-dnepr-harkov/'),
(298, 301, '/blog/zachem-nuzhna-dolzhnostnaya-instrukcziya/', 'blog/rukovoditel/zachem-nuzhna-dolzhnostnaya-instrukciya/'),
(299, 301, '/treningi/lichnaya-efektivnost-rukovoditelya/', 'trainings/'),
(300, 301, '/blog/garvardskaya-shkola-peregovorov/', 'blog/peregovory/garvardskaya-shkola-peregovorov/'),
(301, 301, '/blog/top-10-romanticheskix-filmov/', 'blog/top/top-10-romanticheskih-filmov/'),
(302, 301, '/blog/texniki-vozdejstviya-na-auditoriyu/', 'blog/orator/tehniki-vozdejstviya-na-auditoriyu/'),
(303, 301, '/blog/10-metodov-povyisheniya-lichnoj-effektivnosti/', 'blog/poleznoe/10-metodov-povysheniya-lichnoj-effektivnosti/'),
(304, 301, '/treningi/postroenie-personalnogo-brenda/', 'trainings/'),
(305, 301, '/treningi/avtoritet-rukovoditelya-kak-vliyat-na-podchinennyix/', 'trainings/'),
(306, 301, '/blog/2015/kak-eto-byilo-biznes-na-maksimum-v-poltave/', 'blog/poleznoe/kak-eto-bylo-biznes-na-maksimum-v-poltave/'),
(307, 301, '/blog/imidzh-i-vneshnij-vid-kak-oruzhie-uspexa-v-delovoj-srede/', 'blog/poleznoe/imidzh-i-vneshnij-vid-kak-oruzhie-uspeha-v-delovoj-srede1/'),
(308, 301, '/blog/9-sekretov-upravlenie-vremenem/', 'blog/poleznoe/9-sekretov-upravlenie-vremenem/'),
(309, 301, '/blog/9-sekretov-motivacziya-personala/', 'blog/rukovoditel/9-sekretov-motivaciya-personala/'),
(310, 301, '/blog/2015/kontrol-i-nakazanie-personala-7-rekomendaczij-rukovoditelyu/', 'blog/rukovoditel/kontrol-i-nakazanie-personala-7-rekomendacij-rukovoditelyu/'),
(311, 301, '/poleznoe/pritchi/nechto-czennoe/', ''),
(312, 301, '/blog/video-infografika-trening-czentr-lider/', 'blog/poleznoe/video-infografika-trening-centr-lider/'),
(313, 301, '/poleznoe/pritchi/topor-dzhenifer-lopes-i-ketrin-zeta-dzhons/', ''),
(314, 301, '/treningi/kak-organizovat-i-uspeshno-provesti-konferencziyu/', 'blog/poleznoe/kak-organizovat-konferenciyu/'),
(315, 301, '/treningi/sekretyi-manipulyaczij/', 'blog/poleznoe/sekrety-manipulyacij1/'),
(316, 301, '/treningi/malobyudzhetnyij-marketing/', 'blog/marketing/malobyudzhetnyj-marketing/'),
(317, 301, '/treningi/peregovoryi-o-czene/', 'blog/prodazhi/peregovory-o-cene1/'),
(318, 301, '/treningi/prodazhi-so-sczenyi/', 'blog/prodazhi/prodazhi-so-sceny1/'),
(319, 301, '/treningi/upravlenie-personalom/', 'blog/rukovoditel/trening-upravleniya-personalom/'),
(320, 301, '/treningi/upravlenie-personalom-instrumentyi-rukovoditelya/', 'blog/rukovoditel/effektivnoe-upravlenie-instrumenty-rukovoditelya/'),
(321, 301, '/treningi/prinyatie-effektivnyix-reshenij/', 'blog/rukovoditel/prinyatie-effektivnyh-reshenij/'),
(322, 301, '/treningi/psixologiya-lzhi/', 'blog/poleznoe/psihologiya-lzhi3/'),
(323, 301, '/poleznoe/pritchi/samyij-luchshij-biznes/', ''),
(324, 301, '/blog/kak-privlech-i-uderzhat-vnimanie-slushatelej/', 'blog/orator/kak-privlech-i-uderzhat-vnimanie-slushatelej/'),
(325, 301, '/blog/statya-bloga21/', 'blog/top/zolotye-zakony-biznesa/'),
(326, 301, '/blog/govorite-krasivo/', 'blog/orator/govorite-krasivo/'),
(327, 301, '/uslugi/audit-sistemyi-prodazh/', ''),
(328, 301, '/blog/oratorskoe-iskusstvo-kak-preodolet-strax-publichnyix-vyistuplenij/', 'blog/orator/oratorskoe-iskusstvo-kak-preodolet-strah-publichnyh-vystuplenij/'),
(329, 301, '/raspisanie/den-rozhdenie-lidera/', 'shop/'),
(330, 301, '/blog/2015/komu-i-dlya-chego-nuzhen-taj-menedzhment/', 'blog/poleznoe/komu-i-dlya-chego-nuzhen-taj-menedzhment/'),
(331, 301, '/poleznoe/pritchi/radio-tv-i-novyie-noski/', ''),
(332, 301, '/blog/2015/top-5-filmov-o-dengax/', 'blog/top/top-5-filmov-o-dengah/'),
(333, 301, '/blog/2015/top-5-filmov-o-finansovyix-geniyax/', 'blog/top/top-5-filmov-o-finansovyh-geniyah/'),
(334, 301, '/blog/5-luchshix-filmov-po-psixologii-lzhi/', 'blog/top/5-luchshih-filmov-po-psihologii-lzhi/'),
(335, 301, '/blog/vip-den-biznes-konferenczii-menedzhment-marketing-prodazhi/', 'blog/mmp/vip-den-biznes-konferencii-menedzhment-marketing-prodazhi/'),
(336, 301, '/blog/kak-postroit-biznes-na-osnovanii-texnologii/', 'blog/poleznoe/kak-postroit-biznes-na-osnovanii-tehnologii/'),
(337, 301, '/blog/2015/top-5-mifov-v-sfere-kopirajtinga/', 'blog/kopirajting/top-5-mifov-v-sfere-kopirajtinga/'),
(338, 301, '/poleznoe/pritchi/vazhno-ne-sobyitie-a-nashe-otnoshenie-k-nemu/', ''),
(339, 301, '/blog/2015/5-pravil-tajm-menedzhmenta-o-kotoryix-molchat/', 'blog/poleznoe/5-pravil-tajm-menedzhmenta-o-kotoryh-molchat/'),
(340, 301, '/o-kompanii/', 'about/'),
(341, 301, '/blog/top-5-dokumentalnyix-filmov-o-geniyax-biznesa/', 'blog/top/top-5-dokumentalnyh-filmov-o-geniyah-biznesa/'),
(342, 301, '/blog/top-5-sovetov-effektivnogo-ispolzovaniya-vremeni/', 'blog/poleznoe/top-5-sovetov-effektivnogo-ispolzovaniya-vremeni/'),
(343, 301, '/blog/top-5-oshibok-nachinayushhego-oratora/', 'blog/orator/top-5-oshibok-nachinayushhego-oratora/'),
(344, 301, '/blog/kak-byit-effektivnyim-liderom/', 'blog/rukovoditel/kak-byt-effektivnym-liderom/'),
(345, 301, '/blog/pozdravlenie-s-novyim-godom-ot-komandyi-trening-czentra-lider/', 'blog/poleznoe/pozdravlenie-s-novym-godom-ot-komandy-trening-centra-lider/'),
(346, 301, '/blog/2015/kak-pravilno-vyibrat-trening/', 'blog/poleznoe/kak-pravilno-vybrat-trening/'),
(347, 301, '/raspisanie/effektivnyij-peregovorshhik/', 'shop/'),
(348, 301, '/blog/klyuchevyie-pravila-obshheniya/', 'blog/peregovory/klyuchevye-pravila-obshheniya/'),
(349, 301, '/blog/9-sovetov-po-opredeleniyu-glavnoj-czeli-v-zhizni/', 'blog/poleznoe/9-sovetov-po-opredeleniyu-glavnoj-celi-v-zhizni/'),
(350, 301, '/blog/2015/7-sekretov-vedeniya-effektivnyix-peregovorov/', 'blog/peregovory/7-sekretov-vedeniya-effektivnyh-peregovorov/'),
(351, 301, '/fotogalereya/biznes-na-maksimum/', ''),
(352, 301, '/poleznoe/pritchi/straxovka-u-konkurenta/', ''),
(353, 301, '/blog/2015/davno-mechtaete-poluchit-diplom-mva/', 'blog/rukovoditel/davno-mechtaete-poluchit-diplom-mva/'),
(354, 301, '/poleznoe/pritchi/chto-takoe-sudba-i-est-li-bog', ''),
(355, 301, '/blog/2015/4-bazovyie-veshhi-pravilnogo-podbora-personala/', 'blog/rukovoditel/4-bazovye-veshhi-pravilnogo-podbora-personala/'),
(356, 301, '/poleznoe/pritchi/dobro-vs-zlo/', ''),
(357, 301, '/blog/dva-voprosa1/', 'blog/'),
(358, 301, '/blog/sovetyi-po-zakryitiyu-peregovorov-i-povedeniyu-itogov/', 'blog/peregovory/sovety-po-zakrytiyu-peregovorov-i-podvedeniyu-itogov/'),
(359, 301, '/blog/pochemu-lyudi-ne-stavyat-pered-soboj-czeli/', 'blog/poleznoe/pochemu-lyudi-ne-stavyat-pered-soboj-celi/'),
(360, 301, '/blog/biznes-trening-effektivnyij-rukovoditel-slomalos-mnogo-shablonov/', 'blog/'),
(361, 301, '/blog/2015/5-prichin-pochemu-menedzheryi-otkazyivayutsya-delegirovat-polnomochii-svoim-podchinennyim/', 'blog/rukovoditel/5-prichin-pochemu-menedzhery-otkazyvayutsya-delegirovat-polnomochii-svoim-podchinennym/'),
(362, 301, '/blog/2015/osnovnyie-texniki-vedeniya-delovyix-peregovorov/', 'blog/peregovory/osnovnye-tehniki-vedeniya-delovyh-peregovorov/'),
(363, 301, '/blog/oratorskoe-iskusstvo/', 'blog/orator/oratorskoe-iskusstvo/'),
(364, 301, '/blog/5-klyuchevyix-sovetov-po-postroeniyu-personalnogo-brenda/', 'blog/marketing/5-klyuchevyh-sovetov-po-postroeniyu-personalnogo-brenda/'),
(365, 301, '/blog/sekretyi-oratorskogo-iskusstva1/', 'blog/orator/sekrety-oratorskogo-iskusstva/'),
(366, 301, '/blog/delovoj-etiket-osnovnyie-pravila-obrashheniya/', 'blog/top/delovoj-etiket-osnovnye-pravila-obrashheniya/'),
(367, 301, '/blog/krasnorechie/', 'blog/orator/oratorskoe-masterstvo/'),
(368, 301, '/blog/treningi-dlya-rukovoditelej/', 'blog/rukovoditel/treningi-dlya-rukovoditelej/'),
(369, 301, '/fotogalereya/vip-den-biznes-konferenczii-mmp-2013/', ''),
(370, 301, '/blog/kak-usilit-svoyu-pozicziyu-v-delovyix-peregovorax/', 'blog/peregovory/kak-usilit-svoyu-poziciyu-v-delovyh-peregovorah/'),
(371, 301, '/blog/11-pravil-po-vedeniyu-peregovorov/', 'blog/peregovory/11-pravil-po-vedeniyu-peregovorov/'),
(372, 301, '/blog/kak-nauchitsya-oratorskomu-masterstvu/', 'blog/orator/kak-nauchitsya-oratorskomu-masterstvu/'),
(373, 301, '/blog/chem-vrednyi-treningi/', 'blog/poleznoe/chem-vredny-treningi/'),
(374, 301, '/o-kompanii/infografika/', 'about/'),
(375, 301, '/blog/2015/5-sekretov-liderov-o-kotoryix-govorit-ne-prinyato/', 'blog/rukovoditel/5-sekretov-liderov-o-kotoryh-govorit-ne-prinyato/'),
(376, 301, '/blog/biznes-konferencziya-menedzhment-marketing-prodazhi-2013-kak-eto-byilo/', 'blog/mmp/biznes-konferenciya-mmp-2013-kak-eto-bylo/'),
(377, 301, '/uslugi/marketing/', ''),
(378, 301, '/treningi/kontrol-i-nakazaniya-personala/', 'trainings/'),
(379, 301, '/treningi/organizaczionnaya-kultura-kompanii/', 'trainings/'),
(380, 301, '/blog/chto-znayut-i-delayut-uspeshnyie-peregovorshhiki/', 'blog/peregovory/chto-znayut-i-delayut-uspeshnye-peregovorshhiki/'),
(381, 301, '/blog/2015/vip-den-biznes-konferenczii-menedzhment.-marketing.-prodazhi-2015/', 'blog/mmp/vip-den-biznes-konferencii-menedzhment-marketing-prodazhi-2015/'),
(382, 301, '/treningi/psixologiya-deneg/', 'trainings/'),
(383, 301, '/blog/2015/5-mifov-o-liderstve/', 'blog/poleznoe/5-mifov-o-liderstve/'),
(384, 301, '/blog/kak-uspeshno-provesti-prezentacziyu/', 'blog/peregovory/kak-uspeshno-provesti-prezentaciyu/'),
(385, 301, '/blog/2015/osnovnyie-oshibki-prodazh/', 'blog/prodazhi/osnovnye-oshibki-prodazh/'),
(386, 301, '/blog/2015/sekretyi-prodazh-po-telefonu/', 'blog/prodazhi/sekrety-prodazh-po-telefonu/'),
(387, 301, '/blog/sem-sekretov-podgotovki-k-peregovoram/', 'blog/peregovory/sem-sekretov-podgotovki-k-peregovoram/'),
(388, 301, '/poleznoe/', ''),
(389, 301, '/blog/2015/byit-bogatyim-5-sovetov-povyisheniya-urovnya-blagosostoyaniya/', 'blog/poleznoe/byt-bogatym-5-sovetov-povysheniya-urovnya-blagosostoyaniya/'),
(390, 301, '/blog/a-vyi-umeete-proizvesti-pervoe-vpechatlenie/', 'blog/peregovory/a-vy-umeete-proizvesti-priyatnoe-vpechatlenie/'),
(391, 301, '/blog/2015/7-treningov-kotoryie-sdelayut-iz-vas-lidera/', 'blog/poleznoe/7-treningov-kotorye-sdelayut-iz-vas-lidera/'),
(392, 301, '/blog/2015/5-klyuchevyix-oshibok-pri-sozdanii-sistemyi-motivaczii-personala/', 'blog/rukovoditel/5-klyuchevyh-oshibok-pri-sozdanii-sistemy-motivacii-personala/'),
(393, 301, '/blog/tajm-menedzhment-na-zashhite-vashego-vremeni/', 'blog/poleznoe/tajm-menedzhment-na-zashhite-vashego-vremeni/'),
(394, 301, '/poleznoe/pritchi/v-chem-sekret-iskusstva-zhizni/', ''),
(395, 301, '/fotogalereya/biznes-konferencziya-mmp2015-vesna/', ''),
(396, 301, '/blog/2015/10-sekretov-kak-poborot-strax-pered-publichnyimi-vyistupleniyami/', 'blog/orator/10-sekretov-kak-poborot-strah-pered-publichnymi-vystupleniyami/'),
(397, 301, '/blog/2015/trening-dlya-rukovoditelej-kak-eto-byilo1/', 'blog/rukovoditel/trening-dlya-rukovoditelej-kak-eto-bylo/'),
(398, 301, '/blog/2015/chto-podarit-rukovoditelyu/', 'blog/rukovoditel/chto-podarit-rukovoditelyu/'),
(399, 301, '/treningi/marketing-uvelichenie-potoka-klientov/', 'trainings/'),
(400, 301, '/blog/delovyie-peregovoryi-garvardskij-metod-peregovorov/', 'blog/peregovory/delovye-peregovory-garvardskij-metod-peregovorov/'),
(401, 301, '/treningi/kopirajting/', 'trainings/'),
(402, 301, '/blog/upravlencheskij-czikl/', 'blog/rukovoditel/upravlencheskij-cikl/'),
(403, 301, '/blog/prodazhi-sajt-kotoryij-prodaet/', 'blog/marketing/prodazhi-sajt-kotoryj-prodaet/'),
(404, 301, '/blog/top-7-luchshix-komedij-za-poslednie-5-let/', 'blog/top/top-7-luchshih-komedij-za-poslednie-5-let/'),
(405, 301, '/blog/kak-uvelichit-prodazhi-s-pomoshhyu-podarochnyix-sertifikatov/', 'blog/'),
(406, 301, '/blog/samyie-vazhnyie-pravila-povedeniya-na-ulicze/', 'blog/'),
(407, 301, '/blog/oratorskoe-iskusstvo-vnutrennij-nastroj-i-uvlechennost-temoj/', 'blog/orator/oratorskoe-iskusstvo-vnutrennij-nastroj-i-uvlechennost-temoj/'),
(408, 301, '/blog/prodayushhaya-vizitka-kakaya-ona/', 'blog/marketing/prodayushhaya-vizitka-kakaya-ona/'),
(409, 301, '/blog/2015/sovetyi-dlya-rukovoditelya1/', 'blog/rukovoditel/sovety-dlya-rukovoditelya/'),
(410, 301, '/treningi/sekretyi-oratorskogo-masterstva/', 'trainings/');

-- --------------------------------------------------------

--
-- Структура таблицы `modx_categories`
--

CREATE TABLE `modx_categories` (
  `id` int(11) NOT NULL,
  `category` varchar(45) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Categories to be used snippets,tv,chunks, etc';

--
-- Дамп данных таблицы `modx_categories`
--

INSERT INTO `modx_categories` (`id`, `category`) VALUES
(1, 'Demo Content'),
(2, 'Js'),
(3, 'Manager and Admin'),
(4, 'Search'),
(5, 'Navigation'),
(6, 'Content'),
(7, 'Forms'),
(8, 'Login'),
(9, 'Pages'),
(10, 'Main'),
(11, 'additional'),
(12, 'Menu'),
(13, 'News'),
(14, 'Seo4Evo'),
(15, 'add');

-- --------------------------------------------------------

--
-- Структура таблицы `modx_documentgroup_names`
--

CREATE TABLE `modx_documentgroup_names` (
  `id` int(10) NOT NULL,
  `name` varchar(245) NOT NULL DEFAULT '',
  `private_memgroup` tinyint(4) DEFAULT '0' COMMENT 'determine whether the document group is private to manager users',
  `private_webgroup` tinyint(4) DEFAULT '0' COMMENT 'determines whether the document is private to web users'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Contains data used for access permissions.';

-- --------------------------------------------------------

--
-- Структура таблицы `modx_document_groups`
--

CREATE TABLE `modx_document_groups` (
  `id` int(10) NOT NULL,
  `document_group` int(10) NOT NULL DEFAULT '0',
  `document` int(10) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Contains data used for access permissions.';

-- --------------------------------------------------------

--
-- Структура таблицы `modx_event_log`
--

CREATE TABLE `modx_event_log` (
  `id` int(11) NOT NULL,
  `eventid` int(11) DEFAULT '0',
  `createdon` int(11) NOT NULL DEFAULT '0',
  `type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1- information, 2 - warning, 3- error',
  `user` int(11) NOT NULL DEFAULT '0' COMMENT 'link to user table',
  `usertype` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 - manager, 1 - web',
  `source` varchar(50) NOT NULL DEFAULT '',
  `description` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Stores event and error logs';

--
-- Дамп данных таблицы `modx_event_log`
--

INSERT INTO `modx_event_log` (`id`, `eventid`, `createdon`, `type`, `user`, `usertype`, `source`, `description`) VALUES
(1, 0, 1525116328, 3, 0, 1, 'Plugin - Common / PHP Parse Error', '<b>Trying to get property of non-object</b><br />\n<h2 style=\"color:red\">&laquo; MODX Parse Error &raquo;</h2><div style=\"font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;margin-bottom:15px;\">Error : json_decode() expects parameter 1 to be string, array given</div>\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Error information</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>ErrorType[num]</td>\n		<td>WARNING[2]</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>File</td>\n		<td>D:\\OSPanel\\domains\\solistor.local\\manager\\includes\\document.parser.class.inc.php(1318) : eval()\'d code</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Line</td>\n		<td>50</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Current Plugin</td>\n		<td>Common(OnWebPageInit)</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Basic info</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>REQUEST_URI</td>\n		<td>http://solistor.local/</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Resource</td>\n		<td>[2] <a href=\"http://solistor.local/\" target=\"_blank\">Главная</a></td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Referer</td>\n		<td>http://solistor.local/</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>User Agent</td>\n		<td>Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>IP</td>\n		<td>127.0.0.1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Current time</td>\n		<td>2018-04-30 22:25:28</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Benchmarks</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>MySQL</td>\n		<td>0.0024 s (3 Requests)</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>PHP</td>\n		<td>0.1106 s</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Total</td>\n		<td>0.1130 s</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Memory</td>\n		<td>1.3636779785156 mb</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th>Backtrace</th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->executeParser</strong>()<br />index.php on line 128</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->invokeEvent</strong>(\'OnWebPageInit\')<br />manager/includes/document.parser.class.inc.php on line 2135</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalPlugin</strong>(string $var1, array $var2)<br />manager/includes/document.parser.class.inc.php on line 4482</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eval</strong>()<br />manager/includes/document.parser.class.inc.php on line 1318</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>json_decode</strong>(array $var1)<br />manager/includes/document.parser.class.inc.php(1318) : eval()\'d code on line 50</td>\n	</tr>\n</table>\n'),
(2, 0, 1525116382, 3, 0, 1, 'Plugin - Common / PHP Parse Error', '<b>Trying to get property of non-object</b><br />\n<h2 style=\"color:red\">&laquo; MODX Parse Error &raquo;</h2><div style=\"font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;margin-bottom:15px;\">Error : json_decode() expects parameter 1 to be string, array given</div>\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Error information</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>ErrorType[num]</td>\n		<td>WARNING[2]</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>File</td>\n		<td>D:\\OSPanel\\domains\\solistor.local\\manager\\includes\\document.parser.class.inc.php(1318) : eval()\'d code</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Line</td>\n		<td>50</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Current Plugin</td>\n		<td>Common(OnWebPageInit)</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Basic info</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>REQUEST_URI</td>\n		<td>http://solistor.local/</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Resource</td>\n		<td>[2] <a href=\"http://solistor.local/\" target=\"_blank\">Главная</a></td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Referer</td>\n		<td>http://solistor.local/</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>User Agent</td>\n		<td>Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>IP</td>\n		<td>127.0.0.1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Current time</td>\n		<td>2018-04-30 22:26:22</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Benchmarks</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>MySQL</td>\n		<td>0.0024 s (3 Requests)</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>PHP</td>\n		<td>0.1204 s</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Total</td>\n		<td>0.1228 s</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Memory</td>\n		<td>1.3637008666992 mb</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th>Backtrace</th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->executeParser</strong>()<br />index.php on line 128</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->invokeEvent</strong>(\'OnWebPageInit\')<br />manager/includes/document.parser.class.inc.php on line 2135</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalPlugin</strong>(string $var1, array $var2)<br />manager/includes/document.parser.class.inc.php on line 4482</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eval</strong>()<br />manager/includes/document.parser.class.inc.php on line 1318</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>json_decode</strong>(array $var1, \'1\')<br />manager/includes/document.parser.class.inc.php(1318) : eval()\'d code on line 50</td>\n	</tr>\n</table>\n'),
(3, 0, 1525526496, 3, 0, 1, 'Plugin - Common / PHP Parse Error', '<b>Trying to get property of non-object</b><br />\n<h2 style=\"color:red\">&laquo; MODX Parse Error &raquo;</h2><div style=\"font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;margin-bottom:15px;\">Error : json_decode() expects parameter 1 to be string, array given</div>\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Error information</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>ErrorType[num]</td>\n		<td>WARNING[2]</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>File</td>\n		<td>D:\\OSPanel\\domains\\solistor.local\\manager\\includes\\document.parser.class.inc.php(1318) : eval()\'d code</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Line</td>\n		<td>50</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Current Plugin</td>\n		<td>Common(OnWebPageInit)</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Basic info</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>REQUEST_URI</td>\n		<td>http://solistor.local/contact_us</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Resource</td>\n		<td>[3] <a href=\"http://solistor.local/contact_us\" target=\"_blank\">Контакты</a></td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Referer</td>\n		<td>http://solistor.local/contact_us</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>User Agent</td>\n		<td>Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>IP</td>\n		<td>127.0.0.1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Current time</td>\n		<td>2018-05-05 16:21:36</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Benchmarks</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>MySQL</td>\n		<td>0.0117 s (3 Requests)</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>PHP</td>\n		<td>0.1062 s</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Total</td>\n		<td>0.1179 s</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Memory</td>\n		<td>1.3624038696289 mb</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th>Backtrace</th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->executeParser</strong>()<br />index.php on line 128</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->invokeEvent</strong>(\'OnWebPageInit\')<br />manager/includes/document.parser.class.inc.php on line 2135</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalPlugin</strong>(string $var1, array $var2)<br />manager/includes/document.parser.class.inc.php on line 4482</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eval</strong>()<br />manager/includes/document.parser.class.inc.php on line 1318</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>json_decode</strong>(array $var1)<br />manager/includes/document.parser.class.inc.php(1318) : eval()\'d code on line 50</td>\n	</tr>\n</table>\n'),
(4, 0, 1537816050, 3, 0, 1, 'Snippet - S / Execution of a query to the database', '<h2 style=\"color:red\">&laquo; MODX Parse Error &raquo;</h2><h3 style=\"color:red\">Execution of a query to the database failed - You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'DESC\' at line 1</h3><div style=\"font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;margin-bottom:15px;\">SQL &gt; <span id=\"sqlHolder\">SELECT * FROM `modx_a_news` LIMIT 4 DESC</span></div>\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Error information</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>Current Snippet</td>\n		<td>S</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Basic info</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>REQUEST_URI</td>\n		<td>http://solistor.local/news</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Resource</td>\n		<td>[4] <a href=\"http://solistor.local/news\" target=\"_blank\">Новости</a></td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Referer</td>\n		<td>http://solistor.local/contact_us</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>User Agent</td>\n		<td>Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>IP</td>\n		<td>127.0.0.1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Current time</td>\n		<td>2018-09-24 22:07:30</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Benchmarks</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>MySQL</td>\n		<td>0.0032 s (7 Requests)</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>PHP</td>\n		<td>0.0643 s</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Total</td>\n		<td>0.0675 s</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Memory</td>\n		<td>1.8620834350586 mb</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th>Backtrace</th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->executeParser</strong>()<br />index.php on line 128</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->prepareResponse</strong>()<br />manager/includes/document.parser.class.inc.php on line 2141</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->parseDocumentSource</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 2218</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->evalSnippets</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 2003</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->_get_snip_result</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 1408</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->evalSnippet</strong>(string $var1, array $var2)<br />manager/includes/document.parser.class.inc.php on line 1466</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>eval</strong>()<br />manager/includes/document.parser.class.inc.php on line 1360</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DBAPI->query</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php(1360) : eval()\'d code on line 23</td>\n	</tr>\n</table>\n'),
(5, 0, 1537816090, 3, 0, 1, 'Snippet - S / Execution of a query to the database', '<h2 style=\"color:red\">&laquo; MODX Parse Error &raquo;</h2><h3 style=\"color:red\">Execution of a query to the database failed - You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'ORDER BY NewsId DESC\' at line 1</h3><div style=\"font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;margin-bottom:15px;\">SQL &gt; <span id=\"sqlHolder\">SELECT * FROM `modx_a_news` LIMIT 4 ORDER BY NewsId DESC</span></div>\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Error information</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>Current Snippet</td>\n		<td>S</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Basic info</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>REQUEST_URI</td>\n		<td>http://solistor.local/news</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Resource</td>\n		<td>[4] <a href=\"http://solistor.local/news\" target=\"_blank\">Новости</a></td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Referer</td>\n		<td>http://solistor.local/contact_us</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>User Agent</td>\n		<td>Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>IP</td>\n		<td>127.0.0.1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Current time</td>\n		<td>2018-09-24 22:08:10</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Benchmarks</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>MySQL</td>\n		<td>0.0034 s (7 Requests)</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>PHP</td>\n		<td>0.0601 s</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Total</td>\n		<td>0.0635 s</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Memory</td>\n		<td>1.8620834350586 mb</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th>Backtrace</th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->executeParser</strong>()<br />index.php on line 128</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->prepareResponse</strong>()<br />manager/includes/document.parser.class.inc.php on line 2141</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->parseDocumentSource</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 2218</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->evalSnippets</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 2003</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->_get_snip_result</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 1408</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->evalSnippet</strong>(string $var1, array $var2)<br />manager/includes/document.parser.class.inc.php on line 1466</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>eval</strong>()<br />manager/includes/document.parser.class.inc.php on line 1360</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DBAPI->query</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php(1360) : eval()\'d code on line 23</td>\n	</tr>\n</table>\n'),
(6, 0, 1537891191, 3, 1, 0, 'Plugin - FileSource / PHP Parse Error - Saving Sni', '<h2 style=\"color:red\">&laquo; MODX Parse Error &raquo;</h2><div style=\"font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;margin-bottom:15px;\">Error : file_put_contents(/var/www/solistor/assets/snippets/if/snippet.if.php): failed to open stream: Permission denied</div><div style=\"font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;margin-bottom:15px;\"><br />\n<font size=\'1\'><table class=\'xdebug-error xe-warning\' dir=\'ltr\' border=\'1\' cellspacing=\'0\' cellpadding=\'1\'>\n<tr><th align=\'left\' bgcolor=\'#f57900\' colspan=\"5\"><span style=\'background-color: #cc0000; color: #fce94f; font-size: x-large;\'>( ! )</span> Warning: file_put_contents(/var/www/solistor/assets/snippets/if/snippet.if.php): failed to open stream: Permission denied in /var/www/solistor/assets/plugins/filesource/plugin.filesource.php on line <i>133</i></th></tr>\n<tr><th align=\'left\' bgcolor=\'#e9b96e\' colspan=\'5\'>Call Stack</th></tr>\n<tr><th align=\'center\' bgcolor=\'#eeeeec\'>#</th><th align=\'left\' bgcolor=\'#eeeeec\'>Time</th><th align=\'left\' bgcolor=\'#eeeeec\'>Memory</th><th align=\'left\' bgcolor=\'#eeeeec\'>Function</th><th align=\'left\' bgcolor=\'#eeeeec\'>Location</th></tr>\n<tr><td bgcolor=\'#eeeeec\' align=\'center\'>1</td><td bgcolor=\'#eeeeec\' align=\'center\'>0.0002</td><td bgcolor=\'#eeeeec\' align=\'right\'>391352</td><td bgcolor=\'#eeeeec\'>{main}(  )</td><td title=\'/var/www/solistor/manager/index.php\' bgcolor=\'#eeeeec\'>.../index.php<b>:</b>0</td></tr>\n<tr><td bgcolor=\'#eeeeec\' align=\'center\'>2</td><td bgcolor=\'#eeeeec\' align=\'center\'>0.0064</td><td bgcolor=\'#eeeeec\' align=\'right\'>633056</td><td bgcolor=\'#eeeeec\'>include_once( <font color=\'#00bb00\'>\'/var/www/solistor/manager/processors/save_snippet.processor.php\'</font> )</td><td title=\'/var/www/solistor/manager/index.php\' bgcolor=\'#eeeeec\'>.../index.php<b>:</b>506</td></tr>\n<tr><td bgcolor=\'#eeeeec\' align=\'center\'>3</td><td bgcolor=\'#eeeeec\' align=\'center\'>0.0065</td><td bgcolor=\'#eeeeec\' align=\'right\'>641976</td><td bgcolor=\'#eeeeec\'>DocumentParser->invokeEvent(  )</td><td title=\'/var/www/solistor/manager/processors/save_snippet.processor.php\' bgcolor=\'#eeeeec\'>.../save_snippet.processor.php<b>:</b>114</td></tr>\n<tr><td bgcolor=\'#eeeeec\' align=\'center\'>4</td><td bgcolor=\'#eeeeec\' align=\'center\'>0.0066</td><td bgcolor=\'#eeeeec\' align=\'right\'>642680</td><td bgcolor=\'#eeeeec\'>DocumentParser->evalPlugin(  )</td><td title=\'/var/www/solistor/manager/includes/document.parser.class.inc.php\' bgcolor=\'#eeeeec\'>.../document.parser.class.inc.php<b>:</b>4482</td></tr>\n<tr><td bgcolor=\'#eeeeec\' align=\'center\'>5</td><td bgcolor=\'#eeeeec\' align=\'center\'>0.0067</td><td bgcolor=\'#eeeeec\' align=\'right\'>660704</td><td bgcolor=\'#eeeeec\'>eval( <font color=\'#00bb00\'>\'require MODX_BASE_PATH.\'assets/plugins/filesource/plugin.filesource.php\';\'</font> )</td><td title=\'/var/www/solistor/manager/includes/document.parser.class.inc.php\' bgcolor=\'#eeeeec\'>.../document.parser.class.inc.php<b>:</b>1318</td></tr>\n<tr><td bgcolor=\'#eeeeec\' align=\'center\'>6</td><td bgcolor=\'#eeeeec\' align=\'center\'>0.0068</td><td bgcolor=\'#eeeeec\' align=\'right\'>661312</td><td bgcolor=\'#eeeeec\'>eval( <font color=\'#00bb00\'>\'require MODX_BASE_PATH.\'assets/plugins/filesource/plugin.filesource.php\';\'</font> )</td><td title=\'/var/www/solistor/manager/includes/document.parser.class.inc.php(1318) : eval()\'d code\' bgcolor=\'#eeeeec\'>.../document.parser.class.inc.php(1318) : eval()\'d code<b>:</b>1</td></tr>\n<tr><td bgcolor=\'#eeeeec\' align=\'center\'>7</td><td bgcolor=\'#eeeeec\' align=\'center\'>0.0068</td><td bgcolor=\'#eeeeec\' align=\'right\'>670456</td><td bgcolor=\'#eeeeec\'><a href=\'http://www.php.net/function.file-put-contents\' target=\'_new\'>file_put_contents</a>\n(  )</td><td title=\'/var/www/solistor/assets/plugins/filesource/plugin.filesource.php\' bgcolor=\'#eeeeec\'>.../plugin.filesource.php<b>:</b>133</td></tr>\n</table></font>\n</div>\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Error information</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>ErrorType[num]</td>\n		<td>WARNING[2]</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>File</td>\n		<td>/var/www/solistor/assets/plugins/filesource/plugin.filesource.php</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Line</td>\n		<td>133</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Source</td>\n		<td>Plugin</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Current Plugin</td>\n		<td>FileSource(OnBeforeSnipFormSave)</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Basic info</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>REQUEST_URI</td>\n		<td>http://solistor.local/manager/index.php?a=24</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Manager action</td>\n		<td>24 - Saving Snippet</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Referer</td>\n		<td>http://solistor.local/manager/index.php?id=7&amp;amp;a=22</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>User Agent</td>\n		<td>Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>IP</td>\n		<td>172.17.0.1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Current time</td>\n		<td>2018-09-25 15:59:51</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Benchmarks</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>MySQL</td>\n		<td>0.0024 s (4 Requests)</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>PHP</td>\n		<td>0.0055 s</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Total</td>\n		<td>0.0079 s</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Memory</td>\n		<td>1.6267776489258 mb</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th>Backtrace</th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td><strong>include_once</strong>(string $var1)<br />manager/index.php on line 506</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->invokeEvent</strong>(\'OnBeforeSnipFormSave\', array $var2)<br />manager/processors/save_snippet.processor.php on line 114</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalPlugin</strong>(string $var1, array $var2)<br />manager/includes/document.parser.class.inc.php on line 4482</td>\n	</tr>\n</table>\n'),
(7, 0, 1538136136, 3, 1, 0, 'SQL Query / Execution of a query to the database f', '<h2 style=\"color:red\">&laquo; MODX Parse Error &raquo;</h2><h3 style=\"color:red\">Execution of a query to the database failed - Table \'solistor.modx_a_redirect\' doesn\'t exist</h3><div style=\"font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;margin-bottom:15px;\">SQL &gt; <span id=\"sqlHolder\">select * from `modx_a_redirect` order by redirect_id desc limit 20</span></div>\n<table class=\"grid\">\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Basic info</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>REQUEST_URI</td>\n		<td>http://solistor.local/manager/index.php?a=112&amp;id=4</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Manager action</td>\n		<td>112 - Execute module</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Referer</td>\n		<td>http://solistor.local/manager/index.php?a=1&amp;amp;f=menu</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>User Agent</td>\n		<td>Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>IP</td>\n		<td>172.17.0.1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Current time</td>\n		<td>2018-09-28 12:02:16</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Benchmarks</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>MySQL</td>\n		<td>0.0031 s (5 Requests)</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>PHP</td>\n		<td>0.0064 s</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Total</td>\n		<td>0.0095 s</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Memory</td>\n		<td>1.6573867797852 mb</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th>Backtrace</th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td><strong>include_once</strong>(string $var1)<br />manager/index.php on line 597</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>evalModule</strong>(string $var1, array $var2)<br />manager/processors/execute_module.processor.php on line 60</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>eval</strong>()<br />manager/processors/execute_module.processor.php on line 73</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>require</strong>(string $var1)<br />manager/processors/execute_module.processor.php(73) : eval()\'d code on line 1</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DBAPI->query</strong>(string $var1)<br />assets/modules/seo/index.php on line 61</td>\n	</tr>\n</table>\n'),
(8, 0, 1539027423, 3, 1, 0, 'Системные файлы были изменены.', 'Вы включили проверку системных файлов на наличие изменений, характерных для взломанных сайтов. Это не значит, что сайт был взломан, но желательно просмотреть измененные файлы.(index.php, .htaccess, manager/index.php, manager/includes/config.inc.php) index.php, .htaccess, manager/index.php, manager/includes/config.inc.php'),
(9, 0, 1539028155, 3, 2, 0, '', ''),
(10, 0, 1539028175, 3, 1, 0, 'Системные файлы были изменены.', 'Вы включили проверку системных файлов на наличие изменений, характерных для взломанных сайтов. Это не значит, что сайт был взломан, но желательно просмотреть измененные файлы.(index.php, .htaccess, manager/index.php, manager/includes/config.inc.php) index.php, .htaccess, manager/index.php, manager/includes/config.inc.php'),
(11, 0, 1539028263, 3, 2, 0, 'Системные файлы были изменены.', 'Вы включили проверку системных файлов на наличие изменений, характерных для взломанных сайтов. Это не значит, что сайт был взломан, но желательно просмотреть измененные файлы.(index.php, .htaccess, manager/index.php, manager/includes/config.inc.php) index.php, .htaccess, manager/index.php, manager/includes/config.inc.php'),
(12, 0, 1539028610, 3, 3, 0, 'Системные файлы были изменены.', 'Вы включили проверку системных файлов на наличие изменений, характерных для взломанных сайтов. Это не значит, что сайт был взломан, но желательно просмотреть измененные файлы.(index.php, .htaccess, manager/index.php, manager/includes/config.inc.php) index.php, .htaccess, manager/index.php, manager/includes/config.inc.php'),
(13, 0, 1539028678, 3, 2, 0, 'Системные файлы были изменены.', 'Вы включили проверку системных файлов на наличие изменений, характерных для взломанных сайтов. Это не значит, что сайт был взломан, но желательно просмотреть измененные файлы.(index.php, .htaccess, manager/index.php, manager/includes/config.inc.php) index.php, .htaccess, manager/index.php, manager/includes/config.inc.php'),
(14, 0, 1539028733, 3, 3, 0, 'Системные файлы были изменены.', 'Вы включили проверку системных файлов на наличие изменений, характерных для взломанных сайтов. Это не значит, что сайт был взломан, но желательно просмотреть измененные файлы.(index.php, .htaccess, manager/index.php, manager/includes/config.inc.php) index.php, .htaccess, manager/index.php, manager/includes/config.inc.php'),
(15, 0, 1539182918, 3, 2, 0, 'Системные файлы были изменены.', 'Вы включили проверку системных файлов на наличие изменений, характерных для взломанных сайтов. Это не значит, что сайт был взломан, но желательно просмотреть измененные файлы.(index.php, .htaccess, manager/index.php, manager/includes/config.inc.php) index.php, .htaccess, manager/index.php, manager/includes/config.inc.php');

-- --------------------------------------------------------

--
-- Структура таблицы `modx_keyword_xref`
--

CREATE TABLE `modx_keyword_xref` (
  `content_id` int(11) NOT NULL DEFAULT '0',
  `keyword_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Cross reference bewteen keywords and content';

-- --------------------------------------------------------

--
-- Структура таблицы `modx_manager_log`
--

CREATE TABLE `modx_manager_log` (
  `id` int(10) NOT NULL,
  `timestamp` int(20) NOT NULL DEFAULT '0',
  `internalKey` int(10) NOT NULL DEFAULT '0',
  `username` varchar(255) DEFAULT NULL,
  `action` int(10) NOT NULL DEFAULT '0',
  `itemid` varchar(10) DEFAULT '0',
  `itemname` varchar(255) DEFAULT NULL,
  `message` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Contains a record of user interaction.';

--
-- Дамп данных таблицы `modx_manager_log`
--

INSERT INTO `modx_manager_log` (`id`, `timestamp`, `internalKey`, `username`, `action`, `itemid`, `itemname`, `message`) VALUES
(1, 1523718432, 1, 'admin', 58, '-', 'MODX', 'Logged in'),
(2, 1523718433, 1, 'admin', 17, '-', '-', 'Editing settings'),
(3, 1523718441, 1, 'admin', 17, '-', '-', 'Editing settings'),
(4, 1523718464, 1, 'admin', 30, '-', '-', 'Saving settings'),
(5, 1523718479, 1, 'admin', 76, '-', '-', 'Element management'),
(6, 1523718484, 1, 'admin', 19, '-', 'Новый шаблон', 'Creating a new template'),
(7, 1523718529, 1, 'admin', 20, '-', 'home', 'Saving template'),
(8, 1523718529, 1, 'admin', 76, '-', '-', 'Element management'),
(9, 1523718540, 1, 'admin', 76, '-', '-', 'Element management'),
(10, 1523718543, 1, 'admin', 19, '-', 'Новый шаблон', 'Creating a new template'),
(11, 1523718583, 1, 'admin', 20, '-', 'Contacts', 'Saving template'),
(12, 1523718583, 1, 'admin', 76, '-', '-', 'Element management'),
(13, 1523718706, 1, 'admin', 16, '5', 'home', 'Editing template'),
(14, 1523718803, 1, 'admin', 20, '5', 'home', 'Saving template'),
(15, 1523718803, 1, 'admin', 76, '-', '-', 'Element management'),
(16, 1523718810, 1, 'admin', 17, '-', '-', 'Editing settings'),
(17, 1523718836, 1, 'admin', 30, '-', '-', 'Saving settings'),
(18, 1523718846, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(19, 1523718864, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(20, 1523718878, 1, 'admin', 5, '-', 'Главная', 'Saving resource'),
(21, 1523718879, 1, 'admin', 3, '2', 'Главная', 'Viewing data for resource'),
(22, 1523718881, 1, 'admin', 27, '2', 'Главная', 'Editing resource'),
(23, 1523718883, 1, 'admin', 27, '1', 'MODX CMS Install Success', 'Editing resource'),
(24, 1523718888, 1, 'admin', 27, '1', 'MODX CMS Install Success', 'Editing resource'),
(25, 1523718895, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(26, 1523718926, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(27, 1523718942, 1, 'admin', 5, '-', 'Контакты', 'Saving resource'),
(28, 1523718943, 1, 'admin', 27, '2', 'Главная', 'Editing resource'),
(29, 1523718943, 1, 'admin', 3, '3', 'Контакты', 'Viewing data for resource'),
(30, 1523718945, 1, 'admin', 27, '2', 'Главная', 'Editing resource'),
(31, 1523718948, 1, 'admin', 5, '2', 'Главная', 'Saving resource'),
(32, 1523718949, 1, 'admin', 3, '2', 'Главная', 'Viewing data for resource'),
(33, 1523718952, 1, 'admin', 17, '-', '-', 'Editing settings'),
(34, 1523719074, 1, 'admin', 30, '-', '-', 'Saving settings'),
(35, 1523719076, 1, 'admin', 27, '2', 'Главная', 'Editing resource'),
(36, 1523719088, 1, 'admin', 27, '1', 'MODX CMS Install Success', 'Editing resource'),
(37, 1523719103, 1, 'admin', 31, '-', '-', 'Using file manager'),
(38, 1523719106, 1, 'admin', 76, '-', '-', 'Element management'),
(39, 1523719110, 1, 'admin', 16, '4', 'MODX startup - Bootstrap', 'Editing template'),
(40, 1523719112, 1, 'admin', 76, '-', '-', 'Element management'),
(41, 1523719114, 1, 'admin', 16, '4', 'MODX startup - Bootstrap', 'Editing template'),
(42, 1523719116, 1, 'admin', 21, '4', 'MODX startup - Bootstrap', 'Deleting template'),
(43, 1523719116, 1, 'admin', 76, '-', '-', 'Element management'),
(44, 1523719118, 1, 'admin', 16, '3', 'Minimal Template', 'Editing template'),
(45, 1523719123, 1, 'admin', 27, '1', 'MODX CMS Install Success', 'Editing resource'),
(46, 1523719129, 1, 'admin', 27, '1', 'MODX CMS Install Success', 'Editing resource'),
(47, 1523719141, 1, 'admin', 17, '-', '-', 'Editing settings'),
(48, 1523719238, 1, 'admin', 16, '5', 'home', 'Editing template'),
(49, 1523719284, 1, 'admin', 20, '5', 'Home', 'Saving template'),
(50, 1523719284, 1, 'admin', 76, '-', '-', 'Element management'),
(51, 1523719288, 1, 'admin', 17, '-', '-', 'Editing settings'),
(52, 1523719295, 1, 'admin', 30, '-', '-', 'Saving settings'),
(53, 1523719327, 1, 'admin', 27, '2', 'Главная', 'Editing resource'),
(54, 1523719365, 1, 'admin', 76, '-', '-', 'Element management'),
(55, 1523719367, 1, 'admin', 16, '5', 'Home', 'Editing template'),
(56, 1523719389, 1, 'admin', 20, '5', 'Home', 'Saving template'),
(57, 1523719389, 1, 'admin', 76, '-', '-', 'Element management'),
(58, 1523719452, 1, 'admin', 17, '-', '-', 'Editing settings'),
(59, 1523719465, 1, 'admin', 30, '-', '-', 'Saving settings'),
(60, 1523719468, 1, 'admin', 27, '1', 'MODX CMS Install Success', 'Editing resource'),
(61, 1523719472, 1, 'admin', 27, '1', 'MODX CMS Install Success', 'Editing resource'),
(62, 1523719479, 1, 'admin', 17, '-', '-', 'Editing settings'),
(63, 1523719487, 1, 'admin', 30, '-', '-', 'Saving settings'),
(64, 1523719489, 1, 'admin', 27, '1', 'MODX CMS Install Success', 'Editing resource'),
(65, 1523719492, 1, 'admin', 6, '1', 'MODX CMS Install Success', 'Deleting resource'),
(66, 1523719493, 1, 'admin', 3, '1', 'MODX CMS Install Success', 'Viewing data for resource'),
(67, 1523719501, 1, 'admin', 27, '2', 'Главная', 'Editing resource'),
(68, 1523720711, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(69, 1523720715, 1, 'admin', 31, '-', '-', 'Using file manager'),
(70, 1523720717, 1, 'admin', 76, '-', '-', 'Element management'),
(71, 1523720725, 1, 'admin', 19, '-', 'Новый шаблон', 'Creating a new template'),
(72, 1523720850, 1, 'admin', 20, '-', 'custom_page', 'Saving template'),
(73, 1523720850, 1, 'admin', 76, '-', '-', 'Element management'),
(74, 1523720882, 1, 'admin', 77, '-', 'Новый чанк', 'Creating a new Chunk (HTML Snippet)'),
(75, 1523720895, 1, 'admin', 77, '-', 'Новый чанк', 'Creating a new Chunk (HTML Snippet)'),
(76, 1523720898, 1, 'admin', 76, '-', '-', 'Element management'),
(77, 1523720900, 1, 'admin', 16, '5', 'Home', 'Editing template'),
(78, 1523720911, 1, 'admin', 20, '5', 'Home', 'Saving template'),
(79, 1523720911, 1, 'admin', 16, '5', 'Home', 'Editing template'),
(80, 1523720918, 1, 'admin', 79, '-', 'HEAD', 'Saving Chunk (HTML Snippet)'),
(81, 1523720918, 1, 'admin', 76, '-', '-', 'Element management'),
(82, 1523720953, 1, 'admin', 77, '-', 'Новый чанк', 'Creating a new Chunk (HTML Snippet)'),
(83, 1523720995, 1, 'admin', 20, '5', 'Home', 'Saving template'),
(84, 1523720995, 1, 'admin', 16, '5', 'Home', 'Editing template'),
(85, 1523720999, 1, 'admin', 79, '-', 'HEADER', 'Saving Chunk (HTML Snippet)'),
(86, 1523720999, 1, 'admin', 76, '-', '-', 'Element management'),
(87, 1523721009, 1, 'admin', 77, '-', 'Новый чанк', 'Creating a new Chunk (HTML Snippet)'),
(88, 1523721038, 1, 'admin', 20, '5', 'Home', 'Saving template'),
(89, 1523721038, 1, 'admin', 16, '5', 'Home', 'Editing template'),
(90, 1523721041, 1, 'admin', 79, '-', 'FOOTER', 'Saving Chunk (HTML Snippet)'),
(91, 1523721042, 1, 'admin', 76, '-', '-', 'Element management'),
(92, 1523721045, 1, 'admin', 78, '9', 'HEAD', 'Editing Chunk (HTML Snippet)'),
(93, 1523721048, 1, 'admin', 79, '9', 'HEAD', 'Saving Chunk (HTML Snippet)'),
(94, 1523721048, 1, 'admin', 76, '-', '-', 'Element management'),
(95, 1523721064, 1, 'admin', 76, '-', '-', 'Element management'),
(96, 1523721068, 1, 'admin', 16, '6', 'Contacts', 'Editing template'),
(97, 1523721160, 1, 'admin', 20, '6', 'Contacts', 'Saving template'),
(98, 1523721160, 1, 'admin', 76, '-', '-', 'Element management'),
(99, 1523721166, 1, 'admin', 27, '3', 'Контакты', 'Editing resource'),
(100, 1523721678, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(101, 1523721696, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(102, 1523721700, 1, 'admin', 5, '-', 'Синяя карта', 'Saving resource'),
(103, 1523721701, 1, 'admin', 3, '4', 'Синяя карта', 'Viewing data for resource'),
(104, 1523721741, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(105, 1523721792, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(106, 1523721794, 1, 'admin', 5, '-', 'Помощь в получении документов из органов РАГС (ЗАГС)', 'Saving resource'),
(107, 1523721795, 1, 'admin', 3, '5', 'Помощь в получении документов из органов РАГС (ЗАГС)', 'Viewing data for resource'),
(108, 1523721801, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(109, 1523721815, 1, 'admin', 5, '-', 'Брачный договор, контракт', 'Saving resource'),
(110, 1523721816, 1, 'admin', 3, '6', 'Брачный договор, контракт', 'Viewing data for resource'),
(111, 1523721825, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(112, 1523721838, 1, 'admin', 5, '-', 'Гражданство Украины для граждан Армении', 'Saving resource'),
(113, 1523721840, 1, 'admin', 3, '7', 'Гражданство Украины для граждан Армении', 'Viewing data for resource'),
(114, 1523721842, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(115, 1523721860, 1, 'admin', 5, '-', 'Документы, анкеты, информация', 'Saving resource'),
(116, 1523721862, 1, 'admin', 3, '8', 'Документы, анкеты, информация', 'Viewing data for resource'),
(117, 1523721870, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(118, 1523721883, 1, 'admin', 5, '-', 'Гражданство Украины для граждан Грузии', 'Saving resource'),
(119, 1523721884, 1, 'admin', 3, '9', 'Гражданство Украины для граждан Грузии', 'Viewing data for resource'),
(120, 1523721890, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(121, 1523721897, 1, 'admin', 5, '-', 'О сайте', 'Saving resource'),
(122, 1523721898, 1, 'admin', 3, '10', 'О сайте', 'Viewing data for resource'),
(123, 1523721910, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(124, 1523721926, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(125, 1523721928, 1, 'admin', 5, '-', 'Второе гражданство Евросоюза', 'Saving resource'),
(126, 1523721929, 1, 'admin', 3, '11', 'Второе гражданство Евросоюза', 'Viewing data for resource'),
(127, 1523721936, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(128, 1523721946, 1, 'admin', 5, '-', 'Практический справочник', 'Saving resource'),
(129, 1523721948, 1, 'admin', 3, '12', 'Практический справочник', 'Viewing data for resource'),
(130, 1523721953, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(131, 1523721968, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(132, 1523721969, 1, 'admin', 5, '-', 'Визовая поддержка и консультация', 'Saving resource'),
(133, 1523721970, 1, 'admin', 3, '13', 'Визовая поддержка и консультация', 'Viewing data for resource'),
(134, 1523721979, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(135, 1523721992, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(136, 1523721994, 1, 'admin', 5, '-', 'Легальный вывоз детей за рубеж', 'Saving resource'),
(137, 1523721996, 1, 'admin', 3, '14', 'Легальный вывоз детей за рубеж', 'Viewing data for resource'),
(138, 1523722009, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(139, 1523722037, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(140, 1523722039, 1, 'admin', 5, '-', 'Еврейская иммиграция ФРГ', 'Saving resource'),
(141, 1523722041, 1, 'admin', 3, '15', 'Еврейская иммиграция ФРГ', 'Viewing data for resource'),
(142, 1523722043, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(143, 1523722060, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(144, 1523722064, 1, 'admin', 5, '-', 'Немецкая иммиграция ФРГ', 'Saving resource'),
(145, 1523722065, 1, 'admin', 3, '16', 'Немецкая иммиграция ФРГ', 'Viewing data for resource'),
(146, 1523722289, 1, 'admin', 16, '3', 'Minimal Template', 'Editing template'),
(147, 1523722303, 1, 'admin', 16, '3', 'Minimal Template', 'Editing template'),
(148, 1523722326, 1, 'admin', 76, '-', '-', 'Element management'),
(149, 1523722328, 1, 'admin', 16, '5', 'Home', 'Editing template'),
(150, 1523722349, 1, 'admin', 20, '5', 'Home', 'Saving template'),
(151, 1523722349, 1, 'admin', 16, '5', 'Home', 'Editing template'),
(152, 1523722363, 1, 'admin', 20, '5', 'Home', 'Saving template'),
(153, 1523722363, 1, 'admin', 16, '5', 'Home', 'Editing template'),
(154, 1523722760, 1, 'admin', 20, '5', 'Home', 'Saving template'),
(155, 1523722760, 1, 'admin', 16, '5', 'Home', 'Editing template'),
(156, 1523722801, 1, 'admin', 76, '-', '-', 'Element management'),
(157, 1523722804, 1, 'admin', 78, '11', 'FOOTER', 'Editing Chunk (HTML Snippet)'),
(158, 1523722852, 1, 'admin', 79, '11', 'FOOTER', 'Saving Chunk (HTML Snippet)'),
(159, 1523722852, 1, 'admin', 78, '11', 'FOOTER', 'Editing Chunk (HTML Snippet)'),
(160, 1524317424, 1, 'admin', 76, '-', '-', 'Element management'),
(161, 1524317428, 1, 'admin', 78, '9', 'HEAD', 'Editing Chunk (HTML Snippet)'),
(162, 1524317432, 1, 'admin', 79, '9', 'HEAD', 'Saving Chunk (HTML Snippet)'),
(163, 1524317432, 1, 'admin', 76, '-', '-', 'Element management'),
(164, 1524317444, 1, 'admin', 78, '9', 'HEAD', 'Editing Chunk (HTML Snippet)'),
(165, 1524317485, 1, 'admin', 27, '2', 'Главная', 'Editing resource'),
(166, 1524318021, 1, 'admin', 79, '9', 'HEAD', 'Saving Chunk (HTML Snippet)'),
(167, 1524318021, 1, 'admin', 78, '9', 'HEAD', 'Editing Chunk (HTML Snippet)'),
(168, 1524318200, 1, 'admin', 79, '9', 'HEAD', 'Saving Chunk (HTML Snippet)'),
(169, 1524318200, 1, 'admin', 78, '9', 'HEAD', 'Editing Chunk (HTML Snippet)'),
(170, 1524318360, 1, 'admin', 20, '5', 'Home', 'Saving template'),
(171, 1524318360, 1, 'admin', 16, '5', 'Home', 'Editing template'),
(172, 1524318615, 1, 'admin', 20, '5', 'Home', 'Saving template'),
(173, 1524318615, 1, 'admin', 16, '5', 'Home', 'Editing template'),
(174, 1524318737, 1, 'admin', 20, '5', 'Home', 'Saving template'),
(175, 1524318737, 1, 'admin', 16, '5', 'Home', 'Editing template'),
(176, 1524319186, 1, 'admin', 76, '-', '-', 'Element management'),
(177, 1524319189, 1, 'admin', 78, '10', 'HEADER', 'Editing Chunk (HTML Snippet)'),
(178, 1524319212, 1, 'admin', 20, '5', 'Home', 'Saving template'),
(179, 1524319212, 1, 'admin', 16, '5', 'Home', 'Editing template'),
(180, 1524319222, 1, 'admin', 79, '10', 'HEADER', 'Saving Chunk (HTML Snippet)'),
(181, 1524319222, 1, 'admin', 76, '-', '-', 'Element management'),
(182, 1524319225, 1, 'admin', 78, '10', 'HEADER', 'Editing Chunk (HTML Snippet)'),
(183, 1524319295, 1, 'admin', 79, '10', 'HEADER', 'Saving Chunk (HTML Snippet)'),
(184, 1524319295, 1, 'admin', 76, '-', '-', 'Element management'),
(185, 1524319296, 1, 'admin', 78, '10', 'HEADER', 'Editing Chunk (HTML Snippet)'),
(186, 1524319333, 1, 'admin', 79, '10', 'HEADER', 'Saving Chunk (HTML Snippet)'),
(187, 1524319333, 1, 'admin', 76, '-', '-', 'Element management'),
(188, 1524319338, 1, 'admin', 78, '10', 'HEADER', 'Editing Chunk (HTML Snippet)'),
(189, 1524319400, 1, 'admin', 79, '10', 'HEADER', 'Saving Chunk (HTML Snippet)'),
(190, 1524319400, 1, 'admin', 76, '-', '-', 'Element management'),
(191, 1524319414, 1, 'admin', 78, '10', 'HEADER', 'Editing Chunk (HTML Snippet)'),
(192, 1524319422, 1, 'admin', 79, '10', 'HEADER', 'Saving Chunk (HTML Snippet)'),
(193, 1524319422, 1, 'admin', 76, '-', '-', 'Element management'),
(194, 1524319423, 1, 'admin', 78, '10', 'HEADER', 'Editing Chunk (HTML Snippet)'),
(195, 1524319443, 1, 'admin', 79, '10', 'HEADER', 'Saving Chunk (HTML Snippet)'),
(196, 1524319443, 1, 'admin', 76, '-', '-', 'Element management'),
(197, 1524320915, 1, 'admin', 5, '2', 'Главная', 'Saving resource'),
(198, 1524320915, 1, 'admin', 27, '2', 'Главная', 'Editing resource'),
(199, 1524321008, 1, 'admin', 20, '5', 'Home', 'Saving template'),
(200, 1524321008, 1, 'admin', 16, '5', 'Home', 'Editing template'),
(201, 1524321034, 1, 'admin', 20, '5', 'Home', 'Saving template'),
(202, 1524321034, 1, 'admin', 16, '5', 'Home', 'Editing template'),
(203, 1524321589, 1, 'admin', 78, '10', 'HEADER', 'Editing Chunk (HTML Snippet)'),
(204, 1524321590, 1, 'admin', 76, '-', '-', 'Element management'),
(205, 1524321592, 1, 'admin', 78, '9', 'HEAD', 'Editing Chunk (HTML Snippet)'),
(206, 1524321607, 1, 'admin', 76, '-', '-', 'Element management'),
(207, 1524321612, 1, 'admin', 16, '5', 'Home', 'Editing template'),
(208, 1524321645, 1, 'admin', 20, '5', 'Home', 'Saving template'),
(209, 1524321645, 1, 'admin', 76, '-', '-', 'Element management'),
(210, 1524321849, 1, 'admin', 16, '5', 'Home', 'Editing template'),
(211, 1524321867, 1, 'admin', 20, '5', 'Home', 'Saving template'),
(212, 1524321867, 1, 'admin', 16, '5', 'Home', 'Editing template'),
(213, 1524322065, 1, 'admin', 20, '5', 'Home', 'Saving template'),
(214, 1524322065, 1, 'admin', 16, '5', 'Home', 'Editing template'),
(215, 1524322225, 1, 'admin', 20, '5', 'Home', 'Saving template'),
(216, 1524322225, 1, 'admin', 16, '5', 'Home', 'Editing template'),
(217, 1524324640, 1, 'admin', 20, '5', 'Home', 'Saving template'),
(218, 1524324640, 1, 'admin', 16, '5', 'Home', 'Editing template'),
(219, 1524325338, 1, 'admin', 76, '-', '-', 'Element management'),
(220, 1524325483, 1, 'admin', 78, '11', 'FOOTER', 'Editing Chunk (HTML Snippet)'),
(221, 1524325960, 1, 'admin', 79, '11', 'FOOTER', 'Saving Chunk (HTML Snippet)'),
(222, 1524325960, 1, 'admin', 76, '-', '-', 'Element management'),
(223, 1524325962, 1, 'admin', 78, '11', 'FOOTER', 'Editing Chunk (HTML Snippet)'),
(224, 1524325967, 1, 'admin', 79, '11', 'FOOTER', 'Saving Chunk (HTML Snippet)'),
(225, 1524325967, 1, 'admin', 78, '11', 'FOOTER', 'Editing Chunk (HTML Snippet)'),
(226, 1524326178, 1, 'admin', 76, '-', '-', 'Element management'),
(227, 1524326184, 1, 'admin', 78, '9', 'HEAD', 'Editing Chunk (HTML Snippet)'),
(228, 1524326201, 1, 'admin', 79, '9', 'HEAD', 'Saving Chunk (HTML Snippet)'),
(229, 1524326201, 1, 'admin', 76, '-', '-', 'Element management'),
(230, 1524326246, 1, 'admin', 78, '9', 'HEAD', 'Editing Chunk (HTML Snippet)'),
(231, 1524326260, 1, 'admin', 79, '9', 'HEAD', 'Saving Chunk (HTML Snippet)'),
(232, 1524326260, 1, 'admin', 76, '-', '-', 'Element management'),
(233, 1524326301, 1, 'admin', 79, '11', 'FOOTER', 'Saving Chunk (HTML Snippet)'),
(234, 1524326301, 1, 'admin', 78, '11', 'FOOTER', 'Editing Chunk (HTML Snippet)'),
(235, 1524326993, 1, 'admin', 78, '11', 'FOOTER', 'Editing Chunk (HTML Snippet)'),
(236, 1524327004, 1, 'admin', 79, '11', 'FOOTER', 'Saving Chunk (HTML Snippet)'),
(237, 1524327004, 1, 'admin', 76, '-', '-', 'Element management'),
(238, 1524327038, 1, 'admin', 76, '-', '-', 'Element management'),
(239, 1524327040, 1, 'admin', 78, '11', 'FOOTER', 'Editing Chunk (HTML Snippet)'),
(240, 1524327490, 1, 'admin', 79, '11', 'FOOTER', 'Saving Chunk (HTML Snippet)'),
(241, 1524327490, 1, 'admin', 78, '11', 'FOOTER', 'Editing Chunk (HTML Snippet)'),
(242, 1524327886, 1, 'admin', 79, '11', 'FOOTER', 'Saving Chunk (HTML Snippet)'),
(243, 1524327886, 1, 'admin', 78, '11', 'FOOTER', 'Editing Chunk (HTML Snippet)'),
(244, 1524329243, 1, 'admin', 79, '11', 'FOOTER', 'Saving Chunk (HTML Snippet)'),
(245, 1524329243, 1, 'admin', 78, '11', 'FOOTER', 'Editing Chunk (HTML Snippet)'),
(246, 1524329570, 1, 'admin', 79, '11', 'FOOTER', 'Saving Chunk (HTML Snippet)'),
(247, 1524329570, 1, 'admin', 78, '11', 'FOOTER', 'Editing Chunk (HTML Snippet)'),
(248, 1524329633, 1, 'admin', 76, '-', '-', 'Element management'),
(249, 1524329634, 1, 'admin', 78, '11', 'FOOTER', 'Editing Chunk (HTML Snippet)'),
(250, 1524329964, 1, 'admin', 79, '11', 'FOOTER', 'Saving Chunk (HTML Snippet)'),
(251, 1524329964, 1, 'admin', 76, '-', '-', 'Element management'),
(252, 1524330164, 1, 'admin', 78, '11', 'FOOTER', 'Editing Chunk (HTML Snippet)'),
(253, 1524330171, 1, 'admin', 79, '11', 'FOOTER', 'Saving Chunk (HTML Snippet)'),
(254, 1524330171, 1, 'admin', 78, '11', 'FOOTER', 'Editing Chunk (HTML Snippet)'),
(255, 1524932908, 1, 'admin', 79, '11', 'FOOTER', 'Saving Chunk (HTML Snippet)'),
(256, 1524932908, 1, 'admin', 78, '11', 'FOOTER', 'Editing Chunk (HTML Snippet)'),
(257, 1524933444, 1, 'admin', 76, '-', '-', 'Element management'),
(258, 1524933452, 1, 'admin', 78, '9', 'HEAD', 'Editing Chunk (HTML Snippet)'),
(259, 1524934073, 1, 'admin', 79, '9', 'HEAD', 'Saving Chunk (HTML Snippet)'),
(260, 1524934073, 1, 'admin', 76, '-', '-', 'Element management'),
(261, 1524934077, 1, 'admin', 78, '9', 'HEAD', 'Editing Chunk (HTML Snippet)'),
(262, 1524934112, 1, 'admin', 79, '9', 'HEAD', 'Saving Chunk (HTML Snippet)'),
(263, 1524934113, 1, 'admin', 78, '9', 'HEAD', 'Editing Chunk (HTML Snippet)'),
(264, 1524934177, 1, 'admin', 79, '9', 'HEAD', 'Saving Chunk (HTML Snippet)'),
(265, 1524934177, 1, 'admin', 78, '9', 'HEAD', 'Editing Chunk (HTML Snippet)'),
(266, 1524934195, 1, 'admin', 79, '9', 'HEAD', 'Saving Chunk (HTML Snippet)'),
(267, 1524934195, 1, 'admin', 78, '9', 'HEAD', 'Editing Chunk (HTML Snippet)'),
(268, 1524934855, 1, 'admin', 76, '-', '-', 'Element management'),
(269, 1524934875, 1, 'admin', 101, '-', 'Новый плагин', 'Create new plugin'),
(270, 1525112675, 1, 'admin', 103, '-', 'Common', 'Saving plugin'),
(271, 1525112675, 1, 'admin', 102, '10', 'Common', 'Edit plugin'),
(272, 1525113241, 1, 'admin', 103, '10', 'Common', 'Saving plugin'),
(273, 1525113241, 1, 'admin', 102, '10', 'Common', 'Edit plugin'),
(274, 1525113262, 1, 'admin', 103, '10', 'Common', 'Saving plugin'),
(275, 1525113262, 1, 'admin', 102, '10', 'Common', 'Edit plugin'),
(276, 1525113332, 1, 'admin', 103, '10', 'Common', 'Saving plugin'),
(277, 1525113332, 1, 'admin', 102, '10', 'Common', 'Edit plugin'),
(278, 1525113572, 1, 'admin', 76, '-', '-', 'Element management'),
(279, 1525113577, 1, 'admin', 78, '11', 'FOOTER', 'Editing Chunk (HTML Snippet)'),
(280, 1525113635, 1, 'admin', 79, '11', 'FOOTER', 'Saving Chunk (HTML Snippet)'),
(281, 1525113635, 1, 'admin', 78, '11', 'FOOTER', 'Editing Chunk (HTML Snippet)'),
(282, 1525113756, 1, 'admin', 76, '-', '-', 'Element management'),
(283, 1525113760, 1, 'admin', 16, '5', 'Home', 'Editing template'),
(284, 1525113786, 1, 'admin', 20, '5', 'Home', 'Saving template'),
(285, 1525113786, 1, 'admin', 76, '-', '-', 'Element management'),
(286, 1525113788, 1, 'admin', 16, '6', 'Contacts', 'Editing template'),
(287, 1525113824, 1, 'admin', 20, '6', 'Contacts', 'Saving template'),
(288, 1525113824, 1, 'admin', 76, '-', '-', 'Element management'),
(289, 1525113825, 1, 'admin', 16, '7', 'custom_page', 'Editing template'),
(290, 1525113846, 1, 'admin', 20, '7', 'custom_page', 'Saving template'),
(291, 1525113846, 1, 'admin', 76, '-', '-', 'Element management'),
(292, 1525114025, 1, 'admin', 78, '9', 'HEAD', 'Editing Chunk (HTML Snippet)'),
(293, 1525114034, 1, 'admin', 79, '9', 'HEAD', 'Saving Chunk (HTML Snippet)'),
(294, 1525114034, 1, 'admin', 76, '-', '-', 'Element management'),
(295, 1525114036, 1, 'admin', 78, '11', 'FOOTER', 'Editing Chunk (HTML Snippet)'),
(296, 1525114041, 1, 'admin', 79, '11', 'FOOTER', 'Saving Chunk (HTML Snippet)'),
(297, 1525114041, 1, 'admin', 76, '-', '-', 'Element management'),
(298, 1525114568, 1, 'admin', 103, '10', 'Common', 'Saving plugin'),
(299, 1525114568, 1, 'admin', 102, '10', 'Common', 'Edit plugin'),
(300, 1525114831, 1, 'admin', 78, '11', 'FOOTER', 'Editing Chunk (HTML Snippet)'),
(301, 1525114843, 1, 'admin', 79, '11', 'FOOTER', 'Saving Chunk (HTML Snippet)'),
(302, 1525114843, 1, 'admin', 78, '11', 'FOOTER', 'Editing Chunk (HTML Snippet)'),
(303, 1525115876, 1, 'admin', 17, '-', '-', 'Editing settings'),
(304, 1525116237, 1, 'admin', 103, '10', 'Common', 'Saving plugin'),
(305, 1525116237, 1, 'admin', 102, '10', 'Common', 'Edit plugin'),
(306, 1525116313, 1, 'admin', 103, '10', 'Common', 'Saving plugin'),
(307, 1525116313, 1, 'admin', 102, '10', 'Common', 'Edit plugin'),
(308, 1525116368, 1, 'admin', 103, '10', 'Common', 'Saving plugin'),
(309, 1525116368, 1, 'admin', 102, '10', 'Common', 'Edit plugin'),
(310, 1525116594, 1, 'admin', 103, '10', 'Common', 'Saving plugin'),
(311, 1525116594, 1, 'admin', 102, '10', 'Common', 'Edit plugin'),
(312, 1525116706, 1, 'admin', 103, '10', 'Common', 'Saving plugin'),
(313, 1525116706, 1, 'admin', 102, '10', 'Common', 'Edit plugin'),
(314, 1525116837, 1, 'admin', 76, '-', '-', 'Element management'),
(315, 1525116842, 1, 'admin', 78, '11', 'FOOTER', 'Editing Chunk (HTML Snippet)'),
(316, 1525116853, 1, 'admin', 79, '11', 'FOOTER', 'Saving Chunk (HTML Snippet)'),
(317, 1525116853, 1, 'admin', 76, '-', '-', 'Element management'),
(318, 1525116856, 1, 'admin', 77, '-', 'Новый чанк', 'Creating a new Chunk (HTML Snippet)'),
(319, 1525116938, 1, 'admin', 79, '-', 'callback_message', 'Saving Chunk (HTML Snippet)'),
(320, 1525116938, 1, 'admin', 78, '12', 'callback_message', 'Editing Chunk (HTML Snippet)'),
(321, 1525117030, 1, 'admin', 78, '12', 'callback_message', 'Editing Chunk (HTML Snippet)'),
(322, 1525117033, 1, 'admin', 76, '-', '-', 'Element management'),
(323, 1525117034, 1, 'admin', 78, '11', 'FOOTER', 'Editing Chunk (HTML Snippet)'),
(324, 1525117118, 1, 'admin', 79, '11', 'FOOTER', 'Saving Chunk (HTML Snippet)'),
(325, 1525117118, 1, 'admin', 76, '-', '-', 'Element management'),
(326, 1525181545, 1, 'admin', 103, '10', 'Common', 'Saving plugin'),
(327, 1525181546, 1, 'admin', 102, '10', 'Common', 'Edit plugin'),
(328, 1525181554, 1, 'admin', 78, '10', 'HEADER', 'Editing Chunk (HTML Snippet)'),
(329, 1525182030, 1, 'admin', 27, '4', 'Синяя карта', 'Editing resource'),
(330, 1525182051, 1, 'admin', 5, '4', 'Новости', 'Saving resource'),
(331, 1525182052, 1, 'admin', 3, '4', 'Новости', 'Viewing data for resource'),
(332, 1525182053, 1, 'admin', 27, '4', 'Новости', 'Editing resource'),
(333, 1525182061, 1, 'admin', 5, '4', 'Новости', 'Saving resource'),
(334, 1525182062, 1, 'admin', 3, '4', 'Новости', 'Viewing data for resource'),
(335, 1525182064, 1, 'admin', 27, '5', 'Помощь в получении документов из органов РАГС (ЗАГС)', 'Editing resource'),
(336, 1525182096, 1, 'admin', 5, '5', 'Получение статуса беженца за рубежом', 'Saving resource'),
(337, 1525182097, 1, 'admin', 3, '5', 'Получение статуса беженца за рубежом', 'Viewing data for resource'),
(338, 1525182099, 1, 'admin', 27, '5', 'Получение статуса беженца за рубежом', 'Editing resource'),
(339, 1525182163, 1, 'admin', 5, '5', 'Получение статуса беженца за рубежом', 'Saving resource'),
(340, 1525182165, 1, 'admin', 3, '5', 'Получение статуса беженца за рубежом', 'Viewing data for resource'),
(341, 1525182176, 1, 'admin', 27, '6', 'Брачный договор, контракт', 'Editing resource'),
(342, 1525182325, 1, 'admin', 5, '6', 'Гражданство Украины и ПМЖ в Украине', 'Saving resource'),
(343, 1525182326, 1, 'admin', 3, '6', 'Гражданство Украины и ПМЖ в Украине', 'Viewing data for resource'),
(344, 1525182328, 1, 'admin', 27, '7', 'Гражданство Украины для граждан Армении', 'Editing resource'),
(345, 1525182342, 1, 'admin', 27, '13', 'Визовая поддержка и консультация', 'Editing resource'),
(346, 1525182403, 1, 'admin', 5, '13', 'Визовая поддержка для государств ЕС, США, Канады', 'Saving resource'),
(347, 1525182404, 1, 'admin', 3, '13', 'Визовая поддержка для государств ЕС, США, Канады', 'Viewing data for resource'),
(348, 1525182424, 1, 'admin', 27, '14', 'Легальный вывоз детей за рубеж', 'Editing resource'),
(349, 1525182468, 1, 'admin', 5, '14', 'Выезд несовершеннолетних детей за рубеж', 'Saving resource'),
(350, 1525182470, 1, 'admin', 3, '14', 'Выезд несовершеннолетних детей за рубеж', 'Viewing data for resource'),
(351, 1525182487, 1, 'admin', 27, '16', 'Немецкая иммиграция ФРГ', 'Editing resource'),
(352, 1525182533, 1, 'admin', 5, '16', 'Иммиграция в Германию ', 'Saving resource'),
(353, 1525182534, 1, 'admin', 3, '16', 'Иммиграция в Германию ', 'Viewing data for resource'),
(354, 1525182540, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(355, 1525182623, 1, 'admin', 5, '-', 'Еврейская иммиграция ', 'Saving resource'),
(356, 1525182624, 1, 'admin', 3, '17', 'Еврейская иммиграция ', 'Viewing data for resource'),
(357, 1525182641, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(358, 1525182697, 1, 'admin', 5, '-', 'Немецкая иммиграция', 'Saving resource'),
(359, 1525182698, 1, 'admin', 3, '18', 'Немецкая иммиграция', 'Viewing data for resource'),
(360, 1525182714, 1, 'admin', 27, '15', 'Еврейская иммиграция ФРГ', 'Editing resource'),
(361, 1525182754, 1, 'admin', 5, '15', 'Брачный договор/контракт', 'Saving resource'),
(362, 1525182755, 1, 'admin', 3, '15', 'Брачный договор/контракт', 'Viewing data for resource'),
(363, 1525182766, 1, 'admin', 27, '12', 'Практический справочник', 'Editing resource'),
(364, 1525182803, 1, 'admin', 27, '12', 'Практический справочник', 'Editing resource'),
(365, 1525182864, 1, 'admin', 5, '12', 'Консультации', 'Saving resource'),
(366, 1525182865, 1, 'admin', 3, '12', 'Консультации', 'Viewing data for resource'),
(367, 1525182880, 1, 'admin', 27, '11', 'Второе гражданство Евросоюза', 'Editing resource'),
(368, 1525183057, 1, 'admin', 5, '11', 'Иммиграция в Великобританию', 'Saving resource'),
(369, 1525183059, 1, 'admin', 3, '11', 'Иммиграция в Великобританию', 'Viewing data for resource'),
(370, 1525183080, 1, 'admin', 27, '9', 'Гражданство Украины для граждан Грузии', 'Editing resource'),
(371, 1525183112, 1, 'admin', 27, '9', 'Гражданство Украины для граждан Грузии', 'Editing resource'),
(372, 1525183116, 1, 'admin', 5, '9', 'Помощь в получение документов из органов РАГС и архивов', 'Saving resource'),
(373, 1525183117, 1, 'admin', 3, '9', 'Помощь в получение документов из органов РАГС и архивов', 'Viewing data for resource'),
(374, 1525183132, 1, 'admin', 27, '8', 'Документы, анкеты, информация', 'Editing resource'),
(375, 1525183158, 1, 'admin', 27, '8', 'Документы, анкеты, информация', 'Editing resource'),
(376, 1525183162, 1, 'admin', 5, '8', 'Документы и анкеты', 'Saving resource'),
(377, 1525183163, 1, 'admin', 3, '8', 'Документы и анкеты', 'Viewing data for resource'),
(378, 1525183175, 1, 'admin', 6, '10', 'О сайте', 'Deleting resource'),
(379, 1525183176, 1, 'admin', 3, '10', 'О сайте', 'Viewing data for resource'),
(380, 1525183182, 1, 'admin', 6, '7', 'Гражданство Украины для граждан Армении', 'Deleting resource'),
(381, 1525183183, 1, 'admin', 3, '7', 'Гражданство Украины для граждан Армении', 'Viewing data for resource'),
(382, 1525183191, 1, 'admin', 27, '7', 'Гражданство Украины для граждан Армении', 'Editing resource'),
(383, 1525183197, 1, 'admin', 6, '7', 'Гражданство Украины для граждан Армении', 'Deleting resource'),
(384, 1525183199, 1, 'admin', 3, '7', 'Гражданство Украины для граждан Армении', 'Viewing data for resource'),
(385, 1525183207, 1, 'admin', 64, '-', '-', 'Removing deleted content'),
(386, 1525183404, 1, 'admin', 76, '-', '-', 'Element management'),
(387, 1525183407, 1, 'admin', 78, '10', 'HEADER', 'Editing Chunk (HTML Snippet)'),
(388, 1525183428, 1, 'admin', 79, '10', 'HEADER', 'Saving Chunk (HTML Snippet)'),
(389, 1525183428, 1, 'admin', 78, '10', 'HEADER', 'Editing Chunk (HTML Snippet)'),
(390, 1525183883, 1, 'admin', 76, '-', '-', 'Element management'),
(391, 1525183894, 1, 'admin', 16, '5', 'Home', 'Editing template'),
(392, 1525183952, 1, 'admin', 20, '5', 'Home', 'Saving template'),
(393, 1525183952, 1, 'admin', 16, '5', 'Home', 'Editing template'),
(394, 1525184000, 1, 'admin', 20, '5', 'Home', 'Saving template'),
(395, 1525184000, 1, 'admin', 16, '5', 'Home', 'Editing template'),
(396, 1525184032, 1, 'admin', 76, '-', '-', 'Element management'),
(397, 1525184038, 1, 'admin', 77, '-', 'Новый чанк', 'Creating a new Chunk (HTML Snippet)'),
(398, 1525184092, 1, 'admin', 79, '-', 'tpl_latest_news', 'Saving Chunk (HTML Snippet)'),
(399, 1525184092, 1, 'admin', 78, '13', 'tpl_latest_news', 'Editing Chunk (HTML Snippet)'),
(400, 1525184125, 1, 'admin', 20, '5', 'Home', 'Saving template'),
(401, 1525184126, 1, 'admin', 16, '5', 'Home', 'Editing template'),
(402, 1525184129, 1, 'admin', 79, '13', 'tpl_latest_news', 'Saving Chunk (HTML Snippet)'),
(403, 1525184129, 1, 'admin', 78, '13', 'tpl_latest_news', 'Editing Chunk (HTML Snippet)'),
(404, 1525184132, 1, 'admin', 76, '-', '-', 'Element management'),
(405, 1525184134, 1, 'admin', 77, '-', 'Новый чанк', 'Creating a new Chunk (HTML Snippet)'),
(406, 1525184313, 1, 'admin', 79, '-', 'tpl_menu', 'Saving Chunk (HTML Snippet)'),
(407, 1525184313, 1, 'admin', 78, '14', 'tpl_menu', 'Editing Chunk (HTML Snippet)'),
(408, 1525184335, 1, 'admin', 20, '5', 'Home', 'Saving template'),
(409, 1525184335, 1, 'admin', 16, '5', 'Home', 'Editing template'),
(410, 1525185166, 1, 'admin', 76, '-', '-', 'Element management'),
(411, 1525185491, 1, 'admin', 76, '-', '-', 'Element management'),
(412, 1525185503, 1, 'admin', 77, '-', 'Новый чанк', 'Creating a new Chunk (HTML Snippet)'),
(413, 1525185574, 1, 'admin', 79, '-', 'tpl_outer', 'Saving Chunk (HTML Snippet)'),
(414, 1525185574, 1, 'admin', 76, '-', '-', 'Element management'),
(415, 1525185584, 1, 'admin', 77, '-', 'Новый чанк', 'Creating a new Chunk (HTML Snippet)'),
(416, 1525185660, 1, 'admin', 79, '-', 'tpl_row', 'Saving Chunk (HTML Snippet)'),
(417, 1525185660, 1, 'admin', 76, '-', '-', 'Element management'),
(418, 1525185686, 1, 'admin', 78, '16', 'tpl_row', 'Editing Chunk (HTML Snippet)'),
(419, 1525185691, 1, 'admin', 79, '16', 'tpl_row', 'Saving Chunk (HTML Snippet)'),
(420, 1525185691, 1, 'admin', 76, '-', '-', 'Element management'),
(421, 1525185731, 1, 'admin', 77, '-', 'Новый чанк', 'Creating a new Chunk (HTML Snippet)'),
(422, 1525185867, 1, 'admin', 79, '-', 'tpl_row_active', 'Saving Chunk (HTML Snippet)'),
(423, 1525185867, 1, 'admin', 76, '-', '-', 'Element management'),
(424, 1525185870, 1, 'admin', 77, '-', 'Новый чанк', 'Creating a new Chunk (HTML Snippet)'),
(425, 1525185920, 1, 'admin', 79, '-', 'tpl_inner', 'Saving Chunk (HTML Snippet)'),
(426, 1525185920, 1, 'admin', 76, '-', '-', 'Element management'),
(427, 1525185924, 1, 'admin', 78, '14', 'tpl_menu', 'Editing Chunk (HTML Snippet)'),
(428, 1525185930, 1, 'admin', 79, '14', 'tpl_menu', 'Saving Chunk (HTML Snippet)'),
(429, 1525185930, 1, 'admin', 76, '-', '-', 'Element management'),
(430, 1525185940, 1, 'admin', 78, '17', 'tpl_row_active', 'Editing Chunk (HTML Snippet)'),
(431, 1525185946, 1, 'admin', 79, '17', 'tpl_row_active', 'Saving Chunk (HTML Snippet)'),
(432, 1525185946, 1, 'admin', 76, '-', '-', 'Element management'),
(433, 1525185988, 1, 'admin', 77, '-', 'Новый чанк', 'Creating a new Chunk (HTML Snippet)'),
(434, 1525186093, 1, 'admin', 79, '14', 'tpl_menu', 'Saving Chunk (HTML Snippet)'),
(435, 1525186093, 1, 'admin', 78, '14', 'tpl_menu', 'Editing Chunk (HTML Snippet)'),
(436, 1525186131, 1, 'admin', 79, '-', 'tpl_inner_row', 'Saving Chunk (HTML Snippet)'),
(437, 1525186131, 1, 'admin', 76, '-', '-', 'Element management'),
(438, 1525186146, 1, 'admin', 78, '16', 'tpl_row', 'Editing Chunk (HTML Snippet)'),
(439, 1525186149, 1, 'admin', 76, '-', '-', 'Element management'),
(440, 1525186151, 1, 'admin', 78, '15', 'tpl_outer', 'Editing Chunk (HTML Snippet)'),
(441, 1525186153, 1, 'admin', 76, '-', '-', 'Element management'),
(442, 1525186154, 1, 'admin', 78, '19', 'tpl_inner_row', 'Editing Chunk (HTML Snippet)'),
(443, 1525186156, 1, 'admin', 76, '-', '-', 'Element management'),
(444, 1525186157, 1, 'admin', 78, '18', 'tpl_inner', 'Editing Chunk (HTML Snippet)'),
(445, 1525186161, 1, 'admin', 76, '-', '-', 'Element management'),
(446, 1525186162, 1, 'admin', 78, '17', 'tpl_row_active', 'Editing Chunk (HTML Snippet)'),
(447, 1525186165, 1, 'admin', 76, '-', '-', 'Element management'),
(448, 1525186176, 1, 'admin', 79, '14', 'tpl_menu', 'Saving Chunk (HTML Snippet)'),
(449, 1525186176, 1, 'admin', 78, '14', 'tpl_menu', 'Editing Chunk (HTML Snippet)'),
(450, 1525186192, 1, 'admin', 78, '17', 'tpl_row_active', 'Editing Chunk (HTML Snippet)'),
(451, 1525186194, 1, 'admin', 76, '-', '-', 'Element management'),
(452, 1525186197, 1, 'admin', 78, '16', 'tpl_row', 'Editing Chunk (HTML Snippet)'),
(453, 1525186204, 1, 'admin', 76, '-', '-', 'Element management'),
(454, 1525186207, 1, 'admin', 78, '19', 'tpl_inner_row', 'Editing Chunk (HTML Snippet)'),
(455, 1525186226, 1, 'admin', 76, '-', '-', 'Element management'),
(456, 1525186228, 1, 'admin', 78, '15', 'tpl_outer', 'Editing Chunk (HTML Snippet)'),
(457, 1525186231, 1, 'admin', 76, '-', '-', 'Element management'),
(458, 1525186233, 1, 'admin', 78, '16', 'tpl_row', 'Editing Chunk (HTML Snippet)'),
(459, 1525186242, 1, 'admin', 76, '-', '-', 'Element management'),
(460, 1525186246, 1, 'admin', 78, '15', 'tpl_outer', 'Editing Chunk (HTML Snippet)'),
(461, 1525186251, 1, 'admin', 76, '-', '-', 'Element management'),
(462, 1525186252, 1, 'admin', 78, '18', 'tpl_inner', 'Editing Chunk (HTML Snippet)'),
(463, 1525186259, 1, 'admin', 79, '18', 'tpl_inner', 'Saving Chunk (HTML Snippet)'),
(464, 1525186259, 1, 'admin', 76, '-', '-', 'Element management'),
(465, 1525187450, 1, 'admin', 76, '-', '-', 'Element management'),
(466, 1525187457, 1, 'admin', 16, '7', 'custom_page', 'Editing template'),
(467, 1525187467, 1, 'admin', 16, '5', 'Home', 'Editing template'),
(468, 1525187542, 1, 'admin', 20, '7', 'custom_page', 'Saving template'),
(469, 1525187542, 1, 'admin', 16, '7', 'custom_page', 'Editing template'),
(470, 1525187573, 1, 'admin', 20, '7', 'custom_page', 'Saving template'),
(471, 1525187573, 1, 'admin', 16, '7', 'custom_page', 'Editing template'),
(472, 1525188067, 1, 'admin', 16, '5', 'Home', 'Editing template'),
(473, 1525188139, 1, 'admin', 20, '7', 'custom_page', 'Saving template'),
(474, 1525188139, 1, 'admin', 16, '7', 'custom_page', 'Editing template'),
(475, 1525188164, 1, 'admin', 76, '-', '-', 'Element management'),
(476, 1525188166, 1, 'admin', 16, '6', 'Contacts', 'Editing template'),
(477, 1525188203, 1, 'admin', 20, '6', 'Contacts', 'Saving template'),
(478, 1525188203, 1, 'admin', 76, '-', '-', 'Element management'),
(479, 1525188277, 1, 'admin', 16, '6', 'Contacts', 'Editing template'),
(480, 1525188285, 1, 'admin', 20, '6', 'Contacts', 'Saving template'),
(481, 1525188286, 1, 'admin', 16, '6', 'Contacts', 'Editing template'),
(482, 1525188505, 1, 'admin', 20, '6', 'Contacts', 'Saving template'),
(483, 1525188505, 1, 'admin', 16, '6', 'Contacts', 'Editing template'),
(484, 1525188575, 1, 'admin', 20, '6', 'Contacts', 'Saving template'),
(485, 1525188575, 1, 'admin', 16, '6', 'Contacts', 'Editing template'),
(486, 1525188675, 1, 'admin', 20, '6', 'Contacts', 'Saving template'),
(487, 1525188675, 1, 'admin', 16, '6', 'Contacts', 'Editing template'),
(488, 1525188767, 1, 'admin', 20, '6', 'Contacts', 'Saving template'),
(489, 1525188767, 1, 'admin', 16, '6', 'Contacts', 'Editing template'),
(490, 1525188801, 1, 'admin', 20, '6', 'Contacts', 'Saving template'),
(491, 1525188802, 1, 'admin', 16, '6', 'Contacts', 'Editing template'),
(492, 1525526152, 1, 'admin', 76, '-', '-', 'Element management'),
(493, 1525526157, 1, 'admin', 102, '10', 'Common', 'Edit plugin'),
(494, 1525526320, 1, 'admin', 76, '-', '-', 'Element management'),
(495, 1525526342, 1, 'admin', 22, '5', 'eForm', 'Editing Snippet'),
(496, 1525526347, 1, 'admin', 76, '-', '-', 'Element management'),
(497, 1525526371, 1, 'admin', 22, '1', 'AjaxSearch', 'Editing Snippet'),
(498, 1525526473, 1, 'admin', 103, '10', 'Common', 'Saving plugin'),
(499, 1525526473, 1, 'admin', 102, '10', 'Common', 'Edit plugin'),
(500, 1525526516, 1, 'admin', 103, '10', 'Common', 'Saving plugin'),
(501, 1525526517, 1, 'admin', 102, '10', 'Common', 'Edit plugin'),
(502, 1525527862, 1, 'admin', 103, '10', 'Common', 'Saving plugin'),
(503, 1525527862, 1, 'admin', 102, '10', 'Common', 'Edit plugin'),
(504, 1525528050, 1, 'admin', 76, '-', '-', 'Element management'),
(505, 1525528121, 1, 'admin', 76, '-', '-', 'Element management'),
(506, 1525528124, 1, 'admin', 102, '10', 'Common', 'Edit plugin'),
(507, 1525528136, 1, 'admin', 103, '10', 'Common', 'Saving plugin'),
(508, 1525528137, 1, 'admin', 76, '-', '-', 'Element management'),
(509, 1525528178, 1, 'admin', 102, '10', 'Common', 'Edit plugin'),
(510, 1525528299, 1, 'admin', 103, '10', 'Common', 'Saving plugin'),
(511, 1525528299, 1, 'admin', 102, '10', 'Common', 'Edit plugin'),
(512, 1525528336, 1, 'admin', 103, '10', 'Common', 'Saving plugin'),
(513, 1525528336, 1, 'admin', 102, '10', 'Common', 'Edit plugin'),
(514, 1525528467, 1, 'admin', 103, '10', 'Common', 'Saving plugin'),
(515, 1525528467, 1, 'admin', 102, '10', 'Common', 'Edit plugin'),
(516, 1525528563, 1, 'admin', 103, '10', 'Common', 'Saving plugin'),
(517, 1525528563, 1, 'admin', 102, '10', 'Common', 'Edit plugin'),
(518, 1525529139, 1, 'admin', 103, '10', 'Common', 'Saving plugin'),
(519, 1525529139, 1, 'admin', 102, '10', 'Common', 'Edit plugin'),
(520, 1525533209, 1, 'admin', 103, '10', 'Common', 'Saving plugin'),
(521, 1525533209, 1, 'admin', 102, '10', 'Common', 'Edit plugin'),
(522, 1525533335, 1, 'admin', 103, '10', 'Common', 'Saving plugin'),
(523, 1525533335, 1, 'admin', 102, '10', 'Common', 'Edit plugin'),
(524, 1526320339, 1, 'admin', 76, '-', '-', 'Element management'),
(525, 1526320352, 1, 'admin', 27, '13', 'Визовая поддержка для государств ЕС, США, Канады', 'Editing resource'),
(526, 1526320356, 1, 'admin', 27, '14', 'Выезд несовершеннолетних детей за рубеж', 'Editing resource'),
(527, 1526320389, 1, 'admin', 76, '-', '-', 'Element management'),
(528, 1526320393, 1, 'admin', 16, '7', 'custom_page', 'Editing template'),
(529, 1526320432, 1, 'admin', 76, '-', '-', 'Element management'),
(530, 1526320436, 1, 'admin', 27, '8', 'Документы и анкеты', 'Editing resource'),
(531, 1526320461, 1, 'admin', 20, '7', 'custom_page', 'Saving template'),
(532, 1526320461, 1, 'admin', 16, '7', 'custom_page', 'Editing template'),
(533, 1526320477, 1, 'admin', 20, '7', 'custom_page', 'Saving template'),
(534, 1526320478, 1, 'admin', 16, '7', 'custom_page', 'Editing template'),
(535, 1527016566, 1, 'admin', 16, '7', 'custom_page', 'Editing template'),
(536, 1527016594, 1, 'admin', 27, '8', 'Документы и анкеты', 'Editing resource'),
(537, 1527016595, 1, 'admin', 102, '10', 'Common', 'Edit plugin'),
(538, 1527017204, 1, 'admin', 20, '7', 'custom_page', 'Saving template'),
(539, 1527017204, 1, 'admin', 16, '7', 'custom_page', 'Editing template'),
(540, 1527017225, 1, 'admin', 20, '7', 'custom_page', 'Saving template'),
(541, 1527017225, 1, 'admin', 16, '7', 'custom_page', 'Editing template'),
(542, 1527017274, 1, 'admin', 20, '7', 'custom_page', 'Saving template'),
(543, 1527017274, 1, 'admin', 16, '7', 'custom_page', 'Editing template'),
(544, 1527017410, 1, 'admin', 5, '8', 'Документы и анкеты', 'Saving resource'),
(545, 1527017410, 1, 'admin', 27, '8', 'Документы и анкеты', 'Editing resource'),
(546, 1527017603, 1, 'admin', 20, '7', 'custom_page', 'Saving template'),
(547, 1527017603, 1, 'admin', 16, '7', 'custom_page', 'Editing template'),
(548, 1527017874, 1, 'admin', 20, '7', 'custom_page', 'Saving template'),
(549, 1527017875, 1, 'admin', 16, '7', 'custom_page', 'Editing template'),
(550, 1527017891, 1, 'admin', 5, '8', 'Документы и анкеты', 'Saving resource'),
(551, 1527017891, 1, 'admin', 27, '8', 'Документы и анкеты', 'Editing resource'),
(552, 1527019901, 1, 'admin', 27, '2', 'Главная', 'Editing resource'),
(553, 1527019920, 1, 'admin', 5, '2', 'Главная', 'Saving resource'),
(554, 1527019920, 1, 'admin', 27, '2', 'Главная', 'Editing resource'),
(555, 1527019922, 1, 'admin', 27, '2', 'Главная', 'Editing resource'),
(556, 1527020185, 1, 'admin', 76, '-', '-', 'Element management'),
(557, 1527020190, 1, 'admin', 16, '5', 'Home', 'Editing template'),
(558, 1527020208, 1, 'admin', 20, '5', 'Home', 'Saving template'),
(559, 1527020208, 1, 'admin', 76, '-', '-', 'Element management'),
(560, 1527020281, 1, 'admin', 16, '5', 'Home', 'Editing template'),
(561, 1527020295, 1, 'admin', 20, '5', 'Home', 'Saving template'),
(562, 1527020295, 1, 'admin', 16, '5', 'Home', 'Editing template'),
(563, 1527020321, 1, 'admin', 20, '5', 'Home', 'Saving template'),
(564, 1527020321, 1, 'admin', 16, '5', 'Home', 'Editing template'),
(565, 1529146511, 1, 'admin', 76, '-', '-', 'Element management'),
(566, 1529146833, 1, 'admin', 106, '-', '-', 'Viewing Modules'),
(567, 1529147430, 1, 'admin', 106, '-', '-', 'Viewing Modules'),
(568, 1529147432, 1, 'admin', 107, '-', 'Новый модуль', 'Create new module'),
(569, 1529147516, 1, 'admin', 109, '-', 'Новости', 'Saving module'),
(570, 1529147516, 1, 'admin', 106, '-', '-', 'Viewing Modules'),
(571, 1529148920, 1, 'admin', 106, '-', 'Новости', 'Viewing Modules'),
(572, 1529150699, 1, 'admin', 106, '-', 'Новости', 'Viewing Modules'),
(573, 1529151185, 1, 'admin', 112, '1', 'Doc Manager', 'Execute module'),
(574, 1529153418, 1, 'admin', 112, '1', 'Doc Manager', 'Execute module'),
(575, 1529153420, 1, 'admin', 112, '2', 'Extras', 'Execute module'),
(576, 1529155894, 1, 'admin', 112, '2', 'Extras', 'Execute module'),
(577, 1529239675, 1, 'admin', 106, '-', 'Новости', 'Viewing Modules'),
(578, 1529439030, 1, 'admin', 27, '2', 'Главная', 'Editing resource'),
(579, 1529439032, 1, 'admin', 112, '2', 'Extras', 'Execute module'),
(580, 1529439034, 1, 'admin', 102, '10', 'Common', 'Edit plugin'),
(581, 1537219046, 1, 'admin', 27, '3', 'Контакты', 'Editing resource'),
(582, 1537219065, 1, 'admin', 76, '-', '-', 'Element management'),
(583, 1537219069, 1, 'admin', 16, '6', 'Contacts', 'Editing template'),
(584, 1537219185, 1, 'admin', 20, '6', 'Contacts', 'Saving template'),
(585, 1537219185, 1, 'admin', 76, '-', '-', 'Element management'),
(586, 1537219190, 1, 'admin', 16, '6', 'Contacts', 'Editing template'),
(587, 1537219218, 1, 'admin', 20, '6', 'Contacts', 'Saving template'),
(588, 1537219219, 1, 'admin', 76, '-', '-', 'Element management'),
(589, 1537219246, 1, 'admin', 27, '2', 'Главная', 'Editing resource'),
(590, 1537219296, 1, 'admin', 5, '2', 'Главная', 'Saving resource'),
(591, 1537219298, 1, 'admin', 3, '2', 'Главная', 'Viewing data for resource'),
(592, 1537219547, 1, 'admin', 27, '6', 'Гражданство Украины и ПМЖ в Украине', 'Editing resource'),
(593, 1537219564, 1, 'admin', 5, '6', 'Гражданство Украины и ПМЖ в Украине', 'Saving resource'),
(594, 1537219564, 1, 'admin', 27, '6', 'Гражданство Украины и ПМЖ в Украине', 'Editing resource'),
(595, 1537219664, 1, 'admin', 27, '14', 'Выезд несовершеннолетних детей за рубеж', 'Editing resource'),
(596, 1537219670, 1, 'admin', 5, '14', 'Выезд несовершеннолетних детей за рубеж', 'Saving resource'),
(597, 1537219671, 1, 'admin', 3, '14', 'Выезд несовершеннолетних детей за рубеж', 'Viewing data for resource'),
(598, 1537219699, 1, 'admin', 27, '17', 'Еврейская иммиграция ', 'Editing resource'),
(599, 1537219746, 1, 'admin', 5, '17', 'Еврейская иммиграция ', 'Saving resource'),
(600, 1537219747, 1, 'admin', 3, '17', 'Еврейская иммиграция ', 'Viewing data for resource'),
(601, 1537219780, 1, 'admin', 27, '18', 'Немецкая иммиграция', 'Editing resource'),
(602, 1537219795, 1, 'admin', 5, '18', 'Немецкая иммиграция', 'Saving resource'),
(603, 1537219796, 1, 'admin', 3, '18', 'Немецкая иммиграция', 'Viewing data for resource'),
(604, 1537219825, 1, 'admin', 27, '6', 'Гражданство Украины и ПМЖ в Украине', 'Editing resource'),
(605, 1537219839, 1, 'admin', 27, '13', 'Визовая поддержка для государств ЕС, США, Канады', 'Editing resource'),
(606, 1537219851, 1, 'admin', 5, '13', 'Визовая поддержка для государств ЕС, США, Канады', 'Saving resource'),
(607, 1537219853, 1, 'admin', 3, '13', 'Визовая поддержка для государств ЕС, США, Канады', 'Viewing data for resource'),
(608, 1537219912, 1, 'admin', 27, '5', 'Получение статуса беженца за рубежом', 'Editing resource'),
(609, 1537219946, 1, 'admin', 5, '5', 'Получение статуса беженца за рубежом', 'Saving resource'),
(610, 1537219948, 1, 'admin', 3, '5', 'Получение статуса беженца за рубежом', 'Viewing data for resource'),
(611, 1537220168, 1, 'admin', 27, '3', 'Контакты', 'Editing resource'),
(612, 1537220559, 1, 'admin', 27, '2', 'Главная', 'Editing resource'),
(613, 1537220586, 1, 'admin', 5, '2', 'Главная', 'Saving resource'),
(614, 1537220586, 1, 'admin', 27, '2', 'Главная', 'Editing resource'),
(615, 1537220689, 1, 'admin', 5, '2', 'Главная', 'Saving resource'),
(616, 1537220689, 1, 'admin', 27, '2', 'Главная', 'Editing resource'),
(617, 1537220817, 1, 'admin', 5, '2', 'Главная', 'Saving resource'),
(618, 1537220818, 1, 'admin', 27, '2', 'Главная', 'Editing resource'),
(619, 1537220856, 1, 'admin', 5, '2', 'Главная', 'Saving resource'),
(620, 1537220856, 1, 'admin', 27, '2', 'Главная', 'Editing resource'),
(621, 1537220867, 1, 'admin', 5, '2', 'Главная', 'Saving resource'),
(622, 1537220868, 1, 'admin', 27, '2', 'Главная', 'Editing resource'),
(623, 1537220886, 1, 'admin', 5, '2', 'Главная', 'Saving resource'),
(624, 1537220886, 1, 'admin', 27, '2', 'Главная', 'Editing resource'),
(625, 1537220973, 1, 'admin', 5, '2', 'Главная', 'Saving resource'),
(626, 1537220973, 1, 'admin', 27, '2', 'Главная', 'Editing resource'),
(627, 1537220986, 1, 'admin', 5, '2', 'Главная', 'Saving resource'),
(628, 1537220986, 1, 'admin', 27, '2', 'Главная', 'Editing resource'),
(629, 1537221033, 1, 'admin', 56, '-', '-', 'Refresh resource tree'),
(630, 1537221041, 1, 'admin', 56, '-', '-', 'Refresh resource tree'),
(631, 1537221110, 1, 'admin', 75, '-', '-', 'User/ role management'),
(632, 1537221113, 1, 'admin', 11, '-', 'Новый пользователь', 'Creating a user'),
(633, 1537221119, 1, 'admin', 75, '-', '-', 'User/ role management'),
(634, 1537221155, 1, 'admin', 76, '-', '-', 'Element management'),
(635, 1537221158, 1, 'admin', 16, '6', 'Contacts', 'Editing template'),
(636, 1537221234, 1, 'admin', 20, '6', 'Contacts', 'Saving template'),
(637, 1537221234, 1, 'admin', 76, '-', '-', 'Element management'),
(638, 1537221248, 1, 'admin', 16, '6', 'Contacts', 'Editing template'),
(639, 1537221263, 1, 'admin', 20, '6', 'Contacts', 'Saving template'),
(640, 1537221263, 1, 'admin', 76, '-', '-', 'Element management'),
(641, 1537810571, 1, 'admin', 27, '3', 'Контакты', 'Editing resource'),
(642, 1537810592, 1, 'admin', 76, '-', '-', 'Element management'),
(643, 1537810595, 1, 'admin', 16, '6', 'Contacts', 'Editing template'),
(644, 1537810710, 1, 'admin', 20, '6', 'Contacts', 'Saving template'),
(645, 1537810710, 1, 'admin', 16, '6', 'Contacts', 'Editing template'),
(646, 1537810822, 1, 'admin', 20, '6', 'Contacts', 'Saving template'),
(647, 1537810822, 1, 'admin', 16, '6', 'Contacts', 'Editing template'),
(648, 1537810858, 1, 'admin', 76, '-', '-', 'Element management'),
(649, 1537810862, 1, 'admin', 78, '9', 'HEAD', 'Editing Chunk (HTML Snippet)'),
(650, 1537810870, 1, 'admin', 79, '9', 'HEAD', 'Saving Chunk (HTML Snippet)'),
(651, 1537810871, 1, 'admin', 76, '-', '-', 'Element management'),
(652, 1537810883, 1, 'admin', 78, '9', 'HEAD', 'Editing Chunk (HTML Snippet)'),
(653, 1537811075, 1, 'admin', 79, '9', 'HEAD', 'Saving Chunk (HTML Snippet)'),
(654, 1537811076, 1, 'admin', 76, '-', '-', 'Element management'),
(655, 1537811256, 1, 'admin', 20, '6', 'Contacts', 'Saving template'),
(656, 1537811256, 1, 'admin', 16, '6', 'Contacts', 'Editing template'),
(657, 1537811330, 1, 'admin', 20, '6', 'Contacts', 'Saving template'),
(658, 1537811330, 1, 'admin', 16, '6', 'Contacts', 'Editing template'),
(659, 1537811400, 1, 'admin', 20, '6', 'Contacts', 'Saving template'),
(660, 1537811400, 1, 'admin', 16, '6', 'Contacts', 'Editing template'),
(661, 1537811473, 1, 'admin', 20, '6', 'Contacts', 'Saving template'),
(662, 1537811474, 1, 'admin', 16, '6', 'Contacts', 'Editing template'),
(663, 1537812335, 1, 'admin', 20, '6', 'Contacts', 'Saving template'),
(664, 1537812335, 1, 'admin', 16, '6', 'Contacts', 'Editing template'),
(665, 1537812367, 1, 'admin', 20, '6', 'Contacts', 'Saving template');
INSERT INTO `modx_manager_log` (`id`, `timestamp`, `internalKey`, `username`, `action`, `itemid`, `itemname`, `message`) VALUES
(666, 1537812367, 1, 'admin', 16, '6', 'Contacts', 'Editing template'),
(667, 1537812389, 1, 'admin', 20, '6', 'Contacts', 'Saving template'),
(668, 1537812389, 1, 'admin', 16, '6', 'Contacts', 'Editing template'),
(669, 1537812408, 1, 'admin', 20, '6', 'Contacts', 'Saving template'),
(670, 1537812408, 1, 'admin', 16, '6', 'Contacts', 'Editing template'),
(671, 1537812465, 1, 'admin', 20, '6', 'Contacts', 'Saving template'),
(672, 1537812465, 1, 'admin', 16, '6', 'Contacts', 'Editing template'),
(673, 1537812611, 1, 'admin', 20, '6', 'Contacts', 'Saving template'),
(674, 1537812611, 1, 'admin', 16, '6', 'Contacts', 'Editing template'),
(675, 1537812767, 1, 'admin', 20, '6', 'Contacts', 'Saving template'),
(676, 1537812767, 1, 'admin', 16, '6', 'Contacts', 'Editing template'),
(677, 1537813611, 1, 'admin', 23, '-', 'Новый сниппет', 'Creating a new Snippet'),
(678, 1537813662, 1, 'admin', 24, '-', 'S', 'Saving Snippet'),
(679, 1537813662, 1, 'admin', 22, '18', 'S', 'Editing Snippet'),
(680, 1537813740, 1, 'admin', 76, '-', '-', 'Element management'),
(681, 1537813872, 1, 'admin', 76, '-', '-', 'Element management'),
(682, 1537813874, 1, 'admin', 16, '5', 'Home', 'Editing template'),
(683, 1537813881, 1, 'admin', 19, '-', 'Новый шаблон', 'Creating a new template'),
(684, 1537814018, 1, 'admin', 20, '-', 'Новости', 'Saving template'),
(685, 1537814018, 1, 'admin', 76, '-', '-', 'Element management'),
(686, 1537814024, 1, 'admin', 27, '4', 'Новости', 'Editing resource'),
(687, 1537814029, 1, 'admin', 27, '4', 'Новости', 'Editing resource'),
(688, 1537814031, 1, 'admin', 5, '4', 'Новости', 'Saving resource'),
(689, 1537814032, 1, 'admin', 3, '4', 'Новости', 'Viewing data for resource'),
(690, 1537814066, 1, 'admin', 76, '-', '-', 'Element management'),
(691, 1537814068, 1, 'admin', 16, '8', 'Новости', 'Editing template'),
(692, 1537814131, 1, 'admin', 20, '8', 'Новости', 'Saving template'),
(693, 1537814131, 1, 'admin', 16, '8', 'Новости', 'Editing template'),
(694, 1537814188, 1, 'admin', 20, '8', 'Новости', 'Saving template'),
(695, 1537814188, 1, 'admin', 16, '8', 'Новости', 'Editing template'),
(696, 1537814377, 1, 'admin', 24, '18', 'S', 'Saving Snippet'),
(697, 1537814377, 1, 'admin', 22, '18', 'S', 'Editing Snippet'),
(698, 1537814401, 1, 'admin', 76, '-', '-', 'Element management'),
(699, 1537814500, 1, 'admin', 77, '-', 'Новый чанк', 'Creating a new Chunk (HTML Snippet)'),
(700, 1537814515, 1, 'admin', 76, '-', '-', 'Element management'),
(701, 1537814523, 1, 'admin', 78, '17', 'tpl_row_active', 'Editing Chunk (HTML Snippet)'),
(702, 1537814529, 1, 'admin', 76, '-', '-', 'Element management'),
(703, 1537814544, 1, 'admin', 77, '-', 'Новый чанк', 'Creating a new Chunk (HTML Snippet)'),
(704, 1537814682, 1, 'admin', 79, '-', 'tpl_news_item', 'Saving Chunk (HTML Snippet)'),
(705, 1537814682, 1, 'admin', 78, '20', 'tpl_news_item', 'Editing Chunk (HTML Snippet)'),
(706, 1537814933, 1, 'admin', 24, '18', 'S', 'Saving Snippet'),
(707, 1537814933, 1, 'admin', 22, '18', 'S', 'Editing Snippet'),
(708, 1537814957, 1, 'admin', 20, '8', 'Новости', 'Saving template'),
(709, 1537814957, 1, 'admin', 16, '8', 'Новости', 'Editing template'),
(710, 1537815046, 1, 'admin', 24, '18', 'S', 'Saving Snippet'),
(711, 1537815046, 1, 'admin', 22, '18', 'S', 'Editing Snippet'),
(712, 1537815075, 1, 'admin', 79, '20', 'tpl_news_item', 'Saving Chunk (HTML Snippet)'),
(713, 1537815075, 1, 'admin', 78, '20', 'tpl_news_item', 'Editing Chunk (HTML Snippet)'),
(714, 1537815133, 1, 'admin', 24, '18', 'S', 'Saving Snippet'),
(715, 1537815133, 1, 'admin', 22, '18', 'S', 'Editing Snippet'),
(716, 1537815257, 1, 'admin', 79, '20', 'tpl_news_item', 'Saving Chunk (HTML Snippet)'),
(717, 1537815258, 1, 'admin', 78, '20', 'tpl_news_item', 'Editing Chunk (HTML Snippet)'),
(718, 1537815270, 1, 'admin', 24, '18', 'S', 'Saving Snippet'),
(719, 1537815270, 1, 'admin', 22, '18', 'S', 'Editing Snippet'),
(720, 1537815299, 1, 'admin', 24, '18', 'S', 'Saving Snippet'),
(721, 1537815299, 1, 'admin', 22, '18', 'S', 'Editing Snippet'),
(722, 1537815312, 1, 'admin', 24, '18', 'S', 'Saving Snippet'),
(723, 1537815312, 1, 'admin', 22, '18', 'S', 'Editing Snippet'),
(724, 1537815319, 1, 'admin', 24, '18', 'S', 'Saving Snippet'),
(725, 1537815319, 1, 'admin', 22, '18', 'S', 'Editing Snippet'),
(726, 1537816023, 1, 'admin', 24, '18', 'S', 'Saving Snippet'),
(727, 1537816023, 1, 'admin', 22, '18', 'S', 'Editing Snippet'),
(728, 1537816044, 1, 'admin', 20, '8', 'Новости', 'Saving template'),
(729, 1537816044, 1, 'admin', 16, '8', 'Новости', 'Editing template'),
(730, 1537816087, 1, 'admin', 24, '18', 'S', 'Saving Snippet'),
(731, 1537816087, 1, 'admin', 22, '18', 'S', 'Editing Snippet'),
(732, 1537816119, 1, 'admin', 24, '18', 'S', 'Saving Snippet'),
(733, 1537816120, 1, 'admin', 22, '18', 'S', 'Editing Snippet'),
(734, 1537816171, 1, 'admin', 76, '-', '-', 'Element management'),
(735, 1537816176, 1, 'admin', 76, '-', '-', 'Element management'),
(736, 1537816185, 1, 'admin', 78, '13', 'tpl_latest_news', 'Editing Chunk (HTML Snippet)'),
(737, 1537816219, 1, 'admin', 76, '-', '-', 'Element management'),
(738, 1537816225, 1, 'admin', 78, '14', 'tpl_menu', 'Editing Chunk (HTML Snippet)'),
(739, 1537816228, 1, 'admin', 76, '-', '-', 'Element management'),
(740, 1537816238, 1, 'admin', 78, '10', 'HEADER', 'Editing Chunk (HTML Snippet)'),
(741, 1537816252, 1, 'admin', 79, '10', 'HEADER', 'Saving Chunk (HTML Snippet)'),
(742, 1537816252, 1, 'admin', 76, '-', '-', 'Element management'),
(743, 1537816260, 1, 'admin', 16, '7', 'custom_page', 'Editing template'),
(744, 1537816292, 1, 'admin', 76, '-', '-', 'Element management'),
(745, 1537816296, 1, 'admin', 77, '-', 'Новый чанк', 'Creating a new Chunk (HTML Snippet)'),
(746, 1537816412, 1, 'admin', 79, '13', 'tpl_latest_news_item', 'Saving Chunk (HTML Snippet)'),
(747, 1537816412, 1, 'admin', 78, '13', 'tpl_latest_news_item', 'Editing Chunk (HTML Snippet)'),
(748, 1537816420, 1, 'admin', 79, '-', 'tpl_latest_news_row', 'Saving Chunk (HTML Snippet)'),
(749, 1537816420, 1, 'admin', 76, '-', '-', 'Element management'),
(750, 1537816432, 1, 'admin', 16, '5', 'Home', 'Editing template'),
(751, 1537816437, 1, 'admin', 20, '5', 'Home', 'Saving template'),
(752, 1537816437, 1, 'admin', 76, '-', '-', 'Element management'),
(753, 1537816439, 1, 'admin', 16, '7', 'custom_page', 'Editing template'),
(754, 1537816442, 1, 'admin', 20, '7', 'custom_page', 'Saving template'),
(755, 1537816442, 1, 'admin', 76, '-', '-', 'Element management'),
(756, 1537816444, 1, 'admin', 16, '6', 'Contacts', 'Editing template'),
(757, 1537816447, 1, 'admin', 20, '6', 'Contacts', 'Saving template'),
(758, 1537816447, 1, 'admin', 76, '-', '-', 'Element management'),
(759, 1537816449, 1, 'admin', 16, '8', 'Новости', 'Editing template'),
(760, 1537816452, 1, 'admin', 20, '8', 'Новости', 'Saving template'),
(761, 1537816452, 1, 'admin', 76, '-', '-', 'Element management'),
(762, 1537816454, 1, 'admin', 16, '8', 'Новости', 'Editing template'),
(763, 1537816463, 1, 'admin', 20, '8', 'Новости', 'Saving template'),
(764, 1537816463, 1, 'admin', 76, '-', '-', 'Element management'),
(765, 1537816572, 1, 'admin', 24, '18', 'S', 'Saving Snippet'),
(766, 1537816572, 1, 'admin', 22, '18', 'S', 'Editing Snippet'),
(767, 1537816662, 1, 'admin', 79, '13', 'tpl_latest_news_item', 'Saving Chunk (HTML Snippet)'),
(768, 1537816662, 1, 'admin', 78, '13', 'tpl_latest_news_item', 'Editing Chunk (HTML Snippet)'),
(769, 1537816670, 1, 'admin', 78, '21', 'tpl_latest_news_row', 'Editing Chunk (HTML Snippet)'),
(770, 1537816707, 1, 'admin', 79, '21', 'tpl_latest_news_row', 'Saving Chunk (HTML Snippet)'),
(771, 1537816707, 1, 'admin', 76, '-', '-', 'Element management'),
(772, 1537816731, 1, 'admin', 78, '21', 'tpl_latest_news_row', 'Editing Chunk (HTML Snippet)'),
(773, 1537816735, 1, 'admin', 79, '21', 'tpl_latest_news_row', 'Saving Chunk (HTML Snippet)'),
(774, 1537816735, 1, 'admin', 76, '-', '-', 'Element management'),
(775, 1537816768, 1, 'admin', 78, '21', 'tpl_latest_news_row', 'Editing Chunk (HTML Snippet)'),
(776, 1537816802, 1, 'admin', 24, '18', 'S', 'Saving Snippet'),
(777, 1537816802, 1, 'admin', 22, '18', 'S', 'Editing Snippet'),
(778, 1537816848, 1, 'admin', 24, '18', 'S', 'Saving Snippet'),
(779, 1537816848, 1, 'admin', 22, '18', 'S', 'Editing Snippet'),
(780, 1537816873, 1, 'admin', 24, '18', 'S', 'Saving Snippet'),
(781, 1537816873, 1, 'admin', 22, '18', 'S', 'Editing Snippet'),
(782, 1537816887, 1, 'admin', 24, '18', 'S', 'Saving Snippet'),
(783, 1537816887, 1, 'admin', 22, '18', 'S', 'Editing Snippet'),
(784, 1537816905, 1, 'admin', 24, '18', 'S', 'Saving Snippet'),
(785, 1537816905, 1, 'admin', 22, '18', 'S', 'Editing Snippet'),
(786, 1537816923, 1, 'admin', 24, '18', 'S', 'Saving Snippet'),
(787, 1537816923, 1, 'admin', 22, '18', 'S', 'Editing Snippet'),
(788, 1537817031, 1, 'admin', 76, '-', '-', 'Element management'),
(789, 1537817315, 1, 'admin', 24, '18', 'S', 'Saving Snippet'),
(790, 1537817315, 1, 'admin', 22, '18', 'S', 'Editing Snippet'),
(791, 1537817326, 1, 'admin', 106, '-', '-', 'Viewing Modules'),
(792, 1537817504, 1, 'admin', 26, '-', 'Новости', 'Refreshing site'),
(793, 1537817590, 1, 'admin', 24, '18', 'S', 'Saving Snippet'),
(794, 1537817590, 1, 'admin', 22, '18', 'S', 'Editing Snippet'),
(795, 1537817616, 1, 'admin', 26, '-', 'Новости', 'Refreshing site'),
(796, 1537817650, 1, 'admin', 76, '-', '-', 'Element management'),
(797, 1537817656, 1, 'admin', 16, '8', 'Новости', 'Editing template'),
(798, 1537817668, 1, 'admin', 20, '8', 'Новости', 'Saving template'),
(799, 1537817668, 1, 'admin', 76, '-', '-', 'Element management'),
(800, 1537817677, 1, 'admin', 16, '8', 'Новости', 'Editing template'),
(801, 1537817708, 1, 'admin', 76, '-', '-', 'Element management'),
(802, 1537817712, 1, 'admin', 78, '20', 'tpl_news_item', 'Editing Chunk (HTML Snippet)'),
(803, 1537818299, 1, 'admin', 24, '18', 'S', 'Saving Snippet'),
(804, 1537818299, 1, 'admin', 22, '18', 'S', 'Editing Snippet'),
(805, 1537888132, 1, 'admin', 26, '-', '-', 'Refreshing site'),
(806, 1537888294, 1, 'admin', 76, '-', '-', 'Element management'),
(807, 1537888309, 1, 'admin', 16, '7', 'custom_page', 'Editing template'),
(808, 1537888316, 1, 'admin', 19, '-', 'Новый шаблон', 'Creating a new template'),
(809, 1537888343, 1, 'admin', 20, '-', 'one_news', 'Saving template'),
(810, 1537888343, 1, 'admin', 16, '9', 'one_news', 'Editing template'),
(811, 1537888352, 1, 'admin', 76, '-', '-', 'Element management'),
(812, 1537888363, 1, 'admin', 16, '9', 'one_news', 'Editing template'),
(813, 1537888382, 1, 'admin', 76, '-', '-', 'Element management'),
(814, 1537888387, 1, 'admin', 22, '18', 'S', 'Editing Snippet'),
(815, 1537888539, 1, 'admin', 24, '18', 'S', 'Saving Snippet'),
(816, 1537888539, 1, 'admin', 22, '18', 'S', 'Editing Snippet'),
(817, 1537888594, 1, 'admin', 24, '18', 'S', 'Saving Snippet'),
(818, 1537888594, 1, 'admin', 22, '18', 'S', 'Editing Snippet'),
(819, 1537888607, 1, 'admin', 20, '9', 'one_news', 'Saving template'),
(820, 1537888607, 1, 'admin', 16, '9', 'one_news', 'Editing template'),
(821, 1537888613, 1, 'admin', 22, '18', 'S', 'Editing Snippet'),
(822, 1537888647, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(823, 1537888664, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(824, 1537888675, 1, 'admin', 5, '-', 'Одна новость', 'Saving resource'),
(825, 1537888676, 1, 'admin', 3, '19', 'Одна новость', 'Viewing data for resource'),
(826, 1537888831, 1, 'admin', 22, '18', 'S', 'Editing Snippet'),
(827, 1537888836, 1, 'admin', 16, '9', 'one_news', 'Editing template'),
(828, 1537888845, 1, 'admin', 3, '19', 'Одна новость', 'Viewing data for resource'),
(829, 1537888942, 1, 'admin', 27, '4', 'Новости', 'Editing resource'),
(830, 1537888944, 1, 'admin', 27, '19', 'Одна новость', 'Editing resource'),
(831, 1537888962, 1, 'admin', 27, '19', 'Одна новость', 'Editing resource'),
(832, 1537889184, 1, 'admin', 5, '19', 'Одна новость', 'Saving resource'),
(833, 1537889185, 1, 'admin', 3, '19', 'Одна новость', 'Viewing data for resource'),
(834, 1537889186, 1, 'admin', 27, '19', 'Одна новость', 'Editing resource'),
(835, 1537889207, 1, 'admin', 27, '19', 'Одна новость', 'Editing resource'),
(836, 1537889210, 1, 'admin', 6, '19', 'Одна новость', 'Deleting resource'),
(837, 1537889211, 1, 'admin', 3, '19', 'Одна новость', 'Viewing data for resource'),
(838, 1537889214, 1, 'admin', 64, '-', '-', 'Removing deleted content'),
(839, 1537889218, 1, 'admin', 27, '4', 'Новости', 'Editing resource'),
(840, 1537889284, 1, 'admin', 76, '-', '-', 'Element management'),
(841, 1537889288, 1, 'admin', 16, '8', 'Новости', 'Editing template'),
(842, 1537889345, 1, 'admin', 76, '-', '-', 'Element management'),
(843, 1537889351, 1, 'admin', 22, '7', 'if', 'Editing Snippet'),
(844, 1537891186, 1, 'admin', 24, '18', 'S', 'Saving Snippet'),
(845, 1537891186, 1, 'admin', 22, '18', 'S', 'Editing Snippet'),
(846, 1537891189, 1, 'admin', 20, '8', 'Новости', 'Saving template'),
(847, 1537891189, 1, 'admin', 76, '-', '-', 'Element management'),
(848, 1537891196, 1, 'admin', 76, '-', '-', 'Element management'),
(849, 1538134268, 1, 'admin', 76, '-', '-', 'Element management'),
(850, 1538134279, 1, 'admin', 22, '18', 'S', 'Editing Snippet'),
(851, 1538134313, 1, 'admin', 22, '18', 'S', 'Editing Snippet'),
(852, 1538134316, 1, 'admin', 76, '-', '-', 'Element management'),
(853, 1538134330, 1, 'admin', 16, '7', 'custom_page', 'Editing template'),
(854, 1538134336, 1, 'admin', 76, '-', '-', 'Element management'),
(855, 1538134341, 1, 'admin', 78, '21', 'tpl_latest_news_row', 'Editing Chunk (HTML Snippet)'),
(856, 1538134597, 1, 'admin', 24, '18', 'S', 'Saving Snippet'),
(857, 1538134597, 1, 'admin', 22, '18', 'S', 'Editing Snippet'),
(858, 1538134616, 1, 'admin', 79, '21', 'tpl_latest_news_row', 'Saving Chunk (HTML Snippet)'),
(859, 1538134616, 1, 'admin', 78, '21', 'tpl_latest_news_row', 'Editing Chunk (HTML Snippet)'),
(860, 1538134695, 1, 'admin', 106, '-', 'Новости', 'Viewing Modules'),
(861, 1538134698, 1, 'admin', 76, '-', '-', 'Element management'),
(862, 1538134702, 1, 'admin', 78, '21', 'tpl_latest_news_row', 'Editing Chunk (HTML Snippet)'),
(863, 1538134757, 1, 'admin', 79, '21', 'tpl_latest_news_row', 'Saving Chunk (HTML Snippet)'),
(864, 1538134757, 1, 'admin', 78, '21', 'tpl_latest_news_row', 'Editing Chunk (HTML Snippet)'),
(865, 1538134831, 1, 'admin', 79, '21', 'tpl_latest_news_row', 'Saving Chunk (HTML Snippet)'),
(866, 1538134831, 1, 'admin', 78, '21', 'tpl_latest_news_row', 'Editing Chunk (HTML Snippet)'),
(867, 1538134848, 1, 'admin', 79, '21', 'tpl_latest_news_row', 'Saving Chunk (HTML Snippet)'),
(868, 1538134848, 1, 'admin', 78, '21', 'tpl_latest_news_row', 'Editing Chunk (HTML Snippet)'),
(869, 1538134865, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(870, 1538136049, 1, 'admin', 106, '-', '-', 'Viewing Modules'),
(871, 1538136054, 1, 'admin', 112, '2', 'Extras', 'Execute module'),
(872, 1538136055, 1, 'admin', 112, '1', 'Doc Manager', 'Execute module'),
(873, 1538136078, 1, 'admin', 106, '-', '-', 'Viewing Modules'),
(874, 1538136081, 1, 'admin', 107, '-', 'Новый модуль', 'Create new module'),
(875, 1538136130, 1, 'admin', 109, '-', 'SEO', 'Saving module'),
(876, 1538136130, 1, 'admin', 108, '4', 'SEO', 'Edit module'),
(877, 1538136763, 1, 'admin', 76, '-', 'SEO', 'Element management'),
(878, 1538658438, 1, 'admin', 27, '5', 'Получение статуса беженца за рубежом', 'Editing resource'),
(879, 1538658439, 1, 'admin', 27, '6', 'Гражданство Украины и ПМЖ в Украине', 'Editing resource'),
(880, 1538658441, 1, 'admin', 27, '8', 'Документы и анкеты', 'Editing resource'),
(881, 1538658922, 1, 'admin', 27, '8', 'Документы и анкеты', 'Editing resource'),
(882, 1538658924, 1, 'admin', 6, '8', 'Документы и анкеты', 'Deleting resource'),
(883, 1538658926, 1, 'admin', 3, '8', 'Документы и анкеты', 'Viewing data for resource'),
(884, 1538658962, 1, 'admin', 27, '8', 'Документы и анкеты', 'Editing resource'),
(885, 1538658970, 1, 'admin', 63, '8', 'Документы и анкеты', 'Un-deleting a resource'),
(886, 1538658971, 1, 'admin', 3, '8', 'Документы и анкеты', 'Viewing data for resource'),
(887, 1538658981, 1, 'admin', 27, '11', 'Иммиграция в Великобританию', 'Editing resource'),
(888, 1538660141, 1, 'admin', 76, '-', '-', 'Element management'),
(889, 1538660145, 1, 'admin', 78, '11', 'FOOTER', 'Editing Chunk (HTML Snippet)'),
(890, 1538660204, 1, 'admin', 79, '11', 'FOOTER', 'Saving Chunk (HTML Snippet)'),
(891, 1538660204, 1, 'admin', 78, '11', 'FOOTER', 'Editing Chunk (HTML Snippet)'),
(892, 1538660216, 1, 'admin', 79, '11', 'FOOTER', 'Saving Chunk (HTML Snippet)'),
(893, 1538660216, 1, 'admin', 78, '11', 'FOOTER', 'Editing Chunk (HTML Snippet)'),
(894, 1538660237, 1, 'admin', 79, '11', 'FOOTER', 'Saving Chunk (HTML Snippet)'),
(895, 1538660237, 1, 'admin', 78, '11', 'FOOTER', 'Editing Chunk (HTML Snippet)'),
(896, 1538660262, 1, 'admin', 79, '11', 'FOOTER', 'Saving Chunk (HTML Snippet)'),
(897, 1538660262, 1, 'admin', 78, '11', 'FOOTER', 'Editing Chunk (HTML Snippet)'),
(898, 1538660269, 1, 'admin', 79, '11', 'FOOTER', 'Saving Chunk (HTML Snippet)'),
(899, 1538660270, 1, 'admin', 78, '11', 'FOOTER', 'Editing Chunk (HTML Snippet)'),
(900, 1538660285, 1, 'admin', 79, '11', 'FOOTER', 'Saving Chunk (HTML Snippet)'),
(901, 1538660285, 1, 'admin', 78, '11', 'FOOTER', 'Editing Chunk (HTML Snippet)'),
(902, 1538660295, 1, 'admin', 79, '11', 'FOOTER', 'Saving Chunk (HTML Snippet)'),
(903, 1538660295, 1, 'admin', 78, '11', 'FOOTER', 'Editing Chunk (HTML Snippet)'),
(904, 1538660308, 1, 'admin', 79, '11', 'FOOTER', 'Saving Chunk (HTML Snippet)'),
(905, 1538660308, 1, 'admin', 78, '11', 'FOOTER', 'Editing Chunk (HTML Snippet)'),
(906, 1538660321, 1, 'admin', 79, '11', 'FOOTER', 'Saving Chunk (HTML Snippet)'),
(907, 1538660321, 1, 'admin', 78, '11', 'FOOTER', 'Editing Chunk (HTML Snippet)'),
(908, 1538660331, 1, 'admin', 79, '11', 'FOOTER', 'Saving Chunk (HTML Snippet)'),
(909, 1538660331, 1, 'admin', 78, '11', 'FOOTER', 'Editing Chunk (HTML Snippet)'),
(910, 1538660346, 1, 'admin', 79, '11', 'FOOTER', 'Saving Chunk (HTML Snippet)'),
(911, 1538660346, 1, 'admin', 78, '11', 'FOOTER', 'Editing Chunk (HTML Snippet)'),
(912, 1538660358, 1, 'admin', 78, '11', 'FOOTER', 'Editing Chunk (HTML Snippet)'),
(913, 1538660362, 1, 'admin', 26, '-', '-', 'Refreshing site'),
(914, 1538660375, 1, 'admin', 79, '11', 'FOOTER', 'Saving Chunk (HTML Snippet)'),
(915, 1538660375, 1, 'admin', 78, '11', 'FOOTER', 'Editing Chunk (HTML Snippet)'),
(916, 1538660378, 1, 'admin', 26, '-', '-', 'Refreshing site'),
(917, 1538660569, 1, 'admin', 79, '11', 'FOOTER', 'Saving Chunk (HTML Snippet)'),
(918, 1538660569, 1, 'admin', 78, '11', 'FOOTER', 'Editing Chunk (HTML Snippet)'),
(919, 1538660622, 1, 'admin', 76, '-', '-', 'Element management'),
(920, 1538660626, 1, 'admin', 27, '3', 'Контакты', 'Editing resource'),
(921, 1538660632, 1, 'admin', 76, '-', '-', 'Element management'),
(922, 1538660637, 1, 'admin', 16, '6', 'Contacts', 'Editing template'),
(923, 1538660701, 1, 'admin', 79, '11', 'FOOTER', 'Saving Chunk (HTML Snippet)'),
(924, 1538660702, 1, 'admin', 78, '11', 'FOOTER', 'Editing Chunk (HTML Snippet)'),
(925, 1538660716, 1, 'admin', 79, '11', 'FOOTER', 'Saving Chunk (HTML Snippet)'),
(926, 1538660716, 1, 'admin', 78, '11', 'FOOTER', 'Editing Chunk (HTML Snippet)'),
(927, 1538660739, 1, 'admin', 79, '11', 'FOOTER', 'Saving Chunk (HTML Snippet)'),
(928, 1538660739, 1, 'admin', 78, '11', 'FOOTER', 'Editing Chunk (HTML Snippet)'),
(929, 1538660749, 1, 'admin', 16, '6', 'Contacts', 'Editing template'),
(930, 1538660751, 1, 'admin', 26, '-', '-', 'Refreshing site'),
(931, 1538660775, 1, 'admin', 79, '11', 'FOOTER', 'Saving Chunk (HTML Snippet)'),
(932, 1538660775, 1, 'admin', 78, '11', 'FOOTER', 'Editing Chunk (HTML Snippet)'),
(933, 1538660779, 1, 'admin', 26, '-', '-', 'Refreshing site'),
(934, 1538660797, 1, 'admin', 79, '11', 'FOOTER', 'Saving Chunk (HTML Snippet)'),
(935, 1538660797, 1, 'admin', 78, '11', 'FOOTER', 'Editing Chunk (HTML Snippet)'),
(936, 1538660802, 1, 'admin', 26, '-', '-', 'Refreshing site'),
(937, 1538662720, 1, 'admin', 20, '6', 'Contacts', 'Saving template'),
(938, 1538662720, 1, 'admin', 16, '6', 'Contacts', 'Editing template'),
(939, 1538662793, 1, 'admin', 79, '11', 'FOOTER', 'Saving Chunk (HTML Snippet)'),
(940, 1538662793, 1, 'admin', 78, '11', 'FOOTER', 'Editing Chunk (HTML Snippet)'),
(941, 1538662873, 1, 'admin', 79, '11', 'FOOTER', 'Saving Chunk (HTML Snippet)'),
(942, 1538662873, 1, 'admin', 78, '11', 'FOOTER', 'Editing Chunk (HTML Snippet)'),
(943, 1538663508, 1, 'admin', 106, '-', '-', 'Viewing Modules'),
(944, 1538663510, 1, 'admin', 107, '-', 'Новый модуль', 'Create new module'),
(945, 1538663521, 1, 'admin', 112, '1', 'Doc Manager', 'Execute module'),
(946, 1538663541, 1, 'admin', 112, '2', 'Extras', 'Execute module'),
(947, 1538663689, 1, 'admin', 112, '2', 'Extras', 'Execute module'),
(948, 1538664104, 1, 'admin', 106, '-', 'Extras', 'Viewing Modules'),
(949, 1538664108, 1, 'admin', 107, '-', 'Новый модуль', 'Create new module'),
(950, 1538664119, 1, 'admin', 106, '-', '-', 'Viewing Modules'),
(951, 1538664120, 1, 'admin', 112, '1', 'Doc Manager', 'Execute module'),
(952, 1538667484, 1, 'admin', 79, '11', 'FOOTER', 'Saving Chunk (HTML Snippet)'),
(953, 1538667484, 1, 'admin', 78, '11', 'FOOTER', 'Editing Chunk (HTML Snippet)'),
(954, 1538667506, 1, 'admin', 26, '-', '-', 'Refreshing site'),
(955, 1538667784, 1, 'admin', 79, '11', 'FOOTER', 'Saving Chunk (HTML Snippet)'),
(956, 1538667784, 1, 'admin', 78, '11', 'FOOTER', 'Editing Chunk (HTML Snippet)'),
(957, 1538667793, 1, 'admin', 26, '-', '-', 'Refreshing site'),
(958, 1538667806, 1, 'admin', 79, '11', 'FOOTER', 'Saving Chunk (HTML Snippet)'),
(959, 1538667806, 1, 'admin', 78, '11', 'FOOTER', 'Editing Chunk (HTML Snippet)'),
(960, 1538667808, 1, 'admin', 26, '-', '-', 'Refreshing site'),
(961, 1538668131, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(962, 1538668177, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(963, 1538668868, 1, 'admin', 5, '-', '10 мифов об эмиграции', 'Saving resource'),
(964, 1538668868, 1, 'admin', 27, '20', '10 мифов об эмиграции', 'Editing resource'),
(965, 1538730137, 1, 'admin', 5, '20', '10 мифов об эмиграции', 'Saving resource'),
(966, 1538730137, 1, 'admin', 27, '20', '10 мифов об эмиграции', 'Editing resource'),
(967, 1538730652, 1, 'admin', 27, '6', 'Гражданство Украины и ПМЖ в Украине', 'Editing resource'),
(968, 1538730670, 1, 'admin', 5, '6', 'Гражданство  ПМЖ в Украине', 'Saving resource'),
(969, 1538730671, 1, 'admin', 3, '6', 'Гражданство  ПМЖ в Украине', 'Viewing data for resource'),
(970, 1538730684, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(971, 1538730720, 1, 'admin', 5, '-', 'Гражданство или вид на жительство.', 'Saving resource'),
(972, 1538730721, 1, 'admin', 3, '21', 'Гражданство или вид на жительство.', 'Viewing data for resource'),
(973, 1538730725, 1, 'admin', 27, '21', 'Гражданство или вид на жительство.', 'Editing resource'),
(974, 1538730732, 1, 'admin', 5, '21', 'Гражданство или вид на жительство.', 'Saving resource'),
(975, 1538730733, 1, 'admin', 3, '21', 'Гражданство или вид на жительство.', 'Viewing data for resource'),
(976, 1538730757, 1, 'admin', 27, '21', 'Гражданство или вид на жительство.', 'Editing resource'),
(977, 1538730761, 1, 'admin', 27, '21', 'Гражданство или вид на жительство.', 'Editing resource'),
(978, 1538730768, 1, 'admin', 5, '21', 'Гражданство или вид на жительство.', 'Saving resource'),
(979, 1538730768, 1, 'admin', 27, '21', 'Гражданство или вид на жительство.', 'Editing resource'),
(980, 1538730807, 1, 'admin', 27, '21', 'Гражданство или вид на жительство.', 'Editing resource'),
(981, 1538731624, 1, 'admin', 5, '21', 'Гражданство или вид на жительство.', 'Saving resource'),
(982, 1538731624, 1, 'admin', 27, '21', 'Гражданство или вид на жительство.', 'Editing resource'),
(983, 1538731869, 1, 'admin', 5, '21', 'Гражданство или вид на жительство.', 'Saving resource'),
(984, 1538731869, 1, 'admin', 27, '21', 'Гражданство или вид на жительство.', 'Editing resource'),
(985, 1538731873, 1, 'admin', 26, '-', '-', 'Refreshing site'),
(986, 1538732090, 1, 'admin', 76, '-', '-', 'Element management'),
(987, 1538732099, 1, 'admin', 78, '18', 'tpl_inner', 'Editing Chunk (HTML Snippet)'),
(988, 1538732103, 1, 'admin', 76, '-', '-', 'Element management'),
(989, 1538732105, 1, 'admin', 78, '19', 'tpl_inner_row', 'Editing Chunk (HTML Snippet)'),
(990, 1538732113, 1, 'admin', 79, '19', 'tpl_inner_row', 'Saving Chunk (HTML Snippet)'),
(991, 1538732113, 1, 'admin', 76, '-', '-', 'Element management'),
(992, 1538732115, 1, 'admin', 26, '-', '-', 'Refreshing site'),
(993, 1538732126, 1, 'admin', 76, '-', '-', 'Element management'),
(994, 1538732173, 1, 'admin', 5, '21', 'Гражданство или вид на жительство.', 'Saving resource'),
(995, 1538732173, 1, 'admin', 27, '21', 'Гражданство или вид на жительство.', 'Editing resource'),
(996, 1538732223, 1, 'admin', 4, '-', 'Новый ресурс', 'Creating a resource'),
(997, 1538732347, 1, 'admin', 5, '-', 'Почему не нужно покупать паспорт Украины', 'Saving resource'),
(998, 1538732347, 1, 'admin', 27, '22', 'Почему не нужно покупать паспорт Украины', 'Editing resource'),
(999, 1538732352, 1, 'admin', 27, '22', 'Почему не нужно покупать паспорт Украины', 'Editing resource'),
(1000, 1538732359, 1, 'admin', 5, '22', 'Почему не нужно покупать паспорт Украины', 'Saving resource'),
(1001, 1538732359, 1, 'admin', 27, '22', 'Почему не нужно покупать паспорт Украины', 'Editing resource'),
(1002, 1538732867, 1, 'admin', 5, '22', 'Почему не нужно покупать паспорт Украины', 'Saving resource'),
(1003, 1538732867, 1, 'admin', 27, '22', 'Почему не нужно покупать паспорт Украины', 'Editing resource'),
(1004, 1538733069, 1, 'admin', 78, '17', 'tpl_row_active', 'Editing Chunk (HTML Snippet)'),
(1005, 1538733083, 1, 'admin', 79, '17', 'tpl_row_active', 'Saving Chunk (HTML Snippet)'),
(1006, 1538733083, 1, 'admin', 78, '17', 'tpl_row_active', 'Editing Chunk (HTML Snippet)'),
(1007, 1538733092, 1, 'admin', 5, '22', 'Почему не нужно покупать паспорт Украины', 'Saving resource'),
(1008, 1538733092, 1, 'admin', 27, '22', 'Почему не нужно покупать паспорт Украины', 'Editing resource'),
(1009, 1538733095, 1, 'admin', 26, '-', '-', 'Refreshing site'),
(1010, 1538733283, 1, 'admin', 27, '13', 'Визовая поддержка для государств ЕС, США, Канады', 'Editing resource'),
(1011, 1538733452, 1, 'admin', 5, '13', 'Визовая поддержка для государств ЕС, США, Канады', 'Saving resource'),
(1012, 1538733453, 1, 'admin', 3, '13', 'Визовая поддержка для государств ЕС, США, Канады', 'Viewing data for resource'),
(1013, 1538733526, 1, 'admin', 27, '14', 'Выезд несовершеннолетних детей за рубеж', 'Editing resource'),
(1014, 1538734411, 1, 'admin', 5, '14', 'Выезд несовершеннолетних за рубеж', 'Saving resource'),
(1015, 1538734412, 1, 'admin', 3, '14', 'Выезд несовершеннолетних за рубеж', 'Viewing data for resource'),
(1016, 1538736354, 1, 'admin', 27, '6', 'Гражданство  ПМЖ в Украине', 'Editing resource'),
(1017, 1538736407, 1, 'admin', 27, '6', 'Гражданство  ПМЖ в Украине', 'Editing resource'),
(1018, 1538736825, 1, 'admin', 5, '6', 'Гражданство  ПМЖ в Украине', 'Saving resource'),
(1019, 1538736825, 1, 'admin', 27, '6', 'Гражданство  ПМЖ в Украине', 'Editing resource'),
(1020, 1538736928, 1, 'admin', 27, '16', 'Иммиграция в Германию ', 'Editing resource'),
(1021, 1538737199, 1, 'admin', 27, '17', 'Еврейская иммиграция ', 'Editing resource'),
(1022, 1538739534, 1, 'admin', 5, '17', 'Еврейская иммиграция ', 'Saving resource'),
(1023, 1538739534, 1, 'admin', 27, '17', 'Еврейская иммиграция ', 'Editing resource'),
(1024, 1538739543, 1, 'admin', 27, '17', 'Еврейская иммиграция ', 'Editing resource'),
(1025, 1538742788, 1, 'admin', 27, '18', 'Немецкая иммиграция', 'Editing resource'),
(1026, 1538742809, 1, 'admin', 5, '18', 'Немецкая иммиграция', 'Saving resource'),
(1027, 1538742809, 1, 'admin', 27, '18', 'Немецкая иммиграция', 'Editing resource'),
(1028, 1538747412, 1, 'admin', 5, '17', 'Еврейская иммиграция ', 'Saving resource'),
(1029, 1538747413, 1, 'admin', 27, '17', 'Еврейская иммиграция ', 'Editing resource'),
(1030, 1538752285, 1, 'admin', 27, '15', 'Брачный договор/контракт', 'Editing resource'),
(1031, 1538753505, 1, 'admin', 17, '-', '-', 'Editing settings'),
(1032, 1538753553, 1, 'admin', 30, '-', '-', 'Saving settings'),
(1033, 1538753565, 1, 'admin', 17, '-', '-', 'Editing settings'),
(1034, 1538753578, 1, 'admin', 27, '6', 'Гражданство  ПМЖ в Украине', 'Editing resource'),
(1035, 1538753599, 1, 'admin', 27, '15', 'Брачный договор/контракт', 'Editing resource'),
(1036, 1538754813, 1, 'admin', 5, '15', 'Брачный договор/контракт', 'Saving resource'),
(1037, 1538754813, 1, 'admin', 27, '15', 'Брачный договор/контракт', 'Editing resource'),
(1038, 1538754951, 1, 'admin', 27, '12', 'Консультации', 'Editing resource'),
(1039, 1538755024, 1, 'admin', 5, '12', 'Консультации', 'Saving resource'),
(1040, 1538755025, 1, 'admin', 3, '12', 'Консультации', 'Viewing data for resource'),
(1041, 1538755036, 1, 'admin', 27, '11', 'Иммиграция в Великобританию', 'Editing resource'),
(1042, 1539027422, 1, 'admin', 58, '-', 'MODX', 'Logged in'),
(1043, 1539027432, 1, 'admin', 17, '-', '-', 'Editing settings'),
(1044, 1539027439, 1, 'admin', 75, '-', '-', 'User/ role management'),
(1045, 1539027443, 1, 'admin', 11, '-', 'Новый пользователь', 'Creating a user'),
(1046, 1539028144, 1, 'admin', 32, '-', 'stasgeller', 'Saving user'),
(1047, 1539028152, 1, 'admin', 8, '-', '-', 'Logged out'),
(1048, 1539028154, 2, 'stasgeller', 58, '-', 'MODX', 'Logged in'),
(1049, 1539028163, 2, 'stasgeller', 17, '-', '-', 'Editing settings'),
(1050, 1539028170, 2, 'stasgeller', 8, '-', '-', 'Logged out'),
(1051, 1539028174, 1, 'admin', 58, '-', 'MODX', 'Logged in'),
(1052, 1539028183, 1, 'admin', 75, '-', '-', 'User/ role management'),
(1053, 1539028189, 1, 'admin', 12, '2', 'stasgeller', 'Editing user'),
(1054, 1539028256, 1, 'admin', 32, '2', 'stasgeller', 'Saving user'),
(1055, 1539028257, 1, 'admin', 75, '-', '-', 'User/ role management'),
(1056, 1539028259, 1, 'admin', 8, '-', '-', 'Logged out'),
(1057, 1539028262, 2, 'stasgeller', 58, '-', 'MODX', 'Logged in'),
(1058, 1539028290, 2, 'stasgeller', 75, '-', '-', 'User/ role management'),
(1059, 1539028292, 2, 'stasgeller', 11, '-', 'Новый пользователь', 'Creating a user'),
(1060, 1539028480, 2, 'stasgeller', 32, '-', 'fregat222', 'Saving user'),
(1061, 1539028480, 2, 'stasgeller', 75, '-', '-', 'User/ role management'),
(1062, 1539028490, 2, 'stasgeller', 40, '-', '-', 'Editing Access Permissions'),
(1063, 1539028509, 2, 'stasgeller', 41, '-', '-', 'Editing Access Permissions'),
(1064, 1539028509, 2, 'stasgeller', 40, '-', '-', 'Editing Access Permissions'),
(1065, 1539028542, 2, 'stasgeller', 41, '-', '-', 'Editing Access Permissions'),
(1066, 1539028543, 2, 'stasgeller', 40, '-', '-', 'Editing Access Permissions'),
(1067, 1539028556, 2, 'stasgeller', 75, '-', '-', 'User/ role management'),
(1068, 1539028563, 2, 'stasgeller', 12, '3', 'fregat222', 'Editing user'),
(1069, 1539028570, 2, 'stasgeller', 32, '3', 'fregat222', 'Saving user'),
(1070, 1539028570, 2, 'stasgeller', 75, '-', '-', 'User/ role management'),
(1071, 1539028575, 2, 'stasgeller', 86, '-', '-', 'Role management'),
(1072, 1539028595, 2, 'stasgeller', 75, '-', '-', 'User/ role management'),
(1073, 1539028598, 2, 'stasgeller', 12, '3', 'fregat222', 'Editing user'),
(1074, 1539028603, 2, 'stasgeller', 32, '3', 'fregat222', 'Saving user'),
(1075, 1539028604, 2, 'stasgeller', 75, '-', '-', 'User/ role management'),
(1076, 1539028606, 2, 'stasgeller', 8, '-', '-', 'Logged out'),
(1077, 1539028610, 3, 'fregat222', 58, '-', 'MODX', 'Logged in'),
(1078, 1539028614, 3, 'fregat222', 76, '-', '-', 'Element management'),
(1079, 1539028667, 3, 'fregat222', 76, '-', '-', 'Element management'),
(1080, 1539028669, 3, 'fregat222', 78, '11', 'FOOTER', 'Editing Chunk (HTML Snippet)'),
(1081, 1539028671, 3, 'fregat222', 76, '-', '-', 'Element management'),
(1082, 1539028674, 3, 'fregat222', 8, '-', '-', 'Logged out'),
(1083, 1539028677, 2, 'stasgeller', 58, '-', 'MODX', 'Logged in'),
(1084, 1539028683, 2, 'stasgeller', 75, '-', '-', 'User/ role management'),
(1085, 1539028684, 2, 'stasgeller', 86, '-', '-', 'Role management'),
(1086, 1539028688, 2, 'stasgeller', 35, '2', 'Editor', 'Editing role'),
(1087, 1539028724, 2, 'stasgeller', 36, '2', 'Editor', 'Saving role'),
(1088, 1539028725, 2, 'stasgeller', 86, '-', '-', 'Role management'),
(1089, 1539028729, 2, 'stasgeller', 8, '-', '-', 'Logged out'),
(1090, 1539028732, 3, 'fregat222', 58, '-', 'MODX', 'Logged in'),
(1091, 1539182917, 2, 'stasgeller', 58, '-', 'MODX', 'Logged in'),
(1092, 1539182921, 2, 'stasgeller', 75, '-', '-', 'User/ role management'),
(1093, 1539182928, 2, 'stasgeller', 33, '1', 'admin', 'Deleting user'),
(1094, 1539182929, 2, 'stasgeller', 75, '-', '-', 'User/ role management'),
(1095, 1539183716, 2, 'stasgeller', 75, '-', '-', 'User/ role management'),
(1096, 1539183717, 2, 'stasgeller', 75, '-', '-', 'User/ role management'),
(1097, 1539183720, 2, 'stasgeller', 76, '-', '-', 'Element management'),
(1098, 1539183732, 2, 'stasgeller', 301, '9', '301 Redirects', 'Edit Template Variable'),
(1099, 1539183765, 2, 'stasgeller', 76, '-', '-', 'Element management'),
(1100, 1539184413, 2, 'stasgeller', 27, '15', 'Брачный договор/контракт', 'Editing resource'),
(1101, 1539184448, 2, 'stasgeller', 27, '12', 'Консультации', 'Editing resource'),
(1102, 1539184459, 2, 'stasgeller', 27, '11', 'Иммиграция в Великобританию', 'Editing resource'),
(1103, 1539184555, 2, 'stasgeller', 27, '16', 'Иммиграция в Германию ', 'Editing resource'),
(1104, 1539184560, 2, 'stasgeller', 27, '6', 'Гражданство  ПМЖ в Украине', 'Editing resource'),
(1105, 1539184579, 2, 'stasgeller', 27, '6', 'Гражданство  ПМЖ в Украине', 'Editing resource'),
(1106, 1539184602, 2, 'stasgeller', 27, '11', 'Иммиграция в Великобританию', 'Editing resource'),
(1107, 1539184616, 2, 'stasgeller', 5, '11', 'Иммиграция в Великобританию', 'Saving resource'),
(1108, 1539184618, 2, 'stasgeller', 3, '11', 'Иммиграция в Великобританию', 'Viewing data for resource'),
(1109, 1539184622, 2, 'stasgeller', 27, '11', 'Иммиграция в Великобританию', 'Editing resource'),
(1110, 1539185836, 2, 'stasgeller', 5, '11', 'Иммиграция в Великобританию', 'Saving resource'),
(1111, 1539185838, 2, 'stasgeller', 3, '11', 'Иммиграция в Великобританию', 'Viewing data for resource'),
(1112, 1539185841, 2, 'stasgeller', 4, '-', 'Новый ресурс', 'Creating a resource'),
(1113, 1539186273, 2, 'stasgeller', 5, '-', 'Sole Representative', 'Saving resource'),
(1114, 1539186274, 2, 'stasgeller', 27, '23', 'Sole Representative', 'Editing resource'),
(1115, 1539186325, 2, 'stasgeller', 27, '23', 'Sole Representative', 'Editing resource'),
(1116, 1539186334, 2, 'stasgeller', 5, '23', 'Sole Representative', 'Saving resource'),
(1117, 1539186335, 2, 'stasgeller', 3, '23', 'Sole Representative', 'Viewing data for resource'),
(1118, 1539186348, 2, 'stasgeller', 27, '23', 'Sole Representative', 'Editing resource'),
(1119, 1539186357, 2, 'stasgeller', 5, '23', 'Sole Representative', 'Saving resource'),
(1120, 1539186357, 2, 'stasgeller', 27, '23', 'Sole Representative', 'Editing resource'),
(1121, 1539186382, 2, 'stasgeller', 5, '23', 'Sole Representative', 'Saving resource'),
(1122, 1539186382, 2, 'stasgeller', 27, '23', 'Sole Representative', 'Editing resource'),
(1123, 1539186449, 2, 'stasgeller', 4, '-', 'Новый ресурс', 'Creating a resource'),
(1124, 1539187412, 2, 'stasgeller', 5, '-', 'Exceptional Talent', 'Saving resource'),
(1125, 1539187414, 2, 'stasgeller', 3, '24', 'Exceptional Talent', 'Viewing data for resource'),
(1126, 1539187424, 2, 'stasgeller', 27, '24', 'Exceptional Talent', 'Editing resource'),
(1127, 1539187427, 2, 'stasgeller', 5, '24', 'Exceptional Talent', 'Saving resource'),
(1128, 1539187428, 2, 'stasgeller', 3, '24', 'Exceptional Talent', 'Viewing data for resource'),
(1129, 1539244590, 2, 'stasgeller', 75, '-', '-', 'User/ role management'),
(1130, 1539244594, 2, 'stasgeller', 27, '6', 'Гражданство  ПМЖ в Украине', 'Editing resource'),
(1131, 1539244600, 2, 'stasgeller', 3, '24', 'Exceptional Talent', 'Viewing data for resource'),
(1132, 1539253667, 2, 'stasgeller', 27, '6', 'Гражданство  ПМЖ в Украине', 'Editing resource'),
(1133, 1539253671, 2, 'stasgeller', 27, '5', 'Получение статуса беженца за рубежом', 'Editing resource'),
(1134, 1539253692, 2, 'stasgeller', 27, '9', 'Помощь в получение документов из органов РАГС и архивов', 'Editing resource'),
(1135, 1539253697, 2, 'stasgeller', 27, '6', 'Гражданство  ПМЖ в Украине', 'Editing resource'),
(1136, 1539253707, 2, 'stasgeller', 27, '11', 'Иммиграция в Великобританию', 'Editing resource'),
(1137, 1539253709, 2, 'stasgeller', 27, '23', 'Sole Representative', 'Editing resource'),
(1138, 1539253713, 2, 'stasgeller', 27, '24', 'Exceptional Talent', 'Editing resource'),
(1139, 1539253736, 2, 'stasgeller', 5, '24', 'Exceptional Talent', 'Saving resource'),
(1140, 1539253737, 2, 'stasgeller', 3, '24', 'Exceptional Talent', 'Viewing data for resource');

-- --------------------------------------------------------

--
-- Структура таблицы `modx_manager_users`
--

CREATE TABLE `modx_manager_users` (
  `id` int(10) NOT NULL,
  `username` varchar(100) NOT NULL DEFAULT '',
  `password` varchar(100) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Contains login information for backend users.';

--
-- Дамп данных таблицы `modx_manager_users`
--

INSERT INTO `modx_manager_users` (`id`, `username`, `password`) VALUES
(2, 'stasgeller', '$P$Boaj.GAIDpp6juYcepQvPzkQYCdE2O1'),
(3, 'fregat222', '$P$BeTIlwQNa3I0q.E.BiVkW8NtcLRq32/');

-- --------------------------------------------------------

--
-- Структура таблицы `modx_membergroup_access`
--

CREATE TABLE `modx_membergroup_access` (
  `id` int(10) NOT NULL,
  `membergroup` int(10) NOT NULL DEFAULT '0',
  `documentgroup` int(10) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Contains data used for access permissions.';

-- --------------------------------------------------------

--
-- Структура таблицы `modx_membergroup_names`
--

CREATE TABLE `modx_membergroup_names` (
  `id` int(10) NOT NULL,
  `name` varchar(245) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Contains data used for access permissions.';

--
-- Дамп данных таблицы `modx_membergroup_names`
--

INSERT INTO `modx_membergroup_names` (`id`, `name`) VALUES
(1, 'Super Admin'),
(2, 'Admin');

-- --------------------------------------------------------

--
-- Структура таблицы `modx_member_groups`
--

CREATE TABLE `modx_member_groups` (
  `id` int(10) NOT NULL,
  `user_group` int(10) NOT NULL DEFAULT '0',
  `member` int(10) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Contains data used for access permissions.';

--
-- Дамп данных таблицы `modx_member_groups`
--

INSERT INTO `modx_member_groups` (`id`, `user_group`, `member`) VALUES
(2, 2, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `modx_site_content`
--

CREATE TABLE `modx_site_content` (
  `id` int(10) NOT NULL,
  `type` varchar(20) NOT NULL DEFAULT 'document',
  `contentType` varchar(50) NOT NULL DEFAULT 'text/html',
  `pagetitle` varchar(255) NOT NULL DEFAULT '',
  `longtitle` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(245) DEFAULT '',
  `link_attributes` varchar(255) NOT NULL DEFAULT '' COMMENT 'Link attriubtes',
  `published` int(1) NOT NULL DEFAULT '0',
  `pub_date` int(20) NOT NULL DEFAULT '0',
  `unpub_date` int(20) NOT NULL DEFAULT '0',
  `parent` int(10) NOT NULL DEFAULT '0',
  `isfolder` int(1) NOT NULL DEFAULT '0',
  `introtext` text COMMENT 'Used to provide quick summary of the document',
  `content` mediumtext,
  `richtext` tinyint(1) NOT NULL DEFAULT '1',
  `template` int(10) NOT NULL DEFAULT '0',
  `menuindex` int(10) NOT NULL DEFAULT '0',
  `searchable` int(1) NOT NULL DEFAULT '1',
  `cacheable` int(1) NOT NULL DEFAULT '1',
  `createdby` int(10) NOT NULL DEFAULT '0',
  `createdon` int(20) NOT NULL DEFAULT '0',
  `editedby` int(10) NOT NULL DEFAULT '0',
  `editedon` int(20) NOT NULL DEFAULT '0',
  `deleted` int(1) NOT NULL DEFAULT '0',
  `deletedon` int(20) NOT NULL DEFAULT '0',
  `deletedby` int(10) NOT NULL DEFAULT '0',
  `publishedon` int(20) NOT NULL DEFAULT '0' COMMENT 'Date the document was published',
  `publishedby` int(10) NOT NULL DEFAULT '0' COMMENT 'ID of user who published the document',
  `menutitle` varchar(255) NOT NULL DEFAULT '' COMMENT 'Menu title',
  `donthit` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Disable page hit count',
  `haskeywords` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'has links to keywords',
  `hasmetatags` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'has links to meta tags',
  `privateweb` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Private web document',
  `privatemgr` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Private manager document',
  `content_dispo` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-inline, 1-attachment',
  `hidemenu` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Hide document from menu',
  `alias_visible` int(2) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Contains the site document tree.';

--
-- Дамп данных таблицы `modx_site_content`
--

INSERT INTO `modx_site_content` (`id`, `type`, `contentType`, `pagetitle`, `longtitle`, `description`, `alias`, `link_attributes`, `published`, `pub_date`, `unpub_date`, `parent`, `isfolder`, `introtext`, `content`, `richtext`, `template`, `menuindex`, `searchable`, `cacheable`, `createdby`, `createdon`, `editedby`, `editedon`, `deleted`, `deletedon`, `deletedby`, `publishedon`, `publishedby`, `menutitle`, `donthit`, `haskeywords`, `hasmetatags`, `privateweb`, `privatemgr`, `content_dispo`, `hidemenu`, `alias_visible`) VALUES
(2, 'document', 'text/html', 'Главная', 'Главная', '', 'главная', '', 1, 0, 0, 0, 0, '', '<p><img style=\"float: left;\" src=\"assets/images/myPhoto.jpg\" alt=\"\" width=\"293\" height=\"309\" /></p>\r\n<p style=\"text-align: left;\">&nbsp;</p>\r\n<p style=\"text-align: left;\">Я, Игорь Геллер, как иммиграционный адвокат, начал работу в области иммиграции с февраля 1998 года.</p>\r\n<p style=\"text-align: left;\">&nbsp;</p>\r\n<p style=\"text-align: left;\">Для многих, осуществить свою мечту легально иммигрировав, возможно только при наличии квалифицированной юридической поддержки, т.к. любая иммиграция и эмиграция это, в первую очередь, юридический процесс. Я уверен, что могу помочь Вам и членам Вашей семьи, потому что помог уже не одной тысяче людей за свою 20-летнюю практику.</p>\r\n<p style=\"text-align: left;\">&nbsp;</p>\r\n<p style=\"text-align: left;\">&nbsp;</p>\r\n<p style=\"text-align: left;\">&nbsp;</p>\r\n<p style=\"text-align: left;\">&nbsp;</p>\r\n<p style=\"text-align: left;\">&nbsp;</p>\r\n<p style=\"text-align: left;\">&nbsp;</p>\r\n<p style=\"text-align: left;\">&nbsp;</p>\r\n<p style=\"text-align: left;\"><strong>Вы можете&nbsp; получить достаточно широкий спектр услуг в следующих сферах</strong>:</p>\r\n<ul>\r\n<li style=\"text-align: left;\">Иммиграция&nbsp;из Украины, РФ, РБ и иных государств бывшего СССР в государства ЕС, США, Канады, Австралии;</li>\r\n<li style=\"text-align: left;\">Широкая и квалифицированная визовая поддержка;</li>\r\n<li style=\"text-align: left;\">Получение статуса беженца;</li>\r\n<li style=\"text-align: left;\">Иммиграция&nbsp;в Украину;</li>\r\n<li style=\"text-align: left;\">Образование в Канаде, Чехии, Великобритании</li>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<p><strong>При осуществлении работы я руководствуюсь принципом:</strong>&nbsp;Индивидуальный подход, особое внимание к деталям и конфиденциальность.</p>\r\n<p>За годы работы была наработана безукоризненная репутация. Я и мои партнеры зарекомендовали себя экспертами в сфере иммиграции, жизни и ведения бизнеса в различных государствах Евросоюза, США, Канады и Украины. А в вопросах получения статуса беженца нам нет равных не только на территории бывшего СССР, но и за рубежом. Наш опыт позволяет нам не только продавать юридические услуги, но также делиться с Вами полезной актуальной информацией по действующему иммиграционному и эмиграционному законодательству многих государств, а также многим другим аспектам жизни при осуществлении иммиграции и эмиграции. Такую информацию Вы найдете на этом сайте в разделе&nbsp;&laquo;Новости&raquo;&nbsp;и на тематических страницах.&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Почему стоит обратиться к нам?</strong></p>\r\n<ul>\r\n<li>Я и моя команда - узкие специалисты, так как занимаемся только вопросами иммиграции и эмиграции, но это позволяет нам всегда держать руку на пульсе и обладать самой полной и актуальной информацией в этой области.&nbsp;</li>\r\n<li>Мы отдаём себе отчёт в том, что не существует совершенно одинаковых дел, и двух похожих судеб. Поэтому каждому нашему клиенту гарантирован индивидуальный подход, особое внимание к деталям и конфиденциальность обращения.</li>\r\n<li>Мы не делаем различий между клиентами, но мы единственные, которые предлагают квалифицированную юридическую помощь при осуществлении иммиграционных услуг во многих государствах мира.</li>\r\n<li>Мы пытаемся всегда, вникнуть в проблематику клиента, найти самый оптимальный, действенный и относительно недорогой способ для осуществления легальных иммиграции и эмиграции.&nbsp;</li>\r\n<li>Именно мы в далеком 1999 году участвовали в составлении и написании ставшего уже классикой иммиграционной литературы &laquo;Практического справочника по иммиграции&raquo;. В дальнейших томах этого справочника мы существенно расширили рассмотрение ключевых вопросов по иммиграции в различных государствах мира. Кроме того, сам адрес нашего головного офиса, указанного во 2 томе справочника с 2000 года остается неизменным, а именно ул. Чайковская 17, кв.2, г. Харьков, Украина. Всего же было издано с 1999 по 2002 год 4 тома справочника. И очень многие люди воспользовались практическими советами этого иммиграционного пособия.</li>\r\n<li>Практическая работа моя и моей команда позволяет практически гарантированно не просто выехать в государство и легально иммигрировать, а также получать всегда квалифицированную, своевременную и необходимую помощь нашим клиентам независимо от времени суток, состояния дела и государства.</li>\r\n</ul>', 1, 5, 0, 1, 1, 1, 1523718878, 1, 1537220985, 0, 0, 0, 1523718948, 1, 'Главная', 0, 0, 0, 0, 0, 0, 0, 1),
(3, 'document', 'text/html', 'Контакты', 'Контакты', '', 'contact_us', '', 1, 0, 0, 0, 0, '', '', 1, 6, 12, 1, 1, 1, 1523718942, 1, 1523718942, 0, 0, 0, 1523718942, 1, 'Контакты', 0, 0, 0, 0, 0, 0, 0, 1),
(4, 'document', 'text/html', 'Новости', 'Новости', '', 'news', '', 1, 0, 0, 0, 0, '', '', 1, 8, 1, 1, 1, 1, 1523721700, 1, 1537814031, 0, 0, 0, 1525182061, 1, '', 0, 0, 0, 0, 0, 0, 0, 1),
(5, 'document', 'text/html', 'Получение статуса беженца за рубежом', 'Получение статуса беженца за рубежом', '', 'refugee_status', '', 1, 0, 0, 0, 0, '', '<p>Недавно опубликованные результаты социологического исследования показывают, что 1/3 граждан Украины не видят себя и своих детей, проживающими в Украине и хотели бы иммигрировать.</p>\r\n<p>Почти столько же не имеют достойной работы. Т.е. та работа, которую они сейчас выполняют не отвечает их жизненным желаниям или возможностям.</p>\r\n<p>В значительной мере, надежды граждан Украины на реформы после Майдана не оправдываются. И это на фоне ухудшения экономической ситуации.</p>\r\n<p>Борьба власти с коррупцией носит больше декларативный характер, рассчитанный на западных партнеров. Практическое отсутствие реформ, обнищание населения, подорожание практически всего и, в первую очередь, продуктов питания и коммунальных платежей при глобальном падении доходов у населения &ndash; все это подталкивает на отъезд даже тех, кто ранее не думал уезжать и надеялся на лучшее.</p>\r\n<p>Безвизовый режим с ЕС (либерализация визового режима, если точнее выражаться), ничего глобально не меняет и не изменит. Многие этого не понимают. Безвизовый режим, в основном, дает возможность посетить страны Евросоюза как турист. В некоторых государствах, в частности Польше, Венгрии, Чехии, Финляндии, Греции, Испании, Кипре и Португалии дает возможность некоторое время работать.</p>\r\n<p>Поэтому для перспектив иммиграции безвизовый режим ничего не меняет. Получить ВНЖ, ПМЖ или гражданство в благополучной стране легче не станет.</p>\r\n<p>Кратко проанализируем доступные варианты:</p>\r\n<p><b>Трудоустройство.</b> Найти легальную работу в ЕС совсем не просто, поскольку рынок труда в благополучных странах достаточно насыщен. Перспективы успешного получения ПМЖ и гражданства не очевидны. Посмотрим на примере польской рабочей визы, являющейся, на первый взгляд, неплохим вариантом. ОДНАКО, виза на 6 месяцев, хотя и с возможностью продления. По ней, согласно Директиве ЕС нельзя работать на территории иных государств ЕС, если только это работа не связана с пуско-наладочными и/или монтажными работами. Т.е. поработать дают. ВНЖ на это время тоже дают. Но ПМЖ только в очень долгосрочной перспективе. О возможности получить гражданство &ndash; лучше помолчать. <b>Создание бизнеса.</b> Открывать компанию и получить первый краткосрочный ВНЖ, обычно на год, в принципе не так уж сложно и дорого. Но вот дальше придется нести значительные расходы связанные, как минимум, с наймом сотрудников и уплатой налогов. Хорошо, если созданная компания будет работать и приносить доход. Но в большинстве случаев, построить самоокупаемый бизнес не получается по многим причинам. И придется немало лет нести указанные затраты, иначе продления ВНЖ не будет. Добавьте к этому затраты на жильё, питание, медицину, обучение детей. Их нужно нести из собственных сбережений, поскольку трудится и зарабатывать деньги можно только управляя своей компанией. А перспектива ПМЖ, а тем более гражданства, возможна по прошествии многих лет. Пожалуй, самый дорогой вариант, нереальный для большинства. Про инвестирование и, на этом основании, получение ПМЖ &ndash; говорить не хочется, т.к. планка начинается от 500.000 евро и выше. Уверен, что, те, кто может себе позволить инвестирование, не входят в эти 30 %. Закрепиться в благополучной стране через <b>получение образования</b> тоже достаточно сложны организационно и затратно финансово. Особенно если речь идёт о семье с детьми, а не молодом одиночке или паре без детей. Перспективы ПМЖ, гражданства не близкие. <b>Профессиональная иммиграция</b> по различным программам в Канаде. Условия &ndash; возраст, образование, опыт работы по специальности, сбережения/финансы, полученные на родине, знание языка не ниже IELTS 5.0, а то и 6.5. ну и специальность в списке востребованных. Схожая иммиграционная программа в Австралию еще более непредсказуема, чем в Канаду. Всем требованиям соответствует не больше 2-3 % из желающих навсегда покинуть Украину. Срок рассмотрения минимум от 1 годы (я не говорю о подготовке на подачу, который может потянуть еще год-полтора, в течение которого готовятся и сдают языковый тест, собирают и переводят документы и т.д.). Либо иммиграция в эти государства через образование. Но стоимость от 6.000 до 15.000 (иногда и выше) долларов за само обучение + от 15.000 долларов в год. Это за проживание 1 человека. Так что сумма на семью может начинаться от 35.000-40.000 долларов в год. Про образование, и, на этом основании получении в дальнейшем ПМЖ, может начинаться от 60.000 долларов в год и выше. Исключение - получение образование в некоторых государствах ЕС. Но придется искать работу после получения образования и получать разрешение на работу. До получения ПМЖ может пройти от 5 лет. В основном (!!!) время обучения в стаж для получения не засчитывается. В некоторых государствах засчитывается, но в соотношении 2 года обучения = 1 году проживания. Остаются еще <b>еврейская или немецкая иммиграция в ФРГ</b>. Но условие &ndash; немецкие или еврейские родители + знание языка. Вопрос - много ли найдется граждан Украины с &laquo;правильной национальностью&raquo; в свидетельстве о рождении? <b>Гражданство Польши, Венгрии, Румынии.</b> Даже останавливаться не хочется, т.к. сейчас очень много пишут, как и каким образом было получено это гражданство и как сейчас отменяют эти решения. Хотя есть, реальные счастливчики. Но, опять же, много подделок, по которым решения отменяют, а людей&hellip; выпроваживают с &laquo;волчьим билетом&raquo;. <b>Лотерея &laquo;</b><b>Green </b><b>Card&raquo;</b> - отличная вещь, НО&hellip; лотерея. Либо выиграешь &ndash; либо нет. Выиграл &ndash; отлично. Есть шанс. Не выиграл. Ждем следующего сезона. Сколько это может продолжаться &ndash; не известно. Плюс один &ndash; низкая начальная стоимость. Еще можно стать <b>лауреатом престижных конкурсов по музыке</b> или <b>писать картины и выставляться по всему миру.</b> Или <b>спортсменом</b> самого высокого уровня. Но такие программы для большинства более чем экзотичны. Ну и конечно зарегистрировать <b>брак с иностранцем</b>. Куда же без этого? О сложностях и нюансах материала на различных форумах масса. Тоже не массово доступный способ уехать, если быть кратким. Много позитивных и счастливых пар. Но, ОЧЕНЬ много ситуаций, когда последние не привели к счастливой семейной жизни. При том, что нужно учитывать массу факторов. В частности другое государство, другой менталитет, традиции, образ жизни, иное законодательство и т.д. О разводах и о последствиях этих разводов мне можно уже даже не повести писать, а романы.</p>\r\n<p>&nbsp;</p>\r\n<p>И что мы имеем на сегодняшний день? Сколько программ, позволяющих выехать и очень сильно любить Украину из &laquo;прекрасного далека&raquo;, став, сначала резидентом, а потом гражданином весьма уважаемого в мире государства?</p>\r\n<p>То, что могут уехать далеко не 30 % жителей Украины &ndash; уже ясно всем. А что тогда можно предпринять?</p>\r\n<p>Как и раньше, самым доступным способом иммигрировать продолжает оставаться <b>запрос убежища в демократической стране</b>. Самое важное, что даёт этот вариант &ndash; решение основных интеграционных проблем (жилье, средства к существованию, изучение языка) с первого дня нахождения в стране.</p>\r\n<p>Кроме того, находясь в БЛАГОПОЛУЧНОЙ, демократической стране <b><u>на процессе</u></b> (т.е. не получив еще статуса) человек уже имеет ряд позитивных моментов, которые он никогда себе не мог бы позволить по другим иммиграционным программам. Он и члены его семьи постепенно изучают язык. Им предоставляется жилье. Дети ходят в садик, школу, а, иногда и колледж. Получение бесплатного медицинского страхования. Именно получение, а не декларирование, как в Украине. Все это оплачивает государство, которое принимает беженцев. И это для людей, пока только находящихся на процессе.</p>\r\n<p>Даже при предварительном подсчете все это, при обычной иммиграции, обошлось бы семье не менее 20-30 тысяч долларов США в год (в зависимости от состава семья).</p>\r\n<p><b>После получения убежища</b>, заявитель получает права аналогичные гражданам страны, за исключением избирательного и некоторых других. Появляется доступ к программам профориентации, возможности получения кредитов на свой бизнес. Например, в Британии, где высшее образование платное, принятый беженец может поступать в ВУЗ, пользуясь кредитом по той же системе, как граждане UK. Эти кредиты предоставляются на весьма льготных условиях (по %, первой выплате и общему сроку выплат).</p>\r\n<p>Если человек хочет изменить свою профессию, то и это тоже возможно сделать бесплатно. Достаточно часто заявителям, с целью адаптации в государстве, предоставляется возможность бесплатно ходить всей семьей, например в бассейн, гольф-клуб, музеи, выставки, на футбольные или хоккейные матчи и т.д.</p>\r\n<p>За время нахождения на процессе, люди постепенно изучают законодательство государства (в т.ч. иммиграционное и налоговое), обрастают связями и знакомствами, изучают традиции и обычаи данного государства. И это, заметьте, без затрат собственных финансовых ресурсов, которые имеют свойство быстро заканчиваться. А при незнании законодательства, людей, специфики государства финансы заканчиваются в 2-3 раза быстрее. И это, как правило, без возможности определенное время (от 6 месяцев с момента въезда) обращаться за финансовой помощью в государственные социальные фонды.</p>\r\n<p>В ряде государств заявителю после определенного времени рассмотрения его кейса предоставляется право на работу.</p>\r\n<p>В ряде государств заявителям предоставляется право на работу, но без права на получение социальных благ.</p>\r\n<p>Т.е. очень важно понимать, что, как и каким образом можно получить заявителю на убежище.</p>\r\n<p>Тема получения убежища была очень актуальна сразу после развала СССР и в нулевых годах 21 века.</p>\r\n<p>На этой теме обогатилось достаточно много людей, занимающихся откровенным обманом. Они рассказывали по &laquo;райские кущи&raquo; в Европе, США и Канаде не особо вдаваясь в особенности въезда, подачи заявления, процедуры рассмотрения, подачи апелляции.</p>\r\n<p>Все концентрировалось исключительно вокруг подачи документов. Человеку давались 3-4 справки из медучереждении о том, что он был избит. Еще 2-4 справки о том, что он обращался в органы внутренних дел и прокуратуру. Бралась одна из 4-х историй из нашего &laquo;Практического справочника по иммиграции&raquo; (которые мы писали с 1999 по 2002 год). Переписывалась под человека и &hellip; вуаля, все готово. Единственное о чем не говорили эти дельцы, что в этих справочниках были написаны проигрышные истории. Итого всегда один &ndash; отказ в иммиграционной службе. Отказ в суде по апелляции. Отказ в повторной апелляции. И &hellip; высылка или депортация с возвращением на родину.</p>\r\n<p>И если бы это были единичные случаи. Все это приобрело массовый характер. Я знаю не одну сотню случаев, когда водитель автобуса или проводник поезда за 3-4 тысячи долларов США толкали эти истории и документы своим пассажирам, убеждая последних, что статус беженца они с такими &laquo;отличными документами&raquo; получат. А все оказывалось именно так как писалось выше. В США вообще обращаются к местным адвокатам, за эти же деньги в надежде, что они местные и помогут им выиграть дело. Однако, практически никто не задается вопросом, как и каким образом местный иммиграционный адвокат может досконально знать ситуация в государстве клиента, т.е. то, от чего заявитель бежал.</p>\r\n<p>В настоящее время многие стремятся за получением статуса беженца в социально более благополучные государства, такие как Швеция, Норвегия, Финляндия, Дания. Это понятно. Там &laquo;социалка&raquo; лучше чем в ФРГ, Великобритании, Франции. Да социальная помощь в государстве для заявителей это хорошо. Но важно поставить себе один единственный вопрос: &laquo;Зачем я хочу получить для себя и членов своей семьи статус беженца?&raquo;. Затем чтобы некоторое время получать социальную помощь в благополучной стране, а потом, после всех отказов вернуться домой? Или цель несколько иная? Получить статус беженца для себя и членов своей семьи при чуть меньших социальных преференциях. Потом получить ПМЖ и, наконец, гражданство соответствующего благополучного государства. Может именно эта цель основная, на которой нужно сконцентрироваться и приложить все усилия для ее достижения? Может именно для этого человек и его семья покинули или собираются покинуть свое место жительство, устоявшийся уклад жизни, своих близких и друзей? Может именно это он хочет дать своим детям?</p>\r\n<p>Еще один важный аспект при работе и надежде на самых лучших иммиграционных адвокатов в стране заявления. Они никогда не будут ни готовить клиентов к прохождению интервью, с целью получения позитива, ни что-то в этом плане им подсказывать. Они имеют право только консультировать и немного корректировать историю клиента. Если они будут заниматься тем, о чем я сказал выше, то очень скоро лишаться своей лицензии, т.к. чем больше выигранных дел, тем больше увеличивается нагрузка на бюджет государства, т.к. беженцу положены весьма неплохие преференции от государственного и местного бюджета. Они работают исключительно на сам процесс, а не на выигрыш. Это специфика местных иммиграционных адвокатов, о которой они не распространяются, т.к. потеряют достаточно большое количество клиентов. Наших же людей интересует не столько сам процесс с непонятными последствиями, сколько прогнозируемый положительный результат. Это далеко не вся правда о которой местные иммиграционные не рассказывают. Вот например. Знаете ли вы о том, что весьма желательно для получения позитивного результата предоставлять информацию о государстве гражданской принадлежности заявителя. Что это за информация, откуда она берется, что в ней указывается, кто и как предоставляет &ndash; об этом местные адвокаты тоже не говорят.</p>\r\n<p>Зная такой подход со стороны иммиграционных адвокатом на местах в государствах заявления, к ситуации самого заявителя (прошу прощение за тавтологию), готов фактически гарантировать, что 80-90 % людей пересмотрели бы свой подход к ситуации, когда они надеются на какую-либо помочь от данных адвокатом.</p>\r\n<p>В настоящее время большое количество людей почему-то считают либо что статус беженца гражданам Украины дают всем из-за войны на Донбассе или аннексии Крыма. Либо считают, что из-за того, что они не хотят служить в армии (я имею ввиду всеобщую воинскую обязанность), то это так же основание для получения статуса беженца.</p>\r\n<p>На сегодняшний день у потенциальных желающих иммигрировать сложилось два диаметрально противоположных мнения. Одни считают, что статус беженца получить не возможно, т.к. Украину признали демократическим государством. Другие считают, что получить его на сегодняшний день проще простого. Не правы ни первые, ни вторые, т.к. истина посередине.</p>\r\n<p>&nbsp;</p>\r\n<p>Вопрос спасения от войны в Донбассе решается переездом в другую часть страны, это называют &laquo;внутренним убежищем&raquo;. Им воспользовались сотни тысяч людей. С точки зрения международного права, нет никаких оснований прибегнуть к защите других государств по причине конфликта на Донбассе. Ни у жителей зоны конфликта, ни у жителей остальной части Украины. Поэтому основание &laquo;война на Донбассе&raquo; - не проходит, это 100 % отказ. Ситуация с Крымом &ndash; такая же.</p>\r\n<p>Но это не означает, что другие причины не могут стать основаниями для получения статуса беженца. Статус беженца (убежище) дают и будут давать, пока действует <b>Женевская конвенция</b> (Конвенция о статусе беженцев принята 28 июля 1951 года в Женеве, даёт определения понятия &laquo;<a href=\"https://ru.wikipedia.org/wiki/%D0%91%D0%B5%D0%B6%D0%B5%D0%BD%D0%B5%D1%86\">беженец</a>&raquo;, устанавливает общие основания предоставления статуса беженца). Конвенция запрещает какую-либо дискриминацию в отношении беженцев. Частью прав беженцы пользуются наравне с гражданами принимающей их страны, частью - на тех же условиях, что и иностранцы. Конвенция запрещает возвращение беженцев в государство, из которого они бежали, опасаясь преследования).</p>\r\n<p>Вопрос лежит в обосновании претензий, т.е. заявления на получение убежища. И вот с этим всё совсем не просто.</p>\r\n<p>Украина действительно проделала немалый путь, реформируя законодательство по демократическим стандартам. С юридической точки зрения в стране доступны все способы защиты прав человека. В том числе и возможность обращения в Европейский суд по правам человека. Более того, после Майдана начаты реформы всех правоохранительных систем МВД, прокуратуры и судебной власти. Работают неправительственные правозащитные организации. Это позволяет иммиграционным службам отказывать гражданам Украины в предоставлении убежища, указывая, что заявитель не исчерпал возможности внутренней защиты.</p>\r\n<p>В таких условиях добиться позитива (статуса беженца) можно только:</p>\r\n<p>- при должном уровне юридической, информационной и психологической подготовки;</p>\r\n<p>- при грамотном сопровождении заявителя, его действий и дела &laquo;в точке прибытия&raquo;.</p>\r\n<p>Этому невозможно научиться по книгам и справочникам. Кроме, того всегда нужно учитывать индивидуальную ситуацию и личность заявителя, его положение и др. факторы, влияющие на результат. Одни факторы нужно отбросить, а другие оставить, выделить.</p>\r\n<p>Иммиграционное законодательство в любом государстве изменяется постоянно. Оно не стоит на месте. С момента написания последнего тома нашего справочника в 2002 году, иммиграционное законодательство очень сильно поменялось. В том числе и в вопросах получения статуса беженца. Сейчас трудно найти компаний, которые серьезно и достаточно долго занимаются этой проблематикой.</p>\r\n<p>&nbsp;</p>\r\n<p>Подведя итог, хочу отметить: на мой взгляд, получение статуса беженца - достаточно демократичный вид иммиграции, с большими возможностями и перспективами при малых затратах, при минимальных сроках попадания в страну. Да, сроки рассмотрения дела заявителя составляют от 6 месяцев до 3,5 лет, но это уже после въезда в страну и находясь &laquo;на обеспечении&raquo; государства и при ЛЕГАЛЬНОМ нахождении в последнем.</p>\r\n<p>А на вопрос: какова вероятность получить статус беженца в демократическом государстве для обычного человека и членов его семьи, для граждан Украины (РФ, РБ и иных государств б. СССР)? Отвечаю: при желании самого заявителя получить этот статус - практически 99,99%.</p>\r\n<p>На сегодняшний день (с 2000 года) положительно прошли процесс уже более 100 семей.</p>', 1, 7, 2, 1, 1, 1, 1523721794, 1, 1537219946, 0, 0, 0, 1525182163, 1, '', 0, 0, 0, 0, 0, 0, 0, 1),
(6, 'document', 'text/html', 'Гражданство  ПМЖ в Украине', 'Разница между постоянным видом на жительство в Украине и гражданством Украины.', '', 'citizenship_and_permanent_residence_in_ukraine', '', 1, 0, 0, 0, 1, '', '<p><strong>Основные нюансы</strong></p>\r\n<p><br />Получение постоянного места жительство в Украине либо получение гражданства Украины возможно только при наличии определенных оснований.<br />В первом случае основным нормативным актом действующего законодательства Украины является Закон Украины &laquo;Об иммиграции&raquo;. Во втором &ndash; закон Украины &laquo;О гражданстве&raquo;<br />Давайте их рассмотрим по порядку</p>\r\n<p><br /><span style=\"text-decoration: underline;\"><strong>ПМЖ в Украине:</strong></span></p>\r\n<p><br />Постоянный вид на жительство &ndash; документ (по-украински посвідка на постійне проживання), удостоверяющий личность иностранного гражданина либо лица без гражданства&nbsp; и подтверждающий получение им разрешения на постоянное проживание в Украине.<br />В Украине на сегодняшний день ПМЖ можно получить вне квоты и по квоте</p>\r\n<p><span style=\"text-decoration: underline;\">Разрешение на иммиграцию вне квоты могут получить иностранцы:</span></p>\r\n<ul>\r\n<li>разрешение на иммиграцию вне квоты могут получить иностранцы если:</li>\r\n<li>один из супругов, если второй из супругов, с которым он/она находится в браке свыше двух лет, является гражданином Украины;</li>\r\n<li>дети или родители граждан Украины;</li>\r\n<li>лица, имеющим право на приобретение гражданства Украины по территориальному происхождению;</li>\r\n<li>лица, которые являются опекунами или попечителями граждан Украины, или находятся на попечении или под опекой граждан Украины;</li>\r\n<li>лица, иммиграция которых представляет государственный интерес для Украины;</li>\r\n<li>иностранные украинцы, семья иностранных украинцев, их дети в случае их общего въезда и пребывания на территории Украины.</li>\r\n</ul>\r\n<p><span style=\"text-decoration: underline;\">Разрешение на иммиграцию по квоте могут получить иностранцы:</span></p>\r\n<ul>\r\n<li>деятели науки и культуры, иммиграция которых отвечает интересам Украины;</li>\r\n<li>высококвалифицированные специалисты и рабочие, острая потребность в которых является ощутимой для экономики Украины;</li>\r\n<li>лица, которые осуществили иностранную инвестицию в экономику Украины иностранной конвертируемой валютой на сумму не менее 100 (ста) тысяч долларов США, зарегистрированную в соответствующем порядке;</li>\r\n<li>лица, которые являются полнородными братом или сестрой, дедушкой или бабушкой, внуком или внучкой граждан Украины;</li>\r\n<li>лица, которые раньше находились в гражданстве Украины;</li>\r\n<li>родители, муж (жена) иммигранта и его несовершеннолетние дети;</li>\r\n<li>лица, которые непрерывно прожили на территории Украины в течение трех лет со дня предоставления им статуса беженца в Украине или убежища в Украине, а также их родители, муж (жена) и несовершеннолетние дети, которые проживают вместе с ними;</li>\r\n<li>лица, которые прослужили в Вооруженных Силах Украины три и больше лет.</li>\r\n</ul>\r\n<p>Иностранцы, находящиеся легально на территории Украины и подавшие документы в органы ГМС Украины по месту предполагаемой регистрации, постоянный вид на жительство в Украине&nbsp;оформляется одновременно с разрешением на иммиграцию.&nbsp;<br />Иностранцам, находящиеся за пределами Украины подают необходимые документы в одно из<br />дипломатических представительств Украины за рубежом для получения&nbsp; разрешения на<br />иммиграцию . После чего украинское дипломатическое представительство оформляет</p>\r\n<p>иммиграционную визу сроком на один год. После получения удостоверения на постоянное<br />проживание, обновление данной визы не требуется, поскольку с момента получения ПМЖ<br />иностранец получает право безвизового въезда в Украину и пребывания на территории<br />государства без ограничений во времени.<br />Иностранцы, имеющие ПМЖ в Украине обладают практически всеми правами граждан Украины, кроме избирать, быть избранными, работать в госструктурах.<br />Кроме этого, иностранец получает право на работу без специального разрешения и приобретает статус резидента Украины в соответствии с действующим законодательством Украины.</p>\r\n<p><span style=\"text-decoration: underline;\"><strong>Гражданство Украины</strong></span></p>\r\n<p><br />Гражданство &mdash; это устойчивая правовая связь между физическим лицом и государством,<br />обусловленная наличием у сторон взаимных прав и обязанностей.</p>\r\n<p>Основаниями для получения гражданства Украины являются:</p>\r\n<ul>\r\n<li>по территориальному происхождению;</li>\r\n<li>по рождению;</li>\r\n<li>вследствие установления опеки, попечительства;</li>\r\n<li>вследствие принятия в гражданство Украины;</li>\r\n<li>вследствие восстановления в гражданстве Украины;</li>\r\n<li>вследствие гражданства Украины одного или обоих родителей;</li>\r\n<li>принадлежность к гражданству Украины.</li>\r\n</ul>', 1, 7, 3, 1, 1, 1, 1523721815, 1, 1538736825, 0, 0, 0, 1525182325, 1, '', 0, 0, 0, 0, 0, 0, 0, 1),
(8, 'document', 'text/html', 'Документы и анкеты', 'Документы и анкеты', '', 'documents_and_questionnaires', '', 1, 0, 0, 0, 0, '', '<p>бла&nbsp;бла&nbsp;бла&nbsp;бла&nbsp;бла&nbsp;бла&nbsp;бла&nbsp;бла&nbsp;</p>', 1, 7, 4, 1, 1, 1, 1523721860, 1, 1527017891, 0, 0, 0, 1525183162, 1, '', 0, 0, 0, 0, 0, 0, 0, 1),
(9, 'document', 'text/html', 'Помощь в получение документов из органов РАГС и архивов', 'Помощь в получение документов из органов РАГС и архивов', '', 'documents_receiving', '', 1, 0, 0, 0, 0, '', '', 1, 7, 5, 1, 1, 1, 1523721883, 1, 1525183116, 0, 0, 0, 1525183116, 1, '', 0, 0, 0, 0, 0, 0, 0, 1),
(11, 'document', 'text/html', 'Иммиграция в Великобританию', 'Иммиграция в Великобританию (виза жены, граждане ЕС, Exceptional Talent, Sole Representative)', '', 'immigration_to_the_uk', '', 1, 0, 0, 0, 1, '', '<p>Страница на стадии разработки</p>', 1, 7, 6, 1, 1, 1, 1523721928, 2, 1539185836, 0, 0, 0, 1525183057, 1, '', 0, 0, 0, 0, 0, 0, 0, 1),
(12, 'document', 'text/html', 'Консультации', 'Консультации', '', 'consultations', '', 1, 0, 0, 0, 0, '', '<p>Вы живете в своей стране и хотите перебраться в другое государство. Иммигрировать или просто съездить в гости или выехать по соответствующей визе. Сделать это не всегда легко. Независимо от профессии, статуса и уровня достатка. В поисках информации о том, как и каким образом все это сделать, человек тратит большую часть своего времен. Если вам повезет встретить нас, то на этом можно остановиться. Мы работаем с конца 90-х годов, поэтому разбираемся в юридических тонкостях получения иммиграционных программ, получения виз, а также получения статуса беженца.<br />Как и какие документы необходимо подавать на визу, как нужно вести себя на интервью в посольстве, если оно необходимо, при досмотре в аэропорту, жить в лагере для беженцев, подавать апелляцию в суд и как получить ПМЖ или<br />гражданство.<br />Если по каким-то причинам мы чего-то незнаем, то либо честно говорим об этом либо стараемся узнать для клиента в минимальные сроки, но с точным подтверждением информации.<br />Мы сводим к минимуму вероятность отказа по визе либо сам стресс от столкновения с реальностью иммиграции.</p>', 1, 7, 7, 1, 1, 1, 1523721946, 1, 1538755024, 0, 0, 0, 1525182864, 1, '', 0, 0, 0, 0, 0, 0, 0, 1),
(13, 'document', 'text/html', 'Визовая поддержка для государств ЕС, США, Канады', 'Визовая поддержка для государств ЕС, США, Канады', '', 'visa_support', '', 1, 0, 0, 0, 0, '', '<p>На сегодняшний день многие наши граждане выезжают за рубеж. Это связано и с бизнесом, и с туризмом, и с учебой, и с браком и т.&nbsp;д. При подаче документов в посольства соответствующего государства всегда существует вероятность отказа в получении соответствующей визы, если что-либо было нарушено заявителем на получение соответствующей визы. В ряде случаев анкеты и документы подаются в бумажном виде. В других случаев только через Интернет либо путем сканирования при подаче. В ряде случаев с заявителем/заявителями на визу проводят интервью (еврейская или немецкая иммиграция в Германию и т.д.). В других случаях просто рассматривают поданные документы (Канада, Великобритания и т.д.). Кроме того, посольства и иммиграционные службы крайне негативно относятся к документам, имеющим статус подложные/ фальшивых документов. Мало того, при выявлении данного факта либо при сокрытии важной информации человек рискует получить пожизненный запрет на въезд не только в государство, в посольство которого были поданы документы, но и попасть в список лиц, пребывание которых на территории Евросоюза и не только нежелательно. На основании иммиграционного опыта и практики (с 1998 года) мы оказываем поддержку в проверке, анализе и подготовке документов, а так же консультациях, которые смогут существенно снизить риск отказа в получении соответствующего как соответствующего типа визы в посольствах, так же и при подаче в иммиграционные службы соответствующего государства.</p>\r\n<p><strong>Обращаясь к нам вы получаете:</strong></p>\r\n<ol>\r\n<li>Все подаваемые документы будут надлежащим образом проверены;</li>\r\n<li>До подачи в визовый отдел посольства заявитель/заявители получат предполагаемый прогноз относительно вынесения позитивного либо негативного решения по подаваемым анкетам и документам, а так же по самой ситуации;</li>\r\n<li>В случае предполагаемого негативного решения визового отдела по ситуации и документам заявителя будет предложен самый оптимальный вариант решения проблемы, если таковая действительно существует;</li>\r\n<li>В случае предполагаемого интервью заявитель не только получит исчерпывающую информацию по его прохождению, но и как себя вести, в какой одежде приходить, т.е. наиболее полный объем информации, позволяющей произвести на сотрудника визового отдела убедительное и позитивное мнение о заявителе, а так же о его/ее целях поездки.</li>\r\n</ol>', 1, 7, 8, 1, 1, 1, 1523721969, 1, 1538733452, 0, 0, 0, 1525182403, 1, '', 0, 0, 0, 0, 0, 0, 0, 1),
(14, 'document', 'text/html', 'Выезд несовершеннолетних за рубеж', 'Выезд несовершеннолетних  за рубеж', '', 'departure_of_minor_abroad', '', 1, 0, 0, 0, 0, '', '<p>В настоящее время уже многое сделано в Украине для того, чтобы решить вопрос о временном выезде ребенка/детей за рубеж. Это и существенное упрощение процедуры. И возможность вывоза ребенка без согласия второго родителя, и самостоятельное путешествие ребенка в сопровождении сотрудника авиакомпании, если планируется перелет ребенком и т.д. Но существуют иные государства бывшего СССР, где не такие либеральные подходы в законодательстве. Поэтому постараюсь остановиться и рассказать более подробно на эту тему. Вывоз несовершеннолетнего ребенка достаточно сложные вопросы, требующие квалифицированного подхода, т.к. касаются выезда за рубеж, а значит, они несут в себе особенный подход в решении.</p>\r\n<p>Выезжающие родители сталкиваются с такими проблемами:</p>\r\n<ul>\r\n<li>Получение разрешения на вывоз несовершеннолетнего ребенка;</li>\r\n<li>Получение разрешения на выезд у родителя ребенка;</li>\r\n<li>Получения решения суда об усыновлении (удочерении) детей;</li>\r\n<li>Подготовка, подача иска и признание лица безвестно отсутствующим.</li>\r\n</ul>\r\n<p>Люди понимают, что эти вопросы регулирует национальное законодательство, но они не понимают, к сожалению, что за рубежом все эти документы могут иметь в будущем очень важное значение, как для них самих, так и для их детей. Нарушение процедуры получения какого-либо разрешения ведет в дальнейшем к серьезным потерям и последствиям, как моральным, так и материальным.</p>\r\n<p><strong>Получение разрешения на вывоз ребенка</strong></p>\r\n<p>Давайте вначале рассмотрим процедуру получения разрешения на вывоз несовершеннолетнего.</p>\r\n<p>При вывозе за рубеж на ПМЖ несовершеннолетнего ребенка одним из родителей, посольство (консульство) иностранного государства, в которую вывозится ребенок, требует разрешение второго родителя на вывоз этого ребенка. Кроме того, это разрешение требуют органы ОВИР (в каждом государстве сейчас по-своему называется) и погранконтроля. Особенно болезненно эта процедура происходит в ситуации, когда родители разошлись и у каждого из них уже другая семья. Зачастую, отношения между бывшими супругами носят натянутый характер, а то и вовсе не складываются.</p>\r\n<p>Для получения такого разрешения необходимо сходить к нотариусу и подписать согласие на вывоз ребенка (детей). Присутствуют, обычно, как родитель, который получает разрешение, так и родитель, у которого получают разрешение на выезд ребенка. Т.е. этот родитель дает разрешение бывшему супругу на вывоз ребенка в другое государство (обязательно указать в какое). Кроме того, если такое разрешение выдается для посольства (консульства), то в &laquo;шапке&raquo; этого документа обязательно надо указать для какого именно.</p>\r\n<p>Но иногда бывает ситуация, когда родителя, который должен дать такое разрешение, просто нет. Он отсутствует (уехал) или его местонахождение не известно. Отсутствуют так же любые данные о его близких и родственниках. Тогда необходимо подавать заявление в суд о признании его безвестно отсутствующим или умершим. Положительным моментом для решения проблемы в данной ситуации может служить то, что ранее одним из супругов подавалось заявление на взыскание алиментов на ребенка, но по каким-либо причинам алименты вторым супругом не платились. В настоящее время в Украине и в России действующим законодательством предусмотрен запрет на выезд должникам по выплате алиментов. Однако это не решает вопрос по существу. Кроме того, желательно, даже необходимо, чтобы супругом, который подает исковое заявление о признании второго супруга безвестно отсутствующим, ранее подавалось заявление в ОВД о розыске бывшего супруга с целью взыскания алиментов на ребенка. Кроме этого могут потребоваться и другие документы. В случае признания судом разыскиваемого супруга безвестно отсутствующим или умершим, выносится<br />соответствующее решение, которое часто должно быть легализовано в министерстве юстиции Украины и министерстве иностранных дел Украины либо простановке печати апостиль. Процедура меняется в зависимости от государства бывшей республики в составе СССР. В случае смерти родителя, предоставляется легализованное свидетельство о смерти. Эта информация получила свое подтверждение не только теоретически, но и практически в посольствах США, Великобритании, ФРГ, Франции, Испании, Швеции и иных посольствах в Украине.</p>\r\n<p>Если бывший супруг лишен родительских прав и об этом есть решение суда, легализованное в министерстве юстиции Украины и министерстве иностранных дел Украины (либо проставлена печать апостиль), то проблем в посольстве (консульстве) не возникает, т.к. у такого родителя нет необходимости получать разрешение. Хотя есть исключения. Тут нужно уточнять ситуацию на момент подачи документов в посольство.</p>\r\n<p>Иногда бывает, что супруги в разводе и вывозящий родитель хочет лишить родительских прав второго родителя уже на том основании, что после развода последний не платит алименты на ребенка, не встречается с ним и т.д. Это возможно по новому Семейному кодексу Украины, но на практике это достаточно сложный процесс и здесь без опытного адвоката (юриста) очень сложно доказать. Как разъяснили в суде, ст. 164 СК Украины четко определены основания лишения родительских прав и поэтому суд не сможет лишить прав родителя второго супруга только исходя из того, что второй не платит алименты или не встречается с детьми.</p>\r\n<p>Лишение родительских прав производится только в судебном порядке. Верховный Суд<br />Украины в своем постановлении пленума разъясняет, что лишение родительских прав<br />является крайним способом влияния на лиц, которые нарушают родительские<br />обязанности, а поэтому вопрос о его применении может быть решен только после<br />полного, всестороннего и объективного исследования обстоятельств дела и характера<br />отношения родителей к детям.</p>\r\n<p><strong>Получение разрешения на выезд у родителя ребенка</strong></p>\r\n<p>Теперь давайте рассмотрим ситуацию, когда совершеннолетнему &laquo;ребенку&raquo; (больше 18 лет) необходимо получение у родителей разрешения на выезд на ПМЖ в другое государство.</p>\r\n<p>В Украине этого правила на сегодняшний день нет, а в других государствах бывшего СССР &ndash; оно присутствует.</p>\r\n<p>Указанное выше разрешение необходимо только для органов иммиграционных служб соответствующего государства (бывший ОВИР), т.к. его необходимо получить только совершеннолетним детям (от 18 лет и выше). Это разрешение подтверждает отсутствие обязательств у детей имущественного и личного неимущественного характера перед родителями, либо лицами их заменяющими (опекунами или попечителями). Это разрешение оформляется только у нотариуса.</p>\r\n<p>Очень часто возникает ситуация, когда родители развелись когда ребенок был еще маленьким и супруг, у которого остался ребенок, не получал от бывшего супруга помощи в воспитании ребенка, не получал от него алименты на ребенка, хотя заявление на взыскание алиментов подавал. Если бывший супруг не интересовался жизнью ребенка, то впоследствии когда ребенок вырос и желает выехать на ПМЖ в другое государство, суд на основании предоставленных документов и доказательств соответствующей стороны может принять решение, по которому от бывшего супруга не потребуется разрешение на выезд. Естественно, такое решение должно быть легализовано в министерстве юстиции Украины и министерстве иностранных дел Украины.</p>\r\n<p><strong>Получения решения суда об усыновлении (удочерении) детей</strong></p>\r\n<p>Последним вопросом в данной статье может быть усыновление (удочерение). Выезжая в другое государство на ПМЖ, один из супругов, по взаимному желанию обоих, может усыновить (удочерить) ребенка второго супруга от предыдущего брака. Это довольно хлопотная и долговременная процедура. Усыновитель должен понимать, что, усыновляя ребенка другого супруга, он принимает не только права на этого ребенка, но и обязанность по его воспитанию, т.е. он несет ответственность за дальнейшую судьбу данного ребенка.</p>\r\n<p>Усыновление (удочерение) производится только в судебном порядке (ст. 223 СК Украины). Для вынесения положительного решения суда об усыновлении (удочерении) необходимо выполнить определенные действия и предоставить ряд<br />документов в суд.<br />В суде обязательно присутствие членов инспекции (инспекторов) по охране материнства и детства (РАНО), расположенного по месту прописки ребенка и их заключение о возможности усыновления (удочерения) ребенка будущим родителем.<br />После вынесения судом решения об усыновлении (удочерении) ребенка, родители подают необходимые документы, а также решение суда в отдел органов РАГС. Там им выписывают новое свидетельство о рождении ребенка, где его настоящим родителем указан супруг, усыновивший (удочеривший) ребенка. Соответственно меняется отчество ребенка.</p>\r\n<p>Хочу особо уделить внимание одному небольшому, но ОЧЕНЬ важному моменту. Все эти разрешения на выезд, решения судов и т.д. исключительно нормативные документы &laquo;своего&raquo; государства и, как показывает, многолетняя практика, они не всегда имеют юридическую силу для вынесения позитивного решения о получении въездной визы на ребенка, ВНЖ, ПМЖ или гражданство для последнего. Поэтому важно знать именно национальное иммиграционное законодательство государства, куда выезжает ребенок, чтобы не возникало действительно проблемных ситуаций, при которых вероятность отказа в визе, карте резидента и/или депортации последнего возрастает на порядок. Именно для таких случаев существуют иммиграционные адвокаты, которые могут как существенно снизить риск так и защищать интересы клиента в посольствах и иммиграционных службах соответствующего государства. Подумайте, готовы ли Вы рисковать здоровьем, временем, нервами и финансами как своим, так и своих близких людей?</p>', 1, 7, 9, 1, 1, 1, 1523721994, 1, 1538734411, 0, 0, 0, 1525182468, 1, '', 0, 0, 0, 0, 0, 0, 0, 1);
INSERT INTO `modx_site_content` (`id`, `type`, `contentType`, `pagetitle`, `longtitle`, `description`, `alias`, `link_attributes`, `published`, `pub_date`, `unpub_date`, `parent`, `isfolder`, `introtext`, `content`, `richtext`, `template`, `menuindex`, `searchable`, `cacheable`, `createdby`, `createdon`, `editedby`, `editedon`, `deleted`, `deletedon`, `deletedby`, `publishedon`, `publishedby`, `menutitle`, `donthit`, `haskeywords`, `hasmetatags`, `privateweb`, `privatemgr`, `content_dispo`, `hidemenu`, `alias_visible`) VALUES
(15, 'document', 'text/html', 'Брачный договор/контракт', 'Брачный договор/контракт', '', 'marriage_contract', '', 1, 0, 0, 0, 0, '', '<p><strong>Каждый человек хочет счастья. Себе, своим близким, родственникам и, конечно&nbsp;</strong><strong>же, своим детям. Понимание счастья у каждого свое. Одни хотят большой любви,&nbsp;</strong><strong>другие делают больший упор на материальные блага, третьи &ndash; и на первое, и на&nbsp;</strong><strong>второе.</strong><br />Нам нередко поступают вопросы о возможности заключения брака за рубежом. Потому что очень часто приходится разбирать буквально по косточкам ситуацию, когда спустя определённое время после регистрации брака с иностранцем и выезда на ПМЖ в другую страну, наши женщины возвращаются домой без денег, без детей, с кучей долгов и со штампом депортации из государства&hellip;</p>\r\n<p>Многие наши люди, выезжая за рубеж, думают, что там их ждут &laquo;райские кущи&raquo;. Но все дело в том, что, выезжая на постоянное место жительства (как они предполагают) в чужое государство, они фактически попадают в другое измерение. В этом измерении живут другие люди, существуют другие отношения, другое мировосприятие. Кроме того, &laquo;там&raquo; другие законы.<br />Что знают люди, выезжая первый раз за рубеж? Как правило, наши женщины, выходя замуж за иностранца, за границей бывают если не впервые, то не проживали там постоянно. Т.е. они не знают ни языка на должном уровне, ни обычаев, ни традиций государства, ни жизненного уклада людей этой страны и элементарных основ законодательства о браке (в идеале, конечно, хорошо бы ещё поинтересоваться иммиграционным законодательством). Основная ошибка наших граждан, уезжающих в поисках лучшей жизни в том, что мы судим о других с точки зрения нашего мировосприятия. А ведь оно складывается из многих факторов &ndash; это и образование, и семья, в которой человек воспитывался и живет, и интеллект, и воспитание, и традиции праздников и еще многое другое. Иностранные граждане вообще другие люди, я уже не говорю о том, что &laquo;там&raquo; другой уровень жизни, а, следовательно, другое отношение к семье и детям.<br />Когда к нам приходят клиенты, первое, что мы рекомендуем &ndash; изучайте язык, традиции, обычаи страны, в которую Вы предполагаете уезжать. Благодаря знанию языка Вы сможете лучше понимать своего супруга и его друзей, быстрее найти работу (если есть такое желание), а значит быстрее адаптироваться в &laquo;тамошнюю&raquo; жизнь. Зная традиции и обычаи государства, Вы сможете быстрее и лучше понять его жителей, а значит быстрее адаптироваться к реалиям жизни. Выполняя все эти рекомендации, Вы не попадете в ситуации, последствия которых будут очень для Вас плачевны. Во-вторых, обратите внимание на один из самых сложных и важных моментов при заключении брака с иностранцем (иностранкой) &ndash; возможное заключение брачного договора (или брачного контракта).</p>\r\n<p>Наше законодательство &ndash; как современное, так и дореволюционное &ndash; традиционно исходили из императивного характера норм, регулирующих правовой режим имущества супругов. Говоря бытовым языком, все, что касается имущественных отношений супругов, законом определялось безальтернативно, т.е. как безусловный принцип поведения. Считалось, что только таким образом возможно должным образом защитить права супругов &ndash; расписав это дело для всех и каждого, раз и навсегда. Следует учитывать, что брачный контракт &ndash; это определенные права и обязательства,<br />которые супруги обязуются выполнять в совместной жизни, а так же в случае<br />расторжения брака. Хорошо конечно иметь квартиру (дом), машины, умных и красивых<br />детей, любящего супруга, но, к сожалению, сказка заканчивается, а иллюзии<br />рассеваются.</p>\r\n<p>Заключение брачного контракта и дополнений к нему &ndash; это, как правило, только личная инициатива жениха и невесты. В Украине (да и не только в ней), где заключение брака и венчание в церкви абсолютно разные процедуры с разными правовыми последствиями, оформление отношений между супругами носило скорее эмоциональный, чем материальный характер. Но в настоящее время эта традиция, к сожалению меняется. На наш взгляд материальные и эмоциональные принципы все же должны дополнять друг друга, а не подменять одно другим.<br />Годы идут, времена меняются. Меняются и люди, и их взгляды на семью. Постепенно и у нас многие молодые пары, желая уточнить свои права и обязанности в браке и на случай развода, уточнить обязательства в отношении детей, перед бракосочетанием все же заключают брачный контракт. Заключение такого контракта &laquo;подстегивает&raquo; людей и заставляет относиться с большей долей ответственности и к самой процедуре брака, и к семье.</p>\r\n<p><strong>Брачный контракт представляет собой</strong> обоюдное соглашение о взаимных правах и обязанностях до брака, в браке, а так же в случае расторжения брака (ст. 92 СК Украины). В современном контракте желательно отрегулировать вопросы собственности, а также имущества, которое получают супруги до заключения и во время брака, личные неимущественные права, вопросы наследования в случае смерти (не дай Бог) одного из супругов (ст. 97 СК Украины). А также вопросы о движимом и недвижимом имуществе, имущественные права и обязанности супругов. Кроме того, хочется особо выделить, что в нем желательно оговорить права и обязанности родителей в отношении детей. Нет не то, кто будет кормить, гулять и проверять дневник. А то, как и каким образом будут складываться финансовые отношения родителей при содержании детей. Права последних особенно необходимо оговорить, т.к. в случае расторжения брака и при отсутствии договоренностей дети могут серьезно пострадать, что очень часто встречается.<br />Брачный контракт может регулировать только имущественные и прямо связанные с ними отношения. Любые условия брачного договора, касающиеся неимущественных отношений супругов должны признаваться недействительными &ndash; брачным контрактом нельзя, например, определить, кто будет мыть посуду в доме или как часто (и как добросовестно) супруги будут исполнять в отношении друг друга супружеские обязанности ( п. 3, ст. 93 СК Украины).</p>\r\n<p>В Украине для заключения брачного контракта устанавливается обязательная нотариальная форма. По желанию сторон брачный контракт может заключаться в присутствии свидетелей. Брачный контракт может включать положение об изменении его условий в ходе совместной жизни супругов. Такие изменения могут быть внесены по согласию сторон во время брака договором, который заключается в том же порядке, который предусмотрен для заключения брачного контракта, т.е. нотариально.</p>\r\n<p>Основное достоинство брачного контракта &ndash; возможность для супругов установить в нем, какой режим собственности будет использоваться для всего имущества или для его части: совместный, долевой или раздельный.<br />Совместная собственность &ndash; это право собственности, при котором все имущество поступает в общую собственность супругов без определения долей каждого, т.е. &laquo;общая копилка&raquo; на имущество. Именно этот режим будет применяться, если брачным контрактом не будет установлено иное.<br />Долевая собственность &ndash; доли супругами определяются изначально. Для отчуждения (продажа, дарение, мена, завещание) имущества, находящегося в долевой собственности необходимо согласие второго супруга. То есть, если один из супругов является сособственником в имуществе другого супруга. Кроме того, супруги могут установить и режим раздельной собственности на все или на отдельные виды имущества.</p>\r\n<p>Удобство брачного контракта состоит в том, что закон допускает распространять его действие как на имущество, уже приобретенное к моменту заключения контракта (в том числе приобретенное до брака), так и на имущество, которое будет приобретено в дальнейшем.<br />Права и обязанности в брачном договоре могут возникать, не возникать или прекращаться в зависимости от наступления или не наступления определенных условий &ndash; например, рождения детей, супружеской измены и т.п. Еще одним<br />преимуществом брачного контракта является возможность последнего защитить интересы супругов от взысканий, обращаемых по их долгам. Например, если супруг занимается бизнесом и &laquo;прогорает&raquo;, то на Западе кредиторы будут налагать штраф на имущество не только супруга, но и его второй половины, Т.е. брачным контрактом желательно определить, что все дорогостоящее имущество (недвижимость, ценные бумаги и т.п.) находится в раздельной собственности и принадлежит супруге. В этом случае, обратить взыскание на долю в этом имуществе будет затруднено, если не сказать невозможно.</p>\r\n<p>Как показывает многолетняя практика, брачные контракты составляются в Украине не признаются за рубежом, т.к. не соответствуют местному законодательству, т.е. семейному законодательству, где супруги предполагают в дальнейшем проживать. Однако, возможен вариант, когда заключаемый в соответствующем государстве брачный контракт может иметь с учетом интересов обоих супругов. К чему это я? Если второй супруг не является гражданином государства, где будет заключен брак и брачный контракт, то он может, после консультации со специалистом, внести свои пункты в данный нормативный документ, т.о. существенно упрощая себе жизнь в дальнейшем.<br />Конечно, брачное агентство далеко не всегда может проконсультировать по таким тонким нюансам, да и знакомые вряд ли помогут &ndash; даже если они уехали на ПМЖ в другое государство и живут там долго. Да и сам человек своими силами вряд ли сумеет себе помочь.<br />Вот показательный пример. Женщина выходила замуж за иностранца в г. Харьков в мае 2004 года. Перед регистрацией брака он попросил ее подписать один документ на английском языке. Уже подписав, она показала этот документ переводчику и адвокату, которые и разъяснили ей, что написано в этом документе и объяснили правовые последствия ее подписи. Там было написано следующее: А. выходит замуж за Б. и последний вывозит А. на ПМЖ в государство К. В дальнейшем Б. дает А. ежемесячно 400 у.е. А., в это время, обязана изучать английский язык, готовить, убирать, стирать, т.е. осуществлять уход за домом и мужем. Далее в контракте (если его можно так назвать) указано, что если А. не изучит английский язык и не устроится на работу &ndash; то брак между А. и Б. расторгается, но они живут вместе на таких же условиях. Притом, что женщине на тот момент было 45 лет. Т.е. Б нашел себе служанку за 400 у.е. Так как в 45 лет выучить на должном уровне английский язык и устроиться на более-менее приличную работу в другом государстве достаточно сложно. Таких примеров можно привести массу. Наших женщин избивают, насилуют, отбирают детей, а они не<br />понимают, что такого ни один иностранец со своей соотечественницей никогда бы не сделал. Она бы ему живо &laquo;хвост&raquo; прищемила, да и полиция с судом его бы раздели до гола. А моральный вред! Кто его подсчитает? Уверен, что у каждый вспомнит не один подобный эпизод. Кто-то слышал, кто-то смотрел по телевизору, кто-то читал и т.д.</p>\r\n<p>Мы, конечно, понимаем, что завтра после прочтения этой статьи наши женщины перед браком с иностранным &laquo;Джеймсом Бондом&raquo; не бросятся подписывать брачные контракты, но дело как-то надо сдвигать с мертвой точки. Сколько еще наших граждан будут считать людьми второго сорта. Сами молодожены должны прекрасно понимать, что защитить себя и свои личные права в иностранном суде они смогут, если будут иметь на руках хоть какие-то основания своих требований и притязаний. И это не только предложение подумать для желающих заключить браки с иностранным элементом, но и для наших соотечественников, регистрирующих свои отношения между собой.<br />За рубежом женщина, прежде чем решится завести себе семью и выйти замуж, узнает о потенциальном муже всё возможное: где родился, кто родители и кем работают, какое образование, где живет, куда и как он тратит деньги, какой у него бизнес или место работы, достойная зарплата или минимальный оклад, есть ли сбережения и т.д. Все это делается не просто из меркантильных интересов, а с целью обеспечить себя и своих детей. Вот именно поэтому &laquo;западная&raquo; невеста буквально следит и принимает самое активное участие во всех стадиях заключения брачного контракта.</p>\r\n<p>Можно отметить, что брачный контракт &ndash; это отличный, в достаточной степени гибкий, но мощный, и самое главное, весьма действенный инструмент для урегулирования отношений между супругами. Поэтому нельзя составлять его без помощи сведущего адвоката, который, насколько это возможно, постарается оговорить в брачном контракте все пожелания и договоренности.<br />Поэтому нашим женщинам, которые заключают брак буквально только с именем и фамилией потенциального мужа, можно привести совет в совершенно нормальной западной поговорке: &laquo;У наших женщин (в смысле у их женщин) в глазах по калькулятору&raquo;.</p>\r\n<p>&nbsp;</p>\r\n<table style=\"height: 133px; width: 857px;\" border=\"0\">\r\n<tbody>\r\n<tr>\r\n<td style=\"width: 337px; text-align: center;\"><strong>Наименование услуг</strong></td>\r\n<td style=\"width: 186px; text-align: center;\"><strong>Стоимость, грн.</strong></td>\r\n<td style=\"width: 320px; text-align: center;\"><strong>Стоимость у.е.</strong></td>\r\n</tr>\r\n<tr>\r\n<td style=\"width: 337px; text-align: center;\">&bull; подготовка брачного контракта (брачного договора)</td>\r\n<td style=\"width: 186px; text-align: center;\">1500</td>\r\n<td style=\"width: 320px; text-align: center;\">280</td>\r\n</tr>\r\n<tr style=\"text-align: center;\">\r\n<td style=\"width: 337px;\">&bull; перевод брачного контракта (при необходимости)</td>\r\n<td style=\"width: 186px;\">150-200</td>\r\n<td style=\"width: 320px;\">30-40</td>\r\n</tr>\r\n<tr>\r\n<td style=\"width: 337px; text-align: center;\">&nbsp;&bull; нотариальное удостоверение брачного контракта</td>\r\n<td style=\"width: 186px; text-align: center;\">&nbsp;100-200</td>\r\n<td style=\"width: 320px; text-align: center;\">&nbsp;20-40</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p><br /><strong><span style=\"color: #ff0000;\">От заказчика необходимо:</span></strong></p>\r\n<ol>\r\n<li>Предоплата 100%;</li>\r\n<li>Скачать и заполнить&nbsp;<span style=\"text-decoration: underline;\"><span style=\"color: #ff0000; text-decoration: underline;\">анкету</span></span><span style=\"color: #000000;\">&nbsp;</span><span style=\"color: #000000;\">(предоставим)</span></li>\r\n<li>Выслать ее по электронному адресу: <a href=\"mailto:av@bag.kharkov.ua\">av@bag.kharkov.ua</a></li>\r\n</ol>', 1, 7, 10, 1, 1, 1, 1523722039, 1, 1538754813, 0, 0, 0, 1525182754, 1, '', 0, 0, 0, 0, 0, 0, 0, 1),
(16, 'document', 'text/html', 'Иммиграция в Германию ', 'Иммиграция в Германию ', '', 'immigration_to_germany', '', 1, 0, 0, 0, 1, '', '', 1, 7, 11, 1, 1, 1, 1523722064, 1, 1525182533, 0, 0, 0, 1525182533, 1, '', 0, 0, 0, 0, 0, 0, 0, 1),
(17, 'document', 'text/html', 'Еврейская иммиграция ', 'Еврейская иммиграция ', '', 'оewish_immigration', '', 1, 0, 0, 16, 0, '', '<p>В настоящее время ФРГ принимает лиц еврейской национальности. Это же относится к членам их семьи.</p>\r\n<p>Общие основания для иммиграции в Германию по еврейской линии Еврейские иммигранты и члены их семей должны быть гражданами одной из стран бывшего СССР (за исключением Балтийских государств) или являться лицами без<br />гражданства с местом проживания в области происхождения не позднее, чем с 1 января 2005 г. и ранее не должны были переезжать в одну из третьих стран.</p>\r\n<p>В качестве еврейских иммигрантов могут быть приняты только лица, которые:</p>\r\n<ul>\r\n<li>Имеют еврейскую национальность (или как минимум одного родителя еврейской национальности), подтвержденную государственными документами, выданными до 1990 года либо после 1990 года, но иметь подтверждающие документы по родителям;</li>\r\n<li>В состоянии постоянно обеспечивать себе приемлемый уровень существования в Германии после иммиграции по еврейской линии. При этом должно обеспечиваться воссоединение семей. Прогноз составляется для члена семьи, имеющего право на переселение, но рассматривает также и его близких родственников. В ходе подачи сведений о себе кандидатам могут задаваться вопросы.</li>\r\n<li>Владеют немецким языком на уровне не ниже Stufe A1 (простые основные знания). Знания должны быть подтверждены путем предоставления сертификата \"Гётё-института\". Это требование не касается заявителей, которые родились до 01.01.1945 года, а также детей, не достигших 14 лет. При этом возможно рассмотрение особо сложных случаев, когда допускается отклонение от данного требования.</li>\r\n<li>Исповедуют иудейскую религию;</li>\r\n<li>Имеют документальное подтверждение возможности приема в одной из еврейских общин ФРГ. Данное документальное подтверждение осуществляется в форме экспертного мнения Центральной благотворительной организации евреев Германии в г. Франкфурте. К данному процессу привлекается также Союз прогрессивных евреев.</li>\r\n</ul>\r\n<p>В отношении жертв национал-социалистического преследования положения о знании языка уровня А1 и исповедание иудейской религии не применяются.</p>\r\n<p>Невозможна иммиграция в Германию по еврейской линии для лиц, которые:</p>\r\n<ul>\r\n<li>Занимали в бывшем СССР должности, являвшиеся ключевыми для сохранения<br />коммунистического режима или являвшимися таковыми в отдельных случаях;</li>\r\n<li>Имеют судимости по преднамеренным преступлениям, за исключением<br />случаев, когда речь идет о судимостях в бывшем СССР по политическим мотивам;</li>\r\n<li>Согласно указаниям в их делах имеют или имели связь с криминальными<br />организациями или террористическими объединениями.</li>\r\n</ul>\r\n<p>При составлении интеграционного прогноза начисляются баллы по ряду критериев:</p>\r\n<ul>\r\n<li>возраст;</li>\r\n<li>наличие высшего или профессионального образования;</li>\r\n<li>опыт работы;</li>\r\n<li>участие в работе еврейских организаций;</li>\r\n<li>наличие родственников в Германии;</li>\r\n<li>наличие предложения от работодателя в Германии;</li>\r\n<li>знание немецкого языка;</li>\r\n<li>субъективный прогноз BAMF.</li>\r\n</ul>\r\n<p><strong>Баллы, начисляемые основному заявителю</strong></p>\r\n<p>Таблица балов интеграционного прогноза.</p>\r\n<ol>\r\n<li>возраст: до 30 лет - 15 пунктов, из которых за каждый последующий год вычитается 1 пункт, после 45 лет 0 пунктов;</li>\r\n<li>высшее образование: 20;</li>\r\n<li>профессиональное образование: 10;</li>\r\n<li>опыт работы: 10;</li>\r\n<li>участие в работе еврейских организаций: 10;</li>\r\n<li>родственники в Германии: 5;</li>\r\n<li>предложение работы в Германии: 5;</li>\r\n<li>знание немецкого языка: до 25 (предположительно: A2 - 5, B1 - 10, B2 - 15, C1 - 20 и C2 - 25 пунктов);</li>\r\n<li>субъективный прогноз BAMF: до 5 пунктов.</li>\r\n</ol>\r\n<p>Достаточно набрать 51 балл из 105.</p>\r\n<p>Условия выезда для супругов и несовершеннолетних детей заявителя&nbsp;<span style=\"text-decoration: underline;\"><strong>Продолжительность брака на момент подачи заявления должна быть не менее&nbsp;</strong></span><span style=\"text-decoration: underline;\"><strong>трех лет.</strong></span> Супруги и несовершеннолетние дети, не состоящие в браке дети для<br />иммиграции в Германию по еврейской линии должны также подтвердить базовые знания немецкого языка (свидетельство о сдаче экзамена А1).</p>\r\n<p>Для детей, не достигших 14 лет, данное подтверждение наличия базовых знаний немецкого языка не обязательно, если нет причин ожидать существенных проблем с интеграцией.</p>\r\n<p>Выдача согласия на прием осуществляется при условии, что въезд данного ребенка в действительности происходит до достижения им 15-летнего возраста.</p>\r\n<p>Иммиграция в Германию по еврейской линии при &laquo;свежем браке&raquo; Брак, заключенный в течение 3-х лет до подачи анкеты или, тем более, после ее подачи, считается &laquo;свежим браком&raquo;. Для таких пар может быть назначено собеседование в посольстве Германии на предмет фиктивности брака. Решение о собеседовании принимает чиновник посольства при приеме документов. Вероятность назначения собеседования прямо пропорциональна &laquo;свежести&raquo; брака. Одновременно проводится проверка на принадлежность основного заявителя к иудейской вере.</p>\r\n<p>Если брак заключен после сдачи анкеты, то новый супруг (а) должны самостоятельно взять анкету, предъявив легализованное свидетельство о браке, паспорта и свидетельства о рождении обоих супругов.</p>\r\n<p><span style=\"text-decoration: underline;\"><strong>Памятка о порядке действий по выезду на ПМЖ в ФРГ по еврейской иммиграции</strong></span></p>\r\n<ol>\r\n<li>Первоначально определиться соответствуете ли Вы возможности выезда на ПМЖ в Германию. Для этого нужно взять свидетельство о рождении основного заявителя и посмотреть указана ли национальность хотя бы одного родителя основного заявителя&nbsp;как еврей. Свидетельство о рождении должно быть выдано не позднее 1991 года. Кроме того, нужно чтобы оно было не повторным. Если оно повторно выдано, то придется брать иные документы, подтверждающие подлинность данного свидетельства (военный билет отца, домовую книгу, свидетельство о браке родителей и т.д.). Все документы предоставляются в посольство только в виде оригиналов. Получив список пакета документов, памятки и таблицу подсчета возможных баллов,</li>\r\n<li>просчитать возможность выезда на ПМЖ.</li>\r\n<li>Если основной заявитель имеет право на выезд по этой иммиграции, нужно связаться со специалистом для получения дальнейших инструкций о порядке подготовки документов и правилах их подачи в посольство.</li>\r\n<li>Все документы, выданные органами регистрации актов гражданского состояния (РАГСами или ЗАГСами), должны быть легалезированны (или проставлена печать апостиль) и нотариально заверены и только потом сделан перевод на немецкий язык.<br />Если же эти документы, выданы (РАГСами или ЗАГСами) до 1991 года, то их нужно просто нотариально заверить и перевести на немецкий язык.</li>\r\n<li>Сама подача документов в посольство ФРГ осуществляется вмести с сертификатом о знании немецкого языка уровня не ниже А1. Это начальный уровень знаний, соответствует 4-м классам начальной школы. Этот сертификат нужно получить всем членам семьи основного заявителя, кто родился после 01.01.1945 года и кому более 14 лет. Сертификат получают в филиалах немецкого Гете-института. Если знания языка получены в ВУЗе и преподавание велось исключительно на немецком языке, то получение такого сертификата не нужно.</li>\r\n<li>Кроме подготовленных документов нужно иметь 2 фотографии 3.5Х4.5. Нужно помнить, что на всех анкетах необходимо в конце расписаться. Если выезжает ребенок, то за него в анкете расписывается один из родителей.</li>\r\n<li>После того как подготовлены все документы (легализация, заверка у нотариуса, перевод на немецкий язык) и заполнены анкеты на всех членов семьи, выезжающих с основным заявителем формируются папки, куда все эти документы и анкеты раскладываются для удобной обработки.</li>\r\n<li>Перед подачей документов необходимо позвонить в посольство и выяснить когда можно подать все подготовленные документы и анкеты, т.е. чтобы сотрудники посольства назначили время подачи.</li>\r\n<li>Приехать в посольство желательно заблаговременно до времени подачи документов, т.к. если человек опаздывает, то он не уважает время другого человека. В ряде случаев, когда брак относительно &laquo;свежий&raquo;, то мужу и жене могут устроить</li>\r\n<li>собеседование на предмет фиктивного брака. В ряде случаев так же могут устроить собеседование на предмет принадлежности основного заявителя к иудейской религии.</li>\r\n<li>После подачи документов основному заявителю и членам его (ее) семьи остается только ждать результата из Германии.</li>\r\n</ol>', 1, 7, 0, 1, 1, 1, 1525182623, 1, 1538747412, 0, 0, 0, 1525182623, 1, '', 0, 0, 0, 0, 0, 0, 0, 1),
(18, 'document', 'text/html', 'Немецкая иммиграция', 'Немецкая иммиграция', '', 'german_immigration', '', 1, 0, 0, 16, 0, '', '<p>В случае, если национальность отца и/или матери &ndash; немец и/или немка, то человек и его/ее семья имеет право претендовать на анализ и возможный выезд в Германию по программе поздних переселенцев.</p>\r\n<p>По 4 параграфу Поздним переселенцем, как правило, является представитель немецкой национальности, который эмигрировал с территории республик бывшего Советского Союза, Эстонии, Латвии или Литвы после 31 декабря 1992 года в рамках приема и в течение шести месяцев после этого обосновал место своего постоянного жительства на территории Германии, если он до этого проживал на вышеназванных территориях.</p>\r\n<p><strong>Сроки:</strong>&nbsp;от 3-х месяцев до 3-х лет</p>\r\n<p style=\"text-align: center;\"><span style=\"text-decoration: underline;\">ЭМИГРАЦИЯ В ГЕРМАНИЮ ДЛЯ НЕМЦЕВ</span><br /><span style=\"text-decoration: underline;\">(немецкая линия эмиграции)</span></p>\r\n<p>Ваша отец этнический немец? Вы проживаете в России, Украине, Казахстане или другой стране СНГ? Вы можете воспользоваться нашими услугами по программе немецкой независимо от Вашего местонахождения.</p>\r\n<ol>\r\n<li><strong>Первичная консультация на основе анализа полученных по электронной почте&nbsp;</strong><strong>документов, для разъяснения процедуры и основных факторов влияния на выезд&nbsp;</strong><strong>по программе &laquo;немецкая эмиграция&raquo;, определение первичных оснований и анализ&nbsp;</strong><strong>документов для эмиграции в Германию по немецкой линии. Стоимость эквивалент -&nbsp;</strong><strong>$100.</strong></li>\r\n<li><strong>В случае согласия на дальнейшее сопровождение со стороны иммиграционного&nbsp;</strong><strong>адвоката, будут переводится и заверяться все необходимые для подачи в&nbsp;</strong><strong>посольство документы. Кроме того, будет заполняться антрага (русский вариант), а&nbsp;</strong><strong>после согласования он будет переводится на немецкий язык. Стоимость будет&nbsp;</strong><strong>зависит от объема работы, но не выше эквивалента $600 на семью.</strong></li>\r\n<li><strong>Оформление пакета документов для немецкой эмиграции в Германию&nbsp;</strong><strong>Утвержденный пакет и подготовленный иммиграционным адвокатом список документов&nbsp;</strong><strong>необходимо передать на оформление для окончательного формирования пакета для подачи в&nbsp;</strong><strong>посольство Германии (легализация/ апостиль, переводы свидетельств и других документов).&nbsp;</strong><strong>Здесь будет проведена окончательная проверка на правильность ксерокопирования и&nbsp;</strong><strong>оформление нотариальных копий, а также контроль их необходимого количества.&nbsp;</strong><strong>Выполненные переводы будут сформированы необходимым образом в пакет для передачи в&nbsp;</strong><strong>посольство Германии.</strong></li>\r\n</ol>\r\n<p style=\"text-align: center;\"><span style=\"text-decoration: underline;\"><strong>ПРАВОВЫЕ ОСНОВАНИЯ И ПРИЧИНЫ ОТКАЗА В СТАТУСЕ ПОЗДНЕГО ПЕРЕСЕЛЕНЦА</strong></span></p>\r\n<p>По 4 параграфу Поздним переселенцем, как правило, является представитель немецкой национальности, который эмигрировал с территории республик бывшего Советского Союза, Эстонии, Латвии или Литвы после 31 декабря 1992 года в рамках приема и в течение шести месяцев после этого обосновал место своего постоянного жительства на территории Германии, если он до этого проживал на вышеназванных территориях.</p>\r\n<p>Поздний переселенец является немцем согласно статье 116 абз. 1 Основного Закона.</p>\r\n<p>По приезду в Германию поздний переселенец обретает статус немца в соответствии со статьей 116, абз.1 Основного Закона (Grundgesetz - GG). Этот статус не идентичен немецкой национальной принадлежности и вполне может не совпадать с ней. Владелец данного конституционного статуса приравнивается в своих правах к гражданам ФРГ и может без выполнения каких-либо дополнительных условий обрести немецкое гражданство по своему заявлению.</p>\r\n<p>В отличие от этнических немцев из Польши, Румынии и Венгрии \"советским\" переселенцам не требуется достоверно доказывать, что они подвергались дискриминации по национальному признаку. Более того, люди из вышеназванных стран могут рассчитывать на признание поздними переселенцами только при доказуемости данного факта. С этой точки зрения наши соотечественники находятся в более привилегированном положении.</p>\r\n<p style=\"text-align: center;\"><strong>Причины отказа в статусе позднего переселенца</strong></p>\r\n<p>Если в вашем внутреннем (\"советском\") паспорте в графе \"национальность\"&nbsp;имеется или имелась иная запись (не \"немец\"), но в 90-х годах вы изменили ее на \"немец\", то вам следует исходить из того, что в статусе позднего переселенца (по &sect; 4) вам откажут. По крайней мере, это произойдет на стадии ответа на ваше заявление о приеме. Самый лучший вариант в такой ситуации - быть включенным в решение о приеме, подателем (Antragssteller) которого является<br />Ваш немецкий родственник, в качестве сопровождающего лица (то есть в соответствии с &sect;&sect; 7 или 8 BVFG). Сейчас есть вариант быть включенным в анкету ранее выехавшего близкого родственника. Если такой возможности не существует или же данный вариант Вас не устраивает, то встает вопрос: \"Смириться с отказом или бороться за право?\" Для решения этой дилеммы я опишу ряд ситуаций, в которых, на мой взгляд, шансы на успех достаточно велики&nbsp;и имеет смысл их использовать:</p>\r\n<p>Вы получили паспорт в 1955 году или раньше в трудовой армии, во время нахождения под надзором комендатуры, в период исторически общеизвестных репрессий против немцев или в иных сопоставимых условиях.</p>\r\n<p>Вы упорно и безуспешно пытались изменить запись о национальности задолго до подачи заявления о переселении в Германию (например, Вы заявили о желании переселиться в 1998 году, но у Вас есть официальные отказы в изменении, датированные 1962, 1965 и 1970 годами). В этом случае можно полагать, что Вы ощутили свою принадлежность к немецкому народу в старшем возрасте (что достаточно для признания поздним переселенцем), и Ваши хлопоты об изменении национальности были связаны с искренним внутренним настроем, а не с желанием выполнить формальные условия для переезда в Германию.</p>\r\n<p>Запись не немецкой национальности произошла против Вашей воли (произвол работников паспортного стола, угрозы родственников), и после прекращения данной насильственной ситуации Вы предпринимали серьезные, документально доказуемые усилия для изменения своей национальности на немецкую.</p>\r\n<p>Помните о том, что для успеха Вашего дела в суде недостаточно голословных утверждений. Во всех перечисленных случаях Вам не поверят на слово, а попросят предоставить доказательства того, что преподносимые Вами факты являются правдой. Если Вы, к примеру, утверждаете, что получали паспорт, будучи под надзором комендатуры, то Вам нужно предъявить соответствующую справку, назвать незаинтересованных в исходе вашего дела свидетелей и т.д. Если Вы ссылаетесь на произвол работников паспортного стола, то сопроводите это утверждение документами о том, как Вы впоследствии пытались добиться восстановления справедливости (письменные отказы в изменении национальности, отклонения судебных исков и т.д.). Не вызывают сомнений сообщения очевидцев о грубости советских чиновников-паспортистов, произносимых ими угрозах и выражаемой ненависти в отношении этнических немцев. Но эти факты перед немецкими судами не стоят и выеденного яйца, если Вы не можете этого однозначно доказать. Весьма необходимым в таких делах является участие опытного адвоката, которого лучше подключить к решению вопроса уже на стадии подачи заявления на переселение в ФРГ. В крайнем случае, обзаведитесь юридической поддержкой сразу же после получения первого отказа и перед подачей протеста. Но жизнь показывает, что, узнав об отказе, люди теряются, впадают в панику, обращаются за советом к некомпетентным людям (а те услужливо и, порой, не бесплатно дают свои &amp;quot;ценные&amp;quot; советы). А время работает против Вас, у Вас есть только один месяц на подачу протеста. Т.е. нужно просто написать в официальном письме, что Вы не согласны с принятым решением и обосновать свое несогласие. Пропустив этот срок, Вы можете спокойно исходить из того, что ни один немецкий чиновник с Вами и разговаривать больше не будет, так как отказ в приеме вступил в законную силу. Даже если Вы знаете о сроках и уложитесь в них, то не менее важно обосновать протест грамотно, четко, основываясь на возможностях законов и учитывая решения современного немецкого судопроизводства.</p>\r\n<p>Часто люди, в общем имея шансы на успех, теряют их в результате поспешных и непродуманных &amp;quot;оправданий&amp;quot; в выборе ненемецкой национальности, которые они еще, к тому же, отправляют в Федеральное административное ведомство в письменном виде. Помните о поговорке: &amp;quot;Написано пером - не вырубишь топором&amp;quot;! Ни один, даже самый блестящий адвокат не сможет убедить судью в уважительных причинах Вашей паспортной записи, если до этого Вы утверждали, что основанием для Вашего выбора являлось пожелание двоюродного дяди или опасение косых взглядов со стороны соседей.</p>\r\n<p>В следующих случаях, напротив, вероятность приема в качестве позднего переселенца весьма<br />невелика. В такой ситуации нужно особенно тщательно продумать целесообразность<br />ведомственного и судебного опротестования отказа, имея в виду предстоящие расходы и<br />затрату усилий. Наряду с этим следует при помощи адвоката взвесить шансы на переезд в<br />Германию иными путями, например, &amp;quot;по линии&amp;quot; воссоединения семьи или включения в &amp;quot;номер&amp;quot;<br />других родственников.</p>\r\n<p><strong>Итак, Ваши шансы на выезд по немецкой линии, к сожалению, малы:</strong></p>\r\n<p>Если Вы впервые получили внутренний паспорт в 1976 году или позднее. В это время в СССР действовало однозначное законодательство о свободном выборе национальности по одному из родителей. Также Вам будет сложно доказать, что в данный период времени Вы подвергались существенной дискриминации по национальному признаку, поскольку немецкие суды, с благословения высшей инстанции, исходят из обратного.</p>\r\n<p>Если Вы изменили национальность на немецкую незадолго до подачи заявления на признание поздним переселенцем или, того хуже, во время его обработки. Такое поведение скорее всего будет расценено как \"Lippenbekenntnis\" - \"лицемерное признание\". Доказательство обратного будет связано с невероятными трудностями.</p>\r\n<p>Если наряду с \"неправильной\" национальностью в Вашем деле имеются и другие основания для отказа в статусе позднего переселенца. Например, Вы плохо или вообще не говорите по-немецки и не знаете немецкую культуру. Либо Вы, Ваши родители, супруг или его родители занимали в СССР высокое профессиональное положение (высокие военные (выше полковника) и милицейские чины, политические деятели и т.п.). В таких случаях судье остается либо вынести отрицательное решение, либо преступить через действующее законодательство. В данном случае считается, что лицу необходимо отказать на основании того, что оно поддерживало тоталитарный режим.</p>\r\n<p>Германские суды и ведомства интересуются не актуальной записью, а той, которая фигурировала в паспорте при его первоначальном получении в 16-летнем возрасте. Они также имеют физическую возможность запросить и получить об этом необходимую информацию от ЗАГСов республик бывшего Союза.</p>\r\n<p style=\"text-align: center;\"><span style=\"text-decoration: underline;\"><strong>ПРОЦЕДУРА ПОДГОТОВКИ И ПОДАЧИ ДОКУМЕНТОВА НА ВИЗУ ПО НЕМЕЦКОЙ ЭМИГРАЦИИ</strong></span></p>\r\n<p style=\"text-align: center;\"><strong>ВАРИАНТ 1 &ndash; непосредственная подача документов в Посольство Германии</strong></p>\r\n<p><strong>Шаг 1</strong></p>\r\n<p>Необходимо первоначально посмотреть в свое свидетельство о рождении. Если там написано, что хотя бы один из родителей указан как немец, то Вы имеете все законные основания для приема по программе поздних переселенцев. Одним из важных моментов является то, чтобы само свидетельство о рождении не подвергалось внесению изменений. Т.е. чтобы в нем не менялась национальность родителя (родителей) на основании решения суда.</p>\r\n<p><strong>Шаг 2</strong></p>\r\n<p>Теперь необходимо грамотно подготовить документы для подачи в посольство и заполнить Антраг, учтя номер параграфа главного заявителя. После того, как Вы убедились в своей &laquo;немецкости&raquo; можно в Интернете скачать анкеты для<br />поздних переселенцев. К Антрагу прилагается памятка. И Антраг, и памятка выдаются на немецком языке.</p>\r\n<p><strong>Шаг 3</strong></p>\r\n<p>Вы сдаете Антраг и оформленные документы и ожидаете Вызова на шпрах-тест (при необходимости).</p>\r\n<p><strong>Шаг 4</strong></p>\r\n<p>После сдачи шпрах-теста Вы ожидаете результатов в виде письменного уведомления или отказа с обоснованием его причин</p>\r\n<p><strong>Шаг 5</strong></p>\r\n<p>В настоящее время после сдачи шпрах-теста, основной заявитель, вызывается на сдачу теста на знание немецкого языка уровня В1.</p>\r\n<p><strong>Шаг 6</strong></p>\r\n<p>Если тест на знание языка пройден успешно, то основной заявитель, а так же все кто вписаны в его анкету получают приглашение в посольство на получение соответствующей въездной визы.<br />После подачи документов в визовый отдел Посольства ФРГ все получают визы. Осуществить это нужно в течение 1 года с момента получения вызова. Соответственно после его получении Вам потребуется оформить загранпаспорт на ПМЖ (отличается от временного).</p>\r\n<p style=\"text-align: center;\"><strong>ВАРИАНТ 2 &ndash; подача осуществляется непосредственно в Германии доверенным лицом</strong></p>\r\n<p>Отличия Варианта 2 от Варианта 1 состоят в следующем:</p>\r\n<p>- Антраг необходимо взять в Германии Вашим родственникам.<br />- Антраг необходимо сдать в Германии Вашему доверенному лицу (по доверенности Ведомства).</p>\r\n<p>Переводы желательно заверять нотариально.<br />Отличия процедуры выезда по 7-му и подобным параграфам. Если Вы выезжаете с кем-либо (7, 8 и другие параграфы), то Вам следует обратить внимание, что это должно быть отмечено в Антраге.</p>\r\n<p>У Вас может быть свой Антраг или Вы можете быть вписаны в Антраг главного заявителя. Это не имеет большого значения.<br />Однако если Вы выезжаете не по &sect; 4, то Вы, по сути, подтверждаете свое родство к главному заявителю, а не принадлежность к немецкой культуре.</p>\r\n<p style=\"text-align: center;\"><strong>Права и льготы в Германии при выезде по немецкой линии эмиграции</strong></p>\r\n<p>В Германии этническим немцам положены следующие льготы:</p>\r\n<p><strong>Spaetaussiedler, &sect; 4 Abs. l, 2 BVFG (поздний переселенец)</strong></p>\r\n<p>Поздний переселенец является немцем согласно статьи 116 абз. 1 Основного Закона (Grundgesetz) Германии и приобретает немецкое гражданство. Он получает бесплатный 6-месячный курс немецкого языка, интеграционное пособие. Он может изменить свое имя и фамилию в соответствии с &sect; 4 BVFG. В отличие от других граждан бывшего Советского Союза поздний переселенец пользуется всеми льготами, предусмотренными законодательствами Германии. Главным же его отличием является то, что он получает выплату \"комендатурских\" в соответствии с &sect; 9 BVFG (2000 евро - для рожденных до 01.04.1956 г.; 3000 евро для рожденных до 01.04.1946 г.) и пенсию в соответствии с Fremdrentengesetz (Закон о чужой<br />пенсии). Правда, эта пенсия в последние годы значительно сократилась и сведена до уровня<br />интеграционного пособия. Кроме того, только поздний переселенец получает льготы при<br />открытии собственного дела. С этой целью в соответствии с &sect; 14 Abs. l BVFG предусмотрено<br />предоставление кредитов под низкие проценты, выгодные условия их погашения и<br />обеспечения, а также другие льготы.</p>\r\n<p><strong>Муж (жена) позднего переселенца, если их брак перед отъездом в ФРГ продолжался 3 года и более, &sect; 7 Abs. 2 (1-й вариант BVFG).</strong></p>\r\n<p>Эта группа лиц получает также статус немцев согласно статье 116 Конституции Германии и немецкое гражданство. Их положение мало чем отличается от положения позднего переселенца. Они также получают удостоверение переселенца, языковой курс и интеграционное пособие. Однако в отличие от поздних переселенцев указанная группа лиц не получает \"комендатурские\" согласно &sect; 9 BVFG. Кроме того, в соответствии с &sect; 13 BVFG и \"Fremdrentengesetz\" (\"Закон о чужой пенсии\") им не предоставляется пенсия. Заработанный ими в бывшем СССР трудовой стаж не учитывается при начислении пенсии.</p>\r\n<p><strong>Муж или жена позднего переселенца, если их брак перед отъездом в Германию продолжался менее 3 лет, &sect; 7 Abs. 2 (2-й вариант BVFG).</strong></p>\r\n<p>Все лица, подпадающие под этот параграф, получают статус иностранца. Они не получают удостоверение личности (Personalausweis), должны иметь вид на жительство (Aufenthaltserlaubnis), который периодически продлевается, а подать заявление на получение немецкого гражданства возможно лишь через три года. На этом основании женщина, не получившая, например, гражданство Германии, теряет при разводе право проживания в ней. Это положение, правда, не касается тех женщин, дети которых уже получили немецкое гражданство. Указанные лица не могут поменять имя и фамилию согласно &sect; 94 BVFG, они, разумеется, не получают &amp;quot;комендатурские&amp;quot; деньги и пенсию. При устройстве на работу им нужно иметь соответствующее разрешение (Arbeitserlaubnis) от биржи труда (Arbeitsamt).</p>\r\n<p><strong>Потомки или дети позднего переселенца (Abkoemmlinge), &sect; 7Abs. 2 (3-й вариант BVFG).</strong></p>\r\n<p>Потомки позднего переселенца являются немцами согласно статье 116 Конституции Германии даже в том случае, если у них в паспорте будет записана другая национальность (не\"немец\"). Они получают немецкое гражданство, при достижении 16 лет им вручают паспорт (Personalausweis). Дети позднего переселенца могут при желании поменять согласно &sect; 94 BVFG фамилию и имя. Пенсию и комендатурские деньги они не получают.</p>\r\n<p><strong>Прочие члены семьи позднего переселенца, &sect; 8 Abs. 2 BVFG.</strong></p>\r\n<p>Всем прочим членам семьи позднего переселенца (sonstige Familienangehцrige des Spaetaussiedlers), не подпадающим под один из трех вариантов &sect; 7 Abs. 2 BVFG, присваивается &sect; 8 BVFG. Они имеют статус иностранца, не получают паспорт (Personalausweis). Им предоставляется возможность получить немецкое гражданство в зависимости от возраста, от состояния в супружеских отношениях и степени родства с поздним переселенцем только через 3 года, 5 лет, 8 лет или через 15 лет. Эти лица не получают интеграционное пособие и бесплатные языковые курсы, они не имеют права поменять имя или фамилию, их учебные дипломы не признаются, им не предоставляется возможность учиться при поддержке гарантийного фонда и фонда Otto Benecke. Однако указанным лицам, прибывшим в Германию по линии Закона о делах насильственно перемещенных лиц и беженцев-BVFG, предоставляется социальная помощь; как члены семьи они могут быть застрахованы в больничной кассе, &sect; 9 RVO (SGBV); студентам вузов предоставляется стипендия, они могут претендовать на Wohngeld, Kindergeld и Erziehungsgeld.</p>\r\n<p>После эмиграции специалистам, особенно программистам, довольно легко найти работу,<br />однако в Германии, по сравнению с другими развитыми странами, большее количество<br />безработных. Если Вы все же хотите работать и зарабатывать деньги, то идеально совместить<br />эмиграцию и образование в вузах Германии. Вы сможете получить европейское образование,<br />европейский диплом, опыт работы в фирмах Германии (что ценится особо) и получать<br />стипендию во время учебы, если все сделаете правильно до отъезда.</p>\r\n<p style=\"text-align: center;\"><strong>ШПРАХ-ТЕСТ</strong><br /><strong>ЭМИГРАЦИЯ ДЛЯ НЕМЦЕВ</strong></p>\r\n<p><strong>Цели и задачи шпрах-теста</strong></p>\r\n<p>Каковы правовые основы и практика проведения экзамена на знание немецкого языка (Sprachtest), который сегодня должны сдавать все желающие переехать на постоянное жительство в Германию? Законно ли вообще требование сдачи экзамена? Можно ли оспорить результаты языкового теста, добиться его пересдачи?</p>\r\n<p>С 15.07.1996 г. Федеральное министерство иностранных дел и Федеральное административное ведомство, принимая на рассмотрение заявления о приеме на проживание в Германии, проводят проверку знаний немецкого языка заявителями в местах их проживания на территории бывшего СССР.</p>\r\n<p>Проверка осуществляется как в зарубежных представительствах ФРГ (посольствах и консульствах), так и, по мере возможности, в месте проживания заявителя в рамках выездов по консульским вопросам.</p>\r\n<p>Языковые тесты призваны помочь в предотвращении особо сложных жизненных ситуаций, которые могут возникнуть в тех случаях, когда в силу незнания немецкого языка в отношении заявителей после их приезда в Германию применяются правовые нормы об иностранцах, в связи с чем им приходится принимать в расчет принудительное возвращение в места прежнего проживания.</p>\r\n<p>Непосредственно в тексте Федерального закона об изгнанных, являющемся правовой основой приема \"российских\" (обобщенное обозначение немцев, прибывающих с территорий стран-наследниц бывшего СССР) немцев в ФРГ, нет упоминания о необходимости сдавать экзамен по немецкому языку, однако, это не означает, что его проведение противозаконно: органы государственного управления ФРГ обязаны проверять сведения, которые заявитель сообщил о<br />себе; они вправе сами устанавливать процедуру и разрабатывать механизм такой проверки.</p>\r\n<p>Кроме того, хорошие знания немецкого языка являются основной предпосылкой быстрого и<br />успешного вступления в трудовую жизнь и интеграции в общество Федеративной Республики<br />Германия.</p>\r\n<p><a title=\"Пример Шпрах-теста\" href=\"http://chemodan.com.ua/germany/test_exampl.html\"><span style=\"text-decoration: underline;\">Пример Шпрах-теста</span></a></p>\r\n<p>В настоящее время после сдаче в посольстве ФРГ шпрах-теста, через некоторое время основном заявителю необходимо предоставить сертификат о знании немецкого языка уровня А1, который выдает Гёте-институт. Поэтому после успешной сдачи шпрах-теста основной заявитель для успешного прохождения иммиграционного процесса должен сосредоточить все свое внимание и усилия на подготовке и успешной сдаче теста на знание немецкого языка,<br />указанного выше уровня. Без этого сертификата получение позитивного решения от иммиграционных структур ФРГ, о возможности выезда основного заявителя и всех членов его семьи, вписанных а анкету (антраг) не возможно.</p>\r\n<p style=\"text-align: center;\"><span style=\"text-decoration: underline;\"><strong>ЧТО СТОИТ БРАТЬ С СОБОЙ В ГЕРМАНИЮ?</strong></span></p>\r\n<p>Общий принцип прост: Вы не можете знать, какие документы Вам могут понадобиться в той или иной жизненной ситуации. Может оказаться так, что груда документов, привезенных с родины, будет просто лежать без движения и, напротив, чтобы решить вопрос приема на работу или получения дополнительной социальной помощи, Вам не хватит маленькой справки.</p>\r\n<p>Поэтому совет такой - везите все. Переводы стоит иметь только для того, чтобы объяснить чиновнику, что это за справка. Если ему потребуется, он попросит перевести данную справку у местного сертифицированного в Германии переводчика.</p>\r\n<p>Особое внимание уделите таким документам, как:</p>\r\n<p>Справка о захоронении. Если у Вас кто-либо из родственников умер и захоронен в Украине, то это будет лишним поводом съездить домой.</p>\r\n<p>Справка из Мосгосархива (Красный Крест) об эвакуации. Благодаря ей Вы можете попробоватьполучить компенсацию за эвакуацию.</p>\r\n<p>Водительские права. Они действительны первые полгода в Германии.</p>', 1, 7, 1, 1, 1, 1, 1525182697, 1, 1538742809, 0, 0, 0, 1525182697, 1, '', 0, 0, 0, 0, 0, 0, 0, 1);
INSERT INTO `modx_site_content` (`id`, `type`, `contentType`, `pagetitle`, `longtitle`, `description`, `alias`, `link_attributes`, `published`, `pub_date`, `unpub_date`, `parent`, `isfolder`, `introtext`, `content`, `richtext`, `template`, `menuindex`, `searchable`, `cacheable`, `createdby`, `createdon`, `editedby`, `editedon`, `deleted`, `deletedon`, `deletedby`, `publishedon`, `publishedby`, `menutitle`, `donthit`, `haskeywords`, `hasmetatags`, `privateweb`, `privatemgr`, `content_dispo`, `hidemenu`, `alias_visible`) VALUES
(20, 'document', 'text/html', '10 мифов об эмиграции', '', '', 'myths', '', 1, 0, 0, 0, 0, '', '<p>Зачастую люди не понимают значения слов эмиграция и иммиграция. Поэтому для начала давайте разберемся, как говорят юристы, с понятиями.</p>\r\n<p><strong>Эмигра́ция</strong> (от лат. emigro &mdash; &laquo;выселяюсь&raquo;) &mdash; переселение из одной страны в другую по экономическим, политическим, личным обстоятельствам.</p>\r\n<p>Эмиграция представляет собой добровольное переселение лица или семьи, в отличие от насильственного переселения &nbsp;(выселения из страны или депортации). Причины эмиграции &mdash; война, голод, бедность, политические репрессии, этнические конфликты, межконфессиональные противоречия, природные и экологические катастрофы, воссоединение семьи, невозможность получить образование, профессию, работу, трудности в реализации творческих, профессиональных, экономических и других личных и семейных планов в стране проживания.</p>\r\n<p>В отличие от просто миграции внутри района или между отдельными регионами одной страны, эмиграция связана с выездом из страны, то есть подразумевает пересечение границы. Эмиграция отличается от кратковременных поездок с личными и деловыми целями или туристских путешествий тем, что обязательно связана со сменой постоянного места жительства.</p>\r\n<p>Антоним эмиграции &mdash; иммиграция, то есть: приезд в страну на постоянное место жительства. <strong>Иммигра́ция</strong> (от лат. Immigro &mdash; &laquo;вселяюсь&raquo;) &mdash; въезд населения одной страны в другую на временное или постоянное проживание, рассматриваемый по отношению к стране, куда въезжают мигранты.</p>\r\n<p><strong>Реэмиграция</strong> &mdash; возвращение эмигранта в страну первоначального проживания, возвратная миграция.</p>\r\n<p>В отличие от реэмиграции, репатриация &mdash; возвращение на родину, но возможно не обязательно непосредственно свою, а своих предков.</p>\r\n<p>Эмиграцию, как сказано выше могут вызывать ряд причин политического, экономического, религиозного, расового и национального характера.</p>\r\n<p>Эмиграция бывает временной, например, выезд на обучение и на постоянное проживание.</p>\r\n<p>Основные виды эмиграции, определенные в законодательствах государств являются:</p>\r\n<ol>\r\n<li>Профессиональная (Канада, Австралия, Великобритания);</li>\r\n<li>На основании инвестиции (от 100.000 долларов США и выше);</li>\r\n<li>На основании брака с иностранцем (гражданином или иностранцем, обладающим ПМЖ в ином /искомом/ государстве);</li>\r\n<li>При регистрации компании и получении на этом основании вида на жительство (либо долгосрочной визы, как правило, на 1 год с правом продления;</li>\r\n<li>На основании рабочего контракта и получение сначала временного, а затем постоянного вида на жительство;</li>\r\n<li>На основании воссоединения с близким родственником (супругом или родителем), имеющим статус гражданина соответствующего государства (США, Великобритания, Австралия, Канада и т.д.)</li>\r\n<li>\r\n<p>На основании получения статуса беженца.</p>\r\nКроме этих основных, существуют и национальные особенности иммиграционного законодательства отдельных государств (той же Испании или Великобритании) для граждан государств &ndash; их бывших колоний.</li>\r\n</ol>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>Давайте попытаемся разобраться с мифами, витающими вокруг эмиграционных процессов и эмиграции.</p>\r\n<p>Подготовка и подача документов для эмиграции сложный и долгий процесс. Это неверно, т.к. эмиграция в первую очередь юридический процесс, связанный с возможность лица выехать в другое государство и оставаться там определенное время (длительно или постоянно). Я имею ввиду только легальную эмиграцию. Процесс подготовки документов и их рассмотрения может достигать от нескольких месяцев до нескольких лет. Все зависит от государства и от типа эмиграции. В США процесс воссоединения с мужем может длиться достаточно долго, иногда до 5 лет, если муж, например, &nbsp;уехал по программе Green Card, а семья осталась дома. Пока муж не получит гражданство США процесс будет затруднителен. Напротив, воссоединение с мужем, если последний находиться на территории другого государства, например Великобритании или ФРГ, будет гораздо быстрее, если муж имеет работу, позволяющую содержать себя и членов своей семьи и жилье. &nbsp;Понятие &laquo;ускорить процесс рассмотрения&raquo; &ndash; отсутствует в законодательстве государств ЕС, США, Канады и Австралии. Все осуществляется в порядке очередности рассмотрения файлов (поданных заявлений, прошений, ходатайств и т.п. документов).</p>\r\n<p>Зачем кому-то нужно платить, если можно все сделать самому. От правильной подготовки документов, их подачи, подготовки к собеседованию в дипломатических представительствах (если таковое необходимо) зависит получения позитивного решения. Процесс подготовки и подачи документов можно осуществлять как самому (благо просторы Интернета огромные), так и при помощи специалиста. &nbsp;В первом случае человек самостоятельно осуществляет процесс подготовки и подачи документов. Так он экономит свои деньги и иногда немалые. В отношении нервов и времени мнения расходятся, т.к. сам сбор информации, ее обработки, анализа может занять достаточно много времени. Риск, что документы будут подготовлены и поданы неправильно достаточный. Этот риск увеличивается значительно, если вместе с человеком едут зависимые члены его семьи. В случае неудачи, потери времени, нервов и денег человек винит себя. Во втором случае работа осуществляется человеком с помощью специалиста. Не стоит думать, что специалист за деньги сделает всю работу, а претендент будит &laquo;почивать на лаврах&raquo;. Этого однозначно не будет, т.к. обязанность специалиста - помочь претенденту верно оценить свои шансы, а при позитивных шансах - помочь подготовить документы, сведя к минимуму риск получения негативного решения. Кроме того, специалист поможет иногда существенно сэкономить время, нервы и деньги. Наиболее подходящий вариант, когда специалист и претендент работают сообща. Однако работа должна строиться на основании договора. В договоре должно быть указано не просто &ndash; &laquo;предоставление консультаций по вопросу иммиграционного законодательства&raquo;, а указано: что конкретно осуществляет каждая из сторон и какова цель самого процесса. Опять же &ndash; эмиграция, в первую очередь, юридический процесс, в котором может и должен разобраться специалист, имеющий определенные знания и опыт в этой области, а также знающий, как соотнести эмиграционное законодательство государства гражданской принадлежности претендента и иммиграционное законодательство соответствующего государства, куда последний намеревается сам или в составе своей семьи выехать.</p>\r\n<p>Эмиграция требует существенных денежных средств. Это правда отчасти, т.к. процессы эмиграции при помощи инвестиций или регистрации компаний действительно требуют наличия достаточных финансовых ресурсов, которые необходимо не только показать, но и доказать факт их легального происхождения. Не стоит думать, что зарегистрировав компанию за рубежом сразу можно получить ПМЖ. Надо знать, что при регистрации компании необходимо будет иметь не менее 2-х наемных работников, которым придется платить заработную плату согласно. Нужно будет показывать деятельность самой компании. Нужно будет платить налоги и т.д. Напротив, при воссоединение с мужем эмиграция не требует серьезных денежных вливаний. Эмиграция по профессиональному признаку требует наличия денежных средств на своем счету и не требует, чтобы эти деньги были использованы только в самом государстве. Одним из важных и правильных, на мой взгляд, требований является следующее: доказательство легального происхождения денежных средств и возможность эмигранта обойтись без обращений за дополнительной помощью в социальные фонды.</p>\r\n<p>Получение въездной визы &ndash; окончания процесса переезда. На мой взгляд &ndash; это весьма распространенное ошибочное мнение. Это напоминает сказку, в которой все перипетии заканчиваются счастливым окончанием &ndash; свадьбой. Однако жизнь не сказка и надо понимать, что виза в паспорте &ndash; начало эмиграции. Из своего опыта скажу, что желательно, чтобы человек выезжающий знал язык государства, куда он едет. В связи с этим целиком и полностью поддерживаю иммиграционное законодательство тех государств, где без знания языка невозможно получить въездную визу. Это такие государства как Канада, Австралия, Великобритания, если мы говорим о профессиональной эмиграции, а так же ФРГ. Сейчас во многих западноевропейских государствах рассматривают вопрос об изменении своего законодательства в части получения эмигрантами визы на въезд и проживание только при наличии соответствующего знания языка, подтвержденного определенным сертификатом. Кроме того, желательно, чтобы эмигрант хотя бы в общих чертах знал иммиграционное законодательство государство, куда выезжает, чтобы не попасть в неприятные ситуации.</p>\r\n<p>При выезде предоставляются хорошие блага и государство оказывает помощь. Этот миф родился, когда наши туристы во времена распада СССР стали ездить за рубеж. &nbsp;При выезде как профессиональный эмигрант государство требует наличие у претендента достаточных средств, чтобы последний не обращался за помощью в социальные фонды. Практически та же ситуация наблюдается при воссоединении с семьей. Чтобы жена получила въездную визу от мужа, требуется показать наличие легально зарабатываемых им средств (либо достаочных сбережений), необходимых для содержания себя и своей семьи, а так же наличие соответствующего жилья. Небольшие дотации и жилье &nbsp;получают лица, чье прошение о предоставлении статуса беженца находится на рассмотрении. В каждом государстве уровень дотаций и качество жилья различный. Обычно дотации не превышают прожиточного минимума, установленного в государстве. Отдельную категорию составляют лица, выезжающие в государство Израиль как репатрианты и лица, выезжающие в ФРГ по немецкой или еврейской эмиграции. Но даже в этих государствах введены жесткие ограничения по суммам и по срокам выплаты этих пособий. Иная ситуация происходит, когда хотят воссоединиться с родителями или ближайшими родственниками. Тогда тот, кто подает соответствующее ходатайство перед иммиграционными службами своего государства обязан подтвердить, что при воссоединение он/она полностью берет содержание своих родителей на себя. А последние фактически не имею право обращаться за помощь в социальные фонды государства, куда переезжают. Существуют очень часто в данной ситуации и дополнительные факторы, весьма существенно влияющие на процесс воссоединения.</p>\r\n<p>Адаптация при эмиграции проходит быстро. Это один из наиболее распространенных мифов об эмиграции. После въезда в страну у человека начинается процесс адаптации к жизни. Он попадает &nbsp;в государство, где действуют иной язык, иные законы, иные традиции и обычаи. Как сказал один из моих клиентов, не стоит путать туризм с эмиграцией. Эмигранту не стоит (ни в коем случае!) осуждать жителей этого государства за то, что их язык, жизненный уклад и законодательство иные, чем у него на родине. Этого делать - категорически не стоит. Жители живут так, как нравиться им, как предписывают им их законы. Одним из важных пунктов, как я говорил ранее &ndash; узнать о жителях, их традициях и обычаях, а так же самом государстве как можно больше. Постараться не запираться в 4-х стенах, боясь общения, а наоборот, как можно больше общаться. Пусть с ошибками, но на этом и будет строиться приобретаемый опыт. Еще одной из проблем является &ndash; общение с бывшими &laquo;нашими&raquo;. Собираясь вместе, они, как правило, осуждают &laquo;местных&raquo; и говорят об их &nbsp;бестолковости. Этим в уме вновь прибывших эмигрантов закладывается превосходство, что в свою очередь влияет на психическое состояние эмигрантов. Этим грешат особенно в семьях, в которых наши женщины выходят замуж за иностранцев. Не стоит пытаться найти различия. Нужно найти точки соприкосновений. И чем больше таких точек, тем быстрее проходит процесс адаптации и интеграции. Сам же процесс растягивается от 4 до 7 лет постоянного проживания в государстве.</p>\r\n<p>При эмиграции я быстро найду работу и буду хорошо зарабатывать, т.к. был первоклассным специалистом на родине. Это в корне неверно, т.к. в большинстве европейских и иных цивилизованных государствах существует целая система подтверждения наших дипломов и сертификатов. Обычно приходится начинать с 0, т.е. с самой низкой ступени, если только сам человек не был уровня Эйнштейна, Ростроповича, Для того, чтобы достичь хотя бы минимального уровня, позволяющего считать себя специалистом начального уровня необходимо хорошее знание языка, опыт работы в данном государстве по специальности и наличие рекомендаций от работодателей либо партнеров, если эмигрант self employed (частный предприниматель). Кроме того, во многих государствах существуют специальные экзамены, позволяющие не только допускать эмигрантов к работе, но и в дальнейшем открывать для него определенные возможности. Так в Великобритании для возможности работать врачом, необходимо получить сертификаты о сдаче экзаменов PLAB1 и PLAB2. И это только для начала. Не говоря о том, что наш врач может иметь &laquo;семь пядей во лбу&raquo; и отработать 10 и более лет в качестве хирурга. И это при том, что сама Великобритания испытывает большой дефицит медработников различных уровней. Т.о. они стараются не допускать &laquo;специалистов&raquo;, не знающих язык и которые не могут доказать свою квалификацию по их требованиям. Естественно от должности зависит и зарплата. Я не говорю об открытии бизнеса, т.к. в данном случае уровень заработка целиком зависит от человека. Не стоит требовать от специалиста, который помогает оформлять документы, чтобы последний нашел работу для претендента. Он может только помочь своими рекомендациями и подготовкой документов &ndash; CV или рекомендательных писем. Сам же поиск работы ложиться на плечи претендента. Если же специалист имеет соответствующую лицензию на трудоустройство, то может оказать реальную помощь. Однако, в своей практике не видел ни разу, чтобы иммиграционный специалист одновременно занимался трудоустройством.</p>\r\n<p>Я уезжаю навсегда и поэтому можно все продать, чтобы купить себе достойное жилье в эмиграции. Это так же не верно. Жизнь достаточно сложная вещь и всегда стоит оставлять себе пути отступления, если что-то пойдет не так, как хотелось бы. Тем более, что за рубежом покупают жилье не многие. В основном его снимают в зависимости от места работы (в пределах страны), финансовой составляющей и т.д.</p>\r\n<p>Я поеду по турпутевке, останусь нелегально, а потом легализуюсь как-нибудь. Это так же неверно, т.к. оставаясь нелегально, человек уже умышлено нарушает закон, и как говорят юристы, знает о противоправности своих действий, знает последствиях, которые наступают вслед за этим, и если не желает, то сознательно допускает их наступления. Рано или поздно человек понимает, что ему, чтобы устроиться на работу, нанять жилье, получить медицинскую помощь или просто безбоязненно ходить по улице - нужны документы. Однако, его нелегальное положение не позволяет их получить. И он начинает искать выход из сложившейся ситуации. Очень часто приходиться сталкиваться с тем, что некоторые люди покупают себе фальшивые документы. Т.о. за одним противоправным действием следует иные, т.к. даже покупка, не говоря об использовании, этих документов является преступлением и санкции в связи с этим достаточно жесткие. Если выявляются данные факты, то за этим следует и тюрьма, как нелегалу и преступнику, депортация и пожизненный запрет на въезд не только в это государство, но и в ряд других, т.к. в полиции в любом случае &laquo;откатают&raquo; пальчики и этот снимок будет в общей базе EuroDAC (и иных системах типа VIS, SIS, и т.п. системы) а, следовательно, во всех посольствах и на пунктах пропуска через границы ЕС. Особенно это практикуется сегодня, когда во многих посольствах иностранных государств введено электронное сканирование отпечатков пальцев и нельзя, как ранее практиковалось, поменять паспорт, изменив фамилию. Поэтому специалисты, серьезно и долго, занимающиеся эмиграцией и иммиграцией категорически не рекомендуют рассматривать нелегальное нахождение на территории иного государства как один из видов эмиграции. То же самое касается и фальшивых документов, которые нередко предлагают использовать при оформлении документов. И иммиграционный специалист, и иммиграционный адвокат, никогда не станет использовать в своей работе такие документы, т.к. репутация зарабатывается годами, и может быть разрушена в одночасье.</p>\r\n<p>Фиктивный (коммерческий) брак &ndash; как один из видов эмиграции. Фиктивный или, как его часто называют, коммерческий брак так же не стоит рассматривать как один из видов эмиграции. Дело в том, что человек рано или поздно попадает в финансовую и психологическую зависимость от другого лица и последний может воспользоваться этой зависимостью. Ведь цель фиктивного брака не создание семьи, а получение разрешение на постоянное место жительство в государстве. Т.о. человек совершает противоправные действия, и опять, тут вступают нормы защиты, введенные в государстве, в отношении таких лиц. Т.е. если иностранец от кого человек зависит, будет чем-либо недовольно, пойдет в полицию и напишет заявление о том, что его вторая половина заключила с ним брак только из желания получить постоянное место жительство в государстве, то человек будет арестован и депортирован из государства со всем набором правовых последствий.</p>\r\n<p>&nbsp;</p>\r\n<p>В заключении хочется сказать, что сам процесс эмиграции состоит из многих образующих и включает в себя много этапов. Поэтому когда человек для себя решит что ехать нужно и выберет себе государство, куда бы хотел выехать и тип иммиграционной программы, то пусть он серьезно отнесется к этому своему решению и не ищет &nbsp;незаконных способов осуществления своего желания.</p>', 1, 7, 13, 1, 1, 1, 1538668868, 1, 1538730137, 0, 0, 0, 1538730137, 1, '', 0, 0, 0, 0, 0, 0, 0, 1),
(21, 'document', 'text/html', 'Гражданство или вид на жительство.', 'Гражданство или вид на жительство. Что лучше? Что выбрать?', '', 'гражданство-или-вид-на-жительство', '', 1, 0, 0, 6, 0, '', '<p>Достаточно часто у людей возникает вопрос что лучше иметь гражданство соответствующего государства либо же можно обойтись картой резидента/картой постоянного резидента. Для того, чтобы понять, что людьми движет, приходится выступать даже в роли психолога. Давайте разберемся вместе.</p>\r\n<p><br />Гражданство - постоянная правовая связь лица и государства, которая выражается в их взаимных правах и обязанностях. Согласно Всеобщей декларации прав человека от 1948 года, международным пактам о правах человека, никто не может быть лишён гражданства или права на его изменение. Каждому гражданину государство гарантирует правовую защиту, где бы он ни находился.</p>\r\n<p>Гражданство человек получает в связи с какими-либо основаниями. Вот как трактует основания получения гражданства ст.6 Закона Украины &laquo;О гражданстве&raquo;:</p>\r\n<p><br /><strong>Гражданство Украины предоставляется</strong>:</p>\r\n<ol>\r\n<li>по рождению;</li>\r\n<li>по территориальному происхождению;</li>\r\n<li>в результате принятия гражданства;</li>\r\n<li>в результате восстановления в гражданстве;</li>\r\n<li>в результате усыновления;</li>\r\n<li>в результате установления над ребенком опеки или попечительства;</li>\r\n<li>в результате установления над лицом, признанного судом недееспособным, опеки;</li>\r\n<li>в связи с пребыванием в гражданстве Украины одного или обоих родителей ребенка;</li>\r\n<li>в результате признания отцовства или материнства или установление факта отцовства или материнства;</li>\r\n<li>по другим основаниями, предусмотренными международными договорами Украины.</li>\r\n</ol>\r\n<p>Примерно такие же положения существуют в законодательстве иностранных государств, регулирующих получение гражданства.&nbsp;&nbsp;Отсюда следует, что второе гражданство и второй паспорт, необходимы человеку, в связи с его нынешним гражданством либо к нему может быть применим, один из нижеприведенных случаев:</p>\r\n<ul style=\"list-style-type: circle;\">\r\n<li>&nbsp;Ваш нынешний паспорт, должен обновляться или продлеваться, ранее, чем через пять лет после выдачи;</li>\r\n<li>Может быть изъят или аннулирован Вашим нынешним правительством или его органами;</li>\r\n<li>Ограничивает Ваши инвестиционные или предпринимательские возможности;</li>\r\n<li>Приводит к налогообложению Ваших доходов, имущества и капитала по всему миру;</li>\r\n<li>Делает Вас уязвимым для террористов;</li>\r\n<li>Позволяет правительству Вашей страны или его органам ограничивать, контролировать или вести учет Ваших передвижений;</li>\r\n<li>Показывает, что Вы являетесь гражданином не очень популярного государства, вызывая предвзятое или негативное отношение к Вам;</li>\r\n<li>Ограничивает Вашу свободу передвижения;</li>\r\n<li>Ваше государство политически не стабильно;</li>\r\n<li>Вы можете ожидать различных провокаций от Ваших государственных органов, криминала или партнеров по бизнесу;</li>\r\n<li>Вы хотите изменить свое место жительства;</li>\r\n<li>Вы привыкли или с годами захотели быть застрахованным от неприятных неожиданностей.</li>\r\n</ul>\r\n<p><strong class=\"cssClass\">Почему это делают?</strong></p>\r\n<p><br />Сразу оговоримся, есть два основных типа граждан, которые получают второе гражданство и паспорт. Первая, это достаточно состоятельные люди, которые устали от варварских налогов и готовы сменить свое место жительства или уже сменили его. Вторая категория - получающих второе гражданство и паспорта &ndash; это не бесконечно богатые люди, а те, кто хочет заниматься бизнесом и производить инвестиции, свободно перемещаться по миру Эти люди продолжают жить и работать у себя, в государстве своей гражданской принадлежности, но иметь возможность, в любое время, спокойно поменять место жительства. Старая американская пословица гласит, что &laquo;в этой жизни только две гарантированные вещи &ndash; это смерть и налоги&raquo;.</p>\r\n<p><br />В настоящее время граждане Великобритании в связи с выходом последней из Евросоюза всячески пытаются решить вопрос со вторым гражданством ЕС. В обратную сторону, процесс также набирает обороты. Т.е. граждане ЕС, пытаются получить гражданство Великобритании.</p>\r\n<p><br /><span class=\"cssClass\"><strong>Кому необходим второй паспорт?</strong></span></p>\r\n<p><br />Он необходим американцам и европейцам, для избегания или планирования налогов. Он нужен восточно-европейцам, гражданам республик бывшего СССР, гражданам и многих других стран, чтобы иметь возможность:</p>\r\n<ul style=\"list-style-type: circle;\">\r\n<li>более свободно передвигаться по миру;</li>\r\n<li>застраховаться от нестабильности в стране и преступности;</li>\r\n<li>открывать за границей счета, бизнес, заниматься инвестициями, одновременно с этим планируя свои налоги.</li>\r\n</ul>\r\n<p class=\"cssClass\"><br /><strong>Почему существуют программы предоставления гражданства?</strong></p>\r\n<p class=\"cssClass\"><br />Программы предоставления гражданства и паспорта в замен на инвестиции существуют во многих странах мира. Например, в таких как: США, Канада, Австралия, Новая Зеландия, Австрия, Ирландия, Великобритания, Доминика. Они отличаются, в основном только четырьмя моментами:</p>\r\n<ul style=\"list-style-type: circle;\">\r\n<li class=\"cssClass\">размером инвестиций;</li>\r\n<li class=\"cssClass\">возвратные или не возвратные;</li>\r\n<li class=\"cssClass\">сроком ожидания великодушного согласия принять инвестиции;</li>\r\n<li class=\"cssClass\">сроком ожидания гражданства после произведения инвестиций.</li>\r\n</ul>\r\n<p>Известная аксиома есть спрос, будет и предложение. Зачем нужны такие программы таким странам, как США и Великобритания? Ответ прост, чтобы компенсировать средства собственных граждан, и бывших граждан бегущих от налогов. Кто ими пользуется? Граждане нестабильных стран, с рискованной или криминальной экономикой. Зачем программы предоставления гражданства, таким странам, как Доминика, Коста-Рика? Ответ также прост, для того, чтобы пополнить бюджет, развить туристическую инфраструктуру, поддержать сельское хозяйство. Кто получает гражданства этих стран, зачем и почему, мы рассмотрели выше.</p>\r\n<p>Теперь рассмотрим вид на жительство. Он может быть срочным &ndash; выданным на определенный срок (работа по контракту, учеба и т.д.) и бессрочным без срока действия. В простонародье зовется ПМЖ (постоянное место жительство). Вид на жительство - документ, выдаваемый иностранным гражданам или апатридам на право проживания в данном государстве. Оно получается так же в связи с какими-либо основаниями. Вот как трактует эти основания ст. 4 Закона Украины &laquo;Об иммиграции&raquo;:</p>\r\n<p><br /><strong>Разрешение на иммиграцию предоставляется в пределах квоты иммиграции.</strong><br />Квота иммиграции устанавливается Кабинетом Министров Украины в определенному им порядку по категориям иммигрантов:</p>\r\n<ol>\r\n<li>деятели науки и культуры, иммиграция которых отвечает интересам Украины;</li>\r\n<li>высококвалифицированные специалисты и рабочие, острая потребность в которых является ощутимой для экономики Украины;</li>\r\n<li>лица, которые осуществили иностранную инвестицию в экономику Украины иностранной конвертированной валютой на сумму не меньше 100 (ста) тысяч долларов США, зарегистрированную в порядке, определенном Кабинетом Министров Украины;</li>\r\n<li>лица, которые являются полнородными братом или сестрой, дедом или бабой, внуком или внучкой граждан Украины;</li>\r\n<li>лица, которые раньше находились в гражданстве Украины;</li>\r\n<li>родители, мужчина (жена) иммигранта и его несовершеннолетние дети;</li>\r\n<li>лица, которые непрерывно прожили на территории Украины на протяжении трех лет со дня предоставления им статуса беженцев в Украине пристанищу ли в Украине, а также их родители, мужчины (жены) и несовершеннолетние дети, которые проживают вместе с ними.</li>\r\n</ol>\r\n<p><strong>Разрешение на иммиграцию вне квоты иммиграции предоставляется:</strong></p>\r\n<ol>\r\n<li>одному из супругов, если второй из супругов, с которыми он находится в браке свыше двух лет, является гражданином Украины, детям и родителям граждан Украины;</li>\r\n<li>лицам, которые являются опекунами или попечителями граждан Украины, или находятся на попечении или заботе граждан Украины;</li>\r\n<li>лицам, которые имеют право на приобретение гражданства Украины за территориальным происхождением;</li>\r\n<li>лицам, иммиграция которых представляет государственный интерес для Украины;</li>\r\n<li>заграничным украинцам, супругами заграничных украинцев их детям в случае их общего въезда и пребывания на территории Украины.</li>\r\n</ol>\r\n<p><br />Ранее в законодательстве Украины было положение, что иностранец, проживший легально 5 лет на территории государства имеет право на получение бессрочного вида на жительства. А через некоторое время он или она имели право на получение гражданства Украины. К большому сожалению, сейчас это положение убрали. Во многих комментариях наши юристы и адвокаты, отвечая на вопросы, говорят, что наличие второго гражданства ведет к автоматическому выходу из гражданства Украины или его утраты. Это, отнюдь не так. Законодательство Украины не запрещает второе или двойное гражданство (разные понятия с разными правовыми последствиями) оно не предусматривает ни второго ни двойного гражданства. Это можно увидеть из ст. 19 Закона Украины &laquo;О гражданстве&raquo;. Оно так же ничего не говорит об автоматическом лишении человека гражданства. Т.е. человек, который имеет двойное или второе гражданство, будет рассматриваться законодательством Украины исключительно как гражданин Украины, если он имеет паспорт гражданина Украины. Я как, юрист, попытался разобрать ситуацию с гражданством и с видом на жительство. На мой взгляд, человек, претендующий на получение гражданства Украины должен себе четко представлять, что он получает, приобретая гражданство и утрачивая предыдущее. А оно сводится все то к нескольким положениям &ndash; не гражданин (имеющий ВНЖ) не имеет право выбирать, быть избранным, служить в вооруженных силах, органах безопасности и МВД и работать на государственной службе. Все визы человек будет получать по месту своего проживания, а не гражданства. Поэтому стоит задать вопрос: &laquo;А нужно ли мне и моим детям гражданство? Или можно обойтись видом на жительство&raquo;</p>', 1, 7, 0, 1, 1, 1, 1538730720, 1, 1538732173, 0, 0, 0, 1538730732, 1, '', 0, 0, 0, 0, 0, 0, 0, 1),
(22, 'document', 'text/html', 'Почему не нужно покупать паспорт Украины', 'Почему не нужно покупать паспорт Украины', '', 'why_do_not_you_need_to_buy_a_passport_of_ukraine', '', 1, 0, 0, 6, 0, '', '<p>В бесконечной сети интернет есть много предложений &laquo;купить паспорт Украины&raquo;, &laquo;сделать паспорт и загранпаспорт Украины&raquo;, &laquo;купить гражданство Украины&raquo; и т.п. При этом &laquo;продавцы&raquo; дают гарантии высокого качества &laquo;оригинальных документов&raquo;, &laquo;проведение по всем базам&raquo;, &laquo;конфиденциальность&raquo; и др.</p>\r\n<p>Стоимость таких услуг может казаться совсем небольшой для человека, которому нужен паспорт гражданина Украины (гражданство Украины), но он не знает, как его получить легально, думает, что в его ситуации это невозможно. Или официальная процедура получения гражданства Украины кажется ему долгой, зарегулированной и дорогой.</p>\r\n<p>Даже если повезёт и в обмен на денежные средства пришлют желанный документ, следует еще до этого задуматься: а что же в итоге покупаешь?</p>\r\n<p><br />Когда речь едёт о проверке паспорта, то это не только определение подлинности бланка. Бланк и все записи, подписи, печать и штампы - возможно, будут оригинальными, даже у документов, купленных в интернете. Проблема в другом. <span style=\"text-decoration: underline;\"><strong>Никаких оснований для выдачи&nbsp;</strong></span><span style=\"text-decoration: underline;\"><strong>этого документа не было!</strong></span> Именно этот факт делает официальный документ нелегитимным, заведомо неправдивым и подложным, юридически &ndash; это подделка. А в случае обнаружения подделки - паспорт подлежит изъятию (паспорт является собственностью государства Украина) и у &laquo;гражданина&raquo; (владельца паспорта) возникают неприятные последствия.</p>\r\n<p>При более глубокой проверке паспорта и его владельца (наличие документов /оснований/ для выдачи, ответов на запросы др.) очень быстро компетентными органами обнаруживается подлог (подделка), а у &laquo;гражданина&raquo; возникают неразрешимые проблемы: восстановление &laquo;утраченного&raquo; документа (см. наш пример), получение загранпаспорта, ведение деятельности предпринимателя, распоряжение недвижимостью, приобретенной по нелегитимному документу, и многое другое.</p>\r\n<p>Для наглядности приведём реальный пример из нашей практики:</p>\r\n<p>Гражданин Вьетнама потерял оригинал Удостоверения о постоянном проживании в Украине. На руках только его ксерокопия. Желание &ndash; получить дубликат документа в минимальные сроки. Мы обратились в интересах клиента в органы паспортной и иммиграционной службы и выяснилось: внутренним распоряжением по ведомству это Удостоверение о ПМЖ - аннулировано! Причина: при проверке дела этого заявителя оказалось, что его папка (дело) - пуста и в ней нет ни одного документа, который должен быть основанием для получения ПМЖ в Украине. &laquo;Органы&raquo; даже не обращались в суд для признания недействительным Удостоверения: а зачем? ведь бланк документа, печать и подпись &ndash; настоящие! Поэтому по ведомству просто было отменено распоряжение о выдаче Удостоверения для этого гражданина. В результате он и немалое число его соотечественников, заказывавших документы в одном и том же &laquo;месте&raquo; и получившие &laquo;на выходе&raquo; аннулированные документы &ndash; нелегально находятся на территории Украины с &laquo;липовым&raquo; (подложным) документом. А при первом же личном обращении &laquo;в органы&raquo; им грозит - задержание с проверкой для выяснения этой самой личности и Протокол о нарушении паспортного режима и нелегальном пребывании на территории Украины... Ну и далее список &laquo;подарков&raquo; для этих лиц со всеми исходящими последствиями.</p>\r\n<p>Какие ещё сюрпризы можно получить от объявлений в сети интернет, что может быть самым проблемным и случиться раньше - сейчас долго рассуждать не будем. Но будет хорошо, если проблемы ограничатся потерей денежных средств (правда, зачастую весьма немалых), т.к. есть реальная перспектива оказаться под уголовным преследованием и получить уголовное наказание, быть депортированным с территории Украины (иногда это наиболее нежелательно), получить отмену регистрации предпринимателя и т.д. Казус ситуации в том, что &laquo;проведенный по базам&raquo; официальный паспорт действительно выдан уполномоченным государственным органом, подписан реальным должностным лицом и скреплен оригинальной печатью. Но административным штрафом &laquo;гражданин&raquo;-владелец такого паспорта вряд ли отделается&hellip;</p>\r\n<p>Поэтому оборотная сторона &laquo;купленного&raquo;, &laquo;проведенного&raquo; паспорта Украины может перечеркнуть все выгоды от его, относительно, невысокой стоимости.<br />Таким образом, объяснения по типу &laquo;оригинальный бланк&raquo;, &laquo;проведено по всем базам&raquo; говорит совершенно ни о чём, поскольку &laquo;базы&raquo; - не есть юридическое основание для получения гражданства Украины и паспорта.</p>\r\n<p>Следует чётко понимать: паспорт может быть выдан только на определённых основаниях, которые подтверждаются документально. Например - достижение 16 лет, что подтверждается Свидетельством о рождении в Украине, или принятие гражданства Украины, что подтверждается Справкой о регистрации гражданином Украины и т.д. (см. раздел Гражданство Украины). Ещё раз повторюсь: главное не само получение паспорта, а наличие законных оснований для этого. Поэтому документ, выданный без оснований, ничем не отличается, по сути, от ворованного паспорта с переклеенной фотографией: он является такой же &laquo;липой&raquo;.</p>\r\n<p>Мы предлагаем полное юридическое сопровождение и оформление гражданства Украины также тем, у кого на сегодня нет оснований для получения паспорта гражданина Украины. Мы ведём процесс от разбирательства с законными основаниями - до получения общегражданского и заграничного паспортов. Таким образом, Вы официально становитесь гражданином Украины, для чего все регистрации и процедуры будут осуществлены в минимально возможные сроки.</p>\r\n<p>Да, стоимость наших услуг несколько выше, чем предложения &laquo;купить паспорт&raquo;. Но Вы становитесь гражданином Украины &ndash; только официально, только на законных основаниях и лично получаете Справку о регистрации гражданином Украины в органах иммиграционной и паспортной службы Украины. После этого лично получаете паспорт гражданина Украины и, если нужен, загранпаспорт и другие документы (военный билет, водительское удостоверение). По всем вопросам о гражданстве Украины обращайтесь.</p>', 1, 7, 1, 1, 1, 1, 1538732347, 1, 1538733092, 0, 0, 0, 1538732359, 1, '', 0, 0, 0, 0, 0, 0, 0, 1),
(23, 'document', 'text/html', 'Sole Representative', 'Sole Representative - Единственный представитель зарубежной компании', '', 'sole_representative', '', 1, 0, 0, 11, 0, '', '<h3><strong>Первоначальная заявка на получение визы Единственный&nbsp;</strong><strong>представитель</strong></h3>\r\n<p>Иммиграционная бизнес категория &lsquo;Единственный представитель зарубежной компании&rsquo; была введена в действие в соответствии с Иммиграционным законом от 1971 года. Это достаточно гибкая категория, позволяющая заявителю приехать в Великобританию для открытия представительства или филиала компании, зарегистрированной в другой стране, если до этого момента таковых филиалов в Великобритании зарегистрировано не было. При этом головная компания должна продолжать вести основной бизнес и выполнять свои функции за рубежом.&nbsp;&nbsp;</p>\r\n<p>Перед тем, как отправить своего единственного представителя в Великобританию, компания должна существовать не менее 12 месяцев и иметь веские основания для открытия филиала: наработанные связи в Великобритании, долгосрочные и перспективные договоры и т.п. Если из представленных документов будет очевидно, что, направляя своего единственного представителя в Великобританию, компания собирается в будущем перенести туда же свой основной бизнес и головной офис, то в визе будет отказано.</p>\r\n<p>Особо подчеркивается, что данная категория не предназначена для личных иммиграционных целей заявителя. Поэтому важнейшим требованием является финансовая состоятельность основной компании. Документы для получения визы бизнес иммиграции в Великобританию &lsquo;Единственный представитель зарубежной компании&rsquo; должны продемонстрировать, что компания в состоянии основать филиал и содержать своего представителя.</p>\r\n<p>Заявитель на визу единственного представителя должен занимать одну из руководящих должностей в головной компании, так как в зарубежном представительстве он должен будет самостоятельно принимать решения в отношении деятельности компании в Великобритании, не обращаясь в главный офис.&nbsp;</p>\r\n<p>Обязательное условие для заявителя:&nbsp;он не должен являться держателем основного пакета акций компании, так как виза&nbsp;Sole&nbsp;Representative&nbsp;не предназначена для владельцев компании или предпринимателей.</p>\r\n<p>Шансы заявителя на получение визы увеличатся, если он работает на компанию продолжительное время и хорошо знаком со всеми аспектами ее деятельности. Если заявитель работает недавно, то он должен будет продемонстрировать соответствие предназначенной ему роли. Иммиграционные адвокаты компании Law Firm обеспечат Вам полное юридическое сопровождение по подготовке документов для бизнес иммиграции в Великобританию &lsquo;Единственный представитель зарубежной компании &ndash; Sole Representative&rsquo;.</p>\r\n<p>Заявитель должен продемонстрировать знание английского языка на уровне A1по CEFR, что должно подтверждаться сертификатом по результатам сдачи теста из утвержденного Home Office&nbsp; списка , либо иметь диплом, эквивалентный британскому диплому бакалавра, при этом преподавание должно было вестись на английском языке.</p>\r\n<p>Большое преимущество данной категории в том, что заявитель не должен располагать собственными либо заемными средствами для организации бизнеса в Великобритании. Единственное финансовое требование к заявителю: он должен показать, что у него достаточно средств на содержание своей семьи без обращения в государственные фонды, если члены семьи собираются приехать в Великобританию&nbsp;в качестве его иждивенцев.</p>\r\n<p>Логическим продолжением этого требования является то, что заявитель не имеет права работать в Великобритании где-либо еще, кроме как в представительстве материнской компании, а также организовывать собственный бизнес.</p>\r\n<p><strong>Продление визы Единственный представитель Sole Representative</strong></p>\r\n<p>Первоначальная виза выдается на&nbsp;3 года. По окончании этого срока визу можно продлить еще на&nbsp;2 года. При продлении визы заявитель&nbsp;&nbsp;должен доказать экономическую активность филиала/представительства. Требований к минимальным оборотам филиала нет, но нужно будет доказать, что филиал финансово стабилен и имеет постоянные коммерческие обязательства.&nbsp;</p>\r\n<p><strong>Постоянный вид на жительство в Англии</strong></p>\r\n<p>По окончании пятилетнего срока проживания в Великобритании по визе единственного представителя, заявитель может подавать на постоянное место жительства (ILR - Indefinite Leave to Remain).&nbsp;&nbsp;При оформлении&nbsp;ILRнеобходимо будет доказать коммерческую активность филиала в Великобритании в течение всех 5-ти лет. Обычно для этого используются бухгалтерские отчеты, контракты, счета, налоговая отчетность и т.д.&nbsp;&nbsp;Также необходимо будет продемонстрировать, что головная зарубежная компания продолжала оставаться таковой на протяжении всего пятилетнего периода.</p>\r\n<p>Необходимые условия для получения постоянного постоянного вида на<br />жительство следующие:</p>\r\n<ul>\r\n<li>заявитель соответствовал условиям иммиграционной категории&nbsp;&lsquo;Единственный представитель&rsquo;&nbsp;в течение всех пяти лет;</li>\r\n<li>работодатель по-прежнему заинтересован в данном работнике;</li>\r\n<li>заявитель организовал стабильно действующий филиал зарубежной компании;</li>\r\n<li>заявитель получал в течение пяти лет зарплату от головной компании в соответствии с контрактом;</li>\r\n<li>заявитель должен будет сдать тест&nbsp;Life&nbsp;in&nbsp;the&nbsp;UK.</li>\r\n</ul>', 1, 7, 0, 1, 1, 2, 1539186273, 2, 1539186382, 0, 0, 0, 1539186273, 2, '', 0, 0, 0, 0, 0, 0, 0, 1),
(24, 'document', 'text/html', 'Exceptional Talent', 'Tier 1 (Exceptional Talent)', '', 'exceptional-talent', '', 1, 0, 0, 11, 0, '', '<p>Иммиграционная категория Tier 1 (Exceptional Talent) предназначена для талантливых людей, получивших международное признание в области наук, культуры, искусства, а так же имеющие все данные для того чтобы жить и работать в Великобритании.</p>\r\n<ul>\r\n<li>признанный лидер (исключительный талант)</li>\r\n<li>потенциальный лидер (исключительное обещание)</li>\r\n</ul>\r\n<p>Ежегодно по данной категории устанавливается лимит для подачи из-за рубежа. Это 2000 виз для кандидатов из всех стран мира. Началом года считается 6 апреля, а окончание - 5 апреля следующего года.</p>\r\n<p>К этим кандидатам относятся инженеры/ведущие инженеры в различных областях, программисты достаточно высокого уровня (специалисты в области цифровых технологий), доктора наук (PhD), имеющие публикации, монографии<br />и т.д.</p>\r\n<p><span style=\"text-decoration: underline;\">Сама процедура делится на два этапа:</span></p>\r\n<p><span style=\"text-decoration: underline;\">1-ый этап</span>. Заявитель может подавать на визу при наличии официального подтверждения/заключения от уполномоченной британской компетентной организации о том, что он/она действительно соответствует данной категории.</p>\r\n<p>Только заявители, находящиеся в Великобритании по категориям Tier 1, Tier 2&nbsp;и Tier 5 (Temporary Worker) -&nbsp; Government Authorised Exchange для научных работников, могут подать аппликации по категории Tier 1 (Exceptional Talent), не выезжая из UK. Все остальные должны подавать на въездную визу в своей стране.</p>\r\n<p><span style=\"text-decoration: underline;\">2-ой этап</span>. Подача на визу и получение самой визы.</p>\r\n<p><span style=\"text-decoration: underline;\"><strong>Первоначальная заявка</strong></span></p>\r\n<p>Заявитель первоначально должен получить официальное подтверждение/заключение от соответствующей уполномоченной в Великобритании компетентной организации. Заявка на подтверждение/заключение подается только в электронном виде, т.е. online/ Подтверждение должно быть получено не ранее, чем за 3 месяца до даты подачи заявки на визу. Список компетентных организаций, признаваемых Home Office:</p>\r\n<p><strong>Arts Council</strong>&nbsp;&ndash; для заявителей в области искусства и культуры;<br /><strong>The British Academy</strong>&nbsp;&ndash; гуманитарные и общественные науки;<br /><strong>The Royal Society</strong>&nbsp;&ndash; естественные науки, медицинские исследования;</p>\r\n<p><strong>The Royal Academy of Engineering</strong>&nbsp;&ndash; для заявителей, имеющих выдающиеся достижения в области техники.<br /><strong>Tech City UK</strong>&nbsp;&ndash; область цифровых технологий.</p>\r\n<p>Роль каждой компетентной организации &ndash; дать заключение, адресованное Home Office, о том, что, по мнению экспертов организации, заявитель на получения данного статуса обладает исключительными талантами в своей области или&nbsp;большим потенциалом. Что в дальнейшем позитивно скажется как для Великобритании, так и для самого кандидата, т.е. поможет ему/ей быстро интегрироваться в общество и осуществлять свою деятельность с максимально возможными выгодами как для самого общества, так и для кандидата.</p>\r\n<p><span style=\"text-decoration: underline;\"><strong>Критерии оценки кандидатов устанавливаются каждой организацией&nbsp;</strong></span><span style=\"text-decoration: underline;\"><strong>самостоятельно, по согласованию с Home Office, то есть понятию&nbsp;</strong></span><span style=\"text-decoration: underline;\"><strong>&laquo;исключительный талант&raquo; четкого определения нет.</strong></span></p>\r\n<p>Первоначально виза выдается на 5 лет и 4 месяца.</p>\r\n<p>Позитивным моментом данной программы, является отсутствуют требования к уровню английского языка и наличию средств на проживание. Однако, это вовсе не означает, что кандидат не должен иметь, ни того, ни другого.</p>\r\n<p>Отсутствие требований к знанию языка означает, что у кандидата УЖЕ должен быть соответствующий уровень английского, т.к. без него он не сможет быстро интегрироваться в общество и приступить к работе.</p>\r\n<p>Отсутствие этого требования означает, что последнему не нужно предоставлять соответствующий сертификат об этом знании.</p>\r\n<p>Отсутствие требований к наличию финансов означает, что кандидат должен иметь эти финансы на проживание и не должен обращаться за помощью в различные социальные фонды.</p>\r\n<p>Кроме того, заявитель и члены его/ее семьи должен/должны сдать тест на отсутствие туберкулеза в соответствующих сертифицированных медучереждениях.</p>\r\n<p>После получения визы заявитель имеет право на трудоустройство или ведение собственного бизнеса в Великобритании. Исключения: запрещается работать в качестве Doctor or Dentist in Training, профессионального спортсмена или<br />спортивного тренера.</p>\r\n<p>Заявитель имеет право привезти с собой членов семьи в качестве зависимых лиц (dependants). Членами семьи в этом случае считаются супруги, партнеры по незарегистрированному браку, дети до 18 лет.</p>\r\n<p><strong>Продление визы Tier 1 (Exceptional Talent)&nbsp;</strong></p>\r\n<p>Для продления визы заявитель должен продемонстрировать экономическую&nbsp;активность в своей области, подтвержденной компетентной организацией при первоначальной заявке. Это может быть как работа на работодателя, так и собственный бизнес или их сочетание.</p>\r\n<h3>Отсутствуют требования к уровню английского языка.</h3>\r\n<p><strong>Получение постоянного резидентства</strong></p>\r\n<p>Заявители по данной категории смогут подавать на постоянное место жительства через три года. Это не относится к подкатегории Tier 1 (Exceptional Promise), по этой визе по- прежнему&nbsp; ILR &nbsp;можно будет получить после 5 лет.</p>\r\n<p>Для получения ILR заявитель должен будет продемонстрировать знание английского языка на уровне В1 по общеевропейской схеме CEFR, а также сдать тест Life in the UK. Заявитель не сдает тест по английскому языку, если он имеет диплом бакалавра или магистра, и преподавание велось на английском.</p>\r\n<p><strong>Наши услуги</strong></p>\r\n<p>Мы предлагаем следующие услуги, предоставляемые<br />высококвалифицированными иммиграционными специалистами нашей<br />компании:</p>\r\n<ul>\r\n<li>подготовка пакета документов для обращения за первоначальной визой;</li>\r\n<li>подготовка и подача последующих аппликаций как по продлению статуса, так и получению постоянного резидентства и гражданства;</li>\r\n<li>подробное консультирование по необходимым доказательным документам</li>\r\n<li>постоянные консультации по всем планируемым изменениям иммиграционного законодательства;</li>\r\n<li>административный пересмотр решения или апелляция в случае отказа в зависимости от его причины.</li>\r\n</ul>', 1, 7, 1, 1, 1, 2, 1539187412, 2, 1539253736, 0, 0, 0, 1539187427, 2, '', 0, 0, 0, 0, 0, 0, 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `modx_site_content_metatags`
--

CREATE TABLE `modx_site_content_metatags` (
  `content_id` int(11) NOT NULL DEFAULT '0',
  `metatag_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Reference table between meta tags and content';

-- --------------------------------------------------------

--
-- Структура таблицы `modx_site_htmlsnippets`
--

CREATE TABLE `modx_site_htmlsnippets` (
  `id` int(10) NOT NULL,
  `name` varchar(100) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT 'Chunk',
  `editor_type` int(11) NOT NULL DEFAULT '0' COMMENT '0-plain text,1-rich text,2-code editor',
  `editor_name` varchar(50) NOT NULL DEFAULT 'none',
  `category` int(11) NOT NULL DEFAULT '0' COMMENT 'category id',
  `cache_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Cache option',
  `snippet` mediumtext,
  `locked` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Contains the site chunks.';

--
-- Дамп данных таблицы `modx_site_htmlsnippets`
--

INSERT INTO `modx_site_htmlsnippets` (`id`, `name`, `description`, `editor_type`, `editor_name`, `category`, `cache_type`, `snippet`, `locked`) VALUES
(1, 'AjaxSearch_tplAjaxGrpResult', 'Grp Result Tpl for AjaxSearch', 0, 'none', 1, 0, '[+as.grpResultsDef:is=`1`:then=`\n<div id=\"[+as.grpResultId+]\" class=\"AS_ajax_grpResult\">\n[+as.headerGrpResult+]\n[+as.listResults+]\n[+as.footerGrpResult+]\n</div>\n`:else=`\n<div class=\"AS_ajax_grpResultName\">[+as.grpResultNameShow:is=`1`:then=`[+as.grpResultName+]`+]\n<span class=\"ajaxSearch_grpResultsDisplayed\">[+as.grpResultsDisplayedText+]</span></div>\n`+]', 0),
(2, 'AjaxSearch_tplAjaxResult', 'Result Tpl for AjaxSearch', 0, 'none', 1, 0, '<div class=\"[+as.resultClass+]\">\n  <strong><a class=\"[+as.resultLinkClass+]\" href=\"[+as.resultLink+]\" title=\"[+as.longtitle+]\">[+as.pagetitle+]</a></strong>\n[+as.descriptionShow:is=`1`:then=`\n  <small><span class=\"[+as.descriptionClass+]\">[+as.description+]</span></small>\n`+]\n[+as.extractShow:is=`1`:then=`\n  <div class=\"[+as.extractClass+]\"><p>[+as.extract+]</p></div>\n`+]\n[+as.breadcrumbsShow:is=`1`:then=`\n  <span class=\"[+as.breadcrumbsClass+]\">[+as.breadcrumbs+]</span>\n`+]\n</div>', 0),
(3, 'AjaxSearch_tplAjaxResults', 'Results Tpl for AjaxSearch', 0, 'none', 1, 0, '<div id=\"search_results\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\">\n  <div class=\"modal-dialog\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>\n        <h3 class=\"modal-title\">Search Results</h3>\n      </div>\n      <div class=\"modal-body\">\n        [+as.noResults:is=`1`:then=`\n		  <div class=\"[+as.noResultClass+]\">\n			[+as.noResultText+]\n		  </div>\n		`:else=`\n		<p class=\"AS_ajax_resultsInfos\">[+as.resultsFoundText+]<span class=\"AS_ajax_resultsDisplayed\">[+as.resultsDisplayedText+]</span></p>\n		[+as.listGrpResults+]\n		`+]\n		[+as.moreResults:is=`1`:then=`\n		  <div class=\"[+as.moreClass+]\">\n			<a href=\"[+as.moreLink+]\" title=\"[+as.moreTitle+]\">[+as.moreText+]</a>\n		  </div>\n		`+]\n		[+as.showCmt:is=`1`:then=`\n		[+as.comment+]\n		`+]\n      </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-primary\" data-dismiss=\"modal\">Close</button>\n      </div>\n    </div><!-- /.modal-content -->\n  </div><!-- /.modal-dialog -->\n</div><!-- /.modal -->\n<script>$(\'#search_results\').modal(\'show\')</script>', 0),
(4, 'AjaxSearch_tplInput', 'Input-Form for AjaxSearch', 0, 'none', 1, 0, '[+as.showInputForm:is=`1`:then=`\n<form id=\"[+as.formId+]\" action=\"[+as.formAction+]\" method=\"post\">\n    [+as.showAsId:is=`1`:then=`<input type=\"hidden\" name=\"[+as.asName+]\" value=\"[+as.asId+]\" />`+]\n    <input type=\"hidden\" name=\"advsearch\" value=\"[+as.advSearch+]\" />\n	<div class=\"input-group\">\n		<input id=\"[+as.inputId+]\" class=\"form-control cleardefault\" type=\"text\" name=\"search\" value=\"[+as.inputValue+]\"[+as.inputOptions+] />\n		[+as.liveSearch:is=`0`:then=`\n		<span class=\"input-group-btn\">\n			<button class=\"btn btn-primary\" type=\"submit\"><i class=\"fa fa-search\"></i></button>	\n		</span>\n		`:else=`\n		<div class=\"input-group-addon\"><i class=\"fa fa-search\"></i></div>\n		`+]		\n	</div>\n</form>\n`+]\n[+as.showIntro:is=`1`:then=`\n<p class=\"ajaxSearch_intro\" id=\"ajaxSearch_intro\">[+as.introMessage+]</p>\n`+]', 0),
(5, 'Comments_tplComments', 'Comments (Jot) Form-Template', 0, 'none', 1, 0, '<a name=\"jc[+jot.link.id+][+comment.id+]\"></a>\n<div class=\"panel panel-[+chunk.rowclass:ne=``:then=`primary`:else=`info`+] [+comment.published:is=`0`:then=`jot-row-up`+]\">\n	<div class=\"panel-heading\"><span class=\"jot-subject\">[+comment.title:limit:esc+]<span class=\"pull-right\">\n		[+phx:userinfo=`lastlogin`:ifempty=`9999999999`:lt=`[+comment.createdon+]`:then=`\n		<i class=\"fa fa-fw fa-comment-o\" aria-hidden=\"true\"></i>\n		`:else=`\n		<i class=\"fa fa-fw fa-commenting-o\" aria-hidden=\"true\"></i>\n		`:strip+]\n		</span></span>\n	</div>\n	<div class=\"panel-body\">\n		<div class=\"jot-comment\">\n			<div class=\"jot-user\">\n				[+comment.createdby:isnt=`0`:then=`<b>`+][+comment.createdby:userinfo=`username`:ifempty=`[+comment.custom.name:ifempty=`[+jot.guestname+]`:esc+]`+]\n				[+comment.createdby:isnt=`0`:then=`</b>`+]\n				<br>Posts: [+comment.userpostcount+]\n			</div>\n			<div class=\"jot-content\">\n				<div class=\"pull-right btn-group\">\n					[+jot.moderation.enabled:is=`1`:then=`\n					<a class=\"btn btn-xs btn-danger\" href=\"[+jot.link.delete:esc+][+jot.querykey.id+]=[+comment.id+]#jotmod[+jot.link.id+]\" onclick=\"return confirm(\'Are you sure you wish to delete this comment?\')\" title=\"Delete Comment\"><i class=\"fa fa-fw fa-trash\" aria-hidden=\"true\"></i></a> \n					[+comment.published:is=`0`:then=`\n					<a class=\"btn btn-xs btn-info\"href=\"[+jot.link.publish:esc+][+jot.querykey.id+]=[+comment.id+]#jotmod[+jot.link.id+]\" onclick=\"return confirm(\'Are you sure you wish to publish this comment?\')\" title=\"Publish Comment\"><i class=\"fa fa-fw fa-arrow-up\" aria-hidden=\"true\"></i></a> \n					`+]\n					[+comment.published:is=`1`:then=`\n					<a class=\"btn btn-xs btn-warning\" href=\"[+jot.link.unpublish:esc+][+jot.querykey.id+]=[+comment.id+]#jotmod[+jot.link.id+]\" onclick=\"return confirm(\'Are you sure you wish to unpublish this comment?\')\" title=\"Unpublish Comment\"><i class=\"fa fa-fw fa-arrow-down\" aria-hidden=\"true\"></i></a> \n					`+]\n					`:strip+]\n					[+jot.user.canedit:is=`1`:and:if=`[+comment.createdby+]`:is=`[+jot.user.id+]`:or:if=`[+jot.moderation.enabled+]`:is=`1`:then=`\n					<a class=\"btn btn-xs btn-success\" href=\"[+jot.link.edit:esc+][+jot.querykey.id+]=[+comment.id+]#jf[+jot.link.id+]\" onclick=\"return confirm(\'Are you sure you wish to edit this comment?\')\" title=\"Edit Comment\"><i class=\"fa fa-fw fa-pencil-square-o\" aria-hidden=\"true\"></i></a>\n					`:strip+]\n				</div>\n				<span class=\"jot-poster\"><b>Reply #[+comment.postnumber+] on :</b> [+comment.createdon:date=`%a %B %d, %Y, %H:%M:%S`+]</span>\n				<hr>\n				<div class=\"jot-message\">[+comment.content:wordwrap:esc:nl2br+]</div>\n				<div class=\"jot-extra\">\n					[+comment.editedon:isnt=`0`:then=`\n					<span class=\"jot-editby\">Last Edit: [+comment.editedon:date=`%B %d, %Y, %H:%M:%S`+] by [+comment.editedby:userinfo=`username`:ifempty=` * `+]</span>\n					&nbsp;`+] [+jot.moderation.enabled:is=`1`:then=`<a target=\"_blank\" href=\"http://www.ripe.net/perl/whois?searchtext=[+comment.secip+]\">[+comment.secip+]</a>`+]\n				</div>\n			</div>\n		</div>\n	</div>\n</div>', 0),
(6, 'Comments_tplForm', 'Comments (Jot) Form-Template', 0, 'none', 1, 0, '<a name=\"jf[+jot.link.id+]\"></a>\n<h2>[+form.edit:is=`1`:then=`Edit comment`:else=`Write a comment`+]</h2>\n<div class=\"jot-list\">\n<ul>\n	<li>Required fields are marked with <b>*</b>.</li>\n</ul>\n</div>\n[+form.error:isnt=`0`:then=`\n<div class=\"jot-err\">\n[+form.error:select=`\n&-3=You are trying to re-submit the same post. You have probably clicked the submit button more than once.\n&-2=Your comment has been rejected.\n&-1=Your comment has been saved, it will first be reviewed before it is published.\n&1=You are trying to re-submit the same post. You have probably clicked the submit button more than once.\n&2=The security code you entered was incorrect.\n&3=You can only post once each [+jot.postdelay+] seconds.\n&4=Your comment has been rejected.\n&5=[+form.errormsg:ifempty=`You didn\'t enter all the required fields`+]\n`+]\n</div>\n`:strip+]\n[+form.confirm:isnt=`0`:then=`\n<div class=\"jot-cfm\">\n[+form.confirm:select=`\n&1=Your comment has been published.\n&2=Your comment has been saved, it will first be reviewed before it is published.\n&3=Comment saved.\n`+]\n</div>\n`:strip+]\n<form method=\"post\" action=\"[+form.action:esc+]#jf[+jot.link.id+]\" class=\"jot-form\">\n	<fieldset>\n	<input name=\"JotForm\" type=\"hidden\" value=\"[+jot.id+]\" />\n	<input name=\"JotNow\" type=\"hidden\" value=\"[+jot.seed+]\" />\n	<input name=\"parent\" type=\"hidden\" value=\"[+form.field.parent+]\" />\n	\n	[+form.moderation:is=`1`:then=`\n		<div class=\"jot-row\">\n			<b>Created on:</b> [+form.field.createdon:date=`%a %B %d, %Y at %H:%M`+]<br />\n			<b>Created by:</b> [+form.field.createdby:userinfo=`username`:ifempty=`[+jot.guestname+]`+]<br />\n			<b>IP address:</b> [+form.field.secip+]<br />\n			<b>Published:</b> [+form.field.published:select=`0=No&1=Yes`+]<br />\n			[+form.field.publishedon:gt=`0`:then=`\n				<b>Published on:</b> [+form.field.publishedon:date=`%a %B %d, %Y at %H:%M`+]<br />\n				<b>Published by:</b> [+form.field.publishedby:userinfo=`username`:ifempty=` - `+]<br />\n			`+]\n			[+form.field.editedon:gt=`0`:then=`\n				<b>Edited on:</b> [+form.field.editedon:date=`%a %B %d, %Y at %H:%M`+]<br />\n				<b>Edited by:</b> [+form.field.editedby:userinfo=`username`:ifempty=` -`+]<br />\n			`+]\n		</div>\n	`:strip+]\n	\n	[+form.guest:is=`1`:then=`\n		<div class=\"form-group\">\n			<label for=\"name[+jot.id+]\">Name:</label>\n			<input tabindex=\"[+jot.seed:math=`?+1`+]\" name=\"name\" class=\"form-control\" type=\"text\" size=\"40\" value=\"[+form.field.custom.name:esc+]\" id=\"name[+jot.id+]\" />\n		</div>\n		<div class=\"form-group\">\n			<label for=\"email[+jot.id+]\">Email:</label>\n			<input tabindex=\"[+jot.seed:math=`?+2`+]\" name=\"email\" class=\"form-control\" type=\"text\" size=\"40\" value=\"[+form.field.custom.email:esc+]\" id=\"email[+jot.id+]\"/>\n		</div>\n	`:strip+]\n	<div class=\"form-group\">\n		<label for=\"title[+jot.id+]\">Subject:</label>\n		<input tabindex=\"[+jot.seed:math=`?+3`+]\" name=\"title\" class=\"form-control\" type=\"text\" size=\"40\" value=\"[+form.field.title:esc+]\" id=\"title[+jot.id+]\"/>\n	</div>\n	<div class=\"form-group\">\n		<label for=\"content[+jot.id+]\">Comment: *</label>\n		<textarea tabindex=\"[+jot.seed:math=`?+4`+]\" name=\"content\" class=\"form-control\" rows=\"8\" id=\"content[+jot.id+]\">[+form.field.content:esc+]</textarea>\n	</div>\n	\n[+jot.captcha:is=`1`:then=`\n	<div style=\"width:150px;margin-top: 5px;margin-bottom: 5px;\">\n		<a href=\"[+jot.link.current:esc+]\">\n			<img src=\"[(modx_manager_url)]includes/veriword.php?rand=[+jot.seed+]\" width=\"148\" height=\"60\" alt=\"If you have trouble reading the code, click on the code itself to generate a new random code.\" style=\"border: 1px solid #003399\" />\n		</a>\n	</div>\n	<div class=\"form-group\">\n		<label for=\"vericode[+jot.id+]\">Help prevent spam - enter security code above:</label>\n		<input type=\"text\" name=\"vericode\" style=\"width:150px;\" size=\"20\" id=\"vericode[+jot.id+]\" />\n	</div>\n`:strip+]\n\n	<input tabindex=\"[+jot.seed:math=`?+5`+]\" name=\"submit\" class=\"btn btn-primary\" type=\"submit\" value=\"[+form.edit:is=`1`:then=`Save Comment`:else=`Post Comment`+]\" />\n	[+form.edit:is=`1`:then=`\n		<input tabindex=\"[+jot.seed:math=`?+5`+]\" name=\"submit\" class=\"btn btn-default\" type=\"submit\" value=\"Cancel\" onclick=\"history.go(-1);return false;\" />\n	`+] \n	</fieldset>\n</form>', 0),
(7, 'mm_rules', 'Default ManagerManager rules.', 0, 'none', 2, 0, '// more example rules are in assets/plugins/managermanager/example_mm_rules.inc.php\n// example of how PHP is allowed - check that a TV named documentTags exists before creating rule\n\nif ($modx->db->getValue($modx->db->select(\'count(id)\', $modx->getFullTableName(\'site_tmplvars\'), \"name=\'documentTags\'\"))) {\n	mm_widget_tags(\'documentTags\', \' \'); // Give blog tag editing capabilities to the \'documentTags (3)\' TV\n}\nmm_widget_showimagetvs(); // Always give a preview of Image TVs\n', 0),
(8, 'WebLogin_tplForm', 'WebLogin Tpl', 0, 'none', 1, 0, '<!-- #declare:separator <hr> -->\n<!-- login form section-->\n<form method=\"post\" name=\"loginfrm\" action=\"[+action+]\">\n	<input type=\"hidden\" value=\"[+rememberme+]\" name=\"rememberme\">\n	<div class=\"form-group\">\n		<label for=\"username\">User:</label>\n		<input type=\"text\" name=\"username\" id=\"username\" tabindex=\"1\" class=\"form-control\" onkeypress=\"return webLoginEnter(document.loginfrm.password);\" value=\"[+username+]\">\n	</div>\n	<div class=\"form-group\">\n		<label for=\"password\">Password:</label>\n		<input type=\"password\" name=\"password\" id=\"password\" tabindex=\"2\" class=\"form-control\" onkeypress=\"return webLoginEnter(document.loginfrm.cmdweblogin);\" value=\"\">\n	</div>\n	<div class=\"checkbox\">\n		<label>\n			<input type=\"checkbox\" id=\"checkbox_1\" name=\"checkbox_1\" tabindex=\"3\" size=\"1\" value=\"\" [+checkbox+] onclick=\"webLoginCheckRemember()\"> Remember me\n		</label>\n	</div>\n	<input type=\"submit\" value=\"[+logintext+]\" name=\"cmdweblogin\" class=\"btn btn-primary\">\n	<a href=\"#\" onclick=\"webLoginShowForm(2);return false;\" id=\"forgotpsswd\" class=\"btn btn-text\">Forget Your Password?</a>\n</form>\n<hr>\n<!-- log out hyperlink section -->\n<h4>You\'re already logged in</h4>\nDo you wish to <a href=\"[+action+]\" class=\"button\">[+logouttext+]</a>?\n<hr>\n<!-- Password reminder form section -->\n<form name=\"loginreminder\" method=\"post\" action=\"[+action+]\">\n	<input type=\"hidden\" name=\"txtpwdrem\" value=\"0\">\n	<h4>It happens to everyone...</h4>\n	<div class=\"form-group\">\n		<label for=\"txtwebemail\">Enter the email address of your account to reset your password:</label>\n		<input type=\"text\" name=\"txtwebemail\" id=\"txtwebemail\">\n	</div>\n	<label>To return to the login form, press the cancel button.</label>\n	<input type=\"submit\" value=\"Submit\" name=\"cmdweblogin\" class=\"btn btn-primary\">\n	<input type=\"reset\" value=\"Cancel\" name=\"cmdcancel\" onclick=\"webLoginShowForm(1);\" class=\"btn btn-default\">\n</form>\n', 0),
(9, 'HEAD', '', 2, 'none', 10, 0, '<head>\r\n    <meta charset=\"utf-8\">\r\n    <title>NINA Theme – Free HTML theme</title>\r\n    \r\n    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>\r\n	<link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css\">\r\n    \r\n    <link rel=\"stylesheet\" href=\"[(base_url)]assets/css/reset.css\" type=\"text/css\">\r\n	<link rel=\"stylesheet\" href=\"[(base_url)]assets/css/bootstrap.css\" type=\"text/css\">\r\n    <link rel=\"stylesheet\" href=\"[(base_url)]assets/css/style.css\" type=\"text/css\">\r\n        \r\n    <link href=\'http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,400,600,700&subset=latin,latin-ext\' rel=\'stylesheet\' type=\'text/css\'>\r\n	<link rel=\"stylesheet\" href=\"/assets/css/animate.css\">\r\n</head>', 0),
(10, 'HEADER', '', 2, 'none', 10, 0, '<div id=\"header\">\r\n</div>\r\n<img class=\"main-banner\" src=\"assets/images/law_full.jpeg\" alt=\"\">', 0),
(11, 'FOOTER', '', 2, 'none', 10, 0, '<div id=\"footer\">\r\n	<div class=\"col-md-8 col-md-offset-3\">\r\n		<div class=\"col-md-3 col-md-offset-2\">\r\n			<img src=\"/assets/images/logo.png\" alt=\"\">\r\n		</div>\r\n		<div class=\"col-md-4\">\r\n			<ul>\r\n				<li>\r\n					<a href=\"https://goo.gl/maps/ssWzBpFbsKJ2\" target><span class=\"glyphicon glyphicon-map-marker\"></span>Украина, г. Харьков. Ул. Чайковского 17</a>\r\n				</li>\r\n				<li>\r\n					<a href=\"mailto:fregat222@gmail.co\"><span class=\"glyphicon glyphicon-envelope\"></span>fregat222@gmail.com</a>\r\n				</li>\r\n				<li>\r\n					<a href=\"callto:+38(066)760-64-81\"><span class=\"glyphicon glyphicon-earphone\"></span>+38(066)760-64-81</a>\r\n				</li>\r\n				<li>\r\n					<a href=\"skype:fregatim1\"><span class=\"fa fa-skype\"></span>fregatim1</a>\r\n				</li>\r\n			</ul>\r\n		</div>\r\n	</div>\r\n</div>\r\n<div class=\"call-back\">\r\n	<span class=\"fa fa-phone\"></span>\r\n</div>\r\n<div class=\"modal-wrapper\"></div>\r\n<div class=\"modal-window\">\r\n	<div class=\"close-modal\">x</div>\r\n	<form method=\"POST\" id=\"callback\">\r\n		<h3>Позвоните мне!</h3>\r\n		<input type=\"text\" placeholder=\"Имя\" id=\"name\" name=\"name\">\r\n		<input type=\"tel\" placeholder=\"Номер телефона\" name=\"phone\" id=\"phone\">\r\n		<input type=\"email\" placeholder=\"Email\" name=\"email\" id=\"email\">\r\n		<textarea name=\"message\" placeholder=\"Ваш уточняющий вопрос\" id=\"message\"></textarea>\r\n		<input type=\"submit\" value=\"Отправить\">\r\n	</form>\r\n</div>\r\n{{callback_message}}\r\n<script src=\"/assets/js/common.js\"></script>', 0),
(13, 'tpl_latest_news_item', '', 2, 'none', 13, 0, '<article>\r\n	<a href=\"?newsId=[+link+]\">[+title+]</a>\r\n	<span>[+description+]</span>\r\n	<a href=\"?newsId=[+link+]\">Подробнее</a>\r\n</article>', 0),
(12, 'callback_message', '', 2, 'none', 0, 0, '<div class=\"modal_message\">\r\n	<h3>Спасибо за заявку!</h3>\r\n	<p>Мы свяжемся с вами в ближайшее время.</p>\r\n</div>', 0),
(21, 'tpl_latest_news_row', '', 2, 'none', 13, 0, '[!S ? \r\n	&get=`lastNews`\r\n!]', 0),
(14, 'tpl_menu', '', 2, 'none', 11, 0, '<div class=\"menu\">\r\n	[[Wayfinder?\r\n	&startId=`0`\r\n	&outerTpl=`tpl_outer`\r\n	&rowTpl=`tpl_row`\r\n	&hereTpl=`tpl_row_active`\r\n	&innerTpl=`tpl_inner`\r\n	&innerRowTpl=`tpl_inner_row`\r\n	]]\r\n</div>', 0),
(20, 'tpl_news_item', '', 2, 'none', 13, 0, '<a href=\"?newId=[+link+]\" class=\"list-group-item\"> \r\n	<h4 class=\"list-group-item-heading\">[+title+]</h4> \r\n	<p class=\"list-group-item-text\">[+description+]</p>\r\n</a> ', 0),
(15, 'tpl_outer', '', 2, 'none', 12, 0, '<ul>[+wf.wrapper+]</ul>', 0),
(16, 'tpl_row', '', 2, 'none', 12, 0, '<li><a href=\"[+wf.link+]\">[+wf.linktext+]</a>[+wf.wrapper+]</li>', 0),
(17, 'tpl_row_active', '', 2, 'none', 12, 0, '<li class=\"active\"><a href=\"[+wf.link+]\"> - [+wf.linktext+]</a>[+wf.wrapper+]</li>', 0),
(18, 'tpl_inner', '', 2, 'none', 12, 0, '<ul class=\"submenu\">[+wf.wrapper+]</ul>', 0),
(19, 'tpl_inner_row', '', 2, 'none', 12, 0, '<li><a href=\"[+wf.link+]\">- [+wf.linktext+]</a></li>', 0),
(22, 'Seo4Evo_mm_rules', 'Seo4Evo example rules for ManagerManager SEO Tab', 0, 'none', 14, 0, '// copy and paste in you ManageManeger rules chunk\n\n\n//Seo tab\nmm_createTab(\'Seo\',\'Seo\', \'\', \'\', \'\', \'600\');\nmm_moveFieldsToTab(\'CustomTitle,MetaDescription,MetaKeywords,CanonicalUrl,Aliases,GoogleAuthor,RobotsIndex,RobotsFollow,sitemap_exclude,sitemap_changefreq,sitemap_priority\', \'Seo\', \'\', \'\');\nmm_widget_tags(\'MetaKeywords\'); // Give blog tag editing capabilities to the \'MetaKeywords\' TV\nmm_ddMaxLength(\'MetaDescription\', \'\', \'\', 350);\nmm_ddMaxLength(\'CustomTitle\', \'\', \'\', 70);', 0),
(23, 'Seo4Evo-metaTags', 'Seo4Evo sample call for metatatags', 0, 'none', 14, 0, '[[Seo4Evo? &postTitle=` | [(site_name)]` &OpenGraph=`1` &OGfbappId=`123456789123456789` &OGtype=`article` &GooglePlus=`1` &linkPub=`https://plus.google.com/123456789123456789`]]', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `modx_site_keywords`
--

CREATE TABLE `modx_site_keywords` (
  `id` int(11) NOT NULL,
  `keyword` varchar(40) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Site keyword list';

-- --------------------------------------------------------

--
-- Структура таблицы `modx_site_metatags`
--

CREATE TABLE `modx_site_metatags` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL DEFAULT '',
  `tag` varchar(50) NOT NULL DEFAULT '' COMMENT 'tag name',
  `tagvalue` varchar(255) NOT NULL DEFAULT '',
  `http_equiv` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1 - use http_equiv tag style, 0 - use name'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Site meta tags';

-- --------------------------------------------------------

--
-- Структура таблицы `modx_site_modules`
--

CREATE TABLE `modx_site_modules` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '0',
  `editor_type` int(11) NOT NULL DEFAULT '0' COMMENT '0-plain text,1-rich text,2-code editor',
  `disabled` tinyint(4) NOT NULL DEFAULT '0',
  `category` int(11) NOT NULL DEFAULT '0' COMMENT 'category id',
  `wrap` tinyint(4) NOT NULL DEFAULT '0',
  `locked` tinyint(4) NOT NULL DEFAULT '0',
  `icon` varchar(255) NOT NULL DEFAULT '' COMMENT 'url to module icon',
  `enable_resource` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'enables the resource file feature',
  `resourcefile` varchar(255) NOT NULL DEFAULT '' COMMENT 'a physical link to a resource file',
  `createdon` int(11) NOT NULL DEFAULT '0',
  `editedon` int(11) NOT NULL DEFAULT '0',
  `guid` varchar(32) NOT NULL DEFAULT '' COMMENT 'globally unique identifier',
  `enable_sharedparams` tinyint(4) NOT NULL DEFAULT '0',
  `properties` text,
  `modulecode` mediumtext COMMENT 'module boot up code'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Site Modules';

--
-- Дамп данных таблицы `modx_site_modules`
--

INSERT INTO `modx_site_modules` (`id`, `name`, `description`, `editor_type`, `disabled`, `category`, `wrap`, `locked`, `icon`, `enable_resource`, `resourcefile`, `createdon`, `editedon`, `guid`, `enable_sharedparams`, `properties`, `modulecode`) VALUES
(1, 'Doc Manager', '<strong>1.1</strong> Quickly perform bulk updates to the Documents in your site including templates, publishing details, and permissions', 0, 0, 3, 0, 0, '', 0, '', 0, 0, 'docman435243542tf542t5t', 1, '', ' \n/**\n * Doc Manager\n * \n * Quickly perform bulk updates to the Documents in your site including templates, publishing details, and permissions\n * \n * @category	module\n * @version 	1.1\n * @license 	http://www.gnu.org/copyleft/gpl.html GNU Public License (GPL)\n * @internal	@properties\n * @internal	@guid docman435243542tf542t5t	\n * @internal	@shareparams 1\n * @internal	@dependencies requires files located at /assets/modules/docmanager/\n * @internal	@modx_category Manager and Admin\n * @internal    @installset base, sample\n * @lastupdate  09/04/2016\n */\n\ninclude_once(MODX_BASE_PATH.\'assets/modules/docmanager/classes/docmanager.class.php\');\ninclude_once(MODX_BASE_PATH.\'assets/modules/docmanager/classes/dm_frontend.class.php\');\ninclude_once(MODX_BASE_PATH.\'assets/modules/docmanager/classes/dm_backend.class.php\');\n\n$dm = new DocManager($modx);\n$dmf = new DocManagerFrontend($dm, $modx);\n$dmb = new DocManagerBackend($dm, $modx);\n\n$dm->ph = $dm->getLang();\n$dm->ph[\'theme\'] = $dm->getTheme();\n$dm->ph[\'ajax.endpoint\'] = MODX_SITE_URL.\'assets/modules/docmanager/tv.ajax.php\';\n$dm->ph[\'datepicker.offset\'] = $modx->config[\'datepicker_offset\'];\n$dm->ph[\'datetime.format\'] = $modx->config[\'datetime_format\'];\n\nif (isset($_POST[\'tabAction\'])) {\n    $dmb->handlePostback();\n} else {\n    $dmf->getViews();\n    echo $dm->parseTemplate(\'main.tpl\', $dm->ph);\n}'),
(2, 'Extras', '<strong>0.1.3</strong> first repository for MODX EVO', 0, 0, 3, 0, 0, '', 0, '', 0, 0, 'store435243542tf542t5t', 1, '', ' \r\n/**\r\n * Extras\r\n * \r\n * first repository for MODX EVO\r\n * \r\n * @category	module\r\n * @version 	0.1.3\r\n * @internal	@properties\r\n * @internal	@guid store435243542tf542t5t	\r\n * @internal	@shareparams 1\r\n * @internal	@dependencies requires files located at /assets/modules/store/\r\n * @internal	@modx_category Manager and Admin\r\n * @internal    @installset base, sample\r\n * @lastupdate  25/11/2016\r\n */\r\n\r\n//AUTHORS: Bumkaka & Dmi3yy \r\ninclude_once(\'../assets/modules/store/core.php\');'),
(3, 'Новости', 'Новости', 0, 0, 6, 0, 0, '', 0, '', 0, 0, '1f5616ae25d99fd0dee203395f72d14d', 0, '{}', 'require MODX_BASE_PATH . \"assets/modules/news/index.php\";'),
(4, 'SEO', '', 0, 0, 0, 0, 0, '', 0, '', 0, 0, 'b0398e48239b8525972f3b62188430ed', 0, '{}', 'require MODX_BASE_PATH . \"assets/modules/seo/index.php\";');

-- --------------------------------------------------------

--
-- Структура таблицы `modx_site_module_access`
--

CREATE TABLE `modx_site_module_access` (
  `id` int(10) UNSIGNED NOT NULL,
  `module` int(11) NOT NULL DEFAULT '0',
  `usergroup` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Module users group access permission';

-- --------------------------------------------------------

--
-- Структура таблицы `modx_site_module_depobj`
--

CREATE TABLE `modx_site_module_depobj` (
  `id` int(11) NOT NULL,
  `module` int(11) NOT NULL DEFAULT '0',
  `resource` int(11) NOT NULL DEFAULT '0',
  `type` int(2) NOT NULL DEFAULT '0' COMMENT '10-chunks, 20-docs, 30-plugins, 40-snips, 50-tpls, 60-tvs'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Module Dependencies';

-- --------------------------------------------------------

--
-- Структура таблицы `modx_site_plugins`
--

CREATE TABLE `modx_site_plugins` (
  `id` int(10) NOT NULL,
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT 'Plugin',
  `editor_type` int(11) NOT NULL DEFAULT '0' COMMENT '0-plain text,1-rich text,2-code editor',
  `category` int(11) NOT NULL DEFAULT '0' COMMENT 'category id',
  `cache_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Cache option',
  `plugincode` mediumtext,
  `locked` tinyint(4) NOT NULL DEFAULT '0',
  `properties` text COMMENT 'Default Properties',
  `disabled` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Disables the plugin',
  `moduleguid` varchar(32) NOT NULL DEFAULT '' COMMENT 'GUID of module from which to import shared parameters'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Contains the site plugins.';

--
-- Дамп данных таблицы `modx_site_plugins`
--

INSERT INTO `modx_site_plugins` (`id`, `name`, `description`, `editor_type`, `category`, `cache_type`, `plugincode`, `locked`, `properties`, `disabled`, `moduleguid`) VALUES
(1, 'CodeMirror', '<strong>1.4</strong> JavaScript library that can be used to create a relatively pleasant editor interface based on CodeMirror 5.12', 0, 3, 0, '\r\n/**\r\n * CodeMirror\r\n *\r\n * JavaScript library that can be used to create a relatively pleasant editor interface based on CodeMirror 5.12\r\n *\r\n * @category    plugin\r\n * @version     1.4\r\n * @license     http://www.gnu.org/copyleft/gpl.html GNU Public License (GPL)\r\n * @package     modx\r\n * @internal    @events OnDocFormRender,OnChunkFormRender,OnModFormRender,OnPluginFormRender,OnSnipFormRender,OnTempFormRender,OnRichTextEditorInit\r\n * @internal    @modx_category Manager and Admin\r\n * @internal    @properties &theme=Theme;list;default,ambiance,blackboard,cobalt,eclipse,elegant,erlang-dark,lesser-dark,midnight,monokai,neat,night,rubyblue,solarized,twilight,vibrant-ink,xq-dark,xq-light;default &indentUnit=Indent unit;int;4 &tabSize=The width of a tab character;int;4 &lineWrapping=lineWrapping;list;true,false;true &matchBrackets=matchBrackets;list;true,false;true &activeLine=activeLine;list;true,false;false &emmet=emmet;list;true,false;true &search=search;list;true,false;false &indentWithTabs=indentWithTabs;list;true,false;true &undoDepth=undoDepth;int;200 &historyEventDelay=historyEventDelay;int;1250\r\n * @internal    @installset base\r\n * @reportissues https://github.com/modxcms/evolution\r\n * @documentation Official docs https://codemirror.net/doc/manual.html\r\n * @author      hansek from http://www.modxcms.cz\r\n * @author      update Mihanik71\r\n * @author      update Deesen\r\n * @lastupdate  11/04/2016\r\n */\r\n\r\n$_CM_BASE = \'assets/plugins/codemirror/\';\r\n\r\n$_CM_URL = $modx->config[\'site_url\'] . $_CM_BASE;\r\n\r\nrequire(MODX_BASE_PATH. $_CM_BASE .\'codemirror.plugin.php\');', 0, '&theme=Theme;list;default,ambiance,blackboard,cobalt,eclipse,elegant,erlang-dark,lesser-dark,midnight,monokai,neat,night,rubyblue,solarized,twilight,vibrant-ink,xq-dark,xq-light;default &indentUnit=Indent unit;int;4 &tabSize=The width of a tab character;int;4 &lineWrapping=lineWrapping;list;true,false;true &matchBrackets=matchBrackets;list;true,false;true &activeLine=activeLine;list;true,false;false &emmet=emmet;list;true,false;true &search=search;list;true,false;false &indentWithTabs=indentWithTabs;list;true,false;true &undoDepth=undoDepth;int;200 &historyEventDelay=historyEventDelay;int;1250', 0, ''),
(2, 'ElementsInTree', '<strong>1.5.7</strong> Get access to all Elements and Modules inside Manager sidebar', 0, 3, 0, 'require MODX_BASE_PATH.\'assets/plugins/elementsintree/plugin.elementsintree.php\';\n', 0, '&tabTreeTitle=Tree Tab Title;text;Site Tree;;Custom title of Site Tree tab. &useIcons=Use icons in tabs;list;yes,no;yes;;Icons available in MODX version 1.2 or newer. &treeButtonsInTab=Tree Buttons in tab;list;yes,no;yes;;Move Tree Buttons into Site Tree tab. &unifyFrames=Unify Frames;list;yes,no;yes;;Unify Tree and Main frame style. Right now supports MODxRE2 theme only.', 0, ''),
(3, 'FileSource', '<strong>0.1</strong> Save snippet and plugins to file', 0, 3, 0, 'require MODX_BASE_PATH.\'assets/plugins/filesource/plugin.filesource.php\';', 0, '', 0, ''),
(4, 'Forgot Manager Login', '<strong>1.1.6</strong> Resets your manager login when you forget your password via email confirmation', 0, 3, 0, 'require MODX_BASE_PATH.\'assets/plugins/forgotmanagerlogin/plugin.forgotmanagerlogin.php\';', 0, '', 0, ''),
(5, 'ManagerManager', '<strong>0.6.2</strong> Customize the MODX Manager to offer bespoke admin functions for end users or manipulate the display of document fields in the manager.', 0, 3, 0, '\n/**\n * ManagerManager\n *\n * Customize the MODX Manager to offer bespoke admin functions for end users or manipulate the display of document fields in the manager.\n *\n * @category plugin\n * @version 0.6.2\n * @license http://creativecommons.org/licenses/GPL/2.0/ GNU Public License (GPL v2)\n * @internal @properties &remove_deprecated_tv_types_pref=Remove deprecated TV types;list;yes,no;yes &config_chunk=Configuration Chunk;text;mm_rules\n * @internal @events OnDocFormRender,OnDocFormPrerender,OnBeforeDocFormSave,OnDocFormSave,OnDocDuplicate,OnPluginFormRender,OnTVFormRender\n * @internal @modx_category Manager and Admin\n * @internal @installset base\n * @internal @legacy_names Image TV Preview, Show Image TVs\n * @reportissues https://github.com/DivanDesign/MODXEvo.plugin.ManagerManager/\n * @documentation README [+site_url+]assets/plugins/managermanager/readme.html\n * @documentation Official docs http://code.divandesign.biz/modx/managermanager\n * @link        Latest version http://code.divandesign.biz/modx/managermanager\n * @link        Additional tools http://code.divandesign.biz/modx\n * @link        Full changelog http://code.divandesign.biz/modx/managermanager/changelog\n * @author      Inspired by: HideEditor plugin by Timon Reinhard and Gildas; HideManagerFields by Brett @ The Man Can!\n * @author      DivanDesign studio http://www.DivanDesign.biz\n * @author      Nick Crossland http://www.rckt.co.uk\n * @author      Many others\n * @lastupdate  06/03/2016\n */\n\n// Run the main code\ninclude($modx->config[\'base_path\'].\'assets/plugins/managermanager/mm.inc.php\');', 0, '&remove_deprecated_tv_types_pref=Remove deprecated TV types;list;yes,no;yes &config_chunk=Configuration Chunk;text;mm_rules', 0, ''),
(6, 'Quick Manager+', '<strong>1.5.6</strong> Enables QuickManager+ front end content editing support', 0, 3, 0, '\n/**\n * Quick Manager+\n * \n * Enables QuickManager+ front end content editing support\n *\n * @category 	plugin\n * @version 	1.5.6\n * @license 	http://www.gnu.org/copyleft/gpl.html GNU Public License (GPL v3)\n * @internal    @properties &jqpath=Path to jQuery;text;assets/js/jquery.min.js &loadmanagerjq=Load jQuery in manager;list;true,false;false &loadfrontendjq=Load jQuery in front-end;list;true,false;true &noconflictjq=jQuery noConflict mode in front-end;list;true,false;true &loadtb=Load modal box in front-end;list;true,false;true &tbwidth=Modal box window width;text;80% &tbheight=Modal box window height;text;90% &hidefields=Hide document fields from front-end editors;text;parent &hidetabs=Hide document tabs from front-end editors;text; &hidesections=Hide document sections from front-end editors;text; &addbutton=Show add document here button;list;true,false;true &tpltype=New document template type;list;parent,id,selected;parent &tplid=New document template id;int;3 &custombutton=Custom buttons;textarea; &managerbutton=Show go to manager button;list;true,false;true &logout=Logout to;list;manager,front-end;manager &disabled=Plugin disabled on documents;text; &autohide=Autohide toolbar;list;true,false;true &editbuttons=Inline edit buttons;list;true,false;false &editbclass=Edit button CSS class;text;qm-edit &newbuttons=Inline new resource buttons;list;true,false;false &newbclass=New resource button CSS class;text;qm-new &tvbuttons=Inline template variable buttons;list;true,false;false &tvbclass=Template variable button CSS class;text;qm-tv\n * @internal	@events OnParseDocument,OnWebPagePrerender,OnDocFormPrerender,OnDocFormSave,OnManagerLogout \n * @internal	@modx_category Manager and Admin\n * @internal    @legacy_names QM+,QuickEdit\n * @internal    @installset base, sample\n * @internal    @disabled 1\n * @reportissues https://github.com/modxcms/evolution\n * @documentation Official docs [+site_url+]assets/plugins/qm/readme.html\n * @link        http://www.maagit.fi/modx/quickmanager-plus\n * @author      Mikko Lammi\n * @author      Since 2011: yama, dmi3yy, segr\n * @lastupdate  31/03/2014\n */\n\n// In manager\nif (!$modx->checkSession()) return;\n\n$show = TRUE;\n\nif ($disabled  != \'\') {\n    $arr = array_filter(array_map(\'intval\', explode(\',\', $disabled)));\n    if (in_array($modx->documentIdentifier, $arr)) {\n        $show = FALSE;\n    }\n}\n\nif ($show) {\n    // Replace [*#tv*] with QM+ edit TV button placeholders\n    if ($tvbuttons == \'true\') {\n        if ($modx->event->name == \'OnParseDocument\') {\n             $output = &$modx->documentOutput;\n             $output = preg_replace(\'~\\[\\*#(.*?)\\*\\]~\', \'<!-- \'.$tvbclass.\' $1 -->[*$1*]\', $output);\n             $modx->documentOutput = $output;\n         }\n     }\n    include_once($modx->config[\'base_path\'].\'assets/plugins/qm/qm.inc.php\');\n    $qm = new Qm($modx, $jqpath, $loadmanagerjq, $loadfrontendjq, $noconflictjq, $loadtb, $tbwidth, $tbheight, $hidefields, $hidetabs, $hidesections, $addbutton, $tpltype, $tplid, $custombutton, $managerbutton, $logout, $autohide, $editbuttons, $editbclass, $newbuttons, $newbclass, $tvbuttons, $tvbclass);\n}\n', 0, '&jqpath=Path to jQuery;text;assets/js/jquery.min.js &loadmanagerjq=Load jQuery in manager;list;true,false;false &loadfrontendjq=Load jQuery in front-end;list;true,false;true &noconflictjq=jQuery noConflict mode in front-end;list;true,false;true &loadtb=Load modal box in front-end;list;true,false;true &tbwidth=Modal box window width;text;80% &tbheight=Modal box window height;text;90% &hidefields=Hide document fields from front-end editors;text;parent &hidetabs=Hide document tabs from front-end editors;text; &hidesections=Hide document sections from front-end editors;text; &addbutton=Show add document here button;list;true,false;true &tpltype=New document template type;list;parent,id,selected;parent &tplid=New document template id;int;3 &custombutton=Custom buttons;textarea; &managerbutton=Show go to manager button;list;true,false;true &logout=Logout to;list;manager,front-end;manager &disabled=Plugin disabled on documents;text; &autohide=Autohide toolbar;list;true,false;true &editbuttons=Inline edit buttons;list;true,false;false &editbclass=Edit button CSS class;text;qm-edit &newbuttons=Inline new resource buttons;list;true,false;false &newbclass=New resource button CSS class;text;qm-new &tvbuttons=Inline template variable buttons;list;true,false;false &tvbclass=Template variable button CSS class;text;qm-tv', 1, ''),
(7, 'Search Highlight', '<strong>1.5</strong> Used with AjaxSearch to show search terms highlighted on page linked from search results', 0, 4, 0, '/**\n * Search Highlight\n * \n * Used with AjaxSearch to show search terms highlighted on page linked from search results\n *\n * @category 	plugin\n * @version 	1.5\n * @license 	http://www.gnu.org/copyleft/gpl.html GNU Public License (GPL)\n * @internal	@properties\n * @internal	@events OnWebPagePrerender \n * @internal	@modx_category Search\n * @internal    @legacy_names Search Highlighting\n * @internal    @installset base, sample\n * @internal    @disabled 1\n */\n \n /*\n  ------------------------------------------------------------------------\n  Plugin: Search_Highlight v1.5\n  ------------------------------------------------------------------------\n  Changes:\n  18/03/10 - Remove possibility of XSS attempts being passed in the URL\n           - look-behind assertion improved\n  29/03/09 - Removed urldecode calls;\n           - Added check for magic quotes - if set, remove slashes\n           - Highlights terms searched for when target is a HTML entity\n  18/07/08 - advSearch parameter and pcre modifier added\n  10/02/08 - Strip_tags added to avoid sql injection and XSS. Use of $_REQUEST\n  01/03/07 - Added fies/updates from forum from users mikkelwe/identity\n  (better highlight replacement, additional div around term/removal message)\n  ------------------------------------------------------------------------\n  Description: When a user clicks on the link from the AjaxSearch results\n    the target page will have the terms highlighted.\n  ------------------------------------------------------------------------\n  Created By:  Susan Ottwell (sottwell@sottwell.com)\n               Kyle Jaebker (kjaebker@muddydogpaws.com)\n\n  Refactored by Coroico (www.evo.wangba.fr) and TS\n  ------------------------------------------------------------------------\n  Based off the the code by Susan Ottwell (www.sottwell.com)\n    http://forums.modx.com/thread/47775/plugin-highlight-search-terms\n  ------------------------------------------------------------------------\n  CSS:\n    The classes used for the highlighting are the same as the AjaxSearch\n  ------------------------------------------------------------------------\n  Notes:\n    To add a link to remove the highlighting and to show the searchterms\n    put the following on your page where you would like this to appear:\n\n      <!--search_terms-->\n\n    Example output for this:\n\n      Search Terms: the, template\n      Remove Highlighting\n\n    Set the following variables to change the text:\n\n      $termText - the text before the search terms\n      $removeText - the text for the remove link\n  ------------------------------------------------------------------------\n*/\nglobal $database_connection_charset;\n// Conversion code name between html page character encoding and Mysql character encoding\n// Some others conversions should be added if needed. Otherwise Page charset = Database charset\n$pageCharset = array(\n  \'utf8\' => \'UTF-8\',\n  \'latin1\' => \'ISO-8859-1\',\n  \'latin2\' => \'ISO-8859-2\'\n);\n\nif (isset($_REQUEST[\'searched\']) && isset($_REQUEST[\'highlight\'])) {\n\n  // Set these to customize the text for the highlighting key\n  // --------------------------------------------------------\n     $termText = \'<div class=\"searchTerms\">Search Terms: \';\n     $removeText = \'Remove Highlighting\';\n  // --------------------------------------------------------\n\n  $highlightText = $termText;\n  $advsearch = \'oneword\';\n\n  $dbCharset = $database_connection_charset;\n  $pgCharset = array_key_exists($dbCharset,$pageCharset) ? $pageCharset[$dbCharset] : $dbCharset;\n\n  // magic quotes check\n  if (get_magic_quotes_gpc()){\n    $searched = strip_tags(stripslashes($_REQUEST[\'searched\']));\n    $highlight = strip_tags(stripslashes($_REQUEST[\'highlight\']));\n    if (isset($_REQUEST[\'advsearch\'])) $advsearch = strip_tags(stripslashes($_REQUEST[\'advsearch\']));\n  }\n  else {\n    $searched = strip_tags($_REQUEST[\'searched\']);\n    $highlight = strip_tags($_REQUEST[\'highlight\']);\n    if (isset($_REQUEST[\'advsearch\'])) $advsearch = strip_tags($_REQUEST[\'advsearch\']);\n  }\n\n  if ($advsearch != \'nowords\') {\n\n    $searchArray = array();\n    if ($advsearch == \'exactphrase\') $searchArray[0] = $searched;\n    else $searchArray = explode(\' \', $searched);\n\n    $searchArray = array_unique($searchArray);\n    $nbterms = count($searchArray);\n    $searchTerms = array();\n    for($i=0;$i<$nbterms;$i++){\n      // Consider all possible combinations\n      $word_ents = array();\n      $word_ents[] = $searchArray[$i];\n      $word_ents[] = htmlentities($searchArray[$i], ENT_NOQUOTES, $pgCharset);\n      $word_ents[] = htmlentities($searchArray[$i], ENT_COMPAT, $pgCharset);\n      $word_ents[] = htmlentities($searchArray[$i], ENT_QUOTES, $pgCharset);\n      // Avoid duplication\n      $word_ents = array_unique($word_ents);\n      foreach($word_ents as $word) $searchTerms[]= array(\'term\' => $word, \'class\' => $i+1);\n    }\n\n    $output = $modx->documentOutput; // get the parsed document\n    $body = explode(\"<body\", $output); // break out the head\n\n    $highlightClass = explode(\' \',$highlight); // break out the highlight classes\n    /* remove possibility of XSS attempts being passed in URL */\n    foreach ($highlightClass as $key => $value) {\n       $highlightClass[$key] = preg_match(\'/[^A-Za-z0-9_-]/ms\', $value) == 1 ? \'\' : $value;\n    }\n\n    $pcreModifier = ($pgCharset == \'UTF-8\') ? \'iu\' : \'i\';\n    $lookBehind = \'/(?<!&|&[\\w#]|&[\\w#]\\w|&[\\w#]\\w\\w|&[\\w#]\\w\\w\\w|&[\\w#]\\w\\w\\w\\w|&[\\w#]\\w\\w\\w\\w\\w)\';  // avoid a match with a html entity\n    $lookAhead = \'(?=[^>]*<)/\'; // avoid a match with a html tag\n\n    $nbterms = count($searchTerms);\n    for($i=0;$i<$nbterms;$i++){\n      $word = $searchTerms[$i][\'term\'];\n      $class = $highlightClass[0].\' \'.$highlightClass[$searchTerms[$i][\'class\']];\n\n      $highlightText .= ($i > 0) ? \', \' : \'\';\n      $highlightText .= \'<span class=\"\'.$class.\'\">\'.$word.\'</span>\';\n\n      $pattern = $lookBehind . preg_quote($word, \'/\') . $lookAhead . $pcreModifier;\n      $replacement = \'<span class=\"\' . $class . \'\">${0}</span>\';\n      $body[1] = preg_replace($pattern, $replacement, $body[1]);\n    }\n\n    $output = implode(\"<body\", $body);\n\n    $removeUrl = $modx->makeUrl($modx->documentIdentifier);\n    $highlightText .= \'<br /><a href=\"\'.$removeUrl.\'\" class=\"ajaxSearch_removeHighlight\">\'.$removeText.\'</a></div>\';\n\n    $output = str_replace(\'<!--search_terms-->\',$highlightText,$output);\n    $modx->documentOutput = $output;\n  }\n}', 0, '', 1, ''),
(8, 'TinyMCE4', '<strong>4.3.7.2</strong> Javascript WYSIWYG editor', 0, 3, 0, 'require MODX_BASE_PATH.\'assets/plugins/tinymce4/plugin.tinymce.php\';', 0, '&styleFormats=Custom Style Formats;textarea;Title,cssClass|Title2,cssClass &customParams=Custom Parameters <b>(Be careful or leave empty!)</b>;textarea; &entityEncoding=Entity Encoding;list;named,numeric,raw;named &entities=Entities;text; &pathOptions=Path Options;list;Site config,Absolute path,Root relative,URL,No convert;Site config &resizing=Advanced Resizing;list;true,false;false &disabledButtons=Disabled Buttons;text; &webTheme=Web Theme;test;webuser &webPlugins=Web Plugins;text; &webButtons1=Web Buttons 1;text;bold italic underline strikethrough removeformat alignleft aligncenter alignright &webButtons2=Web Buttons 2;text;link unlink image undo redo &webButtons3=Web Buttons 3;text; &webButtons4=Web Buttons 4;text; &webAlign=Web Toolbar Alignment;list;ltr,rtl;ltr &width=Width;text;100% &height=Height;text;400px &introtextRte=<b>Introtext RTE</b><br/>add richtext-features to \"introtext\";list;enabled,disabled;disabled &inlineMode=<b>Inline-Mode</b>;list;enabled,disabled;disabled &inlineTheme=<b>Inline-Mode</b><br/>Theme;text;inline &browser_spellcheck=<b>Browser Spellcheck</b><br/>At least one dictionary must be installed inside your browser;list;enabled,disabled;disabled', 0, ''),
(9, 'TransAlias', '<strong>1.0.4</strong> Human readible URL translation supporting multiple languages and overrides', 0, 3, 0, 'require MODX_BASE_PATH.\'assets/plugins/transalias/plugin.transalias.php\';', 0, '&table_name=Trans table;list;common,russian,dutch,german,czech,utf8,utf8lowercase;utf8lowercase &char_restrict=Restrict alias to;list;lowercase alphanumeric,alphanumeric,legal characters;legal characters &remove_periods=Remove Periods;list;Yes,No;No &word_separator=Word Separator;list;dash,underscore,none;dash &override_tv=Override TV name;string;', 0, ''),
(10, 'Common', '', 0, 6, 0, 'global $modx;\r\n$e = &$modx->Event->name;\r\nswitch($e){\r\n	case \"OnWebPageInit\":\r\n\r\n		$result = [];\r\n		if($_POST){\r\n			$to = $modx->config[\'emailsender\'];\r\n			$name = $_POST[\'name\'];\r\n			$phone = $_POST[\'phone\'];\r\n			$email = $_POST[\'email\'];\r\n			$comment = $_POST[\'message\'];\r\n			$headers = \'From: webmaster@example.com\' . \"\\r\\n\" .\r\n				\'Reply-To: webmaster@example.com\' . \"\\r\\n\";\r\n\r\n			if(strlen($name) < 3){\r\n				$result[\'error_name\'] = \'Некорректно введено имя.\';\r\n			}\r\n\r\n			if(preg_match(\"/^([a-zA-Zа-яА-Я]{1,})$/\", $phone)){\r\n				$result[\'error_phone\'] = \'Не корректный формат номера телефона\';\r\n			}\r\n\r\n			if(!preg_match(\"/(\\w+@[a-zA-Z_]+?\\.[a-zA-Z]{2,6})/\", $email)){\r\n				$result[\'error_email\'] = \'Не корректный формат email\';\r\n			}\r\n\r\n			$message = \"<table>\r\n				<tr>\r\n					<td>Имя</td>\r\n					<td>$name</td>\r\n				</tr>\r\n				<tr>\r\n					<td>Номер телефона</td>\r\n					<td>$phone</td>\r\n				</tr>\r\n				<tr>\r\n					<td>Email</td>\r\n					<td>$email</td>\r\n				</tr>\r\n				<tr>\r\n					<td>Текст письма</td>\r\n					<td>$comment</td>\r\n				</tr>\r\n			</table>\";\r\n\r\n			if(!result && mail($to, $message, $headers)){\r\n				$result[\'success\'] = \'Запрос успешно отправлен. Мы свяжемся с вами в ближайшее время\';\r\n			}\r\n			return json_encode(\'success\'); die();\r\n		}\r\n}\r\n', 0, '{}', 0, ' '),
(11, 'VirtualAliases', '<strong>0.0.3</strong> Allows for an unlimited number of custom aliases per page.', 0, 14, 0, '$e = &$modx->event;\n\nif ($e->name == \"OnPageNotFound\") {\n   // Retrieve requested path + alias\n   $documentAlias = str_replace($modx->config[\'friendly_url_suffix\'], \'\', $_REQUEST[\'q\']);\n\n   // Search TVs for potential alias matches\n   $sql = \"SELECT tvc.contentid as id, tvc.value as value FROM \" . $modx->getFullTableName(\'site_tmplvars\') . \" tv \";\n   $sql .= \"INNER JOIN \" . $modx->getFullTableName(\'site_tmplvar_templates\') . \" tvtpl ON tvtpl.tmplvarid = tv.id \";\n   $sql .= \"LEFT JOIN \" . $modx->getFullTableName(\'site_tmplvar_contentvalues\') . \" tvc ON tvc.tmplvarid = tv.id \";\n   $sql .= \"LEFT JOIN \" . $modx->getFullTableName(\'site_content\') . \" sc ON sc.id = tvc.contentid \";\n   $sql .= \"WHERE sc.published = 1 AND tvtpl.templateid = sc.template AND tv.name = \'$aliasesTV\' AND tvc.value LIKE \'%\" . $modx->db->escape($documentAlias) . \"%\'\";\n   $results = $modx->dbQuery($sql);\n\n   // Attempt to find an exact match\n   $found = 0;\n   while ($found == 0 && $row = $modx->db->getRow($results)) {\n      $pageAliases = explode(\"\\n\", $row[\"value\"]);\n      while ($found == 0 && $alias = each($pageAliases)) {\n         if (trim($alias[1]) == $documentAlias) {\n            // Check for a match\n            $found = $row[\"id\"];\n         }\n      }\n   }\n\n   if ($found) {\n      // Redirect to new document, if an alias was found\n      if ($found == $modx->config[\'site_start\']) {\n         $pageUrl = $modx->config[\'site_url\'];\n      } else {\n         $pageUrl = $modx->makeUrl($found, \'\', \'\', \"full\");\n      }\n\n      // Send a permanent redirect\n      $modx->sendRedirect($pageUrl, 0, \"REDIRECT_HEADER\", \"HTTP/1.1 301 Moved Permanently\");\n      exit(0);\n   }\n\n   header(\"HTTP/1.0 404 Not Found\");\n   header(\"Status: 404 Not Found\");\n}', 0, '&aliasesTV=Aliases TV name;string;Aliases', 0, ''),
(12, 'GoogleAnalytics', '<strong>2.0.1 PL</strong> Adds your Google Analytics to every page in your site', 0, 14, 0, '//<?\n/**\n * GoogleAnalytics\n *\n * Adds your Google Analytics to every page in your site\n *\n * @author     Mark Kaplan\n * @category   plugin\n * @version    2.0 PL\n * @license	 http://www.gnu.org/copyleft/gpl.html GNU Public License (GPL)\n * @internal    @events OnWebPagePrerender\n * @internal    @disabled 1\n * @internal    @installset base\n * @internal    @modx_category Seo4Evo\n * @internal    @properties  &account=Account;string;UA-000000-0 &testMode=Test Mode;list;true,false;false &trackingNameTV=Alt tracking URL TV name;string; &trackingNamePH=Alt tracking URL placeholder name;string;&extraJs=Chunk containing extra JS;string; &codetype=Analytics code;list;UniversalAnalytics,GoogleAnalytics;UniversalAnalytics &codePosition=Place the analitycs code before closing tag;list;head,body;body\n */\n\n\n// By Mark Kaplan\n// Updated by Nicola Lambathakis, Nick Crossland, David Hyland, Ryan Thrash\n\n\n/* CHANGELOG\n\n2.0 - New Code position parameter\n\n1.9 - New Option to change code type (UniversalAnalytics/GoogleAnalytics)\n\n1.8 - New Google Universal Analitycs code\n\n1.7 - 5 Jan 2011 - fix bug with trackingNameTV (http://modxcms.com/forums/index.php/topic,52422.msg335386.html#msg335386)\n\n1.6 - 25 June 2010 - respect the site config \"Enable stats tracking\" setting **IF YOU UPGRADE AND FIND THE PLUGIN IS NOT WORKING - CHECK THIS SETTING, AS IT MAY NOT BE ENABLED ON YOUR SITE**\n\n1.5 - 9 June 2010 - updated to use only the latest Async tracking code, plus various cleanups. Added \"extraJs\" parameter. Add documentation. Fixed check to surpress output in logged in manager (which was broken)\n\n1.4 - 12-Mar-08 - added option to provide an alternative tracking name (by template variable or placeholder value). Made configuration interface slightly friendlier\n\n1.3 - 14-Dec-07 - added option of new or legacy analytics code\n\n1.2 - 1-Aug-06 - Added InManager check and test mode parameter\n\n1.1 - 30-Jun-06 - Updated to work with current Google docs\n\n*/\n\n\n/* INSTALL\n\n1. Create a new plugin, and paste the contents of this file into the Plugin Code area. Set the plugin name to \"Google Analytics\" and description to \"<strong>1.7</strong>  Adds Google Analytics code to every page in your site\"\n\n2. Copy this line into the Plugin Configuration of the Configuration tab:\n\n&account=Account;string;UA-000000-0 &testMode=Test Mode;list;true,false;false &trackingNameTV=Alt tracking URL TV name;string; &trackingNamePH=Alt tracking URL placeholder name;string;&extraJs=Chunk containing extra JS;string; &codetype=Analytics code;list;UniversalAnalytics,GoogleAnalytics;UniversalAnalytics &codePosition=Place the analitycs code before closing tag;list;head,body;body\n\n3. Enter your Google Account details on the configuration tab\n\n4. Check the OnWebPagePrerender box on the System Events tab\n\n5. Press Save\n\n6. Ensure \"Stats Tracking\" is enabled in the Site Configuration menu (Tools -> Configuration -> [ Site ] tab )  ** IMPORTANT IF UPGRADING - THIS SETTING WAS NOT RESPECTED BEFORE **\n\n*/\n\n/* USAGE\n\nOnce installed, you don\'t need to do anything else - all HTML pages which have the \"Enable Stats Tracking\" box ticked in Manager will automatically have the code inserted.\n\nPARAMETERS (set in configuration tab)\n\naccount = Google Analytics tracking code, which looks like UA-000000-0\ntestMode = boolean - if true, do not send tracking data to Google. Code is outputted as comments, useful for debugging\ntrackingNameTV = see ALTERNATIVE TRACKING NAMES\ntrackingNamePH = see ALTERNATIVE TRACKING NAMES\nextraJs = name of a chunk containing extra JS, which is inserted into the Google tracking code before the page view is recorded, if you wish to take advantage of GA\'s custom tracking features\ncodetype=Analytics js code type: new Universal Analytics or old GoogleAnalytics code\ncodePosition = choose js position : before closing head tag or before closing body tag (suggested)\n\nALTERNATIVE TRACKING NAMES\nBy default, Google will track the pages based on their [friendly] URL. Sometimes you may want to tell Analytics to report alternative tracking names.\nFor example to define stages of a goals, but where the URL may not change - e.g. submitting an eForm\nThe value for the URL that will be tracked can be set either by supplying a Template Variable name - this can then be entered on any page via the Manager\nAlternatively the name of a placeholder can be supplied, which can be set by other plugins or snippets. A placeholder value will override a template value.\n\n*/\n\n// Is stats tracking turned on in the Site Config?\nif ($modx->getConfig(\'track_visitors\') != 1) {\n	return;\n}\n\n\n// Parameters\n$account = isset($account) && $account != \'UA-000000-0\' ? $account : \'\'; // Ignore default value\n$testMode = isset($testMode) && ($testMode == \'true\') ? true: false;\n$extraJs = isset($extraJs) ? $modx->evalSnippets($modx->mergePlaceholderContent($modx->getChunk($extraJs))) : \'\';\n$codetype = isset($codetype)? $codetype: \'UniversalAnalytics\';\n$codePosition = isset($codePosition)? $codePosition: \'body\';\n\n// Tracking name - is it from a placeholder or a template variable?\n$trackingName_value = \'\';\nif (isset($trackingNameTV)) {\n		$trackingNameTv_array = !empty($trackingNameTV)?$modx->getTemplateVarOutput($trackingNameTV):array();\n		$trackingName_value = (isset($trackingNameTv_array[$trackingNameTV]) && !empty($trackingNameTv_array[$trackingNameTV]))?$trackingNameTv_array[$trackingNameTV]:$trackingName_value;\n}\n\n// If a placeholder name is defined, and the placeholder has a value, use that instead\nif (isset($trackingNamePH) && !empty($trackingNamePH)) {\n	$v = $modx->getPlaceholder($trackingNamePH) ;\n	$trackingName_value = !empty($v) ? $v : $trackingName_value;\n}\n\n\n// make sure an account number has been supplied\nif(!empty($account)){\n\n	// Enclose value in quotes and a comma\n	$trackingName_value = (!empty($trackingName_value)) ? \', \"\'.addslashes($trackingName_value).\'\"\' : \'\';\n		if ($codetype == \"GoogleAnalytics\") {\n	$script = \"\n\n	<script type=\\\"text/javascript\\\">\n\n	  var _gaq = _gaq || [];\n	  _gaq.push([\'_setAccount\', \'$account\']);\n	  $extraJs\n	  _gaq.push([\'_trackPageview\' $trackingName_value]);\n\n	  (function() {\n		var ga = document.createElement(\'script\'); ga.type = \'text/javascript\'; ga.async = true;\n		ga.src = (\'https:\' == document.location.protocol ? \'https://ssl\' : \'http://www\') + \'.google-analytics.com/ga.js\';\n		var s = document.getElementsByTagName(\'script\')[0]; s.parentNode.insertBefore(ga, s);\n	  })();\n\n	</script>\";\n	$insert_before = \'\'.$codePosition.\'\';\n		}\n        else {\n    	$script = \"\n\n\n<script>\n(function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){\n  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),\n  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)\n  })(window,document,\'script\',\'//www.google-analytics.com/analytics.js\',\'ga\');\n\n  ga(\'create\',\'$account\',\'auto\');\n  ga(\'send\',\'pageview\');\n\n</script>\";\n	$insert_before = \'\'.$codePosition.\'\';\n        }\n\n\n} else {\n  $script = \'<!-- Google Analytics account not supplied -->\';\n}\n\n\nswitch ($modx->Event->name) {\n\n	case \"OnWebPagePrerender\":\n\n		if ($testMode) {\n			$script = \'<!-- Google Analytics plugin is in test mode - output would be: \'. $script . \' -->\';\n		} else if (isset($_SESSION[\'mgrValidated\'])) {\n			$script = \'<!-- Logged in to Manager - Google Analytics plugin output surpressed, but would be: \'. $script . \' -->\';\n		}\n\n		// Only track HTML documents, in the front end, which have the \"Track\" box checked in the Manager\n		$googleize = ($modx->isFrontEnd() && $modx->documentObject[\'donthit\']==0 && $modx->documentObject[\'contentType\']==\'text/html\');\n\n		if ($googleize) {\n			$modx->documentOutput = preg_replace(\"/(<\\/$insert_before>)/i\", $script.\"\\n\\\\1\", $modx->documentOutput);\n		}\n\n		break;\n\n}', 0, '&account=Account;string;UA-000000-0 &testMode=Test Mode;list;true,false;false &trackingNameTV=Alt tracking URL TV name;string; &trackingNamePH=Alt tracking URL placeholder name;string;&extraJs=Chunk containing extra JS;string; &codetype=Analytics code;list;UniversalAnalytics,GoogleAnalytics;UniversalAnalytics &codePosition=Place the analitycs code before closing tag;list;head,body;body', 1, '');

-- --------------------------------------------------------

--
-- Структура таблицы `modx_site_plugin_events`
--

CREATE TABLE `modx_site_plugin_events` (
  `pluginid` int(10) NOT NULL,
  `evtid` int(10) NOT NULL DEFAULT '0',
  `priority` int(10) NOT NULL DEFAULT '0' COMMENT 'determines plugin run order'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Links to system events';

--
-- Дамп данных таблицы `modx_site_plugin_events`
--

INSERT INTO `modx_site_plugin_events` (`pluginid`, `evtid`, `priority`) VALUES
(1, 23, 0),
(1, 29, 0),
(1, 35, 0),
(1, 41, 0),
(1, 47, 0),
(1, 73, 0),
(1, 88, 0),
(2, 25, 0),
(2, 27, 0),
(2, 37, 0),
(2, 39, 0),
(2, 43, 0),
(2, 45, 0),
(2, 49, 0),
(2, 51, 0),
(2, 55, 0),
(2, 57, 0),
(2, 75, 0),
(2, 77, 0),
(2, 206, 0),
(2, 210, 0),
(2, 211, 0),
(3, 34, 0),
(3, 35, 0),
(3, 36, 0),
(3, 40, 0),
(3, 41, 0),
(3, 42, 0),
(4, 80, 0),
(4, 81, 0),
(4, 93, 0),
(5, 28, 0),
(5, 29, 0),
(5, 30, 0),
(5, 31, 0),
(5, 35, 0),
(5, 53, 0),
(5, 205, 0),
(6, 3, 0),
(6, 13, 0),
(6, 28, 0),
(6, 31, 0),
(6, 92, 0),
(7, 3, 0),
(8, 3, 0),
(8, 20, 0),
(8, 85, 0),
(8, 87, 0),
(8, 88, 0),
(8, 91, 0),
(8, 92, 0),
(9, 100, 0),
(10, 90, 1),
(11, 1000, 0),
(12, 3, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `modx_site_snippets`
--

CREATE TABLE `modx_site_snippets` (
  `id` int(10) NOT NULL,
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT 'Snippet',
  `editor_type` int(11) NOT NULL DEFAULT '0' COMMENT '0-plain text,1-rich text,2-code editor',
  `category` int(11) NOT NULL DEFAULT '0' COMMENT 'category id',
  `cache_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Cache option',
  `snippet` mediumtext,
  `locked` tinyint(4) NOT NULL DEFAULT '0',
  `properties` text COMMENT 'Default Properties',
  `moduleguid` varchar(32) NOT NULL DEFAULT '' COMMENT 'GUID of module from which to import shared parameters'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Contains the site snippets.';

--
-- Дамп данных таблицы `modx_site_snippets`
--

INSERT INTO `modx_site_snippets` (`id`, `name`, `description`, `editor_type`, `category`, `cache_type`, `snippet`, `locked`, `properties`, `moduleguid`) VALUES
(1, 'AjaxSearch', '<strong>1.10.1</strong> Ajax and non-Ajax search that supports results highlighting', 0, 4, 0, 'return require MODX_BASE_PATH.\'assets/snippets/ajaxSearch/snippet.ajaxSearch.php\';', 0, '', ''),
(2, 'Breadcrumbs', '<strong>1.0.5</strong> Configurable breadcrumb page-trail navigation', 0, 5, 0, 'return require MODX_BASE_PATH.\'assets/snippets/breadcrumbs/snippet.breadcrumbs.php\';', 0, '', ''),
(3, 'Ditto', '<strong>2.1.2</strong> Summarizes and lists pages to create blogs, catalogs, PR archives, bio listings and more', 0, 6, 0, 'return require MODX_BASE_PATH.\'assets/snippets/ditto/snippet.ditto.php\';', 0, '', ''),
(4, 'DocLister', '<strong>2.3.0</strong> Snippet to display the information of the tables by the description rules. The main goal - replacing Ditto and CatalogView', 0, 6, 0, 'return require MODX_BASE_PATH.\'assets/snippets/DocLister/snippet.DocLister.php\';', 0, '', ''),
(5, 'eForm', '<strong>1.4.8</strong> Robust form parser/processor with validation, multiple sending options, chunk/page support for forms and reports, and file uploads', 0, 7, 0, 'return require MODX_BASE_PATH.\'assets/snippets/eform/snippet.eform.php\';', 0, '', ''),
(6, 'FirstChildRedirect', '<strong>2.0</strong> Automatically redirects to the first child of a Container Resource', 0, 5, 0, 'return require MODX_BASE_PATH.\'assets/snippets/firstchildredirect/snippet.firstchildredirect.php\';', 0, '', ''),
(7, 'if', '<strong>1.3</strong> A simple conditional snippet. Allows for eq/neq/lt/gt/etc logic within templates, resources, chunks, etc.', 0, 5, 0, 'return require MODX_BASE_PATH.\'assets/snippets/if/snippet.if.php\';', 0, '', ''),
(8, 'Jot', '<strong>1.1.5</strong> User comments with moderation and email subscription', 0, 6, 0, '\n/**\n * Jot\n * \n * User comments with moderation and email subscription\n *\n * @category 	snippet\n * @version 	1.1.5\n * @license 	http://www.gnu.org/copyleft/gpl.html GNU Public License (GPL)\n * @internal	@properties\n * @internal	@modx_category Content\n * @internal    @installset base, sample\n * @documentation MODX Wiki http://wiki.modxcms.com/index.php/Jot\n * @reportissues https://github.com/modxcms/evolution\n * @link 		Latest Version http://modx.com/extras/package/jot\n * @link 		Jot Demo Site http://projects.zerobarrier.nl/modx/\n * @author      Armand \"bS\" Pondman apondman@zerobarrier.nl\n * @lastupdate  09/02/2016\n */\n$jotPath = $modx->config[\'base_path\'] . \'assets/snippets/jot/\';\ninclude_once($jotPath.\'jot.class.inc.php\');\n\n$Jot = new CJot;\n$Jot->VersionCheck(\"1.1.5\");\n$Jot->Set(\"path\",$jotPath);\n$Jot->Set(\"action\", $action);\n$Jot->Set(\"postdelay\", $postdelay);\n$Jot->Set(\"docid\", $docid);\n$Jot->Set(\"tagid\", $tagid);\n$Jot->Set(\"subscribe\", $subscribe);\n$Jot->Set(\"moderated\", $moderated);\n$Jot->Set(\"captcha\", $captcha);\n$Jot->Set(\"badwords\", $badwords);\n$Jot->Set(\"bw\", $bw);\n$Jot->Set(\"sortby\", $sortby);\n$Jot->Set(\"numdir\", $numdir);\n$Jot->Set(\"customfields\", $customfields);\n$Jot->Set(\"guestname\", $guestname);\n$Jot->Set(\"canpost\", $canpost);\n$Jot->Set(\"canview\", $canview);\n$Jot->Set(\"canedit\", $canedit);\n$Jot->Set(\"canmoderate\", $canmoderate);\n$Jot->Set(\"trusted\", $trusted);\n$Jot->Set(\"pagination\", $pagination);\n$Jot->Set(\"placeholders\", $placeholders);\n$Jot->Set(\"subjectSubscribe\", $subjectSubscribe);\n$Jot->Set(\"subjectModerate\", $subjectModerate);\n$Jot->Set(\"subjectAuthor\", $subjectAuthor);\n$Jot->Set(\"notify\", $notify);\n$Jot->Set(\"notifyAuthor\", $notifyAuthor);\n$Jot->Set(\"validate\", $validate);\n$Jot->Set(\"title\", $title);\n$Jot->Set(\"authorid\", $authorid);\n$Jot->Set(\"css\", $css);\n$Jot->Set(\"cssFile\", $cssFile);\n$Jot->Set(\"cssRowAlt\", $cssRowAlt);\n$Jot->Set(\"cssRowMe\", $cssRowMe);\n$Jot->Set(\"cssRowAuthor\", $cssRowAuthor);\n$Jot->Set(\"tplForm\", $tplForm);\n$Jot->Set(\"tplComments\", $tplComments);\n$Jot->Set(\"tplModerate\", $tplModerate);\n$Jot->Set(\"tplNav\", $tplNav);\n$Jot->Set(\"tplNotify\", $tplNotify);\n$Jot->Set(\"tplNotifyModerator\", $tplNotifyModerator);\n$Jot->Set(\"tplNotifyAuthor\", $tplNotifyAuthor);\n$Jot->Set(\"tplSubscribe\", $tplSubscribe);\n$Jot->Set(\"debug\", $debug);\n$Jot->Set(\"output\", $output);\nreturn $Jot->Run();', 0, '', ''),
(9, 'MemberCheck', '<strong>1.1</strong> Show chunks based on a logged in Web User\'s group membership', 0, 8, 0, 'return require MODX_BASE_PATH.\'assets/snippets/membercheck/snippet.membercheck.php\';', 0, '', ''),
(10, 'Personalize', '<strong>2.1</strong> Checks to see if web- / mgr-users are logged in or not, to display accordingly yesChunk/noChunk', 0, 8, 0, 'return require MODX_BASE_PATH.\'assets/snippets/personalize/snippet.personalize.php\';', 0, '', ''),
(11, 'phpthumb', '<strong>1.3</strong> PHPThumb creates thumbnails and altered images on the fly and caches them', 0, 6, 0, 'return require MODX_BASE_PATH.\'assets/snippets/phpthumb/snippet.phpthumb.php\';\r\n', 0, '', ''),
(12, 'Reflect', '<strong>2.2</strong> Generates date-based archives using Ditto', 0, 6, 0, '\n/**\n * Reflect\n * \n * Generates date-based archives using Ditto\n *\n * @category 	snippet\n * @version 	2.2\n * @license 	http://www.gnu.org/copyleft/gpl.html GNU Public License (GPL)\n * @internal	@properties\n * @internal	@modx_category Content\n * @internal    @installset base, sample\n * @documentation Cheatsheet https://de.scribd.com/doc/55919355/MODx-Ditto-and-Reflect-Cheatsheet-v1-2\n * @documentation Inside snippet-code\n * @reportissues https://github.com/modxcms/evolution\n * @author      Mark Kaplan\n * @author      Ryan Thrash http://thrash.me\n * @author      netProphET, Dmi3yy, bossloper, yamamoto\n * @lastupdate  2016-11-21\n */\n\n/*\n *  Note: \n *  If Reflect is not retrieving its own documents, make sure that the\n *  Ditto call feeding it has all of the fields in it that you plan on\n *  calling in your Reflect template. Furthermore, Reflect will ONLY\n *  show what is currently in the Ditto result set.\n *  Thus, if pagination is on it will ONLY show that page\'s items.\n*/\n\nreturn require MODX_BASE_PATH.\'assets/snippets/reflect/snippet.reflect.php\';\n', 0, '', ''),
(13, 'UltimateParent', '<strong>2.0</strong> Travels up the document tree from a specified document and returns its \"ultimate\" non-root parent', 0, 5, 0, 'return require MODX_BASE_PATH.\'assets/snippets/ultimateparent/snippet.ultimateparent.php\';', 0, '', ''),
(14, 'Wayfinder', '<strong>2.0.5</strong> Completely template-driven and highly flexible menu builder', 0, 5, 0, 'return require MODX_BASE_PATH.\'assets/snippets/wayfinder/snippet.wayfinder.php\';\n', 0, '', ''),
(15, 'WebChangePwd', '<strong>1.1.2</strong> Allows Web User to change their password from the front-end of the website', 0, 8, 0, '\n/**\n * WebChangePwd\n * \n * Allows Web User to change their password from the front-end of the website\n *\n * @category 	snippet\n * @version 	1.1.2\n * @license 	http://www.gnu.org/copyleft/gpl.html GNU Public License (GPL)\n * @internal	@properties\n * @internal	@modx_category Login\n * @internal    @installset base\n * @documentation [+site_url+]assets/snippets/weblogin/docs/webchangepwd.html\n * @documentation http://www.opensourcecms.com/news/details.php?newsid=660\n * @reportissues https://github.com/modxcms/evolution\n * @author      Created By Raymond Irving April, 2005\n * @author      Ryan Thrash http://thrash.me\n * @author      Jason Coward http://opengeek.com\n * @author      Shaun McCormick, garryn, Dmi3yy\n * @lastupdate  09/02/2016\n */\n\n# Set Snippet Paths \n$snipPath  = (($modx->isBackend())? \"../\":\"\");\n$snipPath .= \"assets/snippets/\";\n\n# check if inside manager\nif ($m = $modx->isBackend()) {\n	return \'\'; # don\'t go any further when inside manager\n}\n\n\n# Snippet customize settings\n$tpl		= isset($tpl)? $tpl:\"\";\n\n# System settings\n$isPostBack		= count($_POST) && isset($_POST[\'cmdwebchngpwd\']);\n\n# Start processing\ninclude_once $snipPath.\"weblogin/weblogin.common.inc.php\";\ninclude_once $snipPath.\"weblogin/webchangepwd.inc.php\";\n\n# Return\nreturn $output;\n\n\n\n', 0, '', ''),
(16, 'WebLogin', '<strong>1.2</strong> Allows webusers to login to protected pages in the website, supporting multiple user groups', 0, 8, 0, 'return require MODX_BASE_PATH.\'assets/snippets/weblogin/snippet.weblogin.php\';\n', 0, '&loginhomeid=Login Home Id;string; &logouthomeid=Logout Home Id;string; &logintext=Login Button Text;string; &logouttext=Logout Button Text;string; &tpl=Template;string;', ''),
(17, 'WebSignup', '<strong>1.1.2</strong> Basic Web User account creation/signup system', 0, 8, 0, '\n/**\n * WebSignup\n * \n * Basic Web User account creation/signup system\n *\n * @category 	snippet\n * @version 	1.1.2\n * @license 	http://www.gnu.org/copyleft/gpl.html GNU Public License (GPL)\n * @internal	@properties &tpl=Template;string;\n * @internal	@modx_category Login\n * @internal    @installset base, sample\n * @documentation [+site_url+]assets/snippets/weblogin/docs/websignup.html\n * @documentation http://www.opensourcecms.com/news/details.php?newsid=660\n * @reportissues https://github.com/modxcms/evolution\n * @author      Created By Raymond Irving April, 2005\n * @author      Ryan Thrash http://thrash.me\n * @author      Jason Coward http://opengeek.com\n * @author      Shaun McCormick, garryn, Dmi3yy\n * @lastupdate  09/02/2016\n */\n\n# Set Snippet Paths \n$snipPath = $modx->config[\'base_path\'] . \"assets/snippets/\";\n\n# check if inside manager\nif ($m = $modx->isBackend()) {\n    return \'\'; # don\'t go any further when inside manager\n}\n\n\n# Snippet customize settings\n$tpl = isset($tpl)? $tpl:\"\";\n$useCaptcha = isset($useCaptcha)? $useCaptcha : $modx->config[\'use_captcha\'] ;\n// Override captcha if no GD\nif ($useCaptcha && !gd_info()) $useCaptcha = 0;\n\n# setup web groups\n$groups = isset($groups) ? array_filter(array_map(\'trim\', explode(\',\', $groups))):array();\n\n# System settings\n$isPostBack        = count($_POST) && isset($_POST[\'cmdwebsignup\']);\n\n$output = \'\';\n\n# Start processing\ninclude_once $snipPath.\"weblogin/weblogin.common.inc.php\";\ninclude_once $snipPath.\"weblogin/websignup.inc.php\";\n\n# Return\nreturn $output;', 0, '&tpl=Template;string;', ''),
(18, 'S', '', 0, 0, 0, '\r\n$newsId = isset($_GET[\'newsId\']) && $_GET[\'newsId\'] ? intval($_GET[\'newsId\']): false;	\r\n	\r\nswitch($get){\r\n	case \'news\';\r\n		\r\n		if($newsId){\r\n			header(\"Loaction: /news\");\r\n		\r\n			$item = [];\r\n			$query = $modx->db->query(\"SELECT * FROM `modx_a_news` WHERE NewsId = {$newsId}\");\r\n			$item = $modx->db->getRow($query);\r\n			if($item){\r\n\r\n			}\r\n		} else{\r\n			$news = [];\r\n			$query = $modx->db->query(\"SELECT * FROM `modx_a_news`\");		\r\n			$news = $modx->db->makeArray($query);\r\n\r\n			$response = \'\';\r\n			if($news){\r\n				foreach($news as $article){\r\n					if(strlen($article[\'Description\']) > 300){\r\n						$article[\'Description\'] = substr($article[\'Description\'], 0, 300) . \'...\';\r\n					}\r\n\r\n					$response .= $modx->parseChunk(\'tpl_news_item\', [\r\n						\'title\' => $article[\'Title\'], \r\n						\'description\' => strip_tags($article[\'Description\']), \r\n						\'link\' => $article[\'NewsId\']\r\n					], \'[+\', \'+]\');\r\n				}\r\n			}\r\n		}\r\n		\r\n		return $response;\r\n	case \'lastNews\':\r\n		$html = \'\';\r\n		$news = [];\r\n		\r\n		$query = $modx->db->query(\"SELECT * FROM `modx_a_news` ORDER BY NewsId DESC LIMIT 4\");\r\n		$news = $modx->db->makeArray($query);\r\n		\r\n		if($news){\r\n			$html .= \'<div class=\"articles\"><p>Последние новости</p>\';\r\n			\r\n			foreach($news as $article){\r\n				if(strlen($article[\'Description\']) > 100){\r\n					$article[\'Description\'] = substr($article[\'Description\'], 0, 100) . \'...\';\r\n				}\r\n				\r\n				$html .= $modx->parseChunk(\'tpl_latest_news_item\', [\r\n					\'title\' => $article[\'Title\'], \r\n					\'description\' => strip_tags($article[\'Description\']), \r\n					\'link\' => $article[\'NewsId\']\r\n				], \'[+\', \'+]\');\r\n			}\r\n			$html .= \'</div>\';\r\n		}\r\n		\r\n		return $html;\r\n}', 0, '{}', ' '),
(19, 'Seo4Evo', '<strong>1.0.1</strong> Seo4Evo 1.0.1 - Manage and return Meta Tags using Evolution Tvs', 0, 14, 0, '/**\n| --------------------------------------------------------------------------\n| Snippet Title:     Seo4Evo By Nicola  (originally inspired by MetaTagsExtra by Soda)\n| Snippet Version:  1.0.1\n|\n| Description:\n| Manage and return Meta Tags using modx Tvs from Seo4Evo Package\n|\n| Basic Snippet Parameters:\n|\n| Keywords - disable/enable MetaKeywords metatags (default 1 = enabled) - Example: &Keywords=`0`\n| KeywordsTv - custom keywords tv - Example: &KeywordsTv=`documentTags`\n| MetaDescriptionTv -  custom description tv - Example: &MetaDescriptionTv=`introtext`\n| all_page_keywords - chunk or comma separated list of Keywords displayed on all pages - Example: &all_page_keywords=`modx, snippets, plugins`\n| preTitle -  custom pre title - Example: &preTitle &preTitle=`[(site_name)] |`\n| postTitle -  custom post title - Example:  &postTitle=`| [(site_name)]`\n| HeaderExtras - enable meta name=\"viewport\" and meta http-equiv  - Example: &HeaderExtras=`1`\n|\n| ******* Facebook Open Graph  Parameters: ******\n| OpenGraph - enable OG metatags  - Example: &OpenGraph=`1`\n| OGfbappId your facebook app id - Example: &OGfbappId=`123456789123456789`\n| OGtype - default: website - Example &OGtype=`article`\n| OGimageTv - default: thumbnail - Example: &OGimageTv=`my-thumbnail`\n|\n| ******* GooglePlus  Parameters: ******\n| GooglePlus - enable G+ metatags  - Example: &GooglePlus=`1`\n| linkPub your google publisher page  - Example: &linkPub=`https://plus.google.com/123456789123456789`\n|\n| Usage:\n|  Insert [[Seo4Evo]] anywhere in the head section of your template.\n|\n|  with custom MetaTags Tv (for old sites)\n|  [[Seo4Evo? &KeywordsTv=`documentTags`]]\n|\n|  With Facebook Open Graph metatags\n| [[Seo4Evo? &OpenGraph=`1` &OGfbappId=`123456789123456789` &OGimageTv=`my-thumbnail` &OGtype=`article`]]\n|\n| With Facebook Open Graph e Google plus metatags\n| [[Seo4Evo? &OpenGraph=`1` &OGfbappId=`123456789123456789` &OGimageTv=`my-thumbnail` &OGtype=`article` &GooglePlus=`1` &linkPub=`https://plus.google.com/123456789123456789`]]\n|\n| With Facebook Open Graph, Google plus metatags, GeoLocation, DublinCore, Favicons\n| [[Seo4Evo? &OpenGraph=`1` &OGfbappId=`123456789123456789` &OGimageTv=`my-thumbnail` &OGtype=`article` &GooglePlus=`1` &linkPub=`https://plus.google.com/123456789123456789` &GeoLocation=`1` &Country=`IT-RM` &City=`Rome` &LatLon=`41.859061, 12.540894` &DublinCore=`1` &dcLang=`en` &Favicons=`1` &iconDir=`/`]]\n| ---------------------------------------------------------------------------\n\n*/\n$Keywords = isset($Keywords) ? $Keywords : \'1\';\n$KeywordsTv = isset($KeywordsTv) ? $KeywordsTv : \'[*MetaKeywords*]\';\n$MetaDescriptionTv = isset($MetaDescriptionTv) ? $MetaDescriptionTv : \'MetaDescription\';\n$MetaKeywords =\"\";\n$comma=(isset($all_page_keywords))?\', \':\'\';\n\n// *** KEYWORDS ***//\n$MetaKeywords= \"	<meta name=\\\"keywords\\\" content=\\\"{$all_page_keywords}{$comma}{$KeywordsTv}\\\">\\n\";\n$MetaCharset =\"\";\n$BaseUrl =\"\";\n$MetaDesc = \"\";\n$MetaRobots = \"\";\n$MetaCopyright = \"\";\n\n$id = $modx -> documentObject[\'id\'];\n$url = $modx->makeUrl($id, \'\', \'\', \'full\');\n\n// *** Meta Title ***//\n$preTitle = isset($preTitle) ? $preTitle : \'\';\n$postTitle = isset($postTitle) ? $postTitle : \'\';\n\n$pagetitle = $modx->documentObject[\'pagetitle\'];\n$CTitle = $modx->getTemplateVarOutput(\'CustomTitle\',$id);\n$Custom = $CTitle[\'CustomTitle\'];\n\nif(!$Custom == \"\"){\n$MetaTitle = \"	<title>\".$preTitle.$Custom.$postTitle.\" | [(site_name)]</title>\\n\";\n$SocialTitle = $preTitle.$Custom.$postTitle;\n} else {\n      $MetaTitle = \"	<title>\".$preTitle.$pagetitle.$postTitle.\" | [(site_name)]</title>\\n\";\n      $SocialTitle = $preTitle.$pagetitle.$postTitle;\n   }\n\n// *** Header Desc ***//\n$HeaderDesc = \"<!-- Basic Header Needs\n	================================================== -->\\n\";\n	\n// *** Meta Charset ***//\n$MetaCharset = \"	<meta charset=\\\"[(modx_charset)]\\\">\\n\";\n\n// *** BASEURL ***//\n$BaseUrl = \"	<base href=\\\"[(site_url)]\\\">\\n\";\n\n// *** Header Extra ***//\n$HeaderExtra = \"	<meta name=\\\"viewport\\\" content=\\\"width=device-width, initial-scale=1\\\">\n	<meta http-equiv=\\\"X-UA-Compatible\\\" content=\\\"IE=edge\\\">\\n\\n\";\n	\n// *** Meta Description ***//\n$dyndesc = $modx->runSnippet(\n        \"DynamicDescription\",\n        array(\n            \"descriptionTV\" => $MetaDescriptionTv,\n			\"maxWordCount=\" => \"25\"\n        )\n);\n\n$MetaDesc = \"	<meta name=\\\"description\\\" content=\\\"\".$dyndesc.\"\\\">\\n\";\n\n// *** Meta Robots ***//\n$MetaRobots = \"	<meta name=\\\"robots\\\" content=\\\"[*RobotsIndex*], [*RobotsFollow*]\\\">\\n\";\n\n//*** Meta Copiright ***//\n$MetaCopyright = \"	<meta name=\\\"copyright\\\" content=\\\"[(site_name)]\\\">\\n\";\n\n// *** Last Modified ***//\n$editedon = date(r,$modx->documentObject[\'editedon\']);\n$MetaEditedOn = \"	<meta http-equiv=\\\"last-modified\\\" content=\\\"\".$editedon.\"\\\">\\n\";\n\n// ** FACEBOOK OPEN GRAPH PROTOCOL **//\n$imageUrl = isset($OGimageTv) ? $OGimageTv : \'thumbnail\';\n$type = isset($OGtype) ? $OGtype : \'website\';\n$title = isset($OGtitle) ? $OGtitle : $preTitle.$Custom.$postTitle;\n$description = isset($OGdescription) ? $OGdescription : $dyndesc;\n\n\n$FbDesc = \"	<!-- Facebook, more here http://ogp.me/ \n	=================================================== -->\\n\";\n$MetaProperty = \"	<meta property=\\\"og:site_name\\\" content=\\\"[(site_name)]\\\">\\n\";\n$MetaPropertyType = \"	<meta property=\\\"og:type\\\" content=\\\"\".$type.\"\\\">\\n\";\n$MetaPropertyTitle = \"	<meta property=\\\"og:title\\\" content=\\\"\".$SocialTitle.\"\\\">\\n\";\n$MetaPropertyDescription = \"	<meta property=\\\"og:description\\\" content=\\\"\".$description.\"\\\">\\n\";\n$MetaPropertyUrl = \"	<meta property=\\\"og:url\\\" content=\\\"\".$url.\"\\\">\\n\";\n$MetaPropertyImage = \"	<meta property=\\\"og:image\\\" content=\\\"[(site_url)][*$imageUrl*]\\\">\\n\";\n$MetaPropertyFbApp = \"	<meta property=\\\"fb:app_id\\\" content=\\\"\".$OGfbappId.\"\\\">\\n\\n\";\n\n//*** Google Plus ***//\n$GoogleDesc = \"	<!-- Google Publisher. Profile url example: https://plus.google.com/1130658794498306186 \n	=================================================== -->\\n\";\n$linkPub = isset($linkPub) ? $linkPub : \'\';\n$LinkPublisher = \"	<link rel=\\\"publisher\\\" href=\\\"\".$linkPub.\"\\\">\\n\\n\";\n$GAuthor = $modx->getTemplateVarOutput(\'GoogleAuthor\',$id);\n$GoogleAthorship = $GAuthor[\'GoogleAuthor\'];\nif(!$GoogleAthorship == \"\"){\n$LinkAuthor = \"	<link rel=\\\"author\\\" href=\\\"\".$GoogleAthorship.\"\\\">\\n\\n\";\n}\n\n//*** Canonical ***//\n// Custom CanonicalUrl tv\n// with tv empty > link to siteurl for homepage and to full alias url for other pages.\n\n$CUrl = $modx->getTemplateVarOutput(\'CanonicalUrl\',$id);\n$CanonicalUrl = $CUrl[\'CanonicalUrl\'];\n\nif(!$CanonicalUrl == \"\"){\n$Canonical = \"	<link rel=\\\"canonical\\\" href=\\\"\".$CanonicalUrl.\"\\\">\\n\\n\";\n} else {\n	$Canonical = $modx->documentIdentifier == $modx->config[\'site_start\'] ? \"	<link rel=\\\"canonical\\\" href=\\\"[(site_url)]\\\" />\" : \"	<link rel=\\\"canonical\\\" href=\\\"[(site_url)][~[*id*]~]\\\">\\n\\n\";\n}\n\n//*** GeoLocation ***//\n$GeoMeta = \"	<!-- GeoLocation Meta Tags / Geotagging. Used for custom results in Google. Generator here http://www.geo-tag.de/generator/en.html/ \n	=================================================== -->\n	<meta name=\\\"geo.region\\\" content=\\\"\".$Country.\"\\\" /> <!-- CountryCode-RegionalCode ex.: ES-TF -->\n	<meta name=\\\"geo.placename\\\" content=\\\"\".$City.\"\\\" /> <!-- City -->\n	<meta name=\\\"geo.position\\\" content=\\\"\".$LatLon.\"\\\" /> <!-- Lat/Lon -->\n	<meta name=\\\"ICBM\\\" content=\\\"\".$LatLon.\"\\\" /> <!-- Lat/Lon -->\\n\\n\";\n\n//*** Dublin Core ***//\n$DCmeta = \"	<!-- Dublin Core Metadata Element Set\n	=================================================== -->\n	<link rel=\\\"schema.DC\\\" href=\\\"http://purl.org/DC/elements/1.0/\\\" />\n	<meta name=\\\"DC.Title\\\" content=\\\"\".$SocialTitle.\"\\\" />\n	<meta name=\\\"DC.Creator\\\" content=\\\"[(site_name)]\\\" />\n	<meta name=\\\"DC.Type\\\" content=\\\"\".$OGtype.\"\\\" />\n	<meta name=\\\"DC.Date\\\" content=\\\"\".$editedon.\"\\\" />\n	<meta name=\\\"DC.Format\\\" content=\\\"text/html\\\" />\n	<meta name=\\\"DC.Language\\\" content=\\\"\".$dcLang.\"\\\" />\\n\\n\";\n\n//*** Favicons ***//\n$Icons = \"	<!-- Favicons. Generator here: http://www.favicon-generator.org/ \n	=================================================== -->\n	<link rel=\\\"icon\\\" type=\\\"image/png\\\" sizes=\\\"36x36\\\" href=\\\"\".$iconDir.\"android-icon-36x36.png\\\">\n	<link rel=\\\"icon\\\" type=\\\"image/png\\\" sizes=\\\"48x48\\\" href=\\\"\".$iconDir.\"android-icon-48x48.png\\\">\n	<link rel=\\\"icon\\\" type=\\\"image/png\\\" sizes=\\\"72x72\\\" href=\\\"\".$iconDir.\"android-icon-72x72.png\\\">\n	<link rel=\\\"icon\\\" type=\\\"image/png\\\" sizes=\\\"96x96\\\" href=\\\"\".$iconDir.\"android-icon-96x96.png\\\">\n	<link rel=\\\"icon\\\" type=\\\"image/png\\\" sizes=\\\"144x144\\\" href=\\\"\".$iconDir.\"android-icon-144x144.png\\\">\n	<link rel=\\\"icon\\\" type=\\\"image/png\\\" sizes=\\\"192x192\\\" href=\\\"\".$iconDir.\"android-icon-192x192.png\\\">\n	\n	<link rel=\\\"apple-touch-icon\\\" sizes=\\\"192x192\\\" href=\\\"\".$iconDir.\"apple-icon.png\\\">\n	<link rel=\\\"apple-touch-icon\\\" sizes=\\\"57x57\\\" href=\\\"\".$iconDir.\"apple-icon-57x57.png\\\">\n	<link rel=\\\"apple-touch-icon\\\" sizes=\\\"60x60\\\" href=\\\"\".$iconDir.\"apple-icon-60x60.png\\\">\n	<link rel=\\\"apple-touch-icon\\\" sizes=\\\"72x72\\\" href=\\\"\".$iconDir.\"apple-icon-72x72.png\\\">\n	<link rel=\\\"apple-touch-icon\\\" sizes=\\\"76x76\\\" href=\\\"\".$iconDir.\"apple-icon-76x76.png\\\">\n	<link rel=\\\"apple-touch-icon\\\" sizes=\\\"114x114\\\" href=\\\"\".$iconDir.\"apple-icon-114x114.png\\\">\n	<link rel=\\\"apple-touch-icon\\\" sizes=\\\"120x120\\\" href=\\\"\".$iconDir.\"apple-icon-120x120.png\\\">\n	<link rel=\\\"apple-touch-icon\\\" sizes=\\\"144x144\\\" href=\\\"\".$iconDir.\"apple-icon-144x144.png\\\">\n	<link rel=\\\"apple-touch-icon\\\" sizes=\\\"152x152\\\" href=\\\"\".$iconDir.\"apple-icon-152x152.png\\\">\n	<link rel=\\\"apple-touch-icon\\\" sizes=\\\"180x180\\\" href=\\\"\".$iconDir.\"apple-icon-180x180.png\\\">\n	<link rel=\\\"apple-touch-icon\\\" sizes=\\\"192x192\\\" href=\\\"\".$iconDir.\"apple-icon-precomposed.png\\\">\n	\n	<link rel=\\\"shortcut icon\\\" type=\\\"image/x-icon\\\" href=\\\"\".$iconDir.\"favicon.ico\\\" />\n	<link rel=\\\"icon\\\" type=\\\"image/png\\\" sizes=\\\"16x16\\\" href=\\\"\".$iconDir.\"favicon-16x16.png\\\">\n	<link rel=\\\"icon\\\" type=\\\"image/png\\\" sizes=\\\"32x32\\\" href=\\\"\".$iconDir.\"favicon-32x32.png\\\">\n	<link rel=\\\"icon\\\" type=\\\"image/png\\\" sizes=\\\"96x96\\\" href=\\\"\".$iconDir.\"favicon-96x96.png\\\">\n	\n	<meta name=\\\"msapplication-TileImage\\\" content=\\\"\".$iconDir.\"ms-icon-70x70.png\\\">\n	<meta name=\\\"msapplication-TileImage\\\" content=\\\"\".$iconDir.\"ms-icon-144x144.png\\\">\n	<meta name=\\\"msapplication-TileImage\\\" content=\\\"\".$iconDir.\"ms-icon-150x150.png\\\">\n	<meta name=\\\"msapplication-TileImage\\\" content=\\\"\".$iconDir.\"ms-icon-310x310.png\\\">\n	\n	<meta name=\\\"msapplication-config\\\" content=\\\"\".$iconDir.\"browserconfig.xml\\\" />\n	<meta name=\\\"msapplication-TileColor\\\" content=\\\"#ffffff\\\">\n	<link rel=\\\"manifest\\\" href=\\\"\".$iconDir.\"manifest.json\\\">\n	<meta name=\\\"theme-color\\\" content=\\\"#ffffff\\\">\";\n\n// *** RETURN RESULTS ***\n// you can change the order of displayed items:\n$output = $HeaderDesc.$MetaCharset.$BaseUrl;\n//return HeaderExtra metatags if HeaderExtras=1 \nif ($HeaderExtras >= 1) {\n    $output .= $HeaderExtra;\n}\n$output .= $MetaTitle.$MetaDesc;\n//return Keyword metatags if Keywords=1 (default 1)\nif ($Keywords >= 1) {\n    $output .= $MetaKeywords;\n}\n$output .= $MetaRobots.$MetaCopyright.$MetaEditedOn.$Canonical;\n//return OpenGraph metatags if OpenGraph=1\nif ($OpenGraph >= 1) {\n    $output .= $FbDesc.$MetaProperty.$MetaPropertyType.$MetaPropertyTitle.$MetaPropertyDescription.$MetaPropertyUrl.$MetaPropertyImage.$MetaPropertyFbApp;\n}\n//return Google plus metatags if GooglePlus=1\nif ($GooglePlus >= 1) {\n    $output .= $GoogleDesc.$LinkAuthor.$LinkPublisher;\n}\n//return GeoLocation if GeoLocation=1\nif ($GeoLocation >= 1) {\n    $output .= $GeoMeta;\n}\n//return Dublin Core if DublinCore=1\nif ($DublinCore >= 1) {\n    $output .= $DCmeta;\n}\n//return Favicons if Favicons=1\nif ($Favicons >= 1) {\n    $output .= $Icons;\n}\n\nreturn $output;', 0, '', ''),
(20, 'DynamicDescription', '<strong>1.0.1</strong> DynamicDescription 1.0.1', 0, 14, 0, '/**\n * Copyright (C) 2007 James Ehly\n *\n * This program is free software; you can redistribute it and/or modify\n * it under the terms of the GNU General Public License as published by\n * the Free Software Foundation; either version 2 of the License, or\n * (at your option) any later version.\n *\n * This program is distributed in the hope that it will be useful,\n * but WITHOUT ANY WARRANTY; without even the implied warranty of\n * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n * GNU General Public License for more details.\n *\n * You should have received a copy of the GNU General Public License\n * along with this program; if not, write to the Free Software\n * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA\n *\n */\n\n/**\n * Installation:\n *\n * Just copy this code into a new Snippet and name it DynamicDescription\n * Nothin\' to it :)\n *\n */\n\n/**\n * Usage:\n *\n * <META NAME=\"description\" CONTENT=\"[[DynamicDescription]]\">\n *\n * The above example is a typical way to use this snippet. This creates\n * a dynamic description of the first 25 words of the document content.\n * The meta tag and snippet call should be placed in the <head> portion\n * of your template.\n *\n * [[DynamicDescription? &descriptionTV=`MetaDescription`]]\n *\n * The above example creates a dynamic description of the MetaDescription\n * template variable. If MetaDescription is empty it will default to the\n * document content.  This is useful for the meta description tag, where\n * most documents can draw from the document content for a dynamic\n * description. You can use the template variable for pages comprised of\n * snippets and chunks only (like a blog page that only calls the ditto\n * snippet)\n *\n * [[DynamicDescription? &descriptionTV=`MetaDescription` &id=`39` &maxWordCount=`30`]]\n *\n * The above example creates a dynamic description of the MetaDescription\n * template variable for the document with an id of 39. If that is\n * empty then it will default to the content of that document. This also\n * changes the maxWordCount to 30.\n *\n */\n\n/* Parameters */\n\n/**\n * &maxWordCount\n * integer (25)\n * Sets the maximum word count of the description, default is 25 words\n */\n$maxWordCount = (!empty($maxWordCount) && is_numeric($maxWordCount)) ? $maxWordCount : 25;\n\n/**\n * &id\n * integer ($modx->documentObject[\'id\'])\n * the id of the page you want a description for, defaults to the\n * current page.\n */\n$id = (is_numeric($id) && !empty($id)) ? $id : $modx->documentObject[\'id\'];\n\n/**\n * &descriptionTV\n * string (\'\')\n * The name of a template variable you\'ve set up for the description.\n * If you have a separate template variable set up for a description\n * you write in, set that with this parameter.  If the parameter ends\n * up being empty, it will default back to the content. If it is not\n * empty then it will display the value of the TV and stop processing.\n * In other words, it will print it just as you typed it.\n */\n$descriptionTvArray = (!empty($descriptionTV)) ? $modx->getTemplateVarOutput($descriptionTV,$id) : \'\';\n$descriptionTV = $descriptionTvArray[$descriptionTV];\n\n\n/* Snippet Code */\n\n/**\n * Function: getDynaDescription()\n * Returns:  A string of text ready to be placed in a meta description tag\n */\nif (!function_exists(getDynaDescription)) {\n  function getDynaDescription($text=\'\',$excerpt_length=25)\n  {\n    global $modx;\n    $text = str_replace(\']]>\', \']]&gt;\', $text);\n    /* remove line breaks */\n    $text = str_replace(\"\\n\",\' \',$text);\n    $text = str_replace(\"\\r\",\' \',$text);\n    /* remove entity chars */\n    $text = html_entity_decode($text,ENT_QUOTES|ENT_HTML5,\'UTF-8\');\n	$text = str_replace(array(\'\"\',\"\'\"),\'\',$text);	\n    /* remove special MODx tags - chunks, snippets, etc.\n     * If we don\'t do this they\'ll end up expanded in the description.\n     */\n    $text = $modx->stripTags($text);\n    /* remove empty spaces */\n    $text= preg_replace(\'|\\s+|\', \' \', $text);\n    $words = preg_split (\"/\\s+/\", $text,$excerpt_length+1);\n    if (count($words) > $excerpt_length) {\n      array_pop($words);\n      array_push($words, \'...\');\n      $text = implode(\' \', $words);\n    }\n    return trim(stripslashes($text));\n  }\n}\nif (!empty($descriptionTV)) {\n  // if the $descriptionTV is not empty then we just print it with\n  // no extra processing, it\'s up to you to manage your description\n  echo $descriptionTV;\n} else {\n  // for everyone else that is lazy, just grab it from the content, yeah!\n  $content = $modx->getDocument($id,\'content\');\n  $content = $content[\'content\'];\n  echo getDynaDescription($content,$maxWordCount);\n}\n?>\n', 0, '', ''),
(21, 'sitemap', '<strong>1.1</strong> Outputs a machine readable site map for search engines and robots.', 0, 15, 0, 'return require MODX_BASE_PATH.\'assets/snippets/sitemap/snippet.sitemap.php\';', 0, '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `modx_site_templates`
--

CREATE TABLE `modx_site_templates` (
  `id` int(10) NOT NULL,
  `templatename` varchar(100) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT 'Template',
  `editor_type` int(11) NOT NULL DEFAULT '0' COMMENT '0-plain text,1-rich text,2-code editor',
  `category` int(11) NOT NULL DEFAULT '0' COMMENT 'category id',
  `icon` varchar(255) NOT NULL DEFAULT '' COMMENT 'url to icon file',
  `template_type` int(11) NOT NULL DEFAULT '0' COMMENT '0-page,1-content',
  `content` mediumtext,
  `locked` tinyint(4) NOT NULL DEFAULT '0',
  `selectable` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Contains the site templates.';

--
-- Дамп данных таблицы `modx_site_templates`
--

INSERT INTO `modx_site_templates` (`id`, `templatename`, `description`, `editor_type`, `category`, `icon`, `template_type`, `content`, `locked`, `selectable`) VALUES
(3, 'Minimal Template', 'Default minimal empty template (content returned only)', 0, 0, '', 0, '[*content*]', 0, 1),
(7, 'custom_page', '', 0, 9, '', 0, '<!DOCTYPE html>\r\n<html>\r\n	{{HEAD}}\r\n\r\n	<body>\r\n		{{HEADER}}\r\n		<section class=\"wrapper-center\">\r\n			<div id=\"left-sidebar\">\r\n				{{tpl_menu}}\r\n				{{tpl_latest_news_row}}\r\n			</div>\r\n			<hr>\r\n			<!-- *********  Content  ********** -->\r\n\r\n			<div id=\"content\">\r\n\r\n				<!-- ***** Out team + portraits ***** -->\r\n\r\n				<h3>[*longtitle*]</h3>\r\n				\r\n				<div class=\"text_container\">\r\n					[*content*]\r\n				</div>\r\n\r\n				<hr class=\"cleanit\">\r\n\r\n				<!-- ***** Services ***** -->\r\n			</div>\r\n			<hr>\r\n\r\n		</section>\r\n		<!-- *********  Footer  ********** -->\r\n\r\n		<hr class=\"cleanit\">\r\n\r\n		{{FOOTER}}\r\n	</body>\r\n</html>', 0, 1),
(5, 'Home', '', 0, 9, '', 0, '<!DOCTYPE html5>\r\n\r\n<html>\r\n	{{HEAD}}\r\n\r\n	<body>\r\n		{{HEADER}}\r\n		<!-- *********  Content  ********** -->\r\n		<section class=\"wrapper-center\">\r\n			<div id=\"left-sidebar\">\r\n				{{tpl_menu}}\r\n				{{tpl_latest_news_row}}\r\n			</div>\r\n			<hr>\r\n			<div id=\"content\">\r\n				<h3>О сайте</h3>\r\n				<div class=\"text_container\">\r\n					<p>[*content*]</p>\r\n				</div>\r\n			</div>\r\n			<hr>\r\n			\r\n		</section>\r\n\r\n		<!-- *********  Footer  ********** -->\r\n\r\n		<hr class=\"cleanit\">\r\n		{{FOOTER}}\r\n\r\n	</body>\r\n</html>', 0, 1),
(6, 'Contacts', '', 0, 9, '', 0, '<!DOCTYPE html5>\r\n<html>\r\n	{{HEAD}}\r\n	<body>\r\n		{{HEADER}}\r\n\r\n		<section class=\"wrapper-center\">\r\n			<div id=\"left-sidebar\">\r\n				{{tpl_menu}}\r\n				{{tpl_latest_news_row}}\r\n			</div>\r\n			<hr>\r\n			<!-- *********  Content  ********** -->\r\n\r\n			<div id=\"content\">\r\n\r\n				<!-- *** contact form *** -->\r\n\r\n				<h3>Задайте мне вопрос</h3>\r\n\r\n				<form action=\"#\" method=\"post\" class=\"formit\">\r\n					<input type=\"text\" name=\"name\" placeholder=\"ВАШЕ ИМЯ\"/>\r\n					<input type=\"text\" name=\"email\" placeholder=\"EMAIL\"/>\r\n					<textarea name=\"message\" placeholder=\"ТЕКСТ ВОПРОСА...\"></textarea>\r\n					<input type=\"submit\" class=\"button_submit\" value=\"Отправить\">\r\n				</form>\r\n\r\n				<div class=\"cara\"></div>\r\n\r\n				<h3>Контактная информация</h3>\r\n\r\n				<div class=\"list-group\">\r\n					<a href=\"https://goo.gl/maps/ssWzBpFbsKJ2\" class=\"list-group-item\"><span class=\"glyphicon glyphicon-map-marker\"></span>Украина, г. Харьков. Ул. Чайковского 17</a>\r\n					<a href=\"mailto:fregat222@gmail.co\" class=\"list-group-item\"><span class=\"glyphicon glyphicon-envelope\"></span>fregat222@gmail.com</a>\r\n					<a href=\"callto:+38(066)760-64-81\" class=\"list-group-item\"><span class=\"glyphicon glyphicon-earphone\"></span>+38(066)760-64-81</a>\r\n					<a href=\"callto:+38(096)662-92-08\" class=\"list-group-item\"><span class=\"glyphicon glyphicon-phone-alt\"></span>+38(096)662-92-08</a>\r\n					<a href=\"skype:fregatim1\" class=\"list-group-item\"><span class=\"fa fa-skype\"></span>fregatim1</a>\r\n				</div>\r\n				<hr class=\"cleanit\">\r\n\r\n\r\n				<div class=\"mapit\">\r\n					<iframe width=\"100%\" height=\"360\" frameborder=\"0\" style=\"border:0\" src=\"https://www.google.com/maps/embed/v1/place?q=%D0%A5%D0%B0%D1%80%D1%8C%D0%BA%D0%BE%D0%B2%20%D0%A7%D0%B9%D0%BA%D0%BE%D0%B2%D1%81%D0%BA%D0%BE%D0%B3%D0%BE%2017&key=AIzaSyBDSnHb6DS9s_YZLmWXVEm-5VG8A51xBrM\" allowfullscreen></iframe>\r\n					\r\n				</div>\r\n			</div>\r\n			<hr>\r\n\r\n		</section>\r\n		<!-- *********  Footer  ********** -->\r\n\r\n		<hr class=\"cleanit\">\r\n		{{FOOTER}}\r\n	</body>\r\n</html>', 0, 1),
(8, 'Новости', '', 0, 0, '', 0, '<!DOCTYPE html5>\r\n\r\n<html>\r\n	{{HEAD}}\r\n\r\n	<body>\r\n		{{HEADER}}\r\n		<!-- *********  Content  ********** -->\r\n		<section class=\"wrapper-center\">\r\n			<div id=\"left-sidebar\">\r\n				{{tpl_menu}}\r\n				{{tpl_latest_news_row}}\r\n			</div>\r\n			<hr>\r\n			<div id=\"content\">\r\n				<h3>Новости</h3>\r\n				<div class=\"list-group news\"> \r\n					[[S?\r\n					&get=`news`\r\n					]]\r\n				</div>\r\n			</div>\r\n			<hr>\r\n			\r\n		</section>\r\n\r\n		<!-- *********  Footer  ********** -->\r\n\r\n		<hr class=\"cleanit\">\r\n		{{FOOTER}}\r\n\r\n	</body>\r\n</html>', 0, 1),
(9, 'one_news', '', 0, 0, '', 0, '<!DOCTYPE html>\r\n<html>\r\n	{{HEAD}}\r\n\r\n	<body>\r\n		{{HEADER}}\r\n		<section class=\"wrapper-center\">\r\n			<div id=\"left-sidebar\">\r\n				{{tpl_menu}}\r\n				{{tpl_latest_news_row}}\r\n			</div>\r\n			<hr>\r\n			<!-- *********  Content  ********** -->\r\n\r\n			<div id=\"content\">\r\n\r\n				<!-- ***** Out team + portraits ***** -->\r\n\r\n				<h3>[*longtitle*]</h3>\r\n				\r\n				<div class=\"text_container\">\r\n					<div class=\"row\">\r\n						<div class=\"col-md-6\">\r\n							[[S ?\r\n							&get=`one_news`\r\n							]]\r\n						</div>\r\n					</div>\r\n				</div>\r\n\r\n				<hr class=\"cleanit\">\r\n\r\n				<!-- ***** Services ***** -->\r\n			</div>\r\n			<hr>\r\n\r\n		</section>\r\n		<!-- *********  Footer  ********** -->\r\n\r\n		<hr class=\"cleanit\">\r\n\r\n		{{FOOTER}}\r\n	</body>\r\n</html>', 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `modx_site_tmplvars`
--

CREATE TABLE `modx_site_tmplvars` (
  `id` int(11) NOT NULL,
  `type` varchar(50) NOT NULL DEFAULT '',
  `name` varchar(50) NOT NULL DEFAULT '',
  `caption` varchar(80) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `editor_type` int(11) NOT NULL DEFAULT '0' COMMENT '0-plain text,1-rich text,2-code editor',
  `category` int(11) NOT NULL DEFAULT '0' COMMENT 'category id',
  `locked` tinyint(4) NOT NULL DEFAULT '0',
  `elements` text,
  `rank` int(11) NOT NULL DEFAULT '0',
  `display` varchar(20) NOT NULL DEFAULT '' COMMENT 'Display Control',
  `display_params` text COMMENT 'Display Control Properties',
  `default_text` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Site Template Variables';

--
-- Дамп данных таблицы `modx_site_tmplvars`
--

INSERT INTO `modx_site_tmplvars` (`id`, `type`, `name`, `caption`, `description`, `editor_type`, `category`, `locked`, `elements`, `rank`, `display`, `display_params`, `default_text`) VALUES
(1, 'richtext', 'blogContent', 'blogContent', 'RTE for the new blog entries', 0, 1, 0, '', 0, 'RichText', '&w=383px&h=450px&edt=TinyMCE', ''),
(2, 'text', 'documentTags', 'Tags', 'Space delimited tags for the current document', 0, 1, 0, '', 0, '', '', ''),
(3, 'text', 'loginName', 'loginName', 'Conditional name for the Login menu item', 0, 1, 0, '', 0, '', '', '@EVAL if ($modx->getLoginUserID()) return \'Logout\'; else return \'Login\';'),
(4, 'dropdown', 'sitemap_exclude', 'Sitemap exclude', 'Include or exclude this page in Google SiteMap', 0, 14, 0, 'Include==0||Exclude==1', 0, '', '', '0'),
(5, 'dropdown', 'sitemap_changefreq', 'Sitemap update frequency', 'How often the content will be changed', 0, 14, 0, 'Always==always||Hourly==hourly||Daily==daily||Weekly==weekly||Monthly==monthly||Yearly==yearly||Never==never', 0, '', '', 'monthly'),
(6, 'dropdown', 'RobotsIndex', 'Robots - Index this Page', 'Include or exclude this page in Search Engines', 0, 14, 0, 'Yes==index||No==noindex', 0, '', '', 'index'),
(7, 'text', 'CustomTitle', 'CustomTitle', 'Seo Custom Title for metatag title - Default value is pagetitle', 0, 14, 0, '', 0, '', '', ''),
(8, 'text', 'CanonicalUrl', 'CanonicalUrl', 'Custom CanonicalUrl', 0, 14, 0, '', 0, '', '', ''),
(9, 'textareamini', 'Aliases', '301 Redirects', 'Add old url', 0, 14, 0, '', 0, '', '', ''),
(10, 'dropdown', 'sitemap_priority', 'Sitemap Priority', 'Importance of this page', 0, 14, 0, '1.0==1.0||0.7==0.7||0.5==0.5||0.3==0.3||0.0==0.0', 0, '', '', '0.5'),
(11, 'textarea', 'MetaDescription', 'Meta Description', 'If Empty get the first 25 words from content', 0, 14, 0, '', 0, '', '', ''),
(12, 'dropdown', 'RobotsFollow', 'Robots - Following Links', 'Follow links on this page', 0, 14, 0, 'Yes==follow||No==nofollow', 0, '', '', 'follow'),
(13, 'text', 'MetaKeywords', 'Meta Keywords', 'Meta Keywords - comma separated keywords', 0, 14, 0, '', 0, 'delim', '&format=,', ''),
(14, 'text', 'GoogleAuthor', 'Author', 'Google plus author page URL', 0, 14, 0, '', 0, '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `modx_site_tmplvar_access`
--

CREATE TABLE `modx_site_tmplvar_access` (
  `id` int(10) NOT NULL,
  `tmplvarid` int(10) NOT NULL DEFAULT '0',
  `documentgroup` int(10) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Contains data used for template variable access permissions.';

-- --------------------------------------------------------

--
-- Структура таблицы `modx_site_tmplvar_contentvalues`
--

CREATE TABLE `modx_site_tmplvar_contentvalues` (
  `id` int(11) NOT NULL,
  `tmplvarid` int(10) NOT NULL DEFAULT '0' COMMENT 'Template Variable id',
  `contentid` int(10) NOT NULL DEFAULT '0' COMMENT 'Site Content Id',
  `value` mediumtext
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Site Template Variables Content Values Link Table';

-- --------------------------------------------------------

--
-- Структура таблицы `modx_site_tmplvar_templates`
--

CREATE TABLE `modx_site_tmplvar_templates` (
  `tmplvarid` int(10) NOT NULL DEFAULT '0' COMMENT 'Template Variable id',
  `templateid` int(11) NOT NULL DEFAULT '0',
  `rank` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Site Template Variables Templates Link Table';

--
-- Дамп данных таблицы `modx_site_tmplvar_templates`
--

INSERT INTO `modx_site_tmplvar_templates` (`tmplvarid`, `templateid`, `rank`) VALUES
(4, 3, 0),
(5, 3, 0),
(6, 3, 0),
(7, 3, 0),
(8, 3, 0),
(9, 3, 0),
(10, 3, 0),
(11, 3, 0),
(12, 3, 0),
(13, 3, 0),
(14, 3, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `modx_system_eventnames`
--

CREATE TABLE `modx_system_eventnames` (
  `id` int(10) NOT NULL,
  `name` varchar(50) NOT NULL DEFAULT '',
  `service` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'System Service number',
  `groupname` varchar(20) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='System Event Names.';

--
-- Дамп данных таблицы `modx_system_eventnames`
--

INSERT INTO `modx_system_eventnames` (`id`, `name`, `service`, `groupname`) VALUES
(1, 'OnDocPublished', 5, ''),
(2, 'OnDocUnPublished', 5, ''),
(3, 'OnWebPagePrerender', 5, ''),
(4, 'OnWebLogin', 3, ''),
(5, 'OnBeforeWebLogout', 3, ''),
(6, 'OnWebLogout', 3, ''),
(7, 'OnWebSaveUser', 3, ''),
(8, 'OnWebDeleteUser', 3, ''),
(9, 'OnWebChangePassword', 3, ''),
(10, 'OnWebCreateGroup', 3, ''),
(11, 'OnManagerLogin', 2, ''),
(12, 'OnBeforeManagerLogout', 2, ''),
(13, 'OnManagerLogout', 2, ''),
(14, 'OnManagerSaveUser', 2, ''),
(15, 'OnManagerDeleteUser', 2, ''),
(16, 'OnManagerChangePassword', 2, ''),
(17, 'OnManagerCreateGroup', 2, ''),
(18, 'OnBeforeCacheUpdate', 4, ''),
(19, 'OnCacheUpdate', 4, ''),
(107, 'OnMakePageCacheKey', 4, ''),
(20, 'OnLoadWebPageCache', 4, ''),
(21, 'OnBeforeSaveWebPageCache', 4, ''),
(22, 'OnChunkFormPrerender', 1, 'Chunks'),
(23, 'OnChunkFormRender', 1, 'Chunks'),
(24, 'OnBeforeChunkFormSave', 1, 'Chunks'),
(25, 'OnChunkFormSave', 1, 'Chunks'),
(26, 'OnBeforeChunkFormDelete', 1, 'Chunks'),
(27, 'OnChunkFormDelete', 1, 'Chunks'),
(28, 'OnDocFormPrerender', 1, 'Documents'),
(29, 'OnDocFormRender', 1, 'Documents'),
(30, 'OnBeforeDocFormSave', 1, 'Documents'),
(31, 'OnDocFormSave', 1, 'Documents'),
(32, 'OnBeforeDocFormDelete', 1, 'Documents'),
(33, 'OnDocFormDelete', 1, 'Documents'),
(1033, 'OnDocFormUnDelete', 1, 'Documents'),
(1034, 'onBeforeMoveDocument', 1, 'Documents'),
(1035, 'onAfterMoveDocument', 1, 'Documents'),
(34, 'OnPluginFormPrerender', 1, 'Plugins'),
(35, 'OnPluginFormRender', 1, 'Plugins'),
(36, 'OnBeforePluginFormSave', 1, 'Plugins'),
(37, 'OnPluginFormSave', 1, 'Plugins'),
(38, 'OnBeforePluginFormDelete', 1, 'Plugins'),
(39, 'OnPluginFormDelete', 1, 'Plugins'),
(40, 'OnSnipFormPrerender', 1, 'Snippets'),
(41, 'OnSnipFormRender', 1, 'Snippets'),
(42, 'OnBeforeSnipFormSave', 1, 'Snippets'),
(43, 'OnSnipFormSave', 1, 'Snippets'),
(44, 'OnBeforeSnipFormDelete', 1, 'Snippets'),
(45, 'OnSnipFormDelete', 1, 'Snippets'),
(46, 'OnTempFormPrerender', 1, 'Templates'),
(47, 'OnTempFormRender', 1, 'Templates'),
(48, 'OnBeforeTempFormSave', 1, 'Templates'),
(49, 'OnTempFormSave', 1, 'Templates'),
(50, 'OnBeforeTempFormDelete', 1, 'Templates'),
(51, 'OnTempFormDelete', 1, 'Templates'),
(52, 'OnTVFormPrerender', 1, 'Template Variables'),
(53, 'OnTVFormRender', 1, 'Template Variables'),
(54, 'OnBeforeTVFormSave', 1, 'Template Variables'),
(55, 'OnTVFormSave', 1, 'Template Variables'),
(56, 'OnBeforeTVFormDelete', 1, 'Template Variables'),
(57, 'OnTVFormDelete', 1, 'Template Variables'),
(58, 'OnUserFormPrerender', 1, 'Users'),
(59, 'OnUserFormRender', 1, 'Users'),
(60, 'OnBeforeUserFormSave', 1, 'Users'),
(61, 'OnUserFormSave', 1, 'Users'),
(62, 'OnBeforeUserFormDelete', 1, 'Users'),
(63, 'OnUserFormDelete', 1, 'Users'),
(64, 'OnWUsrFormPrerender', 1, 'Web Users'),
(65, 'OnWUsrFormRender', 1, 'Web Users'),
(66, 'OnBeforeWUsrFormSave', 1, 'Web Users'),
(67, 'OnWUsrFormSave', 1, 'Web Users'),
(68, 'OnBeforeWUsrFormDelete', 1, 'Web Users'),
(69, 'OnWUsrFormDelete', 1, 'Web Users'),
(70, 'OnSiteRefresh', 1, ''),
(71, 'OnFileManagerUpload', 1, ''),
(72, 'OnModFormPrerender', 1, 'Modules'),
(73, 'OnModFormRender', 1, 'Modules'),
(74, 'OnBeforeModFormDelete', 1, 'Modules'),
(75, 'OnModFormDelete', 1, 'Modules'),
(76, 'OnBeforeModFormSave', 1, 'Modules'),
(77, 'OnModFormSave', 1, 'Modules'),
(78, 'OnBeforeWebLogin', 3, ''),
(79, 'OnWebAuthentication', 3, ''),
(80, 'OnBeforeManagerLogin', 2, ''),
(81, 'OnManagerAuthentication', 2, ''),
(82, 'OnSiteSettingsRender', 1, 'System Settings'),
(83, 'OnFriendlyURLSettingsRender', 1, 'System Settings'),
(84, 'OnUserSettingsRender', 1, 'System Settings'),
(85, 'OnInterfaceSettingsRender', 1, 'System Settings'),
(86, 'OnMiscSettingsRender', 1, 'System Settings'),
(87, 'OnRichTextEditorRegister', 1, 'RichText Editor'),
(88, 'OnRichTextEditorInit', 1, 'RichText Editor'),
(89, 'OnManagerPageInit', 2, ''),
(90, 'OnWebPageInit', 5, ''),
(101, 'OnLoadDocumentObject', 5, ''),
(104, 'OnBeforeLoadDocumentObject', 5, ''),
(105, 'OnAfterLoadDocumentObject', 5, ''),
(91, 'OnLoadWebDocument', 5, ''),
(92, 'OnParseDocument', 5, ''),
(106, 'OnParseProperties', 5, ''),
(108, 'OnBeforeParseParams', 5, ''),
(93, 'OnManagerLoginFormRender', 2, ''),
(94, 'OnWebPageComplete', 5, ''),
(95, 'OnLogPageHit', 5, ''),
(96, 'OnBeforeManagerPageInit', 2, ''),
(97, 'OnBeforeEmptyTrash', 1, 'Documents'),
(98, 'OnEmptyTrash', 1, 'Documents'),
(99, 'OnManagerLoginFormPrerender', 2, ''),
(100, 'OnStripAlias', 1, 'Documents'),
(102, 'OnMakeDocUrl', 5, ''),
(103, 'OnBeforeLoadExtension', 5, ''),
(200, 'OnCreateDocGroup', 1, 'Documents'),
(201, 'OnManagerWelcomePrerender', 2, ''),
(202, 'OnManagerWelcomeHome', 2, ''),
(203, 'OnManagerWelcomeRender', 2, ''),
(204, 'OnBeforeDocDuplicate', 1, 'Documents'),
(205, 'OnDocDuplicate', 1, 'Documents'),
(206, 'OnManagerMainFrameHeaderHTMLBlock', 2, ''),
(207, 'OnManagerPreFrameLoader', 2, ''),
(208, 'OnManagerFrameLoader', 2, ''),
(209, 'OnManagerTreeInit', 2, ''),
(210, 'OnManagerTreePrerender', 2, ''),
(211, 'OnManagerTreeRender', 2, ''),
(212, 'OnManagerNodePrerender', 2, ''),
(213, 'OnManagerNodeRender', 2, ''),
(214, 'OnManagerMenuPrerender', 2, ''),
(215, 'OnManagerTopPrerender', 2, ''),
(224, 'OnDocFormTemplateRender', 1, 'Documents'),
(999, 'OnPageUnauthorized', 1, ''),
(1000, 'OnPageNotFound', 1, ''),
(1001, 'OnFileBrowserUpload', 1, 'File Browser Events');

-- --------------------------------------------------------

--
-- Структура таблицы `modx_system_settings`
--

CREATE TABLE `modx_system_settings` (
  `setting_name` varchar(50) NOT NULL DEFAULT '',
  `setting_value` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Contains Content Manager settings.';

--
-- Дамп данных таблицы `modx_system_settings`
--

INSERT INTO `modx_system_settings` (`setting_name`, `setting_value`) VALUES
('settings_version', '1.2.1'),
('manager_theme', 'MODxRE2'),
('server_offset_time', '0'),
('manager_language', 'russian-UTF8'),
('modx_charset', 'UTF-8'),
('site_name', 'My MODX Site'),
('site_start', '2'),
('error_page', '2'),
('unauthorized_page', '2'),
('site_status', '1'),
('auto_template_logic', 'parent'),
('default_template', '5'),
('old_template', '5'),
('cache_type', '1'),
('use_udperms', '1'),
('udperms_allowroot', '0'),
('failed_login_attempts', '3'),
('blocked_minutes', '60'),
('use_captcha', '0'),
('emailsender', 'admin@mail.ru'),
('use_editor', '1'),
('use_browser', '1'),
('fe_editor_lang', 'russian-UTF8'),
('session.cookie.lifetime', '604800'),
('theme_refresher', ''),
('site_id', '5ad21912cc194'),
('site_unavailable_page', ''),
('reload_site_unavailable', ''),
('site_unavailable_message', 'В настоящее время сайт недоступен.'),
('siteunavailable_message_default', 'В настоящее время сайт недоступен.'),
('enable_filter', '0'),
('publish_default', '0'),
('cache_default', '1'),
('search_default', '1'),
('auto_menuindex', '1'),
('custom_contenttype', 'application/rss+xml,application/pdf,application/vnd.ms-word,application/vnd.ms-excel,text/html,text/css,text/xml,text/javascript,text/plain,application/json'),
('docid_incrmnt_method', '0'),
('minifyphp_incache', '0'),
('server_protocol', 'http'),
('rss_url_news', 'http://feeds.feedburner.com/modx-announce'),
('track_visitors', '0'),
('top_howmany', '10'),
('friendly_urls', '1'),
('xhtml_urls', '1'),
('friendly_url_prefix', ''),
('friendly_url_suffix', ''),
('make_folders', '0'),
('seostrict', '0'),
('aliaslistingfolder', '0'),
('friendly_alias_urls', '1'),
('use_alias_path', '1'),
('allow_duplicate_alias', '0'),
('automatic_alias', '1'),
('email_method', 'mail'),
('smtp_auth', '0'),
('smtp_secure', 'none'),
('smtp_host', 'smtp.example.com'),
('smtp_port', '25'),
('smtp_username', 'you@example.com'),
('reload_emailsubject', ''),
('emailsubject', 'Данные для авторизации'),
('emailsubject_default', 'Данные для авторизации'),
('reload_signupemail_message', ''),
('signupemail_message', 'Здравствуйте, [+uid+]!\r\n\r\nВаши данные для авторизации в системе управления сайтом [+sname+]:\r\n\r\nИмя пользователя: [+uid+]\r\nПароль: [+pwd+]\r\n\r\nПосле успешной авторизации в системе управления сайтом ([+surl+]), вы сможете изменить свой пароль.\r\n\r\nС уважением, Администрация'),
('system_email_signup_default', 'Здравствуйте, [+uid+]!\r\n\r\nВаши данные для авторизации в системе управления сайтом [+sname+]:\r\n\r\nИмя пользователя: [+uid+]\r\nПароль: [+pwd+]\r\n\r\nПосле успешной авторизации в системе управления сайтом ([+surl+]), вы сможете изменить свой пароль.\r\n\r\nС уважением, Администрация'),
('reload_websignupemail_message', ''),
('websignupemail_message', 'Здравствуйте, [+uid+]!\r\n\r\nВаши данные для авторизации на [+sname+]:\r\n\r\nИмя пользователя: [+uid+]\r\nПароль: [+pwd+]\r\n\r\nПосле успешной авторизации на [+sname+] ([+surl+]), вы сможете изменить свой пароль.\r\n\r\nС уважением, Администрация'),
('system_email_websignup_default', 'Здравствуйте, [+uid+]!\r\n\r\nВаши данные для авторизации на [+sname+]:\r\n\r\nИмя пользователя: [+uid+]\r\nПароль: [+pwd+]\r\n\r\nПосле успешной авторизации на [+sname+] ([+surl+]), вы сможете изменить свой пароль.\r\n\r\nС уважением, Администрация'),
('reload_system_email_webreminder_message', ''),
('webpwdreminder_message', 'Здравствуйте, [+uid+]!\r\n\r\nЧтобы активировать ваш новый пароль, перейдите по следующей ссылке:\r\n\r\n[+surl+]\r\n\r\nПозже вы сможете использовать следующий пароль для авторизации: [+pwd+]\r\n\r\nЕсли это письмо пришло к вам по ошибке, пожалуйста, проигнорируйте его.\r\n\r\nС уважением, Администрация'),
('system_email_webreminder_default', 'Здравствуйте, [+uid+]!\r\n\r\nЧтобы активировать ваш новый пароль, перейдите по следующей ссылке:\r\n\r\n[+surl+]\r\n\r\nПозже вы сможете использовать следующий пароль для авторизации: [+pwd+]\r\n\r\nЕсли это письмо пришло к вам по ошибке, пожалуйста, проигнорируйте его.\r\n\r\nС уважением, Администрация'),
('warning_visibility', '1'),
('tree_page_click', '27'),
('use_breadcrumbs', '0'),
('remember_last_tab', '0'),
('resource_tree_node_name', 'pagetitle'),
('session_timeout', '15'),
('tree_show_protected', '0'),
('show_meta', '0'),
('datepicker_offset', '-10'),
('datetime_format', 'dd-mm-YYYY'),
('number_of_logs', '100'),
('mail_check_timeperiod', '60'),
('number_of_messages', '40'),
('number_of_results', '30'),
('which_editor', 'TinyMCE4'),
('editor_css_path', ''),
('tinymce4_theme', 'full'),
('tinymce4_skin', 'lightgray'),
('tinymce4_template_docs', ''),
('tinymce4_template_chunks', ''),
('tinymce4_entermode', 'p'),
('tinymce4_element_format', 'xhtml'),
('tinymce4_schema', 'html5'),
('tinymce4_custom_plugins', 'advlist autolink lists link image charmap print preview hr anchor pagebreak searchreplace wordcount visualblocks visualchars code fullscreen spellchecker insertdatetime media nonbreaking save table contextmenu directionality emoticons template paste textcolor codesample colorpicker textpattern imagetools paste modxlink youtube'),
('tinymce4_custom_buttons1', 'undo redo | cut copy paste | searchreplace | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent blockquote | styleselect'),
('tinymce4_custom_buttons2', 'link unlink anchor image media codesample table | hr removeformat | subscript superscript charmap | nonbreaking | visualchars visualblocks print preview fullscreen code'),
('tinymce4_custom_buttons3', ''),
('tinymce4_custom_buttons4', ''),
('tinymce4_blockFormats', 'Paragraph=p;Header 1=h1;Header 2=h2;Header 3=h3'),
('allow_eval', 'with_scan'),
('safe_functions_at_eval', 'time,date,strtotime,strftime'),
('check_files_onlogin', 'index.php\r\n.htaccess\r\nmanager/index.php\r\nmanager/includes/config.inc.php'),
('validate_referer', '1'),
('rss_url_security', 'http://feeds.feedburner.com/modxsecurity'),
('error_reporting', '1'),
('send_errormail', '0'),
('pwd_hash_algo', 'UNCRYPT'),
('enable_bindings', '1'),
('reload_captcha_words', ''),
('captcha_words', 'MODX,Access,Better,BitCode,Chunk,Cache,Desc,Design,Excell,Enjoy,URLs,TechView,Gerald,Griff,Humphrey,Holiday,Intel,Integration,Joystick,Join(),Oscope,Genetic,Light,Likeness,Marit,Maaike,Niche,Netherlands,Ordinance,Oscillo,Parser,Phusion,Query,Question,Regalia,Righteous,Snippet,Sentinel,Template,Thespian,Unity,Enterprise,Verily,Tattoo,Veri,Website,WideWeb,Yap,Yellow,Zebra,Zygote'),
('captcha_words_default', 'MODX,Access,Better,BitCode,Chunk,Cache,Desc,Design,Excell,Enjoy,URLs,TechView,Gerald,Griff,Humphrey,Holiday,Intel,Integration,Joystick,Join(),Oscope,Genetic,Light,Likeness,Marit,Maaike,Niche,Netherlands,Ordinance,Oscillo,Parser,Phusion,Query,Question,Regalia,Righteous,Snippet,Sentinel,Template,Thespian,Unity,Enterprise,Verily,Tattoo,Veri,Website,WideWeb,Yap,Yellow,Zebra,Zygote'),
('filemanager_path', 'D:/OSPanel/domains/solistor.local/'),
('upload_files', 'bmp,ico,gif,jpeg,jpg,png,psd,tif,tiff,fla,flv,swf,aac,au,avi,css,cache,doc,docx,gz,gzip,htaccess,htm,html,js,mp3,mp4,mpeg,mpg,ods,odp,odt,pdf,ppt,pptx,rar,tar,tgz,txt,wav,wmv,xls,xlsx,xml,z,zip,JPG,JPEG,PNG,GIF,svg'),
('upload_images', 'bmp,ico,gif,jpeg,jpg,png,psd,tif,tiff,svg'),
('upload_media', 'au,avi,mp3,mp4,mpeg,mpg,wav,wmv'),
('upload_flash', 'fla,flv,swf'),
('upload_maxsize', '5000000'),
('new_file_permissions', '0644'),
('new_folder_permissions', '0755'),
('which_browser', 'mcpuk'),
('rb_webuser', '0'),
('rb_base_dir', 'D:/OSPanel/domains/solistor.local/assets/'),
('rb_base_url', 'assets/'),
('clean_uploaded_filename', '1'),
('strip_image_paths', '1'),
('maxImageWidth', '1600'),
('maxImageHeight', '1200'),
('thumbWidth', '150'),
('thumbHeight', '150'),
('thumbsDir', '.thumbs'),
('jpegQuality', '90'),
('denyZipDownload', '0'),
('denyExtensionRename', '0'),
('showHiddenFiles', '0'),
('lang_code', 'ru'),
('sys_files_checksum', 'a:4:{s:27:\"/var/www/solistor/index.php\";s:32:\"ed8dd02021b28b9227b44d5a76ef7440\";s:27:\"/var/www/solistor/.htaccess\";s:32:\"43c6abeaf47db72511f89ea4cb8eab4f\";s:35:\"/var/www/solistor/manager/index.php\";s:32:\"afb412c538f339b214dfa2218d0e1349\";s:49:\"/var/www/solistor/manager/includes/config.inc.php\";s:32:\"3614f89093697e0a55f14bd030c3d158\";}');

-- --------------------------------------------------------

--
-- Структура таблицы `modx_user_attributes`
--

CREATE TABLE `modx_user_attributes` (
  `id` int(10) NOT NULL,
  `internalKey` int(10) NOT NULL DEFAULT '0',
  `fullname` varchar(100) NOT NULL DEFAULT '',
  `role` int(10) NOT NULL DEFAULT '0',
  `email` varchar(100) NOT NULL DEFAULT '',
  `phone` varchar(100) NOT NULL DEFAULT '',
  `mobilephone` varchar(100) NOT NULL DEFAULT '',
  `blocked` int(1) NOT NULL DEFAULT '0',
  `blockeduntil` int(11) NOT NULL DEFAULT '0',
  `blockedafter` int(11) NOT NULL DEFAULT '0',
  `logincount` int(11) NOT NULL DEFAULT '0',
  `lastlogin` int(11) NOT NULL DEFAULT '0',
  `thislogin` int(11) NOT NULL DEFAULT '0',
  `failedlogincount` int(10) NOT NULL DEFAULT '0',
  `sessionid` varchar(100) NOT NULL DEFAULT '',
  `dob` int(10) NOT NULL DEFAULT '0',
  `gender` int(1) NOT NULL DEFAULT '0' COMMENT '0 - unknown, 1 - Male 2 - female',
  `country` varchar(5) NOT NULL DEFAULT '',
  `street` varchar(255) NOT NULL DEFAULT '',
  `city` varchar(255) NOT NULL DEFAULT '',
  `state` varchar(25) NOT NULL DEFAULT '',
  `zip` varchar(25) NOT NULL DEFAULT '',
  `fax` varchar(100) NOT NULL DEFAULT '',
  `photo` varchar(255) NOT NULL DEFAULT '' COMMENT 'link to photo',
  `comment` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Contains information about the backend users.';

--
-- Дамп данных таблицы `modx_user_attributes`
--

INSERT INTO `modx_user_attributes` (`id`, `internalKey`, `fullname`, `role`, `email`, `phone`, `mobilephone`, `blocked`, `blockeduntil`, `blockedafter`, `logincount`, `lastlogin`, `thislogin`, `failedlogincount`, `sessionid`, `dob`, `gender`, `country`, `street`, `city`, `state`, `zip`, `fax`, `photo`, `comment`) VALUES
(2, 2, 'Геллер Станислав Игоревич', 1, 'stasgeller@gmail.com', '', '', 0, 0, 0, 4, 1539028677, 1539182917, 0, 'ntejisk0oti3enkkbq0e775o65', 0, 0, '', '', '', '', '', '', '', ''),
(3, 3, 'Геллер Игорь Леонидович', 2, 'fregat222@gmail.com', '+380667606481', '', 0, 0, 0, 2, 1539028610, 1539028732, 0, '2gk94h1kpgvck2j62k7rdu57g2', 0, 0, '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `modx_user_messages`
--

CREATE TABLE `modx_user_messages` (
  `id` int(10) NOT NULL,
  `type` varchar(15) NOT NULL DEFAULT '',
  `subject` varchar(60) NOT NULL DEFAULT '',
  `message` text,
  `sender` int(10) NOT NULL DEFAULT '0',
  `recipient` int(10) NOT NULL DEFAULT '0',
  `private` tinyint(4) NOT NULL DEFAULT '0',
  `postdate` int(20) NOT NULL DEFAULT '0',
  `messageread` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Contains messages for the Content Manager messaging system.';

-- --------------------------------------------------------

--
-- Структура таблицы `modx_user_roles`
--

CREATE TABLE `modx_user_roles` (
  `id` int(10) NOT NULL,
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `frames` int(1) NOT NULL DEFAULT '0',
  `home` int(1) NOT NULL DEFAULT '0',
  `view_document` int(1) NOT NULL DEFAULT '0',
  `new_document` int(1) NOT NULL DEFAULT '0',
  `save_document` int(1) NOT NULL DEFAULT '0',
  `publish_document` int(1) NOT NULL DEFAULT '0',
  `delete_document` int(1) NOT NULL DEFAULT '0',
  `empty_trash` int(1) NOT NULL DEFAULT '0',
  `action_ok` int(1) NOT NULL DEFAULT '0',
  `logout` int(1) NOT NULL DEFAULT '0',
  `help` int(1) NOT NULL DEFAULT '0',
  `messages` int(1) NOT NULL DEFAULT '0',
  `new_user` int(1) NOT NULL DEFAULT '0',
  `edit_user` int(1) NOT NULL DEFAULT '0',
  `logs` int(1) NOT NULL DEFAULT '0',
  `edit_parser` int(1) NOT NULL DEFAULT '0',
  `save_parser` int(1) NOT NULL DEFAULT '0',
  `edit_template` int(1) NOT NULL DEFAULT '0',
  `settings` int(1) NOT NULL DEFAULT '0',
  `credits` int(1) NOT NULL DEFAULT '0',
  `new_template` int(1) NOT NULL DEFAULT '0',
  `save_template` int(1) NOT NULL DEFAULT '0',
  `delete_template` int(1) NOT NULL DEFAULT '0',
  `edit_snippet` int(1) NOT NULL DEFAULT '0',
  `new_snippet` int(1) NOT NULL DEFAULT '0',
  `save_snippet` int(1) NOT NULL DEFAULT '0',
  `delete_snippet` int(1) NOT NULL DEFAULT '0',
  `edit_chunk` int(1) NOT NULL DEFAULT '0',
  `new_chunk` int(1) NOT NULL DEFAULT '0',
  `save_chunk` int(1) NOT NULL DEFAULT '0',
  `delete_chunk` int(1) NOT NULL DEFAULT '0',
  `empty_cache` int(1) NOT NULL DEFAULT '0',
  `edit_document` int(1) NOT NULL DEFAULT '0',
  `change_password` int(1) NOT NULL DEFAULT '0',
  `error_dialog` int(1) NOT NULL DEFAULT '0',
  `about` int(1) NOT NULL DEFAULT '0',
  `file_manager` int(1) NOT NULL DEFAULT '0',
  `assets_files` int(1) NOT NULL DEFAULT '0',
  `assets_images` int(1) NOT NULL DEFAULT '0',
  `save_user` int(1) NOT NULL DEFAULT '0',
  `delete_user` int(1) NOT NULL DEFAULT '0',
  `save_password` int(11) NOT NULL DEFAULT '0',
  `edit_role` int(1) NOT NULL DEFAULT '0',
  `save_role` int(1) NOT NULL DEFAULT '0',
  `delete_role` int(1) NOT NULL DEFAULT '0',
  `new_role` int(1) NOT NULL DEFAULT '0',
  `access_permissions` int(1) NOT NULL DEFAULT '0',
  `bk_manager` int(1) NOT NULL DEFAULT '0',
  `new_plugin` int(1) NOT NULL DEFAULT '0',
  `edit_plugin` int(1) NOT NULL DEFAULT '0',
  `save_plugin` int(1) NOT NULL DEFAULT '0',
  `delete_plugin` int(1) NOT NULL DEFAULT '0',
  `new_module` int(1) NOT NULL DEFAULT '0',
  `edit_module` int(1) NOT NULL DEFAULT '0',
  `save_module` int(1) NOT NULL DEFAULT '0',
  `delete_module` int(1) NOT NULL DEFAULT '0',
  `exec_module` int(1) NOT NULL DEFAULT '0',
  `view_eventlog` int(1) NOT NULL DEFAULT '0',
  `delete_eventlog` int(1) NOT NULL DEFAULT '0',
  `manage_metatags` int(1) NOT NULL DEFAULT '0' COMMENT 'manage site meta tags and keywords',
  `edit_doc_metatags` int(1) NOT NULL DEFAULT '0' COMMENT 'edit document meta tags and keywords',
  `new_web_user` int(1) NOT NULL DEFAULT '0',
  `edit_web_user` int(1) NOT NULL DEFAULT '0',
  `save_web_user` int(1) NOT NULL DEFAULT '0',
  `delete_web_user` int(1) NOT NULL DEFAULT '0',
  `web_access_permissions` int(1) NOT NULL DEFAULT '0',
  `view_unpublished` int(1) NOT NULL DEFAULT '0',
  `import_static` int(1) NOT NULL DEFAULT '0',
  `export_static` int(1) NOT NULL DEFAULT '0',
  `remove_locks` int(1) NOT NULL DEFAULT '0',
  `display_locks` int(1) NOT NULL DEFAULT '0',
  `change_resourcetype` int(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Contains information describing the user roles.';

--
-- Дамп данных таблицы `modx_user_roles`
--

INSERT INTO `modx_user_roles` (`id`, `name`, `description`, `frames`, `home`, `view_document`, `new_document`, `save_document`, `publish_document`, `delete_document`, `empty_trash`, `action_ok`, `logout`, `help`, `messages`, `new_user`, `edit_user`, `logs`, `edit_parser`, `save_parser`, `edit_template`, `settings`, `credits`, `new_template`, `save_template`, `delete_template`, `edit_snippet`, `new_snippet`, `save_snippet`, `delete_snippet`, `edit_chunk`, `new_chunk`, `save_chunk`, `delete_chunk`, `empty_cache`, `edit_document`, `change_password`, `error_dialog`, `about`, `file_manager`, `assets_files`, `assets_images`, `save_user`, `delete_user`, `save_password`, `edit_role`, `save_role`, `delete_role`, `new_role`, `access_permissions`, `bk_manager`, `new_plugin`, `edit_plugin`, `save_plugin`, `delete_plugin`, `new_module`, `edit_module`, `save_module`, `delete_module`, `exec_module`, `view_eventlog`, `delete_eventlog`, `manage_metatags`, `edit_doc_metatags`, `new_web_user`, `edit_web_user`, `save_web_user`, `delete_web_user`, `web_access_permissions`, `view_unpublished`, `import_static`, `export_static`, `remove_locks`, `display_locks`, `change_resourcetype`) VALUES
(2, 'Editor', 'Limited to managing content', 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1),
(3, 'Publisher', 'Editor with expanded permissions including manage users, update Elements and site settings', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 1, 0, 0, 1, 1, 1),
(1, 'Administrator', 'Site administrators have full access to all functions', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `modx_user_settings`
--

CREATE TABLE `modx_user_settings` (
  `user` int(11) NOT NULL,
  `setting_name` varchar(50) NOT NULL DEFAULT '',
  `setting_value` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Contains backend user settings.';

--
-- Дамп данных таблицы `modx_user_settings`
--

INSERT INTO `modx_user_settings` (`user`, `setting_name`, `setting_value`) VALUES
(2, 'tinymce4_custom_buttons1', 'undo redo | cut copy paste | searchreplace | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent blockquote | styleselect'),
(2, 'tinymce4_custom_buttons2', 'link unlink anchor image media codesample table | hr removeformat | subscript superscript charmap | nonbreaking | visualchars visualblocks print preview fullscreen code'),
(2, 'allow_manager_access', '1'),
(2, 'which_browser', 'default'),
(2, 'tinymce4_custom_plugins', 'advlist autolink lists link image charmap print preview hr anchor pagebreak searchreplace wordcount visualblocks visualchars code fullscreen spellchecker insertdatetime media nonbreaking save table contextmenu directionality emoticons template paste textcolor codesample colorpicker textpattern imagetools paste modxlink youtube'),
(2, 'manager_language', 'russian-UTF8'),
(2, 'tinymce4_blockFormats', 'Paragraph=p;Header 1=h1;Header 2=h2;Header 3=h3'),
(3, 'tinymce4_blockFormats', 'Paragraph=p;Header 1=h1;Header 2=h2;Header 3=h3'),
(3, 'tinymce4_custom_buttons1', 'undo redo | cut copy paste | searchreplace | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent blockquote | styleselect'),
(3, 'tinymce4_custom_buttons2', 'link unlink anchor image media codesample table | hr removeformat | subscript superscript charmap | nonbreaking | visualchars visualblocks print preview fullscreen code'),
(3, 'allow_manager_access', '1'),
(3, 'which_browser', 'default'),
(3, 'tinymce4_custom_plugins', 'advlist autolink lists link image charmap print preview hr anchor pagebreak searchreplace wordcount visualblocks visualchars code fullscreen spellchecker insertdatetime media nonbreaking save table contextmenu directionality emoticons template paste textcolor codesample colorpicker textpattern imagetools paste modxlink youtube'),
(3, 'manager_language', 'russian-UTF8');

-- --------------------------------------------------------

--
-- Структура таблицы `modx_webgroup_access`
--

CREATE TABLE `modx_webgroup_access` (
  `id` int(10) NOT NULL,
  `webgroup` int(10) NOT NULL DEFAULT '0',
  `documentgroup` int(10) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Contains data used for web access permissions.';

-- --------------------------------------------------------

--
-- Структура таблицы `modx_webgroup_names`
--

CREATE TABLE `modx_webgroup_names` (
  `id` int(10) NOT NULL,
  `name` varchar(245) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Contains data used for web access permissions.';

-- --------------------------------------------------------

--
-- Структура таблицы `modx_web_groups`
--

CREATE TABLE `modx_web_groups` (
  `id` int(10) NOT NULL,
  `webgroup` int(10) NOT NULL DEFAULT '0',
  `webuser` int(10) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Contains data used for web access permissions.';

-- --------------------------------------------------------

--
-- Структура таблицы `modx_web_users`
--

CREATE TABLE `modx_web_users` (
  `id` int(10) NOT NULL,
  `username` varchar(100) NOT NULL DEFAULT '',
  `password` varchar(100) NOT NULL DEFAULT '',
  `cachepwd` varchar(100) NOT NULL DEFAULT '' COMMENT 'Store new unconfirmed password'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `modx_web_user_attributes`
--

CREATE TABLE `modx_web_user_attributes` (
  `id` int(10) NOT NULL,
  `internalKey` int(10) NOT NULL DEFAULT '0',
  `fullname` varchar(100) NOT NULL DEFAULT '',
  `role` int(10) NOT NULL DEFAULT '0',
  `email` varchar(100) NOT NULL DEFAULT '',
  `phone` varchar(100) NOT NULL DEFAULT '',
  `mobilephone` varchar(100) NOT NULL DEFAULT '',
  `blocked` int(1) NOT NULL DEFAULT '0',
  `blockeduntil` int(11) NOT NULL DEFAULT '0',
  `blockedafter` int(11) NOT NULL DEFAULT '0',
  `logincount` int(11) NOT NULL DEFAULT '0',
  `lastlogin` int(11) NOT NULL DEFAULT '0',
  `thislogin` int(11) NOT NULL DEFAULT '0',
  `failedlogincount` int(10) NOT NULL DEFAULT '0',
  `sessionid` varchar(100) NOT NULL DEFAULT '',
  `dob` int(10) NOT NULL DEFAULT '0',
  `gender` int(1) NOT NULL DEFAULT '0' COMMENT '0 - unknown, 1 - Male 2 - female',
  `country` varchar(25) NOT NULL DEFAULT '',
  `street` varchar(255) NOT NULL DEFAULT '',
  `city` varchar(255) NOT NULL DEFAULT '',
  `state` varchar(25) NOT NULL DEFAULT '',
  `zip` varchar(25) NOT NULL DEFAULT '',
  `fax` varchar(100) NOT NULL DEFAULT '',
  `photo` varchar(255) NOT NULL DEFAULT '' COMMENT 'link to photo',
  `comment` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Contains information for web users.';

-- --------------------------------------------------------

--
-- Структура таблицы `modx_web_user_settings`
--

CREATE TABLE `modx_web_user_settings` (
  `webuser` int(11) NOT NULL,
  `setting_name` varchar(50) NOT NULL DEFAULT '',
  `setting_value` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Contains web user settings.';

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `modx_active_users`
--
ALTER TABLE `modx_active_users`
  ADD PRIMARY KEY (`sid`);

--
-- Индексы таблицы `modx_active_user_locks`
--
ALTER TABLE `modx_active_user_locks`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ix_element_id` (`elementType`,`elementId`,`sid`);

--
-- Индексы таблицы `modx_active_user_sessions`
--
ALTER TABLE `modx_active_user_sessions`
  ADD PRIMARY KEY (`sid`);

--
-- Индексы таблицы `modx_a_news`
--
ALTER TABLE `modx_a_news`
  ADD PRIMARY KEY (`NewsId`);

--
-- Индексы таблицы `modx_a_redirect`
--
ALTER TABLE `modx_a_redirect`
  ADD PRIMARY KEY (`redirect_id`);

--
-- Индексы таблицы `modx_categories`
--
ALTER TABLE `modx_categories`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `modx_documentgroup_names`
--
ALTER TABLE `modx_documentgroup_names`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Индексы таблицы `modx_document_groups`
--
ALTER TABLE `modx_document_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `document` (`document`),
  ADD KEY `document_group` (`document_group`);

--
-- Индексы таблицы `modx_event_log`
--
ALTER TABLE `modx_event_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user` (`user`);

--
-- Индексы таблицы `modx_keyword_xref`
--
ALTER TABLE `modx_keyword_xref`
  ADD KEY `content_id` (`content_id`),
  ADD KEY `keyword_id` (`keyword_id`);

--
-- Индексы таблицы `modx_manager_log`
--
ALTER TABLE `modx_manager_log`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `modx_manager_users`
--
ALTER TABLE `modx_manager_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Индексы таблицы `modx_membergroup_access`
--
ALTER TABLE `modx_membergroup_access`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `modx_membergroup_names`
--
ALTER TABLE `modx_membergroup_names`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Индексы таблицы `modx_member_groups`
--
ALTER TABLE `modx_member_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ix_group_member` (`user_group`,`member`);

--
-- Индексы таблицы `modx_site_content`
--
ALTER TABLE `modx_site_content`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `parent` (`parent`),
  ADD KEY `aliasidx` (`alias`),
  ADD KEY `typeidx` (`type`);
ALTER TABLE `modx_site_content` ADD FULLTEXT KEY `content_ft_idx` (`pagetitle`,`description`,`content`);

--
-- Индексы таблицы `modx_site_content_metatags`
--
ALTER TABLE `modx_site_content_metatags`
  ADD KEY `content_id` (`content_id`),
  ADD KEY `metatag_id` (`metatag_id`);

--
-- Индексы таблицы `modx_site_htmlsnippets`
--
ALTER TABLE `modx_site_htmlsnippets`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `modx_site_keywords`
--
ALTER TABLE `modx_site_keywords`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `keyword` (`keyword`);

--
-- Индексы таблицы `modx_site_metatags`
--
ALTER TABLE `modx_site_metatags`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `modx_site_modules`
--
ALTER TABLE `modx_site_modules`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `modx_site_module_access`
--
ALTER TABLE `modx_site_module_access`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `modx_site_module_depobj`
--
ALTER TABLE `modx_site_module_depobj`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `modx_site_plugins`
--
ALTER TABLE `modx_site_plugins`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `modx_site_plugin_events`
--
ALTER TABLE `modx_site_plugin_events`
  ADD PRIMARY KEY (`pluginid`,`evtid`);

--
-- Индексы таблицы `modx_site_snippets`
--
ALTER TABLE `modx_site_snippets`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `modx_site_templates`
--
ALTER TABLE `modx_site_templates`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `modx_site_tmplvars`
--
ALTER TABLE `modx_site_tmplvars`
  ADD PRIMARY KEY (`id`),
  ADD KEY `indx_rank` (`rank`);

--
-- Индексы таблицы `modx_site_tmplvar_access`
--
ALTER TABLE `modx_site_tmplvar_access`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `modx_site_tmplvar_contentvalues`
--
ALTER TABLE `modx_site_tmplvar_contentvalues`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ix_tvid_contentid` (`tmplvarid`,`contentid`),
  ADD KEY `idx_tmplvarid` (`tmplvarid`),
  ADD KEY `idx_id` (`contentid`);
ALTER TABLE `modx_site_tmplvar_contentvalues` ADD FULLTEXT KEY `value_ft_idx` (`value`);

--
-- Индексы таблицы `modx_site_tmplvar_templates`
--
ALTER TABLE `modx_site_tmplvar_templates`
  ADD PRIMARY KEY (`tmplvarid`,`templateid`);

--
-- Индексы таблицы `modx_system_eventnames`
--
ALTER TABLE `modx_system_eventnames`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `modx_system_settings`
--
ALTER TABLE `modx_system_settings`
  ADD PRIMARY KEY (`setting_name`);

--
-- Индексы таблицы `modx_user_attributes`
--
ALTER TABLE `modx_user_attributes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userid` (`internalKey`);

--
-- Индексы таблицы `modx_user_messages`
--
ALTER TABLE `modx_user_messages`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `modx_user_roles`
--
ALTER TABLE `modx_user_roles`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `modx_user_settings`
--
ALTER TABLE `modx_user_settings`
  ADD PRIMARY KEY (`user`,`setting_name`),
  ADD KEY `setting_name` (`setting_name`),
  ADD KEY `user` (`user`);

--
-- Индексы таблицы `modx_webgroup_access`
--
ALTER TABLE `modx_webgroup_access`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `modx_webgroup_names`
--
ALTER TABLE `modx_webgroup_names`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Индексы таблицы `modx_web_groups`
--
ALTER TABLE `modx_web_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ix_group_user` (`webgroup`,`webuser`);

--
-- Индексы таблицы `modx_web_users`
--
ALTER TABLE `modx_web_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Индексы таблицы `modx_web_user_attributes`
--
ALTER TABLE `modx_web_user_attributes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userid` (`internalKey`);

--
-- Индексы таблицы `modx_web_user_settings`
--
ALTER TABLE `modx_web_user_settings`
  ADD PRIMARY KEY (`webuser`,`setting_name`),
  ADD KEY `setting_name` (`setting_name`),
  ADD KEY `webuserid` (`webuser`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `modx_active_user_locks`
--
ALTER TABLE `modx_active_user_locks`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=427;

--
-- AUTO_INCREMENT для таблицы `modx_a_news`
--
ALTER TABLE `modx_a_news`
  MODIFY `NewsId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `modx_a_redirect`
--
ALTER TABLE `modx_a_redirect`
  MODIFY `redirect_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=411;

--
-- AUTO_INCREMENT для таблицы `modx_categories`
--
ALTER TABLE `modx_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT для таблицы `modx_documentgroup_names`
--
ALTER TABLE `modx_documentgroup_names`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `modx_document_groups`
--
ALTER TABLE `modx_document_groups`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `modx_event_log`
--
ALTER TABLE `modx_event_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT для таблицы `modx_manager_log`
--
ALTER TABLE `modx_manager_log`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1141;

--
-- AUTO_INCREMENT для таблицы `modx_manager_users`
--
ALTER TABLE `modx_manager_users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `modx_membergroup_access`
--
ALTER TABLE `modx_membergroup_access`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `modx_membergroup_names`
--
ALTER TABLE `modx_membergroup_names`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `modx_member_groups`
--
ALTER TABLE `modx_member_groups`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `modx_site_content`
--
ALTER TABLE `modx_site_content`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT для таблицы `modx_site_htmlsnippets`
--
ALTER TABLE `modx_site_htmlsnippets`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT для таблицы `modx_site_keywords`
--
ALTER TABLE `modx_site_keywords`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `modx_site_metatags`
--
ALTER TABLE `modx_site_metatags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `modx_site_modules`
--
ALTER TABLE `modx_site_modules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `modx_site_module_access`
--
ALTER TABLE `modx_site_module_access`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `modx_site_module_depobj`
--
ALTER TABLE `modx_site_module_depobj`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `modx_site_plugins`
--
ALTER TABLE `modx_site_plugins`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT для таблицы `modx_site_snippets`
--
ALTER TABLE `modx_site_snippets`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT для таблицы `modx_site_templates`
--
ALTER TABLE `modx_site_templates`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT для таблицы `modx_site_tmplvars`
--
ALTER TABLE `modx_site_tmplvars`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT для таблицы `modx_site_tmplvar_access`
--
ALTER TABLE `modx_site_tmplvar_access`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `modx_site_tmplvar_contentvalues`
--
ALTER TABLE `modx_site_tmplvar_contentvalues`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `modx_system_eventnames`
--
ALTER TABLE `modx_system_eventnames`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1036;

--
-- AUTO_INCREMENT для таблицы `modx_user_attributes`
--
ALTER TABLE `modx_user_attributes`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `modx_user_messages`
--
ALTER TABLE `modx_user_messages`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `modx_user_roles`
--
ALTER TABLE `modx_user_roles`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `modx_webgroup_access`
--
ALTER TABLE `modx_webgroup_access`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `modx_webgroup_names`
--
ALTER TABLE `modx_webgroup_names`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `modx_web_groups`
--
ALTER TABLE `modx_web_groups`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `modx_web_users`
--
ALTER TABLE `modx_web_users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `modx_web_user_attributes`
--
ALTER TABLE `modx_web_user_attributes`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
