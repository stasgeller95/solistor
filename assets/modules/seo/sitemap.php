<?php
echo '<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
<url>
<loc>'.$modx->config["site_url"].'</loc>
<lastmod>'.date("Y-m-d").'</lastmod>
<changefreq>monthly</changefreq>
<priority>0.8</priority>
</url>';
$pages = $modx->db->query("select id from `modx_site_content` where searchable = 1 and published = 1 and deleted = 0 and id != 1");
while ($p = $modx->db->getRow($pages)) {
  echo '<url><loc>'.$_SERVER['HTTP_HOST'] . $modx->makeUrl($p['id']).'</loc><changefreq>weekly</changefreq></url>'."\n";
}
echo '</urlset>';