<?php

define("ROOT", dirname(__FILE__));
error_reporting(E_ERROR);
$get            = isset($_GET['get']) ? $_GET['get'] : "robots";
$res            = Array();
$res['version'] = "v 1.0";
$res['url']     = $table['url'] = $url = "index.php?a=112&id=".$_GET['id']."";
$res['get']     = isset($_GET['get']) ? $_GET['get'] : "";

switch ($get) {
  case "sitemap":
    if (isset($_POST['code'])) {
      file_put_contents(ROOT."/sitemap.php", $_POST['code']);
    }
    if ($_GET['do'] == "reconstruct")  $modx->invokeEvent("OnDocFormSave");
    $code = file_get_contents(ROOT."/sitemap.php");

    $tpl = "/tpl/sitemap.tpl";
    break;
  case "robots":
    if (isset($_POST['code'])) {
      file_put_contents($_SERVER['DOCUMENT_ROOT']."/robots.txt", $_POST['code']);
      $res['alert'] = "Файл robots.txt обновлен";
    }
    $code = file_get_contents($_SERVER['DOCUMENT_ROOT']."/robots.txt");
    $tpl = "/tpl/robots.tpl";
    break;
  case "counters":
    if (count($_POST) > 0) {
      foreach ($_POST as $k => $v)
        $modx->db->query("update `modx_system_settings` set setting_value = '".$modx->db->escape($v)."' where setting_name = '$k'");
      $res['alert'] = "Настройки обновлены!";
      include_once MODX_BASE_PATH . "manager/processors/cache_sync.class.processor.php";
      $sync = new synccache();
      $sync->setCachepath(MODX_BASE_PATH . "assets/cache/");
      $sync->setReport(false);
      $sync->emptyCache(); // first empty the cache
      $modx->getSettings();
    }
    $tpl = "/tpl/counters.tpl";
    break;
  case "redirect":
    if (isset($_GET['delete']))
      $modx->db->query("delete from `modx_a_redirect` where redirect_id = '".$_GET['delete']."'");
    if (count($_POST['add']) > 0) {
      $modx->db->query("insert into `modx_a_redirect` set 
									redirect_code   = '".$modx->db->escape($_POST['add']['code'])."', 
									redirect_source = '".$modx->db->escape($_POST['add']['source'])."', 
									redirect_target = '".$modx->db->escape($_POST['add']['target'])."'");
    }
    if (count($_POST['edit']) > 0)
      $modx->db->query("update `modx_a_redirect` set 
									redirect_code     = '".$modx->db->escape($_POST['edit']['code'])."', 
									redirect_source   = '".$modx->db->escape($_POST['edit']['source'])."', 
									redirect_target   = '".$modx->db->escape($_POST['edit']['target'])."' 
									where redirect_id = '".$modx->db->escape($_POST['edit']['redirect'])."'");
    $redirects = $modx->db->query("select * from `modx_a_redirect` order by redirect_id desc limit 20");
    $redirects = $modx->db->makeArray($redirects);
    $tpl = "/tpl/redirect.tpl";
    break;
}

include ROOT . $tpl;
die;
