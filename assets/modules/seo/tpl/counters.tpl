<!doctype html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="/assets/css/bootstrap.css">
  <link rel="stylesheet" href="/assets/css/bootstrap-theme.css">
  <script src="/assets/js/jquery.min.js"></script>
  <script src="/assets/js/bootstrap.js"></script>
  <script src="/assets/js/ajaxupload.js"></script>
  <title>SEO v 1.0</title>
</head>
<body>
<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
  <div class="navbar-header pull-right">
    <a class="navbar-brand" href="index.php?a=112&amp;id=4">SEO</a>
  </div>
  <div class="collapse navbar-collapse">
    <ul class="nav navbar-nav">
      <li><a href="index.php?a=112&amp;id=4&amp;get=robots"><span class="icon icon-adjust"></span> Robots.txt</a></li>
      <li><a href="index.php?a=112&amp;id=4&amp;get=sitemap"><span class="icon icon-map-marker"></span> Карта сайта</a></li>
      <li class="active"><a href="index.php?a=112&amp;id=4&amp;get=counters"><span class="icon icon-globe"></span> Счетчики</a></li>
    </ul>
    <div class="clearfix"></div>
  </div>
</div>
<br/><br/><br/><br/>
<div class="container">
  <div class="row">
      <? if($res['alert']):?>
          <div class="alert alert-success" role="alert"><?= $res['alert']?></div>
      <? endif;?>
    <form method="post" id="counters_form">
      <table class="table table-bordered table-condensed table-striped">
        <tbody><tr>
          <td width="200px">
            <b>Google Analytics</b><br>
            <small>[(seo_ga)]</small>
          </td>
          <td><textarea name="seo_ga" class="form-control" cols="30" rows="5"><?= $modx->config['seo_ga'];?></textarea></td>
        </tr>
        <tr>
          <td>
            <b>Яндекс Метрика</b><br>
            <small>[(seo_ya)]</small>
          </td>
          <td><textarea name="seo_ya" class="form-control" cols="30" rows="5"><?= $modx->config['seo_ya'];?></textarea></td>
        </tr>
        </tbody></table>
      <input type="submit" value="Сохранить" class="btn btn-large btn-primary">
    </form>
  </div>
</div>
</body>
</html>