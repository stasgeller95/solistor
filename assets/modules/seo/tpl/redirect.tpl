<head>
  <title>SEO v 1.0</title>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="/assets/css/bootstrap.css">
  <link rel="stylesheet" href="/assets/css/bootstrap-theme.css">
  <script src="/assets/js/jquery.min.js"></script>
  <script src="/assets/js/bootstrap.js"></script>
  <script src="/assets/js/ajaxupload.js"></script>
  <style type="text/css">
    form {margin-bottom:0px;} input[type=text] {margin-bottom:0px;}
        .navbar-inner

    {padding:0px;}
  </style>
  <script type="text/javascript">
    $(document).ready(function () {
      $("[data-active]").each(function () {
        _option = $(this).find("option[value='" + $(this).attr("data-active") + "']");
        _option.prop("selected", true);
      });
      $(".update").on("click", function (e) {
        e.preventDefault();
        $(this).parents("tr").wrap("<form method='post' />");//find("input, select")
        $(this).parents("form").submit();
      })
    });
  </script>
</head>
<body>
<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
  <div class="navbar-header pull-right">
    <a class="navbar-brand" href="index.php?a=112&amp;id=4">SEO</a>
  </div>
  <div class="collapse navbar-collapse">
    <ul class="nav navbar-nav">
      <li class="active"><a href="index.php?a=112&amp;id=4&amp;get=redirect"><span class="icon icon-retweet"></span> Перенаправления</a></li>
      <li><a href="index.php?a=112&amp;id=4&amp;get=robots"><span class="icon icon-adjust"></span> Robots.txt</a></li>
      <li><a href="index.php?a=112&amp;id=4&amp;get=sitemap"><span class="icon icon-map-marker"></span> Карта сайта</a></li>
      <li><a href="index.php?a=112&amp;id=4&amp;get=counters"><span class="icon icon-globe"></span> Счетчики</a></li>
    </ul>
    <div class="clearfix"></div>
  </div>
</div>
<br/><br/><br/><br/>
<div class="container">
  <p>
    <a href="#myModal" data-toggle="modal" class="btn btn-primary"><span class="glyphicon glyphicon-plus-sign"></span> Добавить перенаправление</a>
  </p>

  <table class="table table-condensed table-striped table-hover table-bordered">
    <tr>
        <th>id</th>
        <th>code</th>
        <th>source</th>
        <th>target</th>
        <th></th>
    </tr>
    <? foreach($redirects as $redirect): ?>
        <tr>
            <td><?= $redirect['redirect_id'];?></td>
            <td><?= $redirect['redirect_code'];?></td>
            <td><?= $redirect['redirect_source'];?></td>
            <td><?= $redirect['redirect_target'];?></td>
            <td>
                <input type="hidden" name="edit[redirect]" value="<?= $redirect['redirect_id'];?>">
                <a href="index.php?a=112&amp;id=4&amp;get=redirect" title="Обновить" class="btn btn-small btn-success update"><span class="glyphicon glyphicon-repeat"></span></a>
                <a href="index.php?a=112&amp;id=4&amp;get=redirect&amp;delete=<?= $redirect['redirect_id'];?>" title="Удалить" class="btn btn-small btn-danger"><span class="glyphicon glyphicon-remove-sign"></span></a>
            </td>
        </tr>
    <? endforeach;?>
  </table>
</div>



<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Добавить перенаправление</h4>
      </div>
      <form action="index.php?a=112&amp;id=4&amp;get=redirect" method="post">
        <div class="modal-body">
          <table class="table table-condensed table-bordered table-hover table-striped">
            <tbody><tr>
              <td>Код перенаправления</td>
              <td>
                <select name="add[code]" class="form-control">
                  <option value="301">301 - permanent</option>
                  <option value="302">302 - temporary</option>
                  <option value="404">404 - not found</option>
                </select>
              </td>
            </tr>
            <tr>
              <td>Перенаправить с</td>
              <td><input type="text" name="add[source]" class="form-control" value=""></td>
            </tr>
            <tr>
              <td>на</td>
              <td><input type="text" name="add[target]" class="form-control" value=""></td>
            </tr>
            </tbody></table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><span class="glyphicon glyphicon-remove-sign"></span> Отмена</button>
          <button type="submit" class="btn btn-success pull-right"><span class="glyphicon glyphicon-ok-sign"></span> Сохранить</button>
        </div>
      </form>
    </div>
  </div>
</div>
</body>