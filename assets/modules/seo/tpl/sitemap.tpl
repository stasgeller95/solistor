<!doctype html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="/assets/css/bootstrap.css">
  <link rel="stylesheet" href="/assets/css/bootstrap-theme.css">
  <script src="/assets/js/jquery.min.js"></script>
  <script src="/assets/js/bootstrap.js"></script>
  <script src="/assets/js/ajaxupload.js"></script>
  <title>SEO v 1.0</title>
</head>
<body>
<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
  <div class="navbar-header pull-right">
    <a class="navbar-brand" href="index.php?a=112&amp;id=4">SEO</a>
  </div>
  <div class="collapse navbar-collapse">
    <ul class="nav navbar-nav">
      <li><a href="index.php?a=112&amp;id=4&amp;get=robots"><span class="icon icon-adjust"></span> Robots.txt</a></li>
      <li class="active"><a href="index.php?a=112&amp;id=4&amp;get=sitemap"><span class="icon icon-map-marker"></span> Карта сайта</a></li>
      <li><a href="index.php?a=112&amp;id=4&amp;get=counters"><span class="icon icon-globe"></span> Счетчики</a></li>
    </ul>
    <div class="clearfix"></div>
  </div>
</div>
<br/><br/><br/><br/>
<div class="container">
  <div class="row">
    <? if($res['alert']):?>
        <div class="alert alert-success" role="alert"><?= $res['alert']?></div>
    <? endif;?>
    <p>С помощью файла Sitemap веб-мастеры могут сообщать поисковым системам о веб-страницах, которые доступны для сканирования. Файл Sitemap представляет собой XML-файл, в котором перечислены URL-адреса веб-сайта в сочетании с метаданными, связанными с каждым URL-адресом (дата его последнего изменения; частота изменений; его приоритетность на уровне сайта), чтобы поисковые системы могли более грамотно сканировать этот сайт.</p>
    <form action="index.php?a=112&id=4&get=sitemap" id="updateSitemap">
      <textarea class="form-control" name="code" id="code"><?= $code;?></textarea>
      <input type="submit" value="Обновить алгоритм построения sitemap.xml" class="btn btn-large btn-primary">
      <a href="index.php?a=112&id=4&get=sitemap&do=reconstruct" class="btn btn-info pull-right">Обновить <b>sitemap.xml</b></a>
    </form>
  </div>
</div>
<script src="/assets/plugins/codemirror/cm/lib/codemirror-compressed.js"></script>
<script src="/assets/plugins/codemirror/cm/addon-compressed.js"></script>
<script src="/assets/plugins/codemirror/cm/emmet-compressed.js"></script>
<script src="/assets/plugins/codemirror/cm/search-compressed.js"></script>
<link rel="stylesheet" href="/assets/plugins/codemirror/cm/lib/codemirror.css">
<link rel="stylesheet" href="/assets/plugins/codemirror/cm/theme/default.css">
<script>

  CodeMirror.fromTextArea(document.getElementById("code"), {
    theme: 'default',
    indentUnit: 2,
    tabSize: 2,
    lineNumbers: true,
    matchBrackets: true,
    lineWrapping: true,
    gutters: ["CodeMirror-linenumbers", "breakpoints"],
    styleActiveLine: true,
    indentWithTabs: true,
    cursorActivity: true
  });


  $(document).ready(function () {

    $('#updateSitemap').on('submit', function () {
      let code = $(this).find('textarea').val();

      $.ajax({
        url: window.location.href,
        dataType: 'json',
        type: 'post',
        data: {code: code},
        success: function (json) {
          alert(json);
        },
        error: function (json) {
          console.log('Error ' + json);
        }
      });
      return false;
    })
  });
</script>
</body>
</html>