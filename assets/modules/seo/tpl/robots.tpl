<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="/assets/css/bootstrap.css">
  <link rel="stylesheet" href="/assets/css/bootstrap-theme.css">
  <script src="/assets/js/jquery.min.js"></script>
  <script src="/assets/js/bootstrap.js"></script>
  <script src="/assets/js/ajaxupload.js"></script>
  <script src="/assets/plugins/codemirror/cm/lib/codemirror-compressed.js"></script>
  <link rel="stylesheet" href="/assets/plugins/codemirror/cm/lib/codemirror.css">
  <title>SEO v 1.0</title>
</head>
<body>
<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
  <div class="navbar-header pull-right">
    <a class="navbar-brand" href="index.php?a=112&amp;id=4">SEO</a>
  </div>
  <div class="collapse navbar-collapse">
    <ul class="nav navbar-nav">
      <li class="active"><a href="index.php?a=112&amp;id=4&amp;get=robots"><span class="icon icon-adjust"></span> Robots.txt</a></li>
      <li><a href="index.php?a=112&amp;id=4&amp;get=sitemap"><span class="icon icon-map-marker"></span> Карта сайта</a></li>
      <li><a href="index.php?a=112&amp;id=4&amp;get=counters"><span class="icon icon-globe"></span> Счетчики</a></li>
    </ul>
    <div class="clearfix"></div>
  </div>
</div>
<br/><br/><br/><br/>
<div class="container">
  <div class="row">
    <? if($res['alert']):?>
        <div class="alert alert-success" role="alert"><?= $res['alert']?></div>
    <? endif;?>
    <p>Файл robots.txt – это текстовый файл, находящийся в корневой директории сайта, в котором записываются специальные инструкции для поисковых роботов. Эти инструкции могут запрещать к индексации некоторые разделы или страницы на сайте, указывать на правильное «зеркалирование» домена, рекомендовать поисковому роботу соблюдать определенный временной интервал между скачиванием документов с сервера и т.д.</p>
    <form method="post" id="updateRobots">
      <textarea class="form-control" name="code" id="code"><?= $code;?></textarea>
        <input type="submit" value="Обновить файл robots.txt" class="btn btn-large btn-primary">
    </form>
  </div>
</div>
<script>
  CodeMirror.fromTextArea(document.getElementById("code"), {
    theme: 'default',
    indentUnit: 2,
    tabSize: 2,
    lineNumbers: true,
    matchBrackets: true,
    lineWrapping: true,
    gutters: ["CodeMirror-linenumbers", "breakpoints"],
    styleActiveLine: true,
    indentWithTabs: true,
    cursorActivity: true
  });
</script>
</body>
</html>