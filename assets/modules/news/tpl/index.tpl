<div id="all" class="tab-pane fade in active">
    <p>
        <a href="<?=$url?>&get=add" class="btn btn-success"><span
                    class="glyphicon glyphicon-plus-sign"></span>&emsp;Добавить новость</a>
    </p>

    <table class="table table-condensed table-striped table-hover table-bordered">
        <thead>
        <tr>
            <th>id</th>
            <th>Заголовок новости</th>
            <th>Описание новости</th>
            <th>Дата создания</th>
            <th>Действие</th>
        </tr>
        </thead>
        <tbody>
        <? if($news):?>
        <? foreach($news as $item): ?>
        <tr>
            <td><?= $item['NewsId'];?></td>
            <td><?= $item['Title']?></td>
            <td><?= substr($item['Description'], 0, 100) .  '...';?></td>
            <td><?= $item['DateAdded'];?></td>
            <td>
                <a href="<?=$url;?>&get=edit&NewsId=<?=$item['NewsId'];?>" title=""
                   class="btn btn-xs btn-success update" data-original-title="Редактировать"><span
                            class="glyphicon glyphicon-edit"></span>&emsp;Редактировать</a>&emsp;
                <a href="<?=$url?>&get=delete&NewsId=<?=$item['NewsId'];?>" title=""
                   class="btn btn-xs btn-danger" data-original-title="Удалить"><span
                            class="glyphicon glyphicon-remove"></span>&emsp;Удалить</a>&emsp;
            </td>
        </tr>
        <? endforeach; ?>
        <? endif; ?>
        </tbody>

    </table>
</div>