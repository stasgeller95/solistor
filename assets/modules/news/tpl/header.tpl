<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <script src="/assets/js/jquery.min.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2></h2>
    <nav class="navbar navbar-inverse nav nav-tabs">
        <ul class="nav navbar-nav">
            <li class="active"><a href="<?=$url;?>"><span class="glyphicon glyphicon-list-alt"></span>&emsp;Все
                    новости</a>
            </li>
        </ul>
    </nav>