</div>

<script src="/assets/plugins/tinymce4/tinymce/tinymce.min.js"></script>
<script>
    var config_tinymce4_custom = {
        relative_urls:true,
        remove_script_host:true,
        convert_urls:true,
        resize:true,
        forced_root_block:'p',
        skin:'lightgray',
        width:'100%',
        height:'400px',
        menubar:false,
        statusbar:true,
        document_base_url:'http://solistor.local/',
        entity_encoding:'named',
        language:'ru',
        language_url:'/assets/plugins/tinymce4/tinymce/langs/ru.js',
        schema:'html5',
        element_format:'xhtml',
        templates:'/assets/plugins/tinymce4/connector.tinymce4.templates.php',
        image_caption:true,
        image_advtab:true,
        image_class_list:[{title: "None", value: ""},{title: "Float left", value: "justifyleft"},{title: "Float right", value: "justifyright"},{title: "Image Responsive",value: "img-responsive"}],
        browser_spellcheck:false,
        paste_word_valid_elements:'a[href|name],p,b,strong,i,em,h1,h2,h3,h4,h5,h6,table,th,td[colspan|rowspan],tr,thead,tfoot,tbody,br,hr,sub,sup,u',
        plugins:'advlist autolink lists link image charmap print preview hr anchor pagebreak searchreplace wordcount visualblocks visualchars code fullscreen spellchecker insertdatetime media nonbreaking save table contextmenu directionality emoticons template paste textcolor codesample colorpicker textpattern imagetools paste modxlink youtube template',
        toolbar1:'undo redo | cut copy paste | searchreplace | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent blockquote | styleselect',
        toolbar2:'link unlink anchor image media codesample table | hr removeformat | subscript superscript charmap | nonbreaking | visualchars visualblocks print preview fullscreen code',
        style_formats:[{"title":"Inline","items":[{"title":"Title","inline":"span","classes":"cssClass"},{"title":"Title2","inline":"span","classes":"cssClass"}]},{"title":"Block","items":[{"title":"Title","selector":"*","classes":"cssClass"},{"title":"Title2","selector":"*","classes":"cssClass"}]}],
        block_formats:'Paragraph=p;Header 1=h1;Header 2=h2;Header 3=h3',
        setup:function(ed) { ed.on("change", function(e) { documentDirty=true; }); },
        save_onsavecallback:function () { documentDirty=false; document.getElementById("stay").value = 2; document.mutate.save.click(); },
        file_browser_callback: function (field, url, type, win) {
            if (type == 'image') {
                type = 'images';
            }
            if (type == 'file') {
                type = 'files';
            }
            tinyMCE.activeEditor.windowManager.open({
                file: 'http://solistor.local/manager/media/browser/mcpuk/browse.php?opener=tinymce4&field=' + field + '&type=' + type,
                title: 'KCFinder',
                width: 700,
                height: 500,
                inline: true,
                close_previous: false
            }, {
                window: win,
                input: field
            });
            return false;
        }

    }
</script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        config_tinymce4_custom['selector'] = '#description';
        tinymce.init(config_tinymce4_custom);
    });
</script>
</body>
</html>