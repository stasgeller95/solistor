<div>
    <form action="<?= $News['NewsId'] ? $url . '&get=edit&NewsId='.$News['NewsId'] : $url . '&get=add';?>" method="post">
        <div class="form-group">
            <label for="title">Заголовок</label>
            <input type="text" name="Title" class="form-control" id="title" value="<?=$News['Title'];?>">
        </div>
        <div class="form-group">
            <label for="description">Текст новости</label>
            <textarea name="Description" id="description" class="form-control"><?=htmlspecialchars($News['Description']);?></textarea>
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
    </form>
</div>