<?php

define("ROOT", dirname(__FILE__));
error_reporting(E_ERROR);
$get            = isset($_GET['get']) ? $_GET['get'] : "redirect";
$res            = Array();
$res['version'] = "v 1.0";
$res['url']     = $table['url'] = $url = "index.php?a=112&id=".$_GET['id']."";
$res['get']     = isset($_GET['get']) ? $_GET['get'] : "";
$NewsId         = isset($_GET['NewsId']) ? $_GET['NewsId'] : '';
$Action         = isset($_POST['Action']) && $_POST['Action'] ? $_POST['Action'] : false;

switch ($res['get']) {
  case 'add':

    if($_POST){
      $description = trim(htmlspecialchars_decode($_POST['Description']));
      $modx->db->query("INSERT INTO `modx_a_news` SET 
                          Title = '".$modx->db->escape($_POST['Title'])."', 
                          Description = '".$modx->db->escape($description)."'");
      $tpl = ROOT.'/tpl/index.tpl';
    } else{
      $tpl = ROOT.'/tpl/edit.tpl';
    }

    break;
  case 'edit':

    if ($_POST) {
      $description = $_POST['Description'] ? trim(htmlspecialchars_decode($_POST['Description'])) : '';

      $modx->db->query("UPDATE `modx_a_news` SET 
                          Title = '".$modx->db->escape($_POST['Title'])."', 
                          Description = '".$modx->db->escape($description)."' 
                        WHERE NewsId = '".(int)$NewsId."'");
    }

    $query = $modx->db->query("SELECT * FROM `modx_a_news` WHERE NewsId = '".$modx->db->escape($NewsId)."'");
    $News  = $modx->db->getRow($query);

    $tpl = ROOT . '/tpl/edit.tpl';
    break;
  case 'delete':

    if ($_GET['NewsId']) {
      $modx->db->query("DELETE FROM `modx_a_news` WHERE NewsId = '".$modx->db->escape($NewsId)."'");
    }

    $tpl = ROOT . '/tpl/index.tpl';
    break;
  default:

    $tpl = ROOT.'/tpl/index.tpl';
    break;
}

if(strpos($tpl, 'index.tpl')){
  $query = $modx->db->query("SELECT * FROM `modx_a_news` ORDER BY DateAdded DESC ");
  $news  = [];
  while ($result = $modx->db->getRow($query)) {
    $news[] = $result;
  }
}

include_once(ROOT.'/tpl/header.tpl');
include_once($tpl);
include_once(ROOT.'/tpl/footer.tpl');
die();