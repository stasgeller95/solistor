$(document).ready(function () {

    $('.call-back').on('click', function () {
        $('.modal-window').removeClass('fadeOut').addClass('animated fadeIn').css('display', 'block');
        $('.modal-wrapper').addClass('animated ').css('display', 'block');
    });

    $('.close-modal').on('click', function () {
        $('.modal-window').removeClass('fadeIn').addClass('fadeOut').css('display', 'none');
        $('.modal-wrapper').removeClass('fadeIn').css('display', 'none');
    });

    $('#callback').on('submit', function () {

        var form = $(this);
        var _from = $(this).serialize();
        if (validate(form)){
            $.ajax({
                url: document.location,
                type: "POST",
                dataType: "json",
                data: _from,
                complete: function (json) {
                    console.log(json);
                }
            });
        }

        return false;
    });

    function validate(form){
        var error_class = "error";
        var norma_class = "pass";
        var item        = form.find("[required]");
        var e           = 0;
        var reg         = undefined;
        var pass        = form.find('.password').val();
        var pass_1      = form.find('.password_1').val();
        var email       = false;
        var password    = false;
        var phone       = false;
        function mark (object, expression) {
            if (expression) {
                object.parent('div').addClass(error_class).removeClass(norma_class).find('.error-text').css('display','block');
                if (email && (object.val().length > 0)) {
                    object.parent('div').find('.error-text').text('Некорректный формат Email');
                }
                if (password && (object.val().length > 0)) {
                    object.parent('div').find('.error-text').text('Пароль должен быть минимум 6 символов');
                }
                if (phone && (object.val().length > 0)) {
                    object.parent('div').find('.error-text').text('Некорректный формат телефона');
                }
                e++;
            } else
                object.parent('div').addClass(norma_class).removeClass(error_class).find('.error-text').css('display','none')
        }
        form.find("[required]").each(function(){
            switch($(this).attr("data-validate")) {
                case undefined:
                    mark ($(this), $.trim($(this).val()).length === 0);
                    break;
                case "email":
                    email = true;
                    reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
                    mark ($(this), !reg.test($.trim($(this).val())));
                    email = false;
                    break;
                case "phone":
                    phone = true;
                    reg = /[0-9 -()+]{10}$/;
                    mark ($(this), !reg.test($.trim($(this).val())));
                    phone = false;
                    break;
                case "numb":
                    reg = /[0-9]$/;
                    mark ($(this), !reg.test($.trim($(this).val())));
                    break;
                case "pass":
                    password = true;
                    reg = /^[a-zA-Z0-9_-]{6,}$/;
                    mark ($(this), !reg.test($.trim($(this).val())));
                    password = false;
                    break;
                case "pass1":
                    mark ($(this), (pass_1 !== pass || $.trim($(this).val()).length === 0));
                    break;
                default:
                    reg = new RegExp($(this).data("validate"), "g");
                    mark ($(this), !reg.test($.trim($(this).val())));
                    break;
            }
        });
        form.find('.js_valid-radio').each(function(){
            var inp = $(this).find('input.required');
            var rezalt;
            for (var i = 0; i < inp.length; i++) {
                if ($(inp[i]).is(':checked') === true) {
                    rezalt = 1;
                    break;
                } else {
                    rezalt = 0;
                }
            }
            if (rezalt === 0) {
                $(this).addClass(error_class).removeClass(norma_class);
                e = 1;
            } else {
                $(this).addClass(norma_class).removeClass(error_class);
            }
        });
        if (e == 0) {
            // $('#modal-thank').openModal();
            // setTimeout(function () {
            //     $('#modal-thank').closeModal();
            // }, 4000);
            // $('#modal-rega').find('form')[0].reset();
            // $('#modal-rega').closeModal();
            return true;
        }
        else {
            form.find("."+error_class+" input:first").focus();
            return false;
        }
    }
});